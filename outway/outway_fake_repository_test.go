// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package outway_test

import (
	"context"
	"errors"
	"net/http/httptest"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/common/accesstoken"
	"gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/outway/domain/config"
)

type mockService struct {
	GrantHash        string
	Name             string
	PeerID           string
	outwayDelegator  *tls.OrganizationInformation
	serviceDelegator *tls.OrganizationInformation
	Server           *httptest.Server
}

type fakeRepository struct {
	Services []*mockService
}

func newFakeEmptyRepository() *fakeRepository {
	return &fakeRepository{
		Services: []*mockService{
			{
				GrantHash: "$1$4$k4rwlWTsCM_j89Fc3nrbnQa9SkYXK8oAp2j1YA34D0WkqZt7V3Gxr-KB43GGv47YhxadenFdW2xJI8CpBJSsLQ",
				Name:      "service-1",
				PeerID:    orgACertBundle.GetPeerInfo().SerialNumber,
				Server: &httptest.Server{
					URL: "https://idonotexist",
				},
			},
		},
	}
}

func newFakeRepository(services []*mockService) *fakeRepository {
	return &fakeRepository{
		Services: services,
	}
}

func (r *fakeRepository) GetServices(_ context.Context) (config.Services, error) {
	services := make(config.Services, len(r.Services))

	for _, service := range r.Services {
		services[service.GrantHash] = &config.Service{
			Name:   service.Name,
			PeerID: service.PeerID,
		}
	}

	return services, nil
}

func (r *fakeRepository) getMockService(grantHash string) (*mockService, error) {
	for _, service := range r.Services {
		if service.GrantHash == grantHash {
			return service, nil
		}
	}

	return nil, errors.New("cannot find service")
}

func (r *fakeRepository) GetToken(_ context.Context, grantHash string) (*config.TokenInfo, error) {
	inwayService, err := r.getMockService(grantHash)
	if err != nil {
		return nil, err
	}

	signedToken, err := generateValidToken(inwayService.Server.URL, inwayService.Name, grantHash, inwayService.outwayDelegator, inwayService.serviceDelegator)
	if err != nil {
		return nil, errors.New("cannot generate token")
	}

	unsafeToken, err := accesstoken.DecodeUnsafeFromString(signedToken)
	if err != nil {
		return nil, errors.New("cannot decode token")
	}

	return &config.TokenInfo{
		Token:              signedToken,
		UnsafeDecodedToken: unsafeToken,
	}, nil
}

func (r *fakeRepository) Close() error {
	return nil
}

func generateValidToken(serviceInwayAddress, serviceName, grantHash string, outwayDelegator, serviceDelegator *tls.OrganizationInformation) (string, error) {
	tokenArgs := &accesstoken.NewTokenArgs{
		GroupID:                     "fsc-local",
		GrantHash:                   grantHash,
		OutwayPeerID:                orgBCertBundle.GetPeerInfo().SerialNumber,
		OutwayCertificateThumbprint: orgBCertBundle.CertificateThumbprint(),
		ServiceName:                 serviceName,
		ServiceInwayAddress:         serviceInwayAddress,
		ServicePeerID:               orgACertBundle.GetPeerInfo().SerialNumber,
		ExpiryDate:                  testClock.Now().Add(time.Minute),
		NotBefore:                   testClock.Now().Add(-time.Second),
		SignWith: &accesstoken.SignWith{
			CertificateThumbprint: orgACertBundle.CertificateThumbprint(),
			PrivateKey:            orgACertBundle.PrivateKey(),
		},
	}

	if outwayDelegator != nil {
		tokenArgs.OutwayDelegatorPeerID = outwayDelegator.SerialNumber
	}

	if serviceDelegator != nil {
		tokenArgs.ServiceDelegatorPeerID = serviceDelegator.SerialNumber
	}

	signedToken, err := accesstoken.New(tokenArgs)
	if err != nil {
		return "", err
	}

	return signedToken.Value(), nil
}
