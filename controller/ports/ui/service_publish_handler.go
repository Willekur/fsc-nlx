// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import (
	"context"
	"net/http"
	"time"

	"github.com/go-chi/chi/v5"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
)

func (s *Server) publishServiceGetHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	serviceName := chi.URLParam(r, "name")

	page := publishServicePage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathServicesPage,
		},
		SuccessMessage:   "",
		ErrorMessage:     "",
		DirectoryPeerIDs: map[string]string{},
		Form: &PublishServicePageForm{
			Name:            serviceName,
			DirectoryPeerID: "",
			ValidFrom:       time.Now().Format("2006-01-02"),
			ValidUntil:      time.Now().Add(Day).Format("2006-01-02"),
		},
	}

	peers, err := s.apiApp.Queries.ListPeers.Handle(ctx, &query.ListPeersArgs{
		AuthData: authData,
	})
	if err != nil {
		s.logger.Error("could not retrieve PeerID list from manager", err)

		page.renderWithError(w, err)

		return
	}

	peerIDList := make(map[string]string)
	for _, peer := range peers {
		peerIDList[peer.ID] = peer.Name
	}

	page.DirectoryPeerIDs = peerIDList

	page.render(w)
}

func (s *Server) publishServicePostHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	formValues, err := decodePublishServicePageForm(r)
	if err != nil {
		s.logger.Error("failed to read form values", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	serviceName := chi.URLParam(r, "name")

	page := publishServicePage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathServicesPage,
		},
		DirectoryPeerIDs: map[string]string{},
		Form:             formValues,
	}

	page.Form.Name = serviceName

	peers, err := s.apiApp.Queries.ListPeers.Handle(ctx, &query.ListPeersArgs{
		AuthData: authData,
	})
	if err != nil {
		s.logger.Error("could not retrieve PeerID list from manager", err)

		page.renderWithError(w, err)

		return
	}

	peerIDList := make(map[string]string)
	for _, peer := range peers {
		peerIDList[peer.ID] = peer.Name
	}

	page.DirectoryPeerIDs = peerIDList

	peerInfo := s.getPeerInfo(ctx, getAuthzData(r), authData)

	err = s.apiApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractArgs{
		AuthData:          authData,
		HashAlgorithm:     "SHA3-512",
		GroupID:           s.basePage.GroupID,
		ContractNotBefore: formValues.ValidFrom,
		ContractNotAfter:  formValues.ValidUntil,
		ServicePublicationGrants: []*command.ServicePublicationGrant{
			{
				DirectoryPeerID: formValues.DirectoryPeerID,
				ServicePeerID:   peerInfo.ID,
				ServiceName:     formValues.Name,
			},
		},
	})
	if err != nil {
		s.logger.Error("create contract for publishing a service", err)

		page.renderWithError(w, err)

		return
	}

	page.renderWithSuccess(s.i18n.Translate("The Service has been published."), w)
}
