// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package services

import (
	"context"
	"fmt"

	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func (p *PeersCommunicationService) GetCertificate(ctx context.Context, peerID contract.PeerID, certificateThumbprint *contract.CertificateThumbprint) (*contract.PeerCertificate, error) {
	peer, err := p.getManagerClient(peerID)
	if err != nil {
		return nil, fmt.Errorf("could not get client for peer %q to get certificate: %w", peerID.Value(), err)
	}

	certificates, err := peer.GetCertificates(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "could not get certificates from Peer")
	}

	cert, err := certificates.GetCertificate(*certificateThumbprint)
	if err != nil {
		return nil, fmt.Errorf("could not get certificate from peer certificates: %w", err)
	}

	return cert, nil
}
