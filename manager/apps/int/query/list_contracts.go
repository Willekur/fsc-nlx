// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"
	"sort"
	"time"

	"github.com/pkg/errors"
	"golang.org/x/exp/slices"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type ListContractsHandler struct {
	repo       contract.Repository
	selfPeerID contract.PeerID
}

type ListContractsContracts []*ListContractsContract

type ListContractsContract struct {
	IV                                []byte
	Hash                              string
	HashAlgorithm                     HashAlg
	GroupID                           string
	NotBefore                         time.Time
	NotAfter                          time.Time
	CreatedAt                         time.Time
	Peers                             []*Peer
	AcceptSignatures                  map[string]Signature
	RejectSignatures                  map[string]Signature
	RevokeSignatures                  map[string]Signature
	HasRejected                       bool
	HasAccepted                       bool
	HasRevoked                        bool
	ServicePublicationGrants          []*ServicePublicationGrant
	ServiceConnectionGrants           []*ServiceConnectionGrant
	DelegatedServicePublicationGrants []*DelegatedServicePublicationGrant
	DelegatedServiceConnectionGrants  []*DelegatedServiceConnectionGrant
}

type ServicePublicationGrant struct {
	Hash            string
	DirectoryPeerID string
	ServicePeerID   string
	ServiceName     string
	ServiceProtocol contract.ServiceProtocol
}

type ServiceConnectionGrant struct {
	Hash                        string
	ServicePeerID               string
	ServiceName                 string
	OutwayPeerID                string
	OutwayCertificateThumbprint string
}

type DelegatedServiceConnectionGrant struct {
	Hash                        string
	DelegatorPeerID             string
	OutwayPeerID                string
	OutwayCertificateThumbprint string
	ServicePeerID               string
	ServiceName                 string
}

type DelegatedServicePublicationGrant struct {
	Hash            string
	DirectoryPeerID string
	DelegatorPeerID string
	ServicePeerID   string
	ServiceName     string
	ServiceProtocol contract.ServiceProtocol
}

type HashAlg int32

const (
	HashAlgUnspecified HashAlg = iota
	HashAlgSHA3_512
)

type Peer struct {
	ID   string
	Name string
}

type Signature struct {
	Peer     *Peer
	SignedAt time.Time
}

type GrantType string

const (
	GrantTypeUnspecified                 GrantType = ""
	GrantTypeServicePublication          GrantType = "service_publication"
	GrantTypeServiceConnection           GrantType = "service_connection"
	GrantTypeDelegatedServicePublication GrantType = "delegated_service_publication"
	GrantTypeDelegatedServiceConnection  GrantType = "delegated_service_connection"
)

type ListContractsHandlerArgs struct {
	GrantTypes          []GrantType
	ContentHashes       []string
	PaginationStartDate time.Time
	PaginationStartHash string
	PaginationLimit     uint32
	PaginationSortOrder SortOrder
}

func NewListContractsHandler(repository contract.Repository, selfPeerID contract.PeerID) (*ListContractsHandler, error) {
	if repository == nil {
		return nil, errors.New("repository is required")
	}

	return &ListContractsHandler{
		repo:       repository,
		selfPeerID: selfPeerID,
	}, nil
}

const defaultPaginationLimit = uint32(100)

// nolint:funlen,gocognit,gocyclo // refactor when ordering functionality is done
func (h *ListContractsHandler) Handle(ctx context.Context, args *ListContractsHandlerArgs) (ListContractsContracts, error) {
	var (
		resp []*contract.Contract
		err  error
	)

	limit := defaultPaginationLimit
	if args.PaginationLimit > 0 {
		limit = args.PaginationLimit
	}

	sortOrder := contract.SortOrderDescending

	if args.PaginationSortOrder == SortOrderAscending {
		sortOrder = contract.SortOrderAscending
	}

	if len(args.ContentHashes) > 0 {
		resp, err = h.repo.ListContractsByContentHash(ctx, args.ContentHashes)
		if err != nil {
			return nil, fmt.Errorf("%s: %w", newInternalError("could not get contracts by content hash from repository"), err)
		}
	} else {
		listContractsArgs := &contract.ListContractsWithGrantTypeFilterArgs{
			PaginationStartHash:                   args.PaginationStartHash,
			PaginationStartDate:                   args.PaginationStartDate,
			PaginationLimit:                       limit,
			PaginationSortOrder:                   sortOrder,
			WithServiceConnectionGrants:           true,
			WithDelegatedServiceConnectionGrants:  true,
			WithServicePublicationGrants:          true,
			WithDelegatedServicePublicationGrants: true,
		}

		if len(args.GrantTypes) > 0 {
			listContractsArgs.WithServiceConnectionGrants = slices.Contains(args.GrantTypes, GrantTypeServiceConnection)
			listContractsArgs.WithDelegatedServiceConnectionGrants = slices.Contains(args.GrantTypes, GrantTypeDelegatedServiceConnection)
			listContractsArgs.WithServicePublicationGrants = slices.Contains(args.GrantTypes, GrantTypeServicePublication)
			listContractsArgs.WithDelegatedServicePublicationGrants = slices.Contains(args.GrantTypes, GrantTypeDelegatedServicePublication)
		}

		contracts, errByGrant := h.repo.ListContractsWithGrantTypeFilter(ctx, listContractsArgs)
		if errByGrant != nil {
			return nil, fmt.Errorf("%s: %w", newInternalError("could not get contracts by grant type from repository"), err)
		}

		resp = contracts
	}

	contracts := make(ListContractsContracts, len(resp))

	for i, c := range resp {
		rejectSignatures := make(map[string]Signature)

		for _, s := range c.SignaturesRejected() {
			sig := convertSignature(s)

			rejectSignatures[sig.Peer.ID] = sig
		}

		acceptSignatures := make(map[string]Signature)

		for _, s := range c.SignaturesAccepted() {
			sig := convertSignature(s)

			acceptSignatures[sig.Peer.ID] = sig
		}

		revokeSignatures := make(map[string]Signature)

		for _, s := range c.SignaturesRevoked() {
			sig := convertSignature(s)

			revokeSignatures[sig.Peer.ID] = sig
		}

		peers, errPeers := h.repo.ListPeersByID(ctx, c.Content().PeersIDs())
		if errPeers != nil {
			return nil, fmt.Errorf("%s: %w", newInternalError("could not list peers by id"), err)
		}

		contractPeers := make([]*Peer, 0)
		for _, peer := range peers {
			contractPeers = append(contractPeers, &Peer{
				ID:   peer.ID().Value(),
				Name: peer.Name().Value(),
			})
		}

		sort.Slice(contractPeers, func(i, j int) bool {
			return contractPeers[i].ID < contractPeers[j].ID
		})

		servicePublicationGrants := make([]*ServicePublicationGrant, len(c.Content().Grants().ServicePublicationGrants()))

		for j, grant := range c.Content().Grants().ServicePublicationGrants() {
			servicePublicationGrants[j] = &ServicePublicationGrant{
				Hash:            grant.Hash().String(),
				DirectoryPeerID: grant.Directory().Peer().ID().Value(),
				ServicePeerID:   grant.Service().Peer().ID().Value(),
				ServiceName:     grant.Service().Name(),
				ServiceProtocol: grant.Service().Protocol(),
			}
		}

		serviceConnectionGrants := make([]*ServiceConnectionGrant, len(c.Content().Grants().ServiceConnectionGrants()))

		for j, grant := range c.Content().Grants().ServiceConnectionGrants() {
			serviceConnectionGrants[j] = &ServiceConnectionGrant{
				Hash:                        grant.Hash().String(),
				ServicePeerID:               grant.Service().Peer().ID().Value(),
				ServiceName:                 grant.Service().Name(),
				OutwayPeerID:                grant.Outway().Peer().ID().Value(),
				OutwayCertificateThumbprint: grant.Outway().CertificateThumbprint().Value(),
			}
		}

		delegatedServicePublicationGrants := make([]*DelegatedServicePublicationGrant, len(c.Content().Grants().DelegatedServicePublicationGrants()))

		for j, grant := range c.Content().Grants().DelegatedServicePublicationGrants() {
			delegatedServicePublicationGrants[j] = &DelegatedServicePublicationGrant{
				Hash:            grant.Hash().String(),
				DirectoryPeerID: grant.Directory().Peer().ID().Value(),
				DelegatorPeerID: grant.Delegator().Peer().ID().Value(),
				ServicePeerID:   grant.Service().Peer().ID().Value(),
				ServiceName:     grant.Service().Name(),
				ServiceProtocol: grant.Service().Protocol(),
			}
		}

		delegatedServiceConnectionGrants := make([]*DelegatedServiceConnectionGrant, len(c.Content().Grants().DelegatedServiceConnectionGrants()))

		for j, grant := range c.Content().Grants().DelegatedServiceConnectionGrants() {
			delegatedServiceConnectionGrants[j] = &DelegatedServiceConnectionGrant{
				Hash:                        grant.Hash().String(),
				DelegatorPeerID:             grant.Delegator().Peer().ID().Value(),
				OutwayPeerID:                grant.Outway().Peer().ID().Value(),
				OutwayCertificateThumbprint: grant.Outway().CertificateThumbprint().Value(),
				ServicePeerID:               grant.Service().Peer().ID().Value(),
				ServiceName:                 grant.Service().Name(),
			}
		}

		returnedContract := &ListContractsContract{
			IV:                                c.Content().IV().Bytes(),
			Hash:                              c.Content().Hash().String(),
			HashAlgorithm:                     HashAlg(c.Content().Hash().Algorithm()),
			GroupID:                           c.Content().GroupID(),
			NotBefore:                         c.Content().NotBefore(),
			NotAfter:                          c.Content().NotAfter(),
			CreatedAt:                         c.Content().CreatedAt(),
			Peers:                             contractPeers,
			AcceptSignatures:                  acceptSignatures,
			RejectSignatures:                  rejectSignatures,
			RevokeSignatures:                  revokeSignatures,
			HasRejected:                       c.IsRejectedBy(h.selfPeerID),
			HasAccepted:                       c.IsAcceptedBy(h.selfPeerID),
			HasRevoked:                        c.IsRevokedBy(h.selfPeerID),
			ServicePublicationGrants:          servicePublicationGrants,
			ServiceConnectionGrants:           serviceConnectionGrants,
			DelegatedServicePublicationGrants: delegatedServicePublicationGrants,
			DelegatedServiceConnectionGrants:  delegatedServiceConnectionGrants,
		}

		contracts[i] = returnedContract
	}

	return contracts, nil
}

func convertSignature(signature *contract.Signature) Signature {
	return Signature{
		Peer: &Peer{
			ID:   signature.Peer().ID().Value(),
			Name: signature.Peer().Name().Value(),
		},
		SignedAt: signature.SignedAt(),
	}
}
