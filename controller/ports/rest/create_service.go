// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"

	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/rest/api"
)

func (s *Server) CreateService(ctx context.Context, req api.CreateServiceRequestObject) (api.CreateServiceResponseObject, error) {
	err := s.app.Commands.CreateService.Handle(ctx, &command.CreateServiceArgs{
		AuthData:     authData,
		Name:         req.Body.Name,
		EndpointURL:  req.Body.EndpointUrl,
		InwayAddress: req.Body.InwayAddress,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not create service")
	}

	return api.CreateService201Response{}, nil
}
