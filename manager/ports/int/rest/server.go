// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	common_tls "gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	internalapp "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
)

type Server struct {
	app     *internalapp.Application
	logger  *logger.Logger
	cert    *common_tls.CertificateBundle
	clock   clock.Clock
	handler http.Handler
}

type NewArgs struct {
	Logger *logger.Logger
	App    *internalapp.Application
	Cert   *common_tls.CertificateBundle
	Clock  clock.Clock
}

func New(args *NewArgs) (*Server, error) {
	if args.Logger == nil {
		return nil, errors.New("logger is required")
	}

	if args.App == nil {
		return nil, errors.New("app is required")
	}

	if args.Cert == nil {
		return nil, errors.New("cert is required")
	}

	if args.Clock == nil {
		return nil, errors.New("clock is required")
	}

	s := &Server{
		app:    args.App,
		logger: args.Logger,
		cert:   args.Cert,
		clock:  args.Clock,
	}

	strict := api.NewStrictHandlerWithOptions(s, []api.StrictMiddlewareFunc{
		func(f api.StrictHandlerFunc, operationID string) api.StrictHandlerFunc {
			return func(ctx context.Context, w http.ResponseWriter, r *http.Request, args interface{}) (interface{}, error) {
				if len(r.TLS.PeerCertificates) == 0 {
					return nil, fmt.Errorf("client certificate missing")
				}

				return f(ctx, w, r, args)
			}
		},
	}, api.StrictHTTPServerOptions{
		RequestErrorHandlerFunc:  RequestErrorHandler,
		ResponseErrorHandlerFunc: ResponseErrorHandler,
	})

	r := chi.NewRouter()
	api.HandlerFromMux(strict, r)

	s.handler = r

	return s, nil
}

func (s *Server) Handler() http.Handler {
	return s.handler
}
