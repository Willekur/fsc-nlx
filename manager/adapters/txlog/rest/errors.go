// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package resttxlog

import (
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/txlog"
)

type response interface {
	StatusCode() int
}

func mapError(err error, message string) error {
	return fmt.Errorf("%s: %w: %w", message, txlog.ErrInternalError, err)
}

func mapResponse(res response, wantStatusCode int, message string, body []byte) error {
	if res == nil {
		return fmt.Errorf("%s: %w", message, txlog.ErrInternalError)
	}

	if res.StatusCode() == wantStatusCode {
		return nil
	}

	return fmt.Errorf("%s: %s: %w", message, string(body), txlog.ErrInternalError)
}
