// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package command

import "crypto"

type SignArgs struct {
	PrivateKey       crypto.PublicKey
	CertificateChain []byte
}
