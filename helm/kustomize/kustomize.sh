#! /bin/sh

#
# Copyright © VNG Realisatie 2024
# Licensed under the EUPL
#

cat <&0 > ${CHART_DIRECTORY}/kustomize/all.yaml
kubectl kustomize ${CHART_DIRECTORY}/kustomize && rm ${CHART_DIRECTORY}/kustomize/all.yaml
