-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL


BEGIN transaction;

CREATE VIEW contracts.with_grant_type AS
    SELECT
        DISTINCT ON (c.id)
        c.id AS id,
        c.hash AS hash,
        c.hash_algorithm AS hash_algorithm,
        c.group_id AS group_id,
        c.valid_not_before AS valid_not_before,
        c.valid_not_after AS valid_not_after,
        c.created_at AS created_at,
        CASE
            WHEN gsc.consumer_peer_id  IS NOT NULL THEN true ELSE false END
            AS has_service_connection_grant,
        CASE
            WHEN gdsc.outway_peer_id  IS NOT NULL THEN true ELSE false END
            AS has_delegated_service_connection_grant,
        CASE
            WHEN gsp.service_name IS NOT NULL THEN true ELSE false END
            AS has_service_publication_grant,
        CASE
            WHEN gdsp.service_name IS NOT NULL THEN true ELSE false END
            AS has_delegated_service_publication_grant
    FROM contracts.content AS c
         LEFT JOIN contracts.grants_service_connection as gsc
           ON gsc.content_hash = c.hash
         LEFT JOIN contracts.grants_delegated_service_connection as gdsc
           ON gdsc.content_hash = c.hash
         LEFT JOIN contracts.grants_service_publication as gsp
           ON gsp.content_hash = c.hash
         LEFT JOIN contracts.grants_delegated_service_publication as gdsp
           ON gdsp.content_hash = c.hash
;

COMMIT;
