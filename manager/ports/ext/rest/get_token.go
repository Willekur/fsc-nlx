// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"
	"errors"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/ext/query"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api/models"
)

func (s *Server) GetToken(ctx context.Context, req api.GetTokenRequestObject) (api.GetTokenResponseObject, error) {
	peer, err := getConnectingPeerFromCtx(ctx)
	if err != nil {
		return nil, err
	}

	resp, err := s.app.Queries.GetToken.Handle(ctx, &query.GetTokenHandlerArgs{
		ConnectingPeerID: peer.id,
		Scope:            req.Body.Scope,
		OAuthGrantType:   grantTypeToQuery(req.Body.GrantType),
	})
	if err != nil {
		switch {
		case errors.Is(err, query.ErrNoValidContractForGrantHash):
			errorMessage := "no valid contract found for hash"

			return api.GetToken400JSONResponse{
				Error:            models.InvalidScope,
				ErrorDescription: &errorMessage,
			}, nil
		case errors.Is(err, query.ErrInvalidGrantInScope):
			errorMessage := "invalid grant in scope"

			return api.GetToken400JSONResponse{
				Error:            models.InvalidScope,
				ErrorDescription: &errorMessage,
			}, nil
		case errors.Is(err, query.ErrUnsupportedGrantType):
			errorMessage := "provided grant type is not supported. only client_credentials is supported"

			return api.GetToken400JSONResponse{
				Error:            models.UnsupportedGrantType,
				ErrorDescription: &errorMessage,
			}, nil
		}

		return nil, errors.Join(err, errors.New("could not get token from query"))
	}

	return api.GetToken200JSONResponse{
		AccessToken: resp.Token,
		TokenType:   tokenTypeToModel(resp.TokenType),
	}, nil
}

func grantTypeToQuery(t models.FSCCoreOAuthGrantType) query.OAuthGrantType {
	switch t {
	case models.ClientCredentials:
		return query.OAuthGrantTypeClientCredentials
	default:
		return query.OAuthGrantTypeInvalid
	}
}

func tokenTypeToModel(t query.OAuthTokenType) models.FSCCoreOAuthTokenType {
	switch t {
	case query.OAuthTokenTypeBearer:
		return models.Bearer
	default:
		return ""
	}
}
