// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package rest_test

import (
	"context"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
)

type fakeInternalManager struct {
}

func (m fakeInternalManager) ListContracts(_ context.Context, _ []string, _ []manager.GrantType) (manager.Contracts, error) {
	return nil, nil
}

func (m fakeInternalManager) CreateContract(_ context.Context, _ *manager.CreateContractArgs) error {
	return nil
}

func (m fakeInternalManager) AcceptContract(_ context.Context, _ string) error {
	return nil
}

func (m fakeInternalManager) RevokeContract(_ context.Context, _ string) error {
	return nil
}

func (m fakeInternalManager) RejectContract(_ context.Context, _ string) error {
	return nil
}

func (m fakeInternalManager) ListServices(_ context.Context) (manager.Services, error) {
	return nil, nil
}

func (m fakeInternalManager) ListTXLogRecords(_ context.Context, _ *manager.ListTXLogRecordsRequest) ([]*manager.TXLogRecord, error) {
	return nil, nil
}

func (m fakeInternalManager) GetPeerInfo(_ context.Context) (*manager.Peer, error) {
	return nil, nil
}

func (m fakeInternalManager) ListPeers(_ context.Context) (manager.Peers, error) {
	return nil, nil
}

func (m fakeInternalManager) ListContractDistribution(_ context.Context, _ string) (manager.ContractDistributions, error) {
	return nil, nil
}

func (m fakeInternalManager) RetryContract(_ context.Context, _, _ string, _ manager.Action) error {
	return nil
}
