// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package inway

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"
	"regexp"
	"sync"
	"time"

	"github.com/cenkalti/backoff/v4"
	lru "github.com/hashicorp/golang-lru/v2"
	"github.com/pkg/errors"
	"go.uber.org/zap"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	common_tls "gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/common/transactionlog"
	"gitlab.com/commonground/nlx/fsc-nlx/inway/adapters/controller"
	"gitlab.com/commonground/nlx/fsc-nlx/inway/domain/config"
	"gitlab.com/commonground/nlx/fsc-nlx/inway/plugins"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

var nameRegex = regexp.MustCompile(`^[a-zA-Z0-9-]{1,100}$`)

const (
	timeOut               = 30 * time.Second
	keepAlive             = 30 * time.Second
	maxIdleCons           = 100
	IdleConnTimeout       = 20 * time.Second
	TLSHandshakeTimeout   = 10 * time.Second
	ExpectContinueTimeout = 1 * time.Second
)

type Peer struct {
	ID   string
	Name string
}

type Inway struct {
	clock               clock.Clock
	name                string
	groupID             contract.GroupID
	peer                Peer
	address             string
	orgCertBundle       *common_tls.CertificateBundle
	logger              *zap.Logger
	handler             http.Handler
	plugins             []plugins.Plugin
	controller          controller.Controller
	servicesLock        sync.RWMutex
	config              *config.Config
	serviceProxiesCache *lru.Cache[string, *httputil.ReverseProxy]
	crlCache            *common_tls.CRLsCache
}

func (i *Inway) Handler() http.Handler {
	return i.handler
}

func (i *Inway) RegisterToController(ctx context.Context) error {
	register := func() error {
		err := i.controller.RegisterInway(ctx, &controller.RegisterInwayArgs{
			GroupID: i.groupID.String(),
			Name:    i.name,
			Address: i.address,
		})
		if err != nil {
			i.logger.Error("failed to register to controller api", zap.Error(err))

			return err
		}

		i.logger.Info("controller api registration successful")

		return nil
	}

	err := backoff.Retry(register, backoff.WithContext(backoff.NewExponentialBackOff(), ctx))
	if err != nil {
		i.logger.Error("permanently failed to register to controller api", zap.Error(err))

		return err
	}

	return nil
}

type Params struct {
	Context                      context.Context
	Logger                       *zap.Logger
	Txlogger                     transactionlog.TransactionLogger
	Name                         string
	GroupID                      string
	Address                      *url.URL
	OrgCertBundle                *common_tls.CertificateBundle
	Controller                   controller.Controller
	AuthServiceURL               string
	AuthCAPath                   string
	ConfigRepository             config.Repository
	ServiceEndpointCacheDuration time.Duration
	Clock                        clock.Clock
	ServiceProxyCacheSize        int
	CRL                          *common_tls.CRLsCache
}

func NewInway(params *Params) (*Inway, error) {
	logger := params.Logger

	if logger == nil {
		logger = zap.NewNop()
	}

	groupID, err := contract.NewGroupID(params.GroupID)
	if err != nil {
		return nil, fmt.Errorf("invalid groupID in params: %w", err)
	}

	if !nameRegex.MatchString(params.Name) {
		return nil, errors.New("a valid name is required (alphanumeric & dashes, max. 100 characters)")
	}

	orgCert := params.OrgCertBundle.Certificate()

	if len(orgCert.Subject.Organization) != 1 {
		return nil, errors.New("cannot obtain organization name from self cert")
	}

	err = addressIsInOrgCert(params.Address, orgCert)
	if err != nil {
		return nil, err
	}

	if params.Context == nil {
		return nil, errors.New("context is nil. needed to close gracefully")
	}

	cfg, err := config.New(&config.NewConfigArgs{
		Repo:          params.ConfigRepository,
		CacheDuration: params.ServiceEndpointCacheDuration,
		Clock:         clock.New(),
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not create config")
	}

	peerName := orgCert.Subject.Organization[0]
	peerID := orgCert.Subject.SerialNumber

	logger.Info("loaded certificates for inway", zap.String("inway-peer-id", peerID), zap.String("inway-peer-name", peerName))

	cacheSize := params.ServiceProxyCacheSize

	if cacheSize == 0 {
		cacheSize = 1024
	}

	lruCache, err := lru.New[string, *httputil.ReverseProxy](cacheSize)
	if err != nil {
		return nil, errors.Wrap(err, "could not create service proxies cache")
	}

	i := &Inway{
		logger:  logger.With(zap.String("inway-peer-id", peerName)),
		groupID: groupID,
		peer: Peer{
			ID:   peerID,
			Name: peerName,
		},
		address:       params.Address.String(),
		orgCertBundle: params.OrgCertBundle,
		controller:    params.Controller,
		servicesLock:  sync.RWMutex{},
		plugins: []plugins.Plugin{
			plugins.NewAuthenticationPlugin(),
		},
		config:              cfg,
		clock:               params.Clock,
		serviceProxiesCache: lruCache,
		crlCache:            params.CRL,
	}

	authorizationPlugin, err := configureAuthorizationPlugin(params.OrgCertBundle, params.AuthCAPath, params.AuthServiceURL)
	if err != nil {
		return nil, err
	}

	i.plugins = append(i.plugins,
		plugins.NewLogRecordPlugin(peerID, params.Txlogger),
	)

	if authorizationPlugin != nil {
		i.plugins = append(i.plugins, authorizationPlugin)
	}

	crlPlugin, err := plugins.NewCrlPlugin(i.crlCache)
	if err != nil {
		return nil, err
	}

	if crlPlugin != nil {
		i.plugins = append(i.plugins, crlPlugin)
	}

	if params.Name != "" {
		i.name = params.Name
	} else {
		i.name = common_tls.X509CertificateThumbprint(orgCert)
	}

	serveMux := http.NewServeMux()
	serveMux.Handle("/.nlx/", http.NotFoundHandler())
	serveMux.HandleFunc("/", i.handleProxyRequest)

	i.handler = serveMux

	return i, nil
}

func addressIsInOrgCert(address *url.URL, orgCert *x509.Certificate) error {
	hostname := address.Hostname()

	if hostname == orgCert.Subject.CommonName {
		return nil
	}

	for _, dnsName := range orgCert.DNSNames {
		if hostname == dnsName {
			return nil
		}
	}

	return errors.Errorf("'%s' is not in the list of DNS names of the certificate, %v", hostname, orgCert.DNSNames)
}

func configureAuthorizationPlugin(externalCert *common_tls.CertificateBundle, authCAPath, authServiceURL string) (*plugins.AuthorizationPlugin, error) {
	if authServiceURL == "" {
		return plugins.NewAuthorizationPlugin(&plugins.NewAuthorizationPluginArgs{
			CA:                  nil,
			ServiceURL:          "",
			AuthorizationClient: nil,
			AuthServerEnabled:   false,
			ExternalCert:        externalCert,
		})
	}

	if authCAPath == "" {
		return nil, fmt.Errorf("authorization service URL set but no CA for authorization provided")
	}

	authURL, err := url.Parse(authServiceURL)
	if err != nil {
		return nil, err
	}

	if authURL.Scheme != "https" {
		return nil, errors.New("scheme of authorization service URL is not 'https'")
	}

	ca, err := common_tls.NewCertPoolFromFile(authCAPath)
	if err != nil {
		return nil, err
	}

	tlsConfig := common_tls.NewConfig(common_tls.WithTLS12())
	tlsConfig.RootCAs = ca

	return plugins.NewAuthorizationPlugin(&plugins.NewAuthorizationPluginArgs{
		CA:         ca,
		ServiceURL: authURL.String(),
		AuthorizationClient: &http.Client{
			Transport: createHTTPTransport(tlsConfig),
		},
		AuthServerEnabled: true,
		ExternalCert:      externalCert,
	})
}

func createHTTPTransport(tlsConfig *tls.Config) *http.Transport {
	return &http.Transport{
		DialContext: func(ctx context.Context, network, addr string) (net.Conn, error) {
			a := &net.Dialer{
				Timeout:   timeOut,
				KeepAlive: keepAlive,
			}

			return a.DialContext(ctx, network, addr)
		},
		MaxIdleConns:          maxIdleCons,
		IdleConnTimeout:       IdleConnTimeout,
		TLSHandshakeTimeout:   TLSHandshakeTimeout,
		ExpectContinueTimeout: ExpectContinueTimeout,
		TLSClientConfig:       tlsConfig,
	}
}

func newRoundTripHTTPTransport() *http.Transport {
	return &http.Transport{
		Proxy: http.ProxyFromEnvironment,
		DialContext: (&net.Dialer{
			Timeout:   timeOut,
			KeepAlive: timeOut,
		}).DialContext,
		MaxIdleConns:          maxIdleCons,
		MaxIdleConnsPerHost:   maxIdleCons,
		IdleConnTimeout:       IdleConnTimeout,
		TLSHandshakeTimeout:   TLSHandshakeTimeout,
		ExpectContinueTimeout: 1 * time.Second,
		ForceAttemptHTTP2:     true,
	}
}
