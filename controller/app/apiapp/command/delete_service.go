// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package command

import (
	"context"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type DeleteServiceHandler struct {
	groupID contract.GroupID
	lgr     *logger.Logger
	s       storage.Storage
	auth    *auth.Auth
}

func NewDeleteServiceHandler(groupID contract.GroupID, s storage.Storage, a *auth.Auth, l *logger.Logger) (*DeleteServiceHandler, error) {
	if groupID == "" {
		return nil, fmt.Errorf("group id is required")
	}

	if l == nil {
		return nil, fmt.Errorf("logger is required")
	}

	if s == nil {
		return nil, fmt.Errorf("storage is required")
	}

	if a == nil {
		return nil, fmt.Errorf("auth is required")
	}

	return &DeleteServiceHandler{
		groupID: groupID,
		lgr:     l,
		s:       s,
		auth:    a,
	}, nil
}

type DeleteServiceArgs struct {
	AuthData              authentication.Data
	AuthorizationMetadata authorization.Metadata
	Name                  string
	EndpointURL           string
	InwayAddress          string
}

func (h *DeleteServiceHandler) Handle(ctx context.Context, args *DeleteServiceArgs) error {
	err := h.auth.Auth(ctx, args.AuthData, args.AuthorizationMetadata, authz.FscDeleteService, []*authz.ResourceURN{
		authz.NewResourceURN(authz.ResourceTypeService, args.Name),
	})
	if err != nil {
		return mapError(err, "could not authenticate or authorize")
	}

	if err := args.Valid(); err != nil {
		return err
	}

	err = h.s.DeleteService(ctx, &storage.DeleteServiceArgs{
		GroupID: h.groupID.String(),
		Name:    args.Name,
	})
	if err != nil {
		h.lgr.Error("failed to delete service", err)

		return mapError(err, "could not delete service in storage")
	}

	return nil
}

func (c *DeleteServiceArgs) Valid() *ValidationError {
	validationErrors := make([]string, 0)

	if c.Name == "" {
		validationErrors = append(validationErrors, "service name cannot be blank")
	}

	if !serviceNameValidator.MatchString(c.Name) {
		validationErrors = append(validationErrors, "service name must contain only letters, numbers, -, _ and .")
	}

	return mapValidationError(validationErrors)
}
