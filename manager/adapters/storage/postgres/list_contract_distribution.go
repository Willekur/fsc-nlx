// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package postgresadapter

import (
	"context"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func (r *PostgreSQLRepository) ListContractDistributions(ctx context.Context, contentHash *contract.ContentHash) ([]*contract.Distribution, error) {
	rows, err := r.queries.ListContractDistributions(ctx, contentHash.String())
	if err != nil {
		return nil, err
	}

	distributions := make([]*contract.Distribution, len(rows))

	for i, row := range rows {
		pID, errPID := contract.NewPeerID(row.PeerID)
		if errPID != nil {
			return nil, errPID
		}

		d := &contract.Distribution{
			PeerID:       pID,
			ContentHash:  contentHash,
			Action:       contract.DistributionAction(row.Action),
			Signature:    row.Signature,
			LastRetryAt:  row.LastRetryAt,
			Retries:      row.Retries,
			ErrorMessage: row.ErrorMessage,
		}

		if row.NextRetryAt.Valid {
			d.NextRetryAt = &row.NextRetryAt.Time
		}

		distributions[i] = d
	}

	return distributions, nil
}
