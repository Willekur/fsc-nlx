// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/go-chi/chi/v5"
	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
)

func (s *Server) serviceDetailHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	name := chi.URLParam(r, "name")

	page := serviceDetailPage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathServicesPage,
			Title:                       s.i18n.Translate("Services"),
			Description:                 "",
			Username:                    authData.Username(),
			LogoutURL:                   s.authn.LogoutURL(),
			Peer:                        s.getPeerInfo(ctx, getAuthzData(r), authData),
		},
		Service: &serviceDetailPageService{
			Name:                 name,
			PublicationContracts: []*serviceDetailPageServiceContract{},
			AccessContracts:      []*serviceDetailPageServiceContract{},
		},
	}

	service, err := s.apiApp.Queries.GetService.Handle(ctx, &query.GetServiceArgs{
		AuthData:              authData,
		AuthorizationMetadata: getAuthzData(r),
		Name:                  name,
	})
	if err != nil {
		if errors.Is(err, storage.ErrServiceNotFound) {
			page.renderNotFound(w)
			return
		}

		s.logger.Error("failed to retrieve service", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	page.Service = &serviceDetailPageService{
		Name:         service.Name,
		EndpointURL:  service.EndpointURL.String(),
		InwayAddress: service.InwayAddress,
	}

	publicationContracts, err := s.apiApp.Queries.ListContracts.Handle(ctx, &query.ListContractsArgs{
		AuthData: authData,
		Filters: []*query.ListContractsFilter{
			{
				GrantType: query.GrantTypeServicePublication,
			},
			{
				GrantType: query.GrantTypeDelegatedServicePublication,
			},
		},
	})
	if err != nil {
		s.logger.Error("failed to retrieve contracts with service publication grant", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	for _, contract := range publicationContracts {
		var directories []string

		isForService := false

		for _, grant := range contract.ServicePublicationGrants {
			if grant.ServicePeer.ID == s.peerInfo.ID && grant.ServiceName == service.Name {
				isForService = true

				directories = append(directories, fmt.Sprintf("%s (%s)", grant.DirectoryPeer.Name, grant.DirectoryPeer.ID))
			}
		}

		for _, grant := range contract.DelegatedServicePublicationGrants {
			if grant.ServicePeer.ID == s.peerInfo.ID && grant.ServiceName == service.Name {
				isForService = true

				directories = append(directories, fmt.Sprintf("%s (%s) namens %s (%s)", grant.DirectoryPeer.Name, grant.DirectoryPeer.ID, grant.DelegatorPeer.Name, grant.DelegatorPeer.ID))
			}
		}

		if !isForService {
			continue
		}

		page.Service.PublicationContracts = append(page.Service.PublicationContracts, &serviceDetailPageServiceContract{
			IsActive:   contract.State == query.ContractStateValid,
			Status:     convertStateToHumanReadableText(contract.State),
			Title:      strings.Join(directories, ", "),
			Hash:       contract.Hash,
			ValidFrom:  contract.ValidFrom.UTC(),
			ValidUntil: contract.ValidUntil.UTC(),
			CreatedAt:  contract.CreatedAt.UTC(),
		})
	}

	accessContracts, err := s.apiApp.Queries.ListContracts.Handle(ctx, &query.ListContractsArgs{
		AuthData: authData,
		Filters: []*query.ListContractsFilter{
			{
				GrantType: query.GrantTypeServiceConnection,
			},
			{
				GrantType: query.GrantTypeDelegatedServiceConnection,
			},
		},
	})
	if err != nil {
		s.logger.Error("failed to retrieve contracts with service connection grant", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	for _, contract := range accessContracts {
		var descriptions []string

		isForService := false

		for _, grant := range contract.ServiceConnectionGrants {
			if grant.ServicePeer.ID == s.peerInfo.ID && grant.ServiceName == service.Name {
				isForService = true

				descriptions = append(descriptions, fmt.Sprintf("%s (%s)", grant.OutwayPeer.Name, grant.OutwayPeer.ID))
			}
		}

		for _, grant := range contract.DelegatedServiceConnectionGrants {
			if grant.ServicePeer.ID == s.peerInfo.ID && grant.ServiceName == service.Name {
				isForService = true

				descriptions = append(descriptions, fmt.Sprintf("%s (%s) door %s (%s)", grant.OutwayPeer.Name, grant.OutwayPeer.ID, grant.DelegatorPeer.Name, grant.DelegatorPeer.ID))
			}
		}

		if !isForService {
			continue
		}

		page.Service.AccessContracts = append(page.Service.AccessContracts, &serviceDetailPageServiceContract{
			IsActive:   contract.State == query.ContractStateValid,
			Status:     convertStateToHumanReadableText(contract.State),
			Title:      strings.Join(descriptions, ", "),
			Hash:       contract.Hash,
			ValidFrom:  contract.ValidFrom.UTC(),
			ValidUntil: contract.ValidUntil.UTC(),
			CreatedAt:  contract.CreatedAt.UTC(),
		})
	}

	page.render(w)
}
