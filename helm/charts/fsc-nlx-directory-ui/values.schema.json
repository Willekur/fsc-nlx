{
    "title": "Chart Values",
    "type": "object",
    "properties": {
        "global": {
            "type": "object",
            "properties": {
                "imageRegistry": {
                    "type": "string",
                    "description": "Global Docker Image registry",
                    "default": ""
                },
                "imageTag": {
                    "type": "string",
                    "description": "Global Docker Image tag",
                    "default": ""
                },
                "certificates": {
                    "type": "object",
                    "properties": {
                        "group": {
                            "type": "object",
                            "properties": {
                                "caCertificatePEM": {
                                    "type": "string",
                                    "description": "Global FSC-NLX CA root certificate. If not set the value of 'tls.organization.rootCertificatePEM' is used",
                                    "default": ""
                                }
                            }
                        }
                    }
                }
            }
        },
        "image": {
            "type": "object",
            "properties": {
                "registry": {
                    "type": "string",
                    "description": "Image registry (ignored if 'global.imageRegistry' is set)",
                    "default": "docker.io"
                },
                "repository": {
                    "type": "string",
                    "description": "Image repository of the controller API.",
                    "default": "nlxio/fsc-directory-ui"
                },
                "tag": {
                    "type": "string",
                    "description": "Image tag (ignored if 'global.imageTag' is set). When set to null, the AppVersion from the chart is used",
                    "default": ""
                },
                "pullPolicy": {
                    "type": "string",
                    "description": "Image pull policy",
                    "default": "Always"
                },
                "pullSecrets": {
                    "type": "array",
                    "description": "Secrets for the image repository",
                    "default": [],
                    "items": {}
                }
            }
        },
        "serviceAccount": {
            "type": "object",
            "properties": {
                "create": {
                    "type": "boolean",
                    "description": "Specifies whether a service account should be created",
                    "default": true
                },
                "annotations": {
                    "type": "object",
                    "description": "Annotations to add to the service account",
                    "default": {}
                },
                "name": {
                    "type": "string",
                    "description": "The name of the service account to use. If not set and create is true, a name is generated using the fullname template",
                    "default": ""
                }
            }
        },
        "replicaCount": {
            "type": "number",
            "description": "Number of controller replicas",
            "default": 1
        },
        "podSecurityContext": {
            "type": "object",
            "properties": {
                "fsGroup": {
                    "type": "number",
                    "description": "GroupID under which the pod should be started",
                    "default": 1001
                }
            }
        },
        "securityContext": {
            "type": "object",
            "properties": {
                "runAsNonRoot": {
                    "type": "boolean",
                    "description": "Run container as a non-root user",
                    "default": true
                },
                "runAsUser": {
                    "type": "number",
                    "description": "Run container as specified user",
                    "default": 1001
                },
                "capabilities": {
                    "type": "object",
                    "properties": {
                        "drop": {
                            "type": "array",
                            "description": "Drop all capabilities by default",
                            "default": [
                                "ALL"
                            ],
                            "items": {
                                "type": "string"
                            }
                        }
                    }
                }
            }
        },
        "resources": {
            "type": "object",
            "description": "Pod resource requests & limits",
            "default": {}
        },
        "nodeSelector": {
            "type": "object",
            "description": "Node labels for pod assignment",
            "default": {}
        },
        "affinity": {
            "type": "object",
            "description": "Node affinity for pod assignment",
            "default": {}
        },
        "tolerations": {
            "type": "array",
            "description": "Node tolerations for pod assignment",
            "default": [],
            "items": {}
        },
        "extraEnv": {
            "type": "array",
            "description": "Extra env items for pod assignment",
            "default": [],
            "items": {}
        },
        "annotations": {
            "type": "object",
            "description": "Annotations added to the deployment",
            "default": {}
        },
        "nameOverride": {
            "type": "string",
            "description": "Override deployment name",
            "default": ""
        },
        "fullnameOverride": {
            "type": "string",
            "description": "Override full deployment name",
            "default": ""
        },
        "config": {
            "type": "object",
            "properties": {
                "logType": {
                    "type": "string",
                    "description": "Possible values 'live', 'local'. Affects the log output. See NewProduction and NewDevelopment at https://godoc.org/go.uber.org/zap#Logger",
                    "default": "live"
                },
                "logLevel": {
                    "type": "string",
                    "description": "Possible values 'debug', 'warn', 'info'. Override the default logLevel set by 'config.logType'",
                    "default": "info"
                },
                "directoryManagerAddress": {
                    "type": "string",
                    "description": "Address of the manager component of the directory",
                    "default": ""
                },
                "baseURLPath": {
                    "type": "string",
                    "description": "URL path the UI is served from",
                    "default": "/"
                }
            }
        },
        "certificates": {
            "type": "object",
            "properties": {
                "group": {
                    "type": "object",
                    "properties": {
                        "caCertificatePEM": {
                            "type": "string",
                            "description": "The CA certificate of the Group",
                            "default": ""
                        },
                        "certificatePEM": {
                            "type": "string",
                            "description": "The Group certificate",
                            "default": ""
                        },
                        "keyPEM": {
                            "type": "string",
                            "description": "Private Key of 'certificates.group.certificatePEM'",
                            "default": ""
                        },
                        "existingSecret": {
                            "type": "string",
                            "description": "Use existing secret with your FSC-NLX keypair (`certificates.group.certificatePEM` and `certificates.group.keyPEM` will be ignored and picked up from the secret)",
                            "default": ""
                        }
                    }
                }
            }
        },
        "service": {
            "type": "object",
            "properties": {
                "type": {
                    "type": "string",
                    "description": "Service Type (ClusterIP, NodePort, LoadBalancer)",
                    "default": "ClusterIP"
                },
                "port": {
                    "type": "number",
                    "description": "Port exposed by the service\\",
                    "default": 80
                },
                "annotations": {
                    "type": "object",
                    "description": "Annotations to be included in the",
                    "default": {}
                }
            }
        },
        "ingress": {
            "type": "object",
            "properties": {
                "class": {
                    "type": "string",
                    "description": "Ingress class",
                    "default": ""
                },
                "annotations": {
                    "type": "object",
                    "description": "Ingress annotations",
                    "default": {}
                },
                "hosts": {
                    "type": "array",
                    "description": "Ingress accepted hostnames",
                    "default": [],
                    "items": {}
                },
                "tls": {
                    "type": "array",
                    "description": "Ingress TLS configuration",
                    "default": [],
                    "items": {}
                }
            }
        }
    }
}
