// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package internalapp

import (
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/command"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/query"
)

type Application struct {
	Queries  Queries
	Commands Commands
}

type Queries struct {
	GetToken                 *query.GetTokenHandler
	GetServiceEndpointURL    *query.GetServiceEndpointURLHandler
	GetServicesForOutway     *query.GetServicesForOutwayHandler
	GetCertificates          *query.GetCertificatesHandler
	GetContracts             *query.ListContractsHandler
	GetContractDistributions *query.ListContractDistributionsHandler
	GetServices              *query.GetServicesHandler
	GetTXLogRecords          *query.ListTXLogRecordsHandler
	GetPeerInfo              *query.GetPeerInfoHandler
	ListPeers                *query.ListPeersHandler
}

type Commands struct {
	AnnouncePeer              *command.AnnouncePeerHandler
	CreateContract            *command.CreateContractHandler
	AcceptContract            *command.AcceptContractHandler
	RejectContract            *command.RejectContractHandler
	RevokeContract            *command.RevokeContractHandler
	CreateCertificate         *command.CreateCertificateHandler
	RetryContractDistribution *command.RetryContractDistributionHandler
}
