// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//nolint:dupl // looks like accept contract but is different
package command

import (
	"context"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

type RevokeContractHandler struct {
	manager manager.Manager
	auth    *auth.Auth
	lgr     *logger.Logger
}

func NewRevokeContractHandler(m manager.Manager, a *auth.Auth, l *logger.Logger) (*RevokeContractHandler, error) {
	if m == nil {
		return nil, fmt.Errorf("manager is required")
	}

	if a == nil {
		return nil, fmt.Errorf("auth is required")
	}

	if l == nil {
		return nil, fmt.Errorf("logger is required")
	}

	return &RevokeContractHandler{
		manager: m,
		auth:    a,
		lgr:     l,
	}, nil
}

type RevokeContractArgs struct {
	AuthData              authentication.Data
	AuthorizationMetadata authorization.Metadata
	ContentHash           string
}

func (h *RevokeContractHandler) Handle(ctx context.Context, args *RevokeContractArgs) error {
	err := h.auth.Auth(ctx, args.AuthData, args.AuthorizationMetadata, authz.FscRevokeContract, []*authz.ResourceURN{
		authz.NewResourceURN(authz.ResourceTypeContract, args.ContentHash),
	})
	if err != nil {
		return mapError(err, "could not authenticate or authorize")
	}

	if args.ContentHash == "" {
		return newValidationError("contentHash cannot be empty")
	}

	err = h.manager.RevokeContract(ctx, args.ContentHash)
	if err != nil {
		h.lgr.Error("failed to revoke contract", err)

		return mapError(err, "could not revoke contract in manager")
	}

	return nil
}
