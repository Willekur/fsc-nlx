/*
 * Copyright © VNG Realisatie 2024
 * Licensed under the EUPL
 */

package command

import (
	"context"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

type RetryContractHandler struct {
	manager manager.Manager
	auth    *auth.Auth
	lgr     *logger.Logger
}

func NewRetryContractHandler(m manager.Manager, a *auth.Auth, l *logger.Logger) (*RetryContractHandler, error) {
	if m == nil {
		return nil, fmt.Errorf("manager is required")
	}

	if a == nil {
		return nil, fmt.Errorf("auth is required")
	}

	if l == nil {
		return nil, fmt.Errorf("logger is required")
	}

	return &RetryContractHandler{
		manager: m,
		auth:    a,
		lgr:     l,
	}, nil
}

type RetryContractHandlerArgs struct {
	AuthData              authentication.Data
	AuthorizationMetadata authorization.Metadata
	ContentHash           string
	PeerID                string
	Action                Action
}

type Action int

const (
	SubmitContract Action = iota
	SubmitAcceptSignature
	SubmitRejectSignature
	SubmitRevokeSignature
)

func (h *RetryContractHandler) Handle(ctx context.Context, args *RetryContractHandlerArgs) error {
	err := h.auth.Auth(ctx, args.AuthData, args.AuthorizationMetadata, authz.FscRetryContract, []*authz.ResourceURN{
		authz.NewResourceURN(authz.ResourceTypeContract, args.ContentHash),
	})
	if err != nil {
		return mapError(err, "could not authenticate or authorize")
	}

	if args.ContentHash == "" {
		return newValidationError("contentHash cannot be empty")
	}

	if args.PeerID == "" {
		return newValidationError("contentHash cannot be empty")
	}

	err = h.manager.RetryContract(ctx, args.ContentHash, args.PeerID, convertAction(args.Action))
	if err != nil {
		h.lgr.Error("failed to reject contract", err)

		return mapError(err, "could not retry contract in manager")
	}

	return nil
}

func convertAction(action Action) manager.Action {
	switch action {
	case SubmitContract:
		return manager.SubmitContract
	case SubmitAcceptSignature:
		return manager.SubmitAcceptSignature
	case SubmitRejectSignature:
		return manager.SubmitRejectSignature
	case SubmitRevokeSignature:
		return manager.SubmitRevokeSignature
	default:
		return -1
	}
}
