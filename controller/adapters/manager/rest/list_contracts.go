// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package restmanager

import (
	"context"
	"net/http"
	"time"

	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

// nolint:gocyclo,funlen // unable to make less complex
func (m *restManager) ListContracts(ctx context.Context, contentHashes []string, grantTypes []manager.GrantType) (manager.Contracts, error) {
	res, err := m.client.GetContractsWithResponse(ctx, &models.GetContractsParams{
		ContentHash: &contentHashes,
		GrantType:   convertGrantTypes(grantTypes),
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not get contracts from rest manager")
	}

	err = mapResponse(res, http.StatusOK, "could not get contracts", res.Body)
	if err != nil {
		return nil, err
	}

	contracts := make(manager.Contracts, len(res.JSON200.Contracts))

	for i := range res.JSON200.Contracts {
		peers := make(map[string]*manager.Peer)

		for peerID, peer := range res.JSON200.Contracts[i].Peers {
			peers[peerID] = &manager.Peer{
				ID:   peer.Id,
				Name: peer.Name,
			}
		}

		servicePublicationGrants := make([]*manager.ServicePublicationGrant, 0)
		serviceConnectionGrants := make([]*manager.ServiceConnectionGrant, 0)
		delegatedServiceConnectionGrants := make([]*manager.DelegatedServiceConnectionGrant, 0)
		delegatedServicePublicationGrants := make([]*manager.DelegatedServicePublicationGrant, 0)

		for _, grant := range res.JSON200.Contracts[i].Content.Grants {
			discriminator, err := grant.ValueByDiscriminator()
			if err != nil {
				return nil, err
			}

			switch convertedGrant := discriminator.(type) {
			case models.GrantServicePublication:
				servicePublicationGrants = append(servicePublicationGrants, &manager.ServicePublicationGrant{
					Hash:            convertedGrant.Hash,
					DirectoryPeerID: convertedGrant.Directory.PeerId,
					ServicePeerID:   convertedGrant.Service.PeerId,
					ServiceName:     convertedGrant.Service.Name,
				})

			case models.GrantServiceConnection:
				serviceConnectionGrants = append(serviceConnectionGrants, &manager.ServiceConnectionGrant{
					Hash:                        convertedGrant.Hash,
					ServicePeerID:               convertedGrant.Service.PeerId,
					ServiceName:                 convertedGrant.Service.Name,
					OutwayPeerID:                convertedGrant.Outway.PeerId,
					OutwayCertificateThumbprint: convertedGrant.Outway.CertificateThumbprint,
				})

			case models.GrantDelegatedServiceConnection:
				delegatedServiceConnectionGrants = append(delegatedServiceConnectionGrants, &manager.DelegatedServiceConnectionGrant{
					Hash:                        convertedGrant.Hash,
					DelegatorPeerID:             convertedGrant.Delegator.PeerId,
					ServicePeerID:               convertedGrant.Service.PeerId,
					ServiceName:                 convertedGrant.Service.Name,
					OutwayPeerID:                convertedGrant.Outway.PeerId,
					OutwayCertificateThumbprint: convertedGrant.Outway.CertificateThumbprint,
				})

			case models.GrantDelegatedServicePublication:
				delegatedServicePublicationGrants = append(delegatedServicePublicationGrants, &manager.DelegatedServicePublicationGrant{
					Hash:            convertedGrant.Hash,
					DirectoryPeerID: convertedGrant.Directory.PeerId,
					DelegatorPeerID: convertedGrant.Delegator.PeerId,
					ServicePeerID:   convertedGrant.Service.PeerId,
					ServiceName:     convertedGrant.Service.Name,
				})
			}
		}

		contracts[i] = &manager.Contract{
			IV:                                res.JSON200.Contracts[i].Content.Iv,
			Hash:                              res.JSON200.Contracts[i].Hash,
			HashAlgorithm:                     convertHashAlg(res.JSON200.Contracts[i].Content.HashAlgorithm),
			GroupID:                           res.JSON200.Contracts[i].Content.GroupId,
			CreatedAt:                         time.Unix(res.JSON200.Contracts[i].Content.CreatedAt, 0),
			Peers:                             peers,
			AcceptSignatures:                  convertSignatures(res.JSON200.Contracts[i].Signatures.Accept),
			RejectSignatures:                  convertSignatures(res.JSON200.Contracts[i].Signatures.Reject),
			RevokeSignatures:                  convertSignatures(res.JSON200.Contracts[i].Signatures.Revoke),
			ValidFrom:                         time.Unix(res.JSON200.Contracts[i].Content.Validity.NotBefore, 0),
			ValidUntil:                        time.Unix(res.JSON200.Contracts[i].Content.Validity.NotAfter, 0),
			ServicePublicationGrants:          servicePublicationGrants,
			ServiceConnectionGrants:           serviceConnectionGrants,
			DelegatedServicePublicationGrants: delegatedServicePublicationGrants,
			DelegatedServiceConnectionGrants:  delegatedServiceConnectionGrants,
			HasRejected:                       res.JSON200.Contracts[i].HasRejected,
			HasRevoked:                        res.JSON200.Contracts[i].HasRevoked,
			HasAccepted:                       res.JSON200.Contracts[i].HasAccepted,
			State:                             manager.ContractState(res.JSON200.Contracts[i].State),
		}
	}

	return contracts, nil
}

func convertSignatures(signatures models.SignatureMap) map[string]manager.Signature {
	convertedSignatures := make(map[string]manager.Signature)

	for peerID, signature := range signatures {
		convertedSignatures[peerID] = manager.Signature{
			SignedAt: time.Unix(signature.SignedAt, 0),
		}
	}

	return convertedSignatures
}

func convertHashAlg(a models.HashAlgorithm) manager.HashAlg {
	switch a {
	case models.HASHALGORITHMSHA3512:
		return manager.HashAlgSHA3_512
	default:
		return manager.HashAlgUnspecified
	}
}

func convertGrantTypes(grantTypes []manager.GrantType) *[]models.GrantType {
	gTypes := make([]models.GrantType, 0, len(grantTypes))

	for _, t := range grantTypes {
		switch t {
		case manager.GrantTypeServicePublication:
			gTypes = append(gTypes, models.GRANTTYPESERVICEPUBLICATION)
		case manager.GrantTypeServiceConnection:
			gTypes = append(gTypes, models.GRANTTYPESERVICECONNECTION)
		case manager.GrantTypeDelegatedServiceConnection:
			gTypes = append(gTypes, models.GRANTTYPEDELEGATEDSERVICECONNECTION)
		case manager.GrantTypeDelegatedServicePublication:
			gTypes = append(gTypes, models.GRANTTYPEDELEGATEDSERVICEPUBLICATION)
		default:
			break
		}
	}

	return &gTypes
}
