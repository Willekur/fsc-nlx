// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uuidgenerator

import (
	"fmt"

	"github.com/gofrs/uuid"
)

type IVGenerator struct{}

func (s *IVGenerator) New() (string, error) {
	u, err := uuid.NewV7()
	if err != nil {
		return "", fmt.Errorf("could not generate uuid: %w", err)
	}

	return u.String(), nil
}

func New() *IVGenerator {
	return &IVGenerator{}
}
