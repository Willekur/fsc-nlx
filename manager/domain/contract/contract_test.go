// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package contract_test

import (
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func TestContract(t *testing.T) {
	contractIV, err := uuid.Parse("26aad1d8-3653-4e6f-b3ba-70df67a9a0ab")
	assert.NoError(t, err)

	contractIVBytes, err := contractIV.MarshalBinary()
	assert.NoError(t, err)

	now := time.Date(2023, 1, 1, 1, 42, 0, 0, time.UTC)

	c, err := contract.NewContent(&contract.NewContentArgs{
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIVBytes,
		CreatedAt:     now,
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: now,
			NotAfter:  now,
		},
		Grants: []interface{}{
			&contract.NewGrantServicePublicationArgs{
				Directory: &contract.NewGrantServicePublicationDirectoryArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000002",
						Name:           "",
						ManagerAddress: "",
					},
				},
				Service: &contract.NewGrantServicePublicationServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000001",
						Name:           "Gemeente Stijns",
						ManagerAddress: "",
					},
					Name:     "parkeerrechten",
					Protocol: contract.ServiceProtocolTCPHTTP1_1,
				},
			},
		},
	})
	assert.NoError(t, err)

	assert.Equal(t, "$1$1$Q5jDgBOoRjSDRup9asBg4QIGc_-twvkjaict2xAIbFIsTgf-4nwkawmhQUUOe2215RA2xgQFNJ_xRzSBerdFgg", c.Hash().String())
	assert.Equal(t, "$1$2$AEBx0SGX5_5s651r0vJRA2jKeey8G4gVzjEd_qLFiMVwCn_gJzoK2gzyppzSrym_h0O5o37ijHbh0yiYsnueZg", c.Grants().ServicePublicationGrants()[0].Hash().String())
}
