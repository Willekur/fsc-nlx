// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package app

import (
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/app/command"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/app/query"
)

type Application struct {
	Queries  Queries
	Commands Commands
}

type Queries struct {
	ListRecords *query.ListRecordsHandler
}

type Commands struct {
	CreateRecords *command.CreateRecordsHandler
}
