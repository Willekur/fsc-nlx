// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package services

import (
	"context"
	"errors"
	"fmt"
	"time"

	"golang.org/x/sync/errgroup"
	"golang.org/x/sync/singleflight"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

const maxConcurrentCalls = 5

type LocalPeerRepository interface {
	ListPeersByID(ctx context.Context, peerIDs contract.PeersIDs) (contract.Peers, error)
	UpsertPeer(ctx context.Context, peer *contract.Peer) error
}

type ContractDistributionRepository interface {
	UpsertContractDistribution(ctx context.Context, peerID contract.PeerID, contentHash *contract.ContentHash, signature *contract.Signature, action contract.DistributionAction, errorMessage string, nextRetryAt *time.Time) error
}

type PeersCommunicationService struct {
	ctx                            context.Context
	logger                         logger.Logger
	contractDistributionRepository ContractDistributionRepository
	localRepository                LocalPeerRepository
	directoryManagerAddress        string
	managerFactory                 manager.Factory
	selfPeerID                     contract.PeerID
	getPeersFromRepo               *singleflight.Group
	getPeersFromDirectory          *singleflight.Group
}

type NewPeersCommunicationServiceArgs struct {
	Ctx                            context.Context
	Logger                         *logger.Logger
	LocalRepo                      LocalPeerRepository
	DirectoryManagerAddress        string
	ManagerFactory                 manager.Factory
	SelfPeerID                     contract.PeerID
	ContractDistributionRepository ContractDistributionRepository
}

func NewPeersCommunicationService(args *NewPeersCommunicationServiceArgs) (*PeersCommunicationService, error) {
	if args.LocalRepo == nil {
		return nil, errors.New("local contractDistributionRepository is required")
	}

	if args.ContractDistributionRepository == nil {
		return nil, errors.New("contractDistributionRepository is required")
	}

	return &PeersCommunicationService{
		ctx:                            args.Ctx,
		localRepository:                args.LocalRepo,
		managerFactory:                 args.ManagerFactory,
		selfPeerID:                     args.SelfPeerID,
		directoryManagerAddress:        args.DirectoryManagerAddress,
		getPeersFromRepo:               &singleflight.Group{},
		getPeersFromDirectory:          &singleflight.Group{},
		contractDistributionRepository: args.ContractDistributionRepository,
	}, nil
}
func (p *PeersCommunicationService) getManagerClient(peerID contract.PeerID) (manager.Manager, error) {
	address, err := p.getManagerAddress(peerID)
	if err != nil {
		return nil, fmt.Errorf("could not get manager address: %w", err)
	}

	c, err := p.managerFactory.New(address)
	if err != nil {
		return nil, fmt.Errorf("could not create peer client: %w", err)
	}

	return c, nil
}

func (p *PeersCommunicationService) getManagerAddress(peerID contract.PeerID) (string, error) {
	peer, err := p.getPeerFromRepo(peerID)
	if err != nil {
		return "", fmt.Errorf("could not get manager address from contractDistributionRepository: %w", err)
	}

	if peer != nil && peer.ManagerAddress().Value() != "" {
		return peer.ManagerAddress().Value(), nil
	}

	peer, err = p.getPeerFromDirectory(peerID)
	if err == nil {
		return peer.ManagerAddress().Value(), nil
	}

	return "", fmt.Errorf("could not get manager address from directory: %w", err)
}

func (p *PeersCommunicationService) getPeerFromRepo(peerID contract.PeerID) (*contract.Peer, error) {
	peer, err, _ := p.getPeersFromRepo.Do(peerID.Value(), func() (interface{}, error) {
		peers, err := p.localRepository.ListPeersByID(p.ctx, contract.PeersIDs{
			peerID: true,
		})
		if err != nil {
			return nil, fmt.Errorf("could not get peer from contractDistributionRepository: %w", err)
		}

		peer, ok := peers[peerID]
		if !ok {
			return nil, nil
		}

		return peer, nil
	})
	if err != nil {
		return nil, err
	}

	if peer == nil {
		return nil, nil
	}

	return peer.(*contract.Peer), nil
}

func (p *PeersCommunicationService) getPeerFromDirectory(peerID contract.PeerID) (*contract.Peer, error) {
	peer, err, _ := p.getPeersFromDirectory.Do(peerID.Value(), func() (interface{}, error) {
		directory, err := p.managerFactory.New(p.directoryManagerAddress)
		if err != nil {
			return nil, fmt.Errorf("could not get client for directory peer %q: %w", p.directoryManagerAddress, err)
		}

		peers, err := directory.GetPeers(p.ctx, contract.PeersIDs{
			peerID: true,
		})
		if err != nil {
			return nil, fmt.Errorf("could not get peer from directory: %w", err)
		}

		peer, ok := peers[peerID]
		if !ok {
			return nil, fmt.Errorf("peer with ID %q not found in directory", peerID.Value())
		}

		err = p.localRepository.UpsertPeer(p.ctx, peer)
		if err != nil {
			p.logger.Warn("could not upsert peer", err)
		}

		return peer, nil
	})
	if err != nil {
		return nil, err
	}

	return peer.(*contract.Peer), nil
}

func (p *PeersCommunicationService) sendToPeers(peers contract.PeersIDs, action func(peerID contract.PeerID, managerClient manager.Manager) error) error {
	errs := errgroup.Group{}
	errs.SetLimit(maxConcurrentCalls)

	for peerID := range peers {
		peerIDReceiver := peerID

		errs.Go(func() error {
			if p.selfPeerID == peerIDReceiver {
				return nil
			}

			managerClient, err := p.getManagerClient(peerIDReceiver)
			if err != nil {
				return fmt.Errorf("could not get client for peer %q: %w", peerIDReceiver.Value(), err)
			}

			return action(peerIDReceiver, managerClient)
		})
	}

	return errs.Wait()
}
