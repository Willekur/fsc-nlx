// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package service

import (
	"context"

	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/app"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/app/command"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/app/query"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/record"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/ports/logger"
)

type NewApplicationArgs struct {
	Context    context.Context
	Clock      command.Clock
	Logger     logger.Logger
	Repository record.Repository
}

func NewApplication(args *NewApplicationArgs) (*app.Application, error) {
	createRecordHandler, err := command.NewCreateRecordsHandler(args.Repository, args.Clock, args.Logger)
	if err != nil {
		return nil, err
	}

	application := &app.Application{
		Queries: app.Queries{
			ListRecords: query.NewListRecordsHandler(args.Repository),
		},
		Commands: app.Commands{
			CreateRecords: createRecordHandler,
		},
	}

	return application, nil
}
