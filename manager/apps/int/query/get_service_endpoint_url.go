// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"

	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/controller"
)

type GetServiceEndpointURLHandler struct {
	controller controller.Controller
}

func NewGetServiceEndpointURLHandler(repository controller.Controller) (*GetServiceEndpointURLHandler, error) {
	if repository == nil {
		return nil, errors.New("controller repository is required")
	}

	return &GetServiceEndpointURLHandler{
		controller: repository,
	}, nil
}

type GetServiceEndpointURLHandlerArgs struct {
	ServiceName string
}

func (h *GetServiceEndpointURLHandler) Handle(ctx context.Context, args *GetServiceEndpointURLHandlerArgs) (string, error) {
	service, err := h.controller.GetService(ctx, args.ServiceName)
	if err != nil {
		return "", errors.Wrapf(err, "failed to get service %q", args.ServiceName)
	}

	return service.EndpointURL.String(), nil
}
