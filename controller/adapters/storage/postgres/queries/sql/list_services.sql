-- Copyright © VNG Realisatie 2023
-- Licensed under the EUPL

-- name: ListServices :many
SELECT
    s.name,
    s.endpoint_url,
    s.inway_address
FROM
    controller.services as s
WHERE
    group_id = $1;
