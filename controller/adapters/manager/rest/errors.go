// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package restmanager

import (
	"fmt"
	"net/http"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
)

type response interface {
	StatusCode() int
}

func mapError(err error, message string) error {
	return fmt.Errorf("%s: %w: %w", message, manager.ErrInternalError, err)
}

func mapResponse(res response, wantStatusCode int, message string, body []byte) error {
	if res == nil {
		return fmt.Errorf("%s: %w", message, manager.ErrInternalError)
	}

	if res.StatusCode() == wantStatusCode {
		return nil
	}

	if res.StatusCode() == http.StatusUnprocessableEntity {
		return fmt.Errorf("%s: %w", message, manager.NewValidationError(string(body)))
	}

	return fmt.Errorf("%s: %w", message, manager.ErrInternalError)
}
