###########
## Chart ##
###########
global:
  groupID: "fsc-default"

postgresql:
  storageSize: 256Mi

outway:
  ingress:
    enabled: true
    host: nlx-outway-gemeente-riemer-{{DOMAIN_SUFFIX}}

################
## Sub-charts ##
################
fsc-nlx-manager:
  config:
    selfAddress: "https://gemeente-riemer-fsc-nlx-manager-external:8443"
    directoryManagerAddress: "https://shared-fsc-nlx-manager-external:8443"

fsc-nlx-controller:
  config:
    authn:
      oidc:
        discoveryUrl: https://dex-gemeente-riemer-{{DOMAIN_SUFFIX}}
        redirectUrl: https://nlx-controller-gemeente-riemer-{{DOMAIN_SUFFIX}}/oidc/callback
  ingress:
    hosts:
      - nlx-controller-gemeente-riemer-{{DOMAIN_SUFFIX}}

dex:
  config:
    issuer: https://dex-gemeente-riemer-{{DOMAIN_SUFFIX}}
    staticClients:
      - id: nlx-controller
        name: NLX Controller
        secret: 99DbIk7FqlUYqbyD3qSX4Wmf
        redirectURIs:
          - https://nlx-controller-gemeente-riemer-{{DOMAIN_SUFFIX}}/oidc/callback
  ingress:
      hosts:
        - host: dex-gemeente-riemer-{{DOMAIN_SUFFIX}}
          paths:
            - path: /
              pathType: ImplementationSpecific
