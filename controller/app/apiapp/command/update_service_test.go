// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//
//nolint:dupl // this is a test
package command_test

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"golang.org/x/exp/maps"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

// nolint:funlen // these tests should not fit 100 lines
func TestUpdateService(t *testing.T) {
	t.Parallel()

	groupID, err := contract.NewGroupID("my-group.directory.nlx.io")
	assert.NoError(t, err)

	type tests map[string]struct {
		setup   func(context.Context, *mocks)
		args    *command.UpdateServiceArgs
		wantErr error
	}

	validArgs := &command.UpdateServiceArgs{
		AuthData:              &authentication.OIDCData{},
		AuthorizationMetadata: &authorization.RestMetaData{},
		Name:                  "TestService",
		EndpointURL:           "https://petstore.io",
		InwayAddress:          "my-inway.local:443",
	}

	flowTests := tests{
		"when_storage_errors": {
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscUpdateService,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeService, validArgs.Name),
					},
				).Return(nil)

				m.storage.EXPECT().
					UpdateService(ctx, &storage.UpdateServiceArgs{
						GroupID:      groupID.String(),
						Name:         validArgs.Name,
						EndpointURL:  validArgs.EndpointURL,
						InwayAddress: validArgs.InwayAddress,
					}).
					Return(fmt.Errorf("arbitrary"))
			},
			args:    validArgs,
			wantErr: &command.InternalError{},
		},
		"when_unauthenticated": {
			args: func(a command.UpdateServiceArgs) *command.UpdateServiceArgs {
				a.AuthData = nil
				return &a
			}(*validArgs),
			wantErr: &command.AuthenticationError{},
		},
		"when_unauthorized": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscUpdateService,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeService, validArgs.Name),
					},
				).Return(authz.NewAuthorizationError("arbitrary"))
			},
			wantErr: &command.AuthorizationError{},
		},
		"happy_flow": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscUpdateService,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeService, validArgs.Name),
					},
				).Return(nil)

				m.storage.EXPECT().
					UpdateService(ctx, &storage.UpdateServiceArgs{
						GroupID:      groupID.String(),
						Name:         validArgs.Name,
						EndpointURL:  validArgs.EndpointURL,
						InwayAddress: validArgs.InwayAddress,
					}).
					Return(nil)
			},
			wantErr: nil,
		},
	}

	validationTests := tests{
		"invalid_name": {
			args: func(a command.UpdateServiceArgs) *command.UpdateServiceArgs {
				a.Name = ""
				return &a
			}(*validArgs),
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscUpdateService,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeService, ""),
					},
				).Return(nil)
			},
			wantErr: &command.ValidationError{},
		},
		"invalid_endpoint_url": {
			args: func(a command.UpdateServiceArgs) *command.UpdateServiceArgs {
				a.EndpointURL = ""
				return &a
			}(*validArgs),
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscUpdateService,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeService, validArgs.Name),
					},
				).Return(nil)
			},
			wantErr: &command.ValidationError{},
		},
	}

	allTests := tests{}
	maps.Copy(allTests, flowTests)
	maps.Copy(allTests, validationTests)

	// nolint:dupl // not the same
	for name, tt := range allTests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			mocks := newMocks(t)

			h, err := command.NewUpdateServiceHandler(groupID, mocks.storage, mocks.auth, mocks.logger)
			require.NoError(t, err)

			ctx := context.Background()

			if tt.setup != nil {
				tt.setup(ctx, mocks)
			}

			err = h.Handle(ctx, tt.args)

			if tt.wantErr == nil {
				assert.NoError(t, err)
			} else {
				assert.ErrorAs(t, err, &tt.wantErr)
			}
		})
	}
}

func TestUpdateServiceArgsValidation(t *testing.T) {
	t.Parallel()

	type tests map[string]struct {
		args       *command.UpdateServiceArgs
		wantErr    error
		wantErrMsg string
	}

	testCases := tests{
		"invalid_name_spaces": {
			args: &command.UpdateServiceArgs{
				Name:        "test service",
				EndpointURL: "https://example.com",
			},
			wantErr:    &command.ValidationError{},
			wantErrMsg: "validation error: service name must contain only letters, numbers, -, _ and .",
		},
		"invalid_name_blank": {
			args: &command.UpdateServiceArgs{
				Name:        "",
				EndpointURL: "https://example.com",
			},
			wantErr:    &command.ValidationError{},
			wantErrMsg: "validation error: service name cannot be blank\nservice name must contain only letters, numbers, -, _ and .",
		},
		"invalid_url_blank": {
			args: &command.UpdateServiceArgs{
				Name:        "testservice",
				EndpointURL: "",
			},
			wantErr:    &command.ValidationError{},
			wantErrMsg: "validation error: endpoint URL cannot be blank, must start with https:// or http://, cannot contain spaces and cannot contain a trailing slash",
		},
		"invalid_url_trailing_slash": {
			args: &command.UpdateServiceArgs{
				Name:         "testservice",
				EndpointURL:  "https://service/",
				InwayAddress: "https://inway:443",
			},
			wantErr:    &command.ValidationError{},
			wantErrMsg: "validation error: endpoint URL cannot be blank, must start with https:// or http://, cannot contain spaces and cannot contain a trailing slash",
		},
		"invalid_name_and_url": {
			args: &command.UpdateServiceArgs{
				Name:        "",
				EndpointURL: "",
			},
			wantErr:    &command.ValidationError{},
			wantErrMsg: "validation error: service name cannot be blank\nservice name must contain only letters, numbers, -, _ and .\nendpoint URL cannot be blank, must start with https:// or http://, cannot contain spaces and cannot contain a trailing slash",
		},
	}

	for testCase, test := range testCases {
		tt := test

		t.Run(testCase, func(t *testing.T) {
			t.Parallel()

			err := tt.args.Valid()
			assert.ErrorAs(t, err, &tt.wantErr)
			assert.Equal(t, tt.wantErrMsg, err.Error())
		})
	}
}
