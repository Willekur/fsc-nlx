/*
 * Copyright © VNG Realisatie 2023
 * Licensed under the EUPL
 *
 */

package query

import (
	"context"
	"errors"
	"fmt"
	"sort"

	"golang.org/x/sync/singleflight"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type ListPeersHandler struct {
	managerFactory              manager.Factory
	directoryPeerManagerAddress string
	directoryPeer               manager.Manager
	directoryConnection         *singleflight.Group
}

type ListPeersPeers []*ListPeersPeer

type ListPeersPeer struct {
	ID   string
	Name string
}

func NewListPeersHandler(managerFactory manager.Factory, directoryPeerManagerAddress string) (*ListPeersHandler, error) {
	if managerFactory == nil {
		return nil, errors.New("managerFactory is required")
	}

	if directoryPeerManagerAddress == "" {
		return nil, errors.New("directoryPeerManagerAddress is required")
	}

	return &ListPeersHandler{
		managerFactory:              managerFactory,
		directoryPeerManagerAddress: directoryPeerManagerAddress,
		directoryConnection:         &singleflight.Group{},
	}, nil
}

func (h *ListPeersHandler) Handle(ctx context.Context) (ListPeersPeers, error) {
	res, err, _ := h.directoryConnection.Do(h.directoryPeerManagerAddress, func() (interface{}, error) {
		if h.directoryPeer == nil {
			directory, err := h.managerFactory.New(h.directoryPeerManagerAddress)
			if err != nil {
				return nil, fmt.Errorf("could not create directory peer client: %w", err)
			}

			h.directoryPeer = directory
		}
		return h.directoryPeer.GetPeers(ctx, nil)
	})

	if err != nil {
		return nil, errors.New("could not retrieve peers from directory")
	}

	svcPeers := res.(contract.Peers)

	peers := make(ListPeersPeers, 0, len(svcPeers))

	for _, v := range svcPeers {
		peers = append(peers, &ListPeersPeer{
			ID:   v.ID().Value(),
			Name: v.Name().Value(),
		})
	}

	sort.Slice(peers, func(i, j int) bool {
		return peers[i].ID < peers[j].ID
	})

	return peers, nil
}
