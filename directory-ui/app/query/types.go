// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

type Service struct {
	Name      string
	Provider  Peer
	Delegator *Peer
}

func (s *Service) IsDelegatedPublication() bool {
	return s.Delegator != nil
}

type Peer struct {
	ID   string
	Name string
}

type Participant struct {
	ID   string
	Name string
}
