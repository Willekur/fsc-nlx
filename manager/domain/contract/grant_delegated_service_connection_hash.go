// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package contract

import (
	"github.com/pkg/errors"
)

type grantDelegatedServiceConnectionHash Hash

func newGrantDelegatedServiceConnectionHash(gID GroupID, cID contentIV, alg HashAlg, gc *GrantDelegatedServiceConnection) (*grantDelegatedServiceConnectionHash, error) {
	h, err := newHash(alg, HashTypeGrantDelegatedServiceConnection, getSortedGrantDelegatedConnectionBytes(gID, cID, gc))
	if err != nil {
		return nil, errors.Wrap(err, "could not create grant delegated service connection hash")
	}

	return (*grantDelegatedServiceConnectionHash)(h), nil
}

func getSortedGrantDelegatedConnectionBytes(gID GroupID, cID contentIV, gc *GrantDelegatedServiceConnection) []byte {
	grantBytes := make([]byte, 0)

	grantBytes = append(grantBytes, []byte(gID)...)
	grantBytes = append(grantBytes, cID.Bytes()...)
	grantBytes = append(grantBytes, []byte(gc.Outway().Peer().ID().Value())...)
	grantBytes = append(grantBytes, []byte(gc.Outway().CertificateThumbprint().Value())...)
	grantBytes = append(grantBytes, []byte(gc.Service().Peer().ID())...)
	grantBytes = append(grantBytes, []byte(gc.Service().Name())...)
	grantBytes = append(grantBytes, []byte(gc.Delegator().Peer().ID().Value())...)

	return grantBytes
}

func (h grantDelegatedServiceConnectionHash) String() string {
	return Hash(h).String()
}
