// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package restmanager

import (
	"context"
	"fmt"
	"net/http"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
)

func (m *restManager) RevokeContract(ctx context.Context, contentHash string) error {
	res, err := m.client.RevokeContractWithResponse(ctx, contentHash)
	if err != nil {
		return fmt.Errorf("could not revoke contract with rest manager: %s: %w", err, manager.ErrInternalError)
	}

	return mapResponse(res, http.StatusNoContent, "could not revoke contract", res.Body)
}
