#!/bin/bash
# Copyright © VNG Realisatie 2022
# Licensed under the EUPL

# Run this script from the root folder of the git repository with the following command: ./scripts/start-development.sh

# Make sure permissions for pki files are ok
sh ./pki/fix-permissions.sh

# Start docker-compose
# Note: the --build flag is needed to rebuild the auth-opa containers,
# since changes to the content of the files do not trigger a rebuild automatically
if ! docker-compose up -d --build --remove-orphans; then
    echo "Error while starting docker-compose, exiting now"
    exit
fi

# Wait for postgres to accept connections
until docker-compose exec postgres pg_isready
do
    sleep 1;
done;

# Migrate txlog databases
go run ./txlog-api migrate up --postgres-dsn "postgres://postgres:postgres@127.0.0.1:5432/nlx_txlog_directory?sslmode=disable"
go run ./txlog-api migrate up --postgres-dsn "postgres://postgres:postgres@127.0.0.1:5432/nlx_txlog_a?sslmode=disable"
go run ./txlog-api migrate up --postgres-dsn "postgres://postgres:postgres@127.0.0.1:5432/nlx_txlog_b?sslmode=disable"
go run ./txlog-api migrate up --postgres-dsn "postgres://postgres:postgres@127.0.0.1:5432/nlx_txlog_c?sslmode=disable"

# Migrate manager database
go run ./manager migrate up --postgres-dsn "postgres://postgres:postgres@127.0.0.1:5432/nlx_manager_directory?sslmode=disable"
go run ./manager migrate up --postgres-dsn "postgres://postgres:postgres@127.0.0.1:5432/nlx_manager_org_a?sslmode=disable"
go run ./manager migrate up --postgres-dsn "postgres://postgres:postgres@127.0.0.1:5432/nlx_manager_org_b?sslmode=disable"
go run ./manager migrate up --postgres-dsn "postgres://postgres:postgres@127.0.0.1:5432/nlx_manager_org_c?sslmode=disable"

# Migrate nlx_controller databases
go run ./controller migrate up --authn-type oidc --postgres-dsn "postgres://postgres:postgres@127.0.0.1:5432/nlx_controller_org_a?sslmode=disable"
go run ./controller migrate up --postgres-dsn "postgres://postgres:postgres@127.0.0.1:5432/nlx_controller_org_b?sslmode=disable"
go run ./controller migrate up --postgres-dsn "postgres://postgres:postgres@127.0.0.1:5432/nlx_controller_org_c?sslmode=disable"

# start services
modd

function finish {
  docker-compose down --remove-orphans

  # kill all left over debug processes
  ps w | grep '.*\.debugger/.*\.bin' | awk '{print $1}' | xargs kill -9 $1
}

trap finish EXIT
