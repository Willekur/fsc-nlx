// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
)

const (
	formActionContractAccept = "accept"
	formActionContractRevoke = "revoke"
	formActionContractReject = "reject"
)

func (s *Server) signContractPostHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	contentHash := chi.URLParam(r, "hash")

	err := r.ParseForm()
	if err != nil {
		s.logger.Error("sign contract parse form", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	action := r.Form.Get("action")

	switch action {
	case formActionContractAccept:
		err = s.apiApp.Commands.AcceptContract.Handle(ctx, &command.AcceptContractArgs{
			AuthData:              authData,
			AuthorizationMetadata: getAuthzData(r),
			ContentHash:           contentHash,
		})
	case formActionContractRevoke:
		err = s.apiApp.Commands.RevokeContract.Handle(ctx, &command.RevokeContractArgs{
			AuthData:              authData,
			AuthorizationMetadata: getAuthzData(r),
			ContentHash:           contentHash,
		})
	case formActionContractReject:
		err = s.apiApp.Commands.RejectContract.Handle(ctx, &command.RejectContractArgs{
			AuthData:              authData,
			AuthorizationMetadata: getAuthzData(r),
			ContentHash:           contentHash,
		})
	default:
		s.logger.Error(fmt.Sprintf("unknown form action: %s", action), nil)

		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	if err != nil {
		s.logger.Error(fmt.Sprintf("failed to preform %s action on contract", action), err)

		statusCode, message := mapError(err)

		w.WriteHeader(statusCode)

		_, err = w.Write([]byte(message))
		if err != nil {
			s.logger.Error("failed to write response", err)

			http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

			return
		}

		return
	}

	w.Header().Add("Location", fmt.Sprintf("/contract/%s", contentHash))
	w.WriteHeader(http.StatusSeeOther)
}
