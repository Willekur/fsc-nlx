// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package rest

import (
	"context"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
	auth "gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
)

type PeerInfo struct {
	ID   string
	Name string
}

func (s *Server) getPeerInfo(ctx context.Context, authData auth.Data) *PeerInfo {
	if s.peerInfo != nil {
		return s.peerInfo
	}

	p, err := s.app.Queries.GetPeerInfo.Handle(ctx, &query.GetPeerInfoArgs{AuthData: authData})
	if err != nil {
		// this info is not important enough to error on
		return &PeerInfo{
			ID:   "",
			Name: "",
		}
	}

	peerInfo := &PeerInfo{
		ID:   p.ID,
		Name: p.Name,
	}

	s.peerInfo = peerInfo

	return peerInfo
}
