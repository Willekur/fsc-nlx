// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package tls

import (
	"crypto/x509"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"time"

	"github.com/cloudflare/cfssl/log"
	lru "github.com/hashicorp/golang-lru/v2/expirable"
)

type CRLsCache struct {
	cache *lru.LRU[string, map[string]*x509.RevocationList] // LRU[CRLURL, map[Serialnumber]RevocationList]
	urls  map[string]*x509.RevocationList
}

const (
	LRUDefaultCacheSize = 128
	LRUTTL              = 24 * time.Hour
)

type CertificateRevokedError struct {
	message string
}

func (e CertificateRevokedError) Error() string {
	return e.message
}

func NewCertificateRevokedError(serialNumber string) *CertificateRevokedError {
	return &CertificateRevokedError{message: fmt.Sprintf("certificate with serialnumber: %s is present on a Certificate Revocation List", serialNumber)}
}

func NewCRL(cRLURLs []string) (*CRLsCache, error) {
	urls := make(map[string]*x509.RevocationList)

	crl := &CRLsCache{
		urls: urls,
	}
	crl.cache = lru.NewLRU[string, map[string]*x509.RevocationList](LRUDefaultCacheSize, crl.onEvictedEntry, LRUTTL)

	if err := crl.Fetch(cRLURLs); err != nil {
		return nil, err
	}

	return crl, nil
}

func (l *CRLsCache) onEvictedEntry(k string, _ map[string]*x509.RevocationList) {
	err := l.Fetch([]string{k})
	if err != nil {
		log.Error(err)
	}
}

func (l *CRLsCache) Fetch(uRLs []string) error {
	for _, URL := range uRLs {
		revocationList, err := fetchRevocationList(URL)
		if err != nil {
			return err
		}

		err = l.AddRevocationList(URL, revocationList)
		if err != nil {
			return err
		}
	}

	return nil
}

//nolint:gosec // needs to fetch different CRLs
func fetchRevocationList(revocationListURL string) (*x509.RevocationList, error) {
	response, err := http.Get(revocationListURL)
	if err != nil {
		return nil, fmt.Errorf("cannot retrieve CRLsCache from revocationListURL: %s", revocationListURL)
	}
	defer response.Body.Close()

	rawCRL, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, errors.Join(err, fmt.Errorf("cannot parse HTTP response from %q", revocationListURL))
	}

	revocationList, err := x509.ParseRevocationList(rawCRL)
	if err != nil {
		return nil, errors.Join(err, fmt.Errorf("cannot parse CRLsCache from %q", revocationListURL))
	}

	return revocationList, nil
}

func (l *CRLsCache) AddRevocationList(crlURL string, revocationList *x509.RevocationList) error {
	_, err := url.Parse(crlURL)
	if err != nil {
		return err
	}

	l.urls[crlURL] = revocationList

	revokedSerialNumbers := make(map[string]*x509.RevocationList)
	for _, revokedSerialNumber := range revocationList.RevokedCertificateEntries {
		revokedSerialNumbers[revokedSerialNumber.SerialNumber.String()] = revocationList
	}

	l.cache.Add(crlURL, revokedSerialNumbers)

	return nil
}

func (l *CRLsCache) CheckCertificate(certificate *x509.Certificate) error {
	// Check if the cache is up-to-date.
	crlsNotInCache := l.validateCache(certificate.CRLDistributionPoints)

	// If there are crls not present in the cache, fetch the CRLs.
	if len(crlsNotInCache) > 0 {
		if err := l.Fetch(crlsNotInCache); err != nil {
			return err
		}
	}

	// the cache is up-to-date, check if the certificate is not revoked.
	err := l.check(certificate)
	if err != nil {
		return err
	}

	return nil
}

// check verifies that the certificate is not revoked. this function expects the cache to be valid and up-to-date.
func (l *CRLsCache) check(certificate *x509.Certificate) error {
	for _, crl := range l.cache.Values() {
		if crl[certificate.SerialNumber.String()] != nil {
			return NewCertificateRevokedError(certificate.SerialNumber.String())
		}
	}

	return nil
}

func (l *CRLsCache) validateCache(cRLDistributionPoints []string) (crlsWithoutValidCache []string) {
	for _, CRLDistributionPointURL := range cRLDistributionPoints {
		if !l.isCached(CRLDistributionPointURL) {
			crlsWithoutValidCache = append(crlsWithoutValidCache, CRLDistributionPointURL)
		}
	}

	for URL := range l.urls {
		if !l.isCached(URL) {
			crlsWithoutValidCache = append(crlsWithoutValidCache, URL)
		}
	}

	return crlsWithoutValidCache
}

func (l *CRLsCache) isCached(crlURL string) bool {
	crl, ok := l.urls[crlURL]
	if !ok {
		return false
	}

	if crl.NextUpdate.Before(time.Now()) {
		l.cache.Remove(crlURL)
		return false
	}

	return true
}

// function used in crypto/tls.Config#VerifyPeerCertificate to also check client certificates against CRLs
func (l *CRLsCache) VerifyPeerCertificatesCRL() func(rawCerts [][]byte, verifiedChains [][]*x509.Certificate) error {
	return func(rawCerts [][]byte, verifiedChains [][]*x509.Certificate) error {
		for _, rawCert := range rawCerts {
			cert, err := x509.ParseCertificate(rawCert)
			if err != nil {
				return fmt.Errorf("failed to parse certificate: %v", err)
			}

			err = l.CheckCertificate(cert)
			if err != nil {
				return err
			}
		}

		return nil
	}
}
