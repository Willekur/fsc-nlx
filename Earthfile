VERSION 0.5

FROM registry.gitlab.com/commonground/nlx/fsc-nlx-earthly-base-image:latest

WORKDIR /src

all:
    BUILD +mocks
    BUILD +sqlc
    BUILD +oas
    BUILD +helm-generate-docs

oas:
    BUILD +oas-manager-external
    BUILD +oas-manager-internal
    BUILD +oas-manager-internal-unauthenticated
    BUILD +oas-txlog-api
    BUILD +oas-controller-internal-rest
    BUILD +oas-controller-rest
    BUILD +oas-controller-authz

mocks:
    BUILD +mocks-mockery

sqlc:
    BUILD +sqlc-txlog-api
    BUILD +sqlc-manager
    BUILD +sqlc-controller

enums:
    BUILD +enums-permissions

deps:
    COPY go.mod go.sum /src/

oas-manager-external:
    FROM +deps
    COPY ./manager/ports/ext/rest/api/*.yaml /src/

    RUN npx @redocly/cli join core.yaml logging.yaml --prefix-components-with-info-prop title -o fsc.yaml

    RUN mkdir models
    RUN oapi-codegen --config model.cfg.yaml fsc.yaml
    RUN oapi-codegen --config server.cfg.yaml fsc.yaml

    SAVE ARTIFACT /src/*.go AS LOCAL ./manager/ports/ext/rest/api/
    SAVE ARTIFACT /src/models/*.go AS LOCAL ./manager/ports/ext/rest/api/models/

oas-manager-internal:
    FROM +deps
    COPY ./manager/ports/int/rest/api/*.yaml /src/

    RUN mkdir models server
    RUN oapi-codegen --config model.cfg.yaml openapi.yaml
    RUN oapi-codegen --config server.cfg.yaml openapi.yaml

    SAVE ARTIFACT /src/server/*.go AS LOCAL ./manager/ports/int/rest/api/server/
    SAVE ARTIFACT /src/models/*.go AS LOCAL ./manager/ports/int/rest/api/models/

oas-manager-internal-unauthenticated:
    FROM +deps
    COPY ./manager/ports/intunauthenticated/rest/api/*.yaml /src/

    RUN mkdir models server
    RUN oapi-codegen --config model.cfg.yaml openapi.yaml
    RUN oapi-codegen --config server.cfg.yaml openapi.yaml

    SAVE ARTIFACT /src/server/*.go AS LOCAL ./manager/ports/intunauthenticated/rest/api/server/
    SAVE ARTIFACT /src/models/*.go AS LOCAL ./manager/ports/intunauthenticated/rest/api/models/

oas-txlog-api:
    FROM +deps
    COPY ./txlog-api/ports/rest/api/*.yaml /src/

    RUN mkdir models server
    RUN oapi-codegen --config model.cfg.yaml openapi.yaml
    RUN oapi-codegen --config server.cfg.yaml openapi.yaml

    SAVE ARTIFACT /src/server/*.go AS LOCAL ./txlog-api/ports/rest/api/server/
    SAVE ARTIFACT /src/models/*.go AS LOCAL ./txlog-api/ports/rest/api/models/

oas-controller-internal-rest:
    FROM +deps
    COPY ./controller/ports/internalrest/api/*.yaml /src/

    RUN mkdir models
    RUN mkdir server
    RUN oapi-codegen --config model.cfg.yaml openapi.yaml
    RUN oapi-codegen --config server.cfg.yaml openapi.yaml

    SAVE ARTIFACT /src/server/*.go AS LOCAL ./controller/ports/internalrest/api/server/
    SAVE ARTIFACT /src/models/*.go AS LOCAL ./controller/ports/internalrest/api/models/

oas-controller-rest:
    FROM +deps
    COPY ./controller/ports/rest/api/*.yaml /src/

    RUN mkdir models
    RUN oapi-codegen --config model.cfg.yaml api.yaml
    RUN oapi-codegen --config server.cfg.yaml api.yaml

    SAVE ARTIFACT /src/*.go AS LOCAL ./controller/ports/rest/api/
    SAVE ARTIFACT /src/models/*.go AS LOCAL ./controller/ports/rest/api/models/

oas-controller-authz:
    FROM +deps
    COPY ./controller/adapters/authz/rest/api/*.yaml /src/

    RUN mkdir models client
    RUN oapi-codegen --config model.cfg.yaml openapi.yaml
    RUN oapi-codegen --config client.cfg.yaml openapi.yaml

    SAVE ARTIFACT /src/client/*.go AS LOCAL ./controller/adapters/authz/rest/api/client/
    SAVE ARTIFACT /src/models/*.go AS LOCAL ./controller/adapters/authz/rest/api/models/

mocks-mockery:
    FROM +deps

    COPY ./ /src/

    RUN mkdir -p /dist || true
    WORKDIR /src

    RUN mockery

    RUN goimports -w -local "gitlab.com/commonground/nlx/fsc-nlx" /src/

    # common
    SAVE ARTIFACT /src/common/transactionlog/mock/mock_transaction_logger.go AS LOCAL ./common/transactionlog/mock/mock_transaction_logger.go
    SAVE ARTIFACT /src/common/clock/mock/mock_clock.go AS LOCAL ./common/clock/mock/mock_clock.go

    # ca-certportal
    SAVE ARTIFACT /src/ca-certportal/mock/mock_signer.go AS LOCAL ./ca-certportal/mock/mock_signer.go

    # manager
    SAVE ARTIFACT /src/manager/domain/contract/mock/mock_repository.go AS LOCAL ./manager/domain/contract/mock/mock_repository.go
    SAVE ARTIFACT /src/manager/adapters/manager/mock/mock_manager.go AS LOCAL ./manager/adapters/manager/mock/mock_manager.go
    SAVE ARTIFACT /src/manager/adapters/manager/mock/mock_factory.go AS LOCAL ./manager/adapters/manager/mock/mock_factory.go

    # controller
    SAVE ARTIFACT /src/controller/adapters/ivgenerator/mock/mock_iv_generator.go AS LOCAL ./controller/adapters/ivgenerator/mock/mock_iv_generator.go
    SAVE ARTIFACT /src/controller/adapters/manager/mock/mock_manager.go AS LOCAL ./controller/adapters/manager/mock/mock_manager.go
    SAVE ARTIFACT /src/controller/pkg/manager/mock/mock_client.go AS LOCAL ./controller/pkg/manager/mock/mock_client.go
    SAVE ARTIFACT /src/controller/adapters/storage/mock/mock_storage.go AS LOCAL ./controller/adapters/storage/mock/mock_storage.go
    SAVE ARTIFACT /src/controller/adapters/authz/mock/mock_authorization.go AS LOCAL ./controller/adapters/authz/mock/mock_authorization.go
    SAVE ARTIFACT /src/controller/adapters/authz/rest/api/client/mock/mock_client_with_responses_interface.go AS LOCAL ./controller/adapters/authz/rest/api/client/mock/mock_client_with_responses_interface.go

    # outway
    SAVE ARTIFACT /src/outway/domain/config/mock/mock_repository.go AS LOCAL ./outway/domain/config/mock/mock_repository.go

    # txlog
    SAVE ARTIFACT /src/txlog-api/domain/record/mock/mock_repository.go AS LOCAL ./txlog-api/domain/record/mock/mock_repository.go

    # directory-ui
    SAVE ARTIFACT /src/directory-ui/adapters/directory/mock/mock_repository.go AS LOCAL ./directory-ui/adapters/directory/mock/mock_repository.go


sqlc-txlog-api:
    FROM +deps
    COPY ./txlog-api/adapters/storage/postgres/queries /src/txlog-api/adapters/storage/postgres/queries
    COPY ./txlog-api/adapters/storage/postgres/migrations/sql /src/txlog-api/adapters/storage/postgres/migrations/sql

    WORKDIR /src/txlog-api/adapters/storage/postgres/queries

    RUN sqlc generate

    RUN goimports -w -local "go.nlx.io" /src/

    SAVE ARTIFACT /src/txlog-api/adapters/storage/postgres/queries/* AS LOCAL ./txlog-api/adapters/storage/postgres/queries/

sqlc-manager:
    FROM +deps
    COPY ./manager/adapters/storage/postgres/queries /src/manager/adapters/storage/postgres/queries
    COPY ./manager/adapters/storage/postgres/migrations/sql /src/manager/adapters/storage/postgres/migrations/sql

    WORKDIR /src/manager/adapters/storage/postgres/queries

    RUN sqlc generate

    RUN goimports -w -local "go.nlx.io" /src/

    SAVE ARTIFACT /src/manager/adapters/storage/postgres/queries/* AS LOCAL ./manager/adapters/storage/postgres/queries/

sqlc-controller:
    FROM +deps
    COPY ./controller/adapters/storage/postgres/queries /src/controller/adapters/storage/postgres/queries
    COPY ./controller/adapters/storage/postgres/migrations/sql /src/controller/adapters/storage/postgres/migrations/sql

    WORKDIR /src/controller/adapters/storage/postgres/queries

    RUN sqlc generate

    RUN goimports -w -local "go.nlx.io" /src/

    SAVE ARTIFACT /src/controller/adapters/storage/postgres/queries/* AS LOCAL ./controller/adapters/storage/postgres/queries/

helm-generate-docs:
    COPY ./helm/charts /src/helm/charts

    WORKDIR /src/helm/charts

    RUN readme-generator -v fsc-apps-overview/values.yaml -r fsc-apps-overview/README.md -s fsc-apps-overview/values.schema.json
    RUN readme-generator -v fsc-ca-certportal/values.yaml -r fsc-ca-certportal/README.md -s fsc-ca-certportal/values.schema.json
    RUN readme-generator -v fsc-ca-cfssl-unsafe/values.yaml -r fsc-ca-cfssl-unsafe/README.md -s fsc-ca-cfssl-unsafe/values.schema.json
    RUN readme-generator -v fsc-nlx-controller/values.yaml -r fsc-nlx-controller/README.md -s fsc-nlx-controller/values.schema.json
    RUN readme-generator -v fsc-nlx-directory-ui/values.yaml -r fsc-nlx-directory-ui/README.md -s fsc-nlx-directory-ui/values.schema.json
    RUN readme-generator -v fsc-nlx-docs/values.yaml -r fsc-nlx-docs/README.md -s fsc-nlx-docs/values.schema.json
    RUN readme-generator -v fsc-nlx-inway/values.yaml -r fsc-nlx-inway/README.md -s fsc-nlx-inway/values.schema.json
    RUN readme-generator -v fsc-nlx-manager/values.yaml -r fsc-nlx-manager/README.md -s fsc-nlx-manager/values.schema.json
    RUN readme-generator -v fsc-nlx-outway/values.yaml -r fsc-nlx-outway/README.md -s fsc-nlx-outway/values.schema.json
    RUN readme-generator -v fsc-nlx-txlog-api/values.yaml -r fsc-nlx-txlog-api/README.md -s fsc-nlx-txlog-api/values.schema.json

    SAVE ARTIFACT /src/helm/charts/* AS LOCAL ./helm/charts/
