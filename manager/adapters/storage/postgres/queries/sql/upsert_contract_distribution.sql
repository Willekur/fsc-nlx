-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL

-- name: UpsertContractDistribution :exec
INSERT INTO contracts.distributions (
    peer_id,
    content_hash,
    signature,
    action,
    next_retry_at,
    error_message,
    retries,
    last_retry_at
)
VALUES ($1, $2, $3, $4, $5, $6, 1, NOW())
ON CONFLICT (peer_id, content_hash, action) DO UPDATE
    SET retries = distributions.retries + 1,
        last_retry_at = NOW()
;
