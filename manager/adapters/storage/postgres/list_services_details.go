// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package postgresadapter

import (
	"context"

	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/storage/postgres/queries"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func (r *PostgreSQLRepository) ListServicesDetails(ctx context.Context, selfPeerID contract.PeerID, services []string) (map[string]*contract.ServiceDetails, error) {
	rows, err := r.queries.ListServicesDetails(ctx, &queries.ListServicesDetailsParams{
		SelfPeerID:   selfPeerID.Value(),
		ServiceNames: services,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not list services details")
	}

	serviceDetails := make(map[string]*contract.ServiceDetails)

	for _, r := range rows {
		serviceDetails[r] = &contract.ServiceDetails{
			Provider: &contract.Provider{
				PeerID: selfPeerID.Value(),
			},
		}
	}

	delegatedRows, err := r.queries.ListDelegatedServicesDetails(ctx, &queries.ListDelegatedServicesDetailsParams{
		SelfPeerID:   selfPeerID.Value(),
		ServiceNames: services,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not list delegated services details")
	}

	for _, r := range delegatedRows {
		serviceDetails[r.ServiceName] = &contract.ServiceDetails{
			Provider: &contract.DelegatedProvider{
				PeerID:          selfPeerID.Value(),
				DelegatorPeerID: r.DelegatorPeerID,
			},
		}
	}

	return serviceDetails, nil
}
