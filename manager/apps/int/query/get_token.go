// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"errors"
	"fmt"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/common/accesstoken"
	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/services"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type GetTokenHandler struct {
	repository                contract.Repository
	peersCommunicationService *services.PeersCommunicationService
	clk                       clock.Clock
}

type NewGetTokenHandlerArgs struct {
	Repository                contract.Repository
	PeersCommunicationService *services.PeersCommunicationService
	Clock                     clock.Clock
}

func NewGetTokenHandler(args *NewGetTokenHandlerArgs) (*GetTokenHandler, error) {
	if args.Repository == nil {
		return nil, errors.New("repository is required")
	}

	if args.PeersCommunicationService == nil {
		return nil, errors.New("peers communication service is required")
	}

	if args.Clock == nil {
		return nil, errors.New("clock is required")
	}

	return &GetTokenHandler{
		repository:                args.Repository,
		peersCommunicationService: args.PeersCommunicationService,
		clk:                       args.Clock,
	}, nil
}

type GetTokenHandlerArgs struct {
	GrantHash                   string
	OutwayCertificateThumbprint string
}

type Token struct {
	GroupID                     string
	GrantHash                   string
	OutwayPeerID                string
	OutwayCertificateThumbprint string
	OutwayDelegatorPeerID       string
	ServiceName                 string
	ServiceInwayAddress         string
	ServicePeerID               string
	ServiceDelegatorPeerID      string
	ExpiryDate                  time.Time
	Token                       string
}

func (h *GetTokenHandler) Handle(ctx context.Context, args *GetTokenHandlerArgs) (*Token, error) {
	gHash, err := contract.DecodeHashFromString(args.GrantHash)
	if err != nil {
		return nil, errors.Join(err, fmt.Errorf("could not decode grant hash in args"))
	}

	peer, err := h.repository.GetTokenProviderPeer(ctx, gHash)
	if err != nil {
		return nil, errors.Join(err, fmt.Errorf("could not get peer for getting the token from repository"))
	}

	token, err := h.peersCommunicationService.GetToken(ctx, peer, args.GrantHash)
	if err != nil {
		return nil, errors.Join(err, fmt.Errorf("could not get token from peer: %s for grant hash: %s and outway certificate thumbprint: %s", peer.Value(), args.GrantHash, args.OutwayCertificateThumbprint))
	}

	certificateThumbprint, err := accesstoken.GetCertificateThumbprintFromToken(token)
	if err != nil {
		return nil, errors.Join(err, fmt.Errorf("could not get certificate thumbprint from token"))
	}

	cert, err := h.peersCommunicationService.GetCertificate(ctx, peer, certificateThumbprint)
	if err != nil {
		return nil, errors.Join(err, fmt.Errorf("could not get peer certificate for validating token"))
	}

	// also the service peer id should be validated against the connection
	decodedToken, err := accesstoken.DecodeFromString(h.clk, cert, token)
	if err != nil {
		return nil, errors.Join(err, fmt.Errorf("unable to decode token"))
	}

	if args.OutwayCertificateThumbprint != decodedToken.OutwayCertificateThumbprint {
		return nil, fmt.Errorf("grant contains outway certificate thumbprint %s, not %s", decodedToken.OutwayCertificateThumbprint, args.OutwayCertificateThumbprint)
	}

	return &Token{
		GroupID:                     decodedToken.GroupID,
		GrantHash:                   args.GrantHash,
		OutwayPeerID:                decodedToken.OutwayPeerID,
		OutwayCertificateThumbprint: decodedToken.OutwayCertificateThumbprint,
		OutwayDelegatorPeerID:       decodedToken.OutwayDelegatorPeerID,
		ServiceName:                 decodedToken.ServiceName,
		ServiceInwayAddress:         decodedToken.ServiceInwayAddress,
		ServicePeerID:               decodedToken.ServicePeerID,
		ServiceDelegatorPeerID:      decodedToken.ServiceDelegatorPeerID,
		ExpiryDate:                  decodedToken.ExpiryDate,
		Token:                       token,
	}, nil
}
