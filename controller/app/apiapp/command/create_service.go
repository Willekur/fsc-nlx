// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package command

import (
	"context"
	"errors"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type CreateServiceHandler struct {
	groupID contract.GroupID
	lgr     *logger.Logger
	s       storage.Storage
	auth    *auth.Auth
}

var ErrServiceNameInUse = errors.New("service name is already in use")

func NewCreateServiceHandler(groupID contract.GroupID, s storage.Storage, a *auth.Auth, l *logger.Logger) (*CreateServiceHandler, error) {
	if groupID == "" {
		return nil, fmt.Errorf("group id is required")
	}

	if l == nil {
		return nil, fmt.Errorf("logger is required")
	}

	if s == nil {
		return nil, fmt.Errorf("storage is required")
	}

	if a == nil {
		return nil, fmt.Errorf("auth is required")
	}

	return &CreateServiceHandler{
		groupID: groupID,
		lgr:     l,
		s:       s,
		auth:    a,
	}, nil
}

type CreateServiceArgs struct {
	AuthData              authentication.Data
	AuthorizationMetadata authorization.Metadata
	Name                  string
	EndpointURL           string
	InwayAddress          string
}

func (h *CreateServiceHandler) Handle(ctx context.Context, args *CreateServiceArgs) error {
	err := h.auth.Auth(ctx, args.AuthData, args.AuthorizationMetadata, authz.FscCreateService, []*authz.ResourceURN{
		authz.NewResourceURN(authz.ResourceTypeService, args.Name),
	})
	if err != nil {
		return mapError(err, "could not authenticate or authorize")
	}

	if errArgs := args.Valid(); errArgs != nil {
		return errArgs
	}

	err = h.s.CreateService(ctx, &storage.CreateServiceArgs{
		GroupID:      h.groupID.String(),
		Name:         args.Name,
		EndpointURL:  args.EndpointURL,
		InwayAddress: args.InwayAddress,
	})
	if err != nil {
		h.lgr.Error("failed to create service", err)

		if errors.Is(err, storage.ErrServiceNameInUse) {
			return ErrServiceNameInUse
		}

		return mapError(err, "could not create service in storage")
	}

	return nil
}

func (c *CreateServiceArgs) Valid() *ValidationError {
	validationErrors := make([]string, 0)

	if c.Name == "" {
		validationErrors = append(validationErrors, "service name cannot be blank")
	}

	if !serviceNameValidator.MatchString(c.Name) {
		validationErrors = append(validationErrors, "service name must contain only letters, numbers, -, _ and .")
	}

	if !serviceEndpointURLValidator.MatchString(c.EndpointURL) {
		validationErrors = append(validationErrors, "endpoint URL cannot be blank, must start with https:// or http://, cannot contain spaces and cannot contain a trailing slash")
	}

	if c.InwayAddress == "" {
		validationErrors = append(validationErrors, "inway address cannot be blank")
	}

	if !inwayAddressValidator.MatchString(c.InwayAddress) {
		validationErrors = append(validationErrors, "inway address must start with https:// and end with :443")
	}

	return mapValidationError(validationErrors)
}
