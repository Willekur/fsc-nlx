{
    "title": "Chart Values",
    "type": "object",
    "properties": {
        "global": {
            "type": "object",
            "properties": {
                "imageRegistry": {
                    "type": "string",
                    "description": "Global Docker Image registry",
                    "default": ""
                },
                "imageTag": {
                    "type": "string",
                    "description": "Global Docker Image tag",
                    "default": ""
                },
                "groupID": {
                    "type": "string",
                    "description": "Global FSC Group ID",
                    "default": ""
                },
                "certificates": {
                    "type": "object",
                    "properties": {
                        "group": {
                            "type": "object",
                            "properties": {
                                "caCertificatePEM": {
                                    "type": "string",
                                    "description": "Global FSC NLX CA root certificate. If not set the value of 'tls.organization.rootCertificatePEM' is used",
                                    "default": ""
                                }
                            }
                        },
                        "internal": {
                            "type": "object",
                            "properties": {
                                "caCertificatePEM": {
                                    "type": "string",
                                    "description": "Global root certificate of your internal PKI. If not set the value of 'tls.internal.rootCertificatePEM' is used",
                                    "default": ""
                                }
                            }
                        }
                    }
                }
            }
        },
        "image": {
            "type": "object",
            "properties": {
                "registry": {
                    "type": "string",
                    "description": "Image registry (ignored if 'global.imageRegistry' is set)",
                    "default": "docker.io"
                },
                "repository": {
                    "type": "string",
                    "description": "Image repository of the controller API.",
                    "default": "nlxio/fsc-controller"
                },
                "tag": {
                    "type": "string",
                    "description": "Image tag (ignored if 'global.imageTag' is set). When set to null, the AppVersion from the chart is used",
                    "default": ""
                },
                "pullPolicy": {
                    "type": "string",
                    "description": "Image pull policy",
                    "default": "Always"
                },
                "pullSecrets": {
                    "type": "array",
                    "description": "Secrets for the image repository",
                    "default": [],
                    "items": {}
                }
            }
        },
        "replicaCount": {
            "type": "number",
            "description": "Number of controller replicas",
            "default": 1
        },
        "serviceAccount": {
            "type": "object",
            "properties": {
                "create": {
                    "type": "boolean",
                    "description": "Specifies whether a service account should be created",
                    "default": true
                },
                "annotations": {
                    "type": "object",
                    "description": "Annotations to add to the service account",
                    "default": {}
                },
                "name": {
                    "type": "string",
                    "description": "The name of the service account to use. If not set and create is true, a name is generated using the fullname template",
                    "default": ""
                }
            }
        },
        "resources": {
            "type": "object",
            "description": "Pod resource requests & limits",
            "default": {}
        },
        "nodeSelector": {
            "type": "object",
            "description": "Node labels for pod assignment",
            "default": {}
        },
        "affinity": {
            "type": "object",
            "description": "Node affinity for pod assignment",
            "default": {}
        },
        "tolerations": {
            "type": "array",
            "description": "Node tolerations for pod assignment",
            "default": [],
            "items": {}
        },
        "nameOverride": {
            "type": "string",
            "description": "Override deployment name",
            "default": ""
        },
        "fullnameOverride": {
            "type": "string",
            "description": "Override full deployment name",
            "default": ""
        },
        "config": {
            "type": "object",
            "properties": {
                "logType": {
                    "type": "string",
                    "description": "Possible values 'live', 'local'. Affects the log output. See NewProduction and NewDevelopment at https://godoc.org/go.uber.org/zap#Logger",
                    "default": "live"
                },
                "logLevel": {
                    "type": "string",
                    "description": "Possible values 'debug', 'warn', 'info'. Override the default logLevel set by 'config.logType'",
                    "default": "debug"
                },
                "groupID": {
                    "type": "string",
                    "description": "The ID of the Group",
                    "default": ""
                },
                "managerInternalAddress": {
                    "type": "string",
                    "description": "Internal address of the Manager",
                    "default": ""
                },
                "authn": {
                    "type": "object",
                    "properties": {
                        "type": {
                            "type": "string",
                            "description": "Type of authentication mechanism: none or oidc",
                            "default": "none"
                        },
                        "oidc": {
                            "type": "object",
                            "properties": {
                                "secretKey": {
                                    "type": "string",
                                    "description": "Secret key that is used for signing sessions",
                                    "default": ""
                                },
                                "clientId": {
                                    "type": "string",
                                    "description": "the OIDC client ID",
                                    "default": ""
                                },
                                "clientSecret": {
                                    "type": "string",
                                    "description": "the OIDC client secret",
                                    "default": ""
                                },
                                "discoveryUrl": {
                                    "type": "string",
                                    "description": "the OIDC discovery URL",
                                    "default": ""
                                },
                                "redirectUrl": {
                                    "type": "string",
                                    "description": "the OIDC redirect URL",
                                    "default": ""
                                },
                                "sessionCookieSecure": {
                                    "type": "boolean",
                                    "description": "use 'secure' cookies",
                                    "default": true
                                }
                            }
                        }
                    }
                },
                "authz": {
                    "type": "object",
                    "properties": {
                        "type": {
                            "type": "string",
                            "description": "Type of authorization mechanism: rbace or rest",
                            "default": "rbac"
                        },
                        "rest": {
                            "type": "object",
                            "properties": {
                                "address": {
                                    "type": "string",
                                    "description": "Address of the REST authorization server authorizing requests",
                                    "default": ""
                                }
                            }
                        }
                    }
                }
            }
        },
        "certificates": {
            "type": "object",
            "properties": {
                "internal": {
                    "type": "object",
                    "properties": {
                        "caCertificatePEM": {
                            "type": "string",
                            "description": "The CA root certificate of your internal PKI",
                            "default": ""
                        },
                        "certificatePEM": {
                            "type": "string",
                            "description": "The certificate signed by your internal PKI",
                            "default": ""
                        },
                        "keyPEM": {
                            "type": "string",
                            "description": "the private key of 'certificates.internal.certificatePEM'",
                            "default": ""
                        },
                        "existingSecret": {
                            "type": "string",
                            "description": "Use of existing secret with your FSC NLX keypair ('certificates.internal.certificatePEM' and 'certificates.internal.keyPEM'. will be ingored and picked up from this secret)",
                            "default": ""
                        }
                    }
                }
            }
        },
        "securityContext": {
            "type": "object",
            "properties": {
                "runAsNonRoot": {
                    "type": "boolean",
                    "description": "Run container as a non-root user",
                    "default": true
                },
                "runAsUser": {
                    "type": "number",
                    "description": "Run container as specified user",
                    "default": 1001
                },
                "capabilities": {
                    "type": "object",
                    "properties": {
                        "drop": {
                            "type": "array",
                            "description": "Drop all capabilities by default",
                            "default": [
                                "ALL"
                            ],
                            "items": {
                                "type": "string"
                            }
                        }
                    }
                }
            }
        },
        "podSecurityContext": {
            "type": "object",
            "properties": {
                "fsGroup": {
                    "type": "number",
                    "description": "GroupID under which the pod should be started",
                    "default": 1001
                }
            }
        },
        "service": {
            "type": "object",
            "properties": {
                "type": {
                    "type": "string",
                    "description": "Service Type (ClusterIP, NodePort, LoadBalancer)",
                    "default": "ClusterIP"
                },
                "portUi": {
                    "type": "number",
                    "description": "Port exposed by the controller UI service",
                    "default": 80
                },
                "portInternal": {
                    "type": "number",
                    "description": "Port exposed for the internal communication",
                    "default": 443
                },
                "portApi": {
                    "type": "number",
                    "description": "Port exposed by the API service",
                    "default": 81
                }
            }
        },
        "ingress": {
            "type": "object",
            "properties": {
                "enabled": {
                    "type": "boolean",
                    "description": "Enable ingress",
                    "default": false
                },
                "class": {
                    "type": "string",
                    "description": "Ingress class",
                    "default": ""
                },
                "annotations": {
                    "type": "object",
                    "description": "Ingress annotations",
                    "default": {}
                },
                "hosts": {
                    "type": "array",
                    "description": "Ingress accepted hostnames",
                    "default": [],
                    "items": {}
                },
                "tls": {
                    "type": "array",
                    "description": "Ingress TLS configuration",
                    "default": [],
                    "items": {}
                }
            }
        },
        "postgresql": {
            "type": "object",
            "properties": {
                "hostname": {
                    "type": "string",
                    "description": "PostgreSQL hostname",
                    "default": "postgresql"
                },
                "port": {
                    "type": "number",
                    "description": "PostgreSQL port",
                    "default": 5432
                },
                "database": {
                    "type": "string",
                    "description": "PostgreSQL database",
                    "default": "nlx_controller"
                },
                "username": {
                    "type": "string",
                    "description": "PostgreSQL username. Will be stored in a Kubernetes secret",
                    "default": ""
                },
                "password": {
                    "type": "string",
                    "description": "PostgreSQL password. Will be stored in a Kubernetes secret",
                    "default": ""
                },
                "sslMode": {
                    "type": "string",
                    "description": "PostgreSQL SSL mode",
                    "default": "require"
                },
                "connectTimeout": {
                    "type": "number",
                    "description": "The connection timeout for PostgreSQL",
                    "default": 10
                },
                "existingSecret": {
                    "type": "object",
                    "properties": {
                        "name": {
                            "type": "string",
                            "description": "Use existing secret for password details ('postgresql.username' and 'postgresql.password' will be ignored and picked up from this secret",
                            "default": ""
                        },
                        "usernameKey": {
                            "type": "string",
                            "description": "Key for username value in aforementioned existingSecret",
                            "default": "username"
                        },
                        "passwordKey": {
                            "type": "string",
                            "description": "Key for password value in aforementioned existingSecret",
                            "default": "password"
                        }
                    }
                }
            }
        }
    }
}