/*
 * Copyright © VNG Realisatie 2023
 * Licensed under the EUPL
 *
 */

package inway_test

import (
	"context"
	"crypto/x509"
	"log"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"path/filepath"
	"testing"
	"time"

	"go.uber.org/zap"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/common/transactionlog"
	"gitlab.com/commonground/nlx/fsc-nlx/inway"
	"gitlab.com/commonground/nlx/fsc-nlx/inway/domain/config"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

var (
	orgACertBundle *tls.CertificateBundle
	orgBCertBundle *tls.CertificateBundle
	orgCCertBundle *tls.CertificateBundle
	orgOnCRL       *tls.CertificateBundle
	crlFile        *x509.RevocationList
	testClock      = clock.NewMock(time.Date(2023, 11, 6, 14, 10, 5, 0, time.UTC))
)

const mockService = "mock-service"

func TestMain(m *testing.M) {
	var err error

	orgACertBundle, err = testingutils.GetCertificateBundle(filepath.Join("..", "testing", "pki"), testingutils.NLXTestPeerA) //probably need another name since access token is
	if err != nil {
		log.Fatal(err)
	}

	orgBCertBundle, err = testingutils.GetCertificateBundle(filepath.Join("..", "testing", "pki"), testingutils.NLXTestPeerB)
	if err != nil {
		log.Fatal(err)
	}

	orgCCertBundle, err = testingutils.GetCertificateBundle(filepath.Join("..", "testing", "pki"), testingutils.NLXTestPeerC)
	if err != nil {
		log.Fatal(err)
	}

	orgOnCRL, err = testingutils.GetCertificateBundle(filepath.Join("..", "testing", "pki"), testingutils.OrgOnCRL)
	if err != nil {
		log.Fatal(err)
	}

	crlFileDer, err := os.ReadFile(filepath.Join("..", "testing", "pki", "ca.crl"))
	if err != nil {
		log.Fatal(err)
	}

	crlFile, err = x509.ParseRevocationList(crlFileDer)
	if err != nil {
		log.Fatal(err)
	}

	m.Run()
}

func newInway(configRepository config.Repository, transactionLogger transactionlog.TransactionLogger) *httptest.Server {
	inwayURL, _ := url.Parse("https://localhost")

	if transactionLogger == nil {
		transactionLogger = newFakeTransactionLogger()
	}

	if configRepository == nil {
		configRepository = newFakeConfigRepository("")
	}

	crlCache, err := tls.NewCRL(nil)
	if err != nil {
		log.Fatal("cannot create CRLsCache Cache")
	}

	err = crlCache.AddRevocationList("localhost", crlFile)
	if err != nil {
		log.Fatal("cannot add CRL to cache")
	}

	testInway, err := inway.NewInway(&inway.Params{
		Context:                      context.Background(),
		Logger:                       zap.NewNop(),
		Txlogger:                     transactionLogger,
		Name:                         "testInway",
		GroupID:                      "fsc-local",
		Address:                      inwayURL,
		OrgCertBundle:                orgACertBundle,
		Controller:                   nil,
		AuthServiceURL:               "",
		AuthCAPath:                   "",
		ConfigRepository:             configRepository,
		ServiceEndpointCacheDuration: 0,
		ServiceProxyCacheSize:        100,
		Clock:                        testClock,
		CRL:                          crlCache,
	})
	if err != nil {
		log.Fatal(err, "could not create new Inway from parameters")
	}

	srv := httptest.NewUnstartedServer(testInway.Handler())
	srv.TLS = orgACertBundle.TLSConfig(orgACertBundle.WithTLSClientAuth())
	srv.StartTLS()

	return srv
}

func createInwayAPIClient(certBundle *tls.CertificateBundle) *http.Client {
	transport := &http.Transport{
		TLSClientConfig: certBundle.TLSConfig(certBundle.WithTLSClientAuth()),
	}

	return &http.Client{
		Transport: transport,
	}
}
