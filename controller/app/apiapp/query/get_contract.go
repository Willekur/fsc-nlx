// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

type GetContractHandler struct {
	manager manager.Manager
	auth    *auth.Auth
}

type Contract struct {
	IV                                string
	Hash                              string
	HashAlgorithm                     HashAlg
	GroupID                           string
	CreatedAt                         time.Time
	ValidFrom                         time.Time
	ValidUntil                        time.Time
	Peers                             map[string]*Peer
	AcceptSignatures                  map[string]Signature
	RejectSignatures                  map[string]Signature
	RevokeSignatures                  map[string]Signature
	HasRejected                       bool
	HasAccepted                       bool
	HasRevoked                        bool
	ServicePublicationGrants          []*ServicePublicationGrant
	ServiceConnectionGrants           []*ServiceConnectionGrant
	DelegatedServicePublicationGrants []*DelegatedServicePublicationGrant
	DelegatedServiceConnectionGrants  []*DelegatedServiceConnectionGrant
	State                             ContractState
}

type HashAlg int32

const (
	HashAlgUnspecified HashAlg = iota
	HashAlgSHA3_512
)

type ContractState string

const (
	ContractStateUnspecified ContractState = "CONTRACT_STATE_UNSPECIFIED"
	ContractStateExpired     ContractState = "CONTRACT_STATE_EXPIRED"
	ContractStateProposed    ContractState = "CONTRACT_STATE_PROPOSED"
	ContractStateRejected    ContractState = "CONTRACT_STATE_REJECTED"
	ContractStateRevoked     ContractState = "CONTRACT_STATE_REVOKED"
	ContractStateValid       ContractState = "CONTRACT_STATE_VALID"
)

type Peer struct {
	ID   string
	Name string
}

type ServicePublicationGrant struct {
	Hash          string
	DirectoryPeer *Peer
	ServicePeer   *Peer
	ServiceName   string
}

type ServiceConnectionGrant struct {
	Hash                        string
	ServicePeer                 *Peer
	ServiceName                 string
	OutwayPeer                  *Peer
	OutwayCertificateThumbprint string
}

type DelegatedServiceConnectionGrant struct {
	Hash                        string
	DelegatorPeer               *Peer
	OutwayPeer                  *Peer
	OutwayCertificateThumbprint string
	ServicePeer                 *Peer
	ServiceName                 string
}

type DelegatedServicePublicationGrant struct {
	Hash          string
	DirectoryPeer *Peer
	DelegatorPeer *Peer
	ServicePeer   *Peer
	ServiceName   string
}

type Signature struct {
	SignedAt time.Time
}

type ListContractFilter struct {
	ContentHash string
}

func NewGetContractHandler(m manager.Manager, a *auth.Auth) (*GetContractHandler, error) {
	if m == nil {
		return nil, fmt.Errorf("manager is required")
	}

	if a == nil {
		return nil, fmt.Errorf("auth is required")
	}

	return &GetContractHandler{
		manager: m,
		auth:    a,
	}, nil
}

type GetContractArgs struct {
	AuthData              authentication.Data
	AuthorizationMetadata authorization.Metadata
	ContentHash           string
}

func (h *GetContractHandler) Handle(ctx context.Context, args *GetContractArgs) (*Contract, error) {
	err := h.auth.Auth(ctx, args.AuthData, args.AuthorizationMetadata, authz.FscGetContract, []*authz.ResourceURN{
		authz.NewResourceURN(authz.ResourceTypeContract, args.ContentHash),
	})
	if err != nil {
		return nil, mapError(err, "could not authenticate or authorize")
	}

	contracts, err := h.manager.ListContracts(ctx, []string{args.ContentHash}, []manager.GrantType{})
	if err != nil {
		return nil, fmt.Errorf("%s: %w", newInternalError("could not get contracts from manager"), err)
	}

	if len(contracts) != 1 {
		return nil, newInternalError("contract not returned by manager")
	}

	c := contracts[0]

	contract := convertContract(c)

	return contract, nil
}

func convertContract(contract *manager.Contract) *Contract {
	servicePublicationGrants := make([]*ServicePublicationGrant, len(contract.ServicePublicationGrants))

	for i, grant := range contract.ServicePublicationGrants {
		directoryPeer := getPeerFromMap(contract.Peers, grant.DirectoryPeerID)
		servicePeer := getPeerFromMap(contract.Peers, grant.ServicePeerID)

		servicePublicationGrants[i] = &ServicePublicationGrant{
			Hash: grant.Hash,
			DirectoryPeer: &Peer{
				ID:   directoryPeer.ID,
				Name: directoryPeer.Name,
			},
			ServicePeer: &Peer{
				ID:   servicePeer.ID,
				Name: servicePeer.Name,
			},
			ServiceName: grant.ServiceName,
		}
	}

	serviceConnectionGrants := make([]*ServiceConnectionGrant, len(contract.ServiceConnectionGrants))

	for i, grant := range contract.ServiceConnectionGrants {
		servicePeer := getPeerFromMap(contract.Peers, grant.ServicePeerID)
		outwayPeer := getPeerFromMap(contract.Peers, grant.OutwayPeerID)

		serviceConnectionGrants[i] = &ServiceConnectionGrant{
			Hash: grant.Hash,
			ServicePeer: &Peer{
				ID:   servicePeer.ID,
				Name: servicePeer.Name,
			},
			ServiceName: grant.ServiceName,
			OutwayPeer: &Peer{
				ID:   outwayPeer.ID,
				Name: outwayPeer.Name,
			},
			OutwayCertificateThumbprint: grant.OutwayCertificateThumbprint,
		}
	}

	delegatedServiceConnectionGrants := make([]*DelegatedServiceConnectionGrant, len(contract.DelegatedServiceConnectionGrants))

	for i, grant := range contract.DelegatedServiceConnectionGrants {
		delegatorPeer := getPeerFromMap(contract.Peers, grant.DelegatorPeerID)
		outwayPeer := getPeerFromMap(contract.Peers, grant.OutwayPeerID)
		servicePeer := getPeerFromMap(contract.Peers, grant.ServicePeerID)

		delegatedServiceConnectionGrants[i] = &DelegatedServiceConnectionGrant{
			Hash: grant.Hash,
			DelegatorPeer: &Peer{
				ID:   delegatorPeer.ID,
				Name: delegatorPeer.Name,
			},
			OutwayPeer: &Peer{
				ID:   outwayPeer.ID,
				Name: outwayPeer.Name,
			},
			OutwayCertificateThumbprint: grant.OutwayCertificateThumbprint,
			ServicePeer: &Peer{
				ID:   servicePeer.ID,
				Name: servicePeer.Name,
			},
			ServiceName: grant.ServiceName,
		}
	}

	delegatedServicePublicationGrants := make([]*DelegatedServicePublicationGrant, len(contract.DelegatedServicePublicationGrants))

	for i, grant := range contract.DelegatedServicePublicationGrants {
		directoryPeer := getPeerFromMap(contract.Peers, grant.DirectoryPeerID)
		delegatorPeer := getPeerFromMap(contract.Peers, grant.DelegatorPeerID)
		servicePeer := getPeerFromMap(contract.Peers, grant.ServicePeerID)

		delegatedServicePublicationGrants[i] = &DelegatedServicePublicationGrant{
			Hash: grant.Hash,
			DirectoryPeer: &Peer{
				ID:   directoryPeer.ID,
				Name: directoryPeer.Name,
			},
			DelegatorPeer: &Peer{
				ID:   delegatorPeer.ID,
				Name: delegatorPeer.Name,
			},
			ServicePeer: &Peer{
				ID:   servicePeer.ID,
				Name: servicePeer.Name,
			},
			ServiceName: grant.ServiceName,
		}
	}

	convertedContract := &Contract{
		IV:                                contract.IV,
		Hash:                              contract.Hash,
		HashAlgorithm:                     HashAlg(contract.HashAlgorithm),
		GroupID:                           contract.GroupID,
		CreatedAt:                         contract.CreatedAt,
		ValidFrom:                         contract.ValidFrom,
		ValidUntil:                        contract.ValidUntil,
		Peers:                             make(map[string]*Peer),
		AcceptSignatures:                  convertSignatures(contract.AcceptSignatures),
		RejectSignatures:                  convertSignatures(contract.RejectSignatures),
		RevokeSignatures:                  convertSignatures(contract.RevokeSignatures),
		HasRejected:                       contract.HasRejected,
		HasAccepted:                       contract.HasAccepted,
		HasRevoked:                        contract.HasRevoked,
		ServicePublicationGrants:          servicePublicationGrants,
		ServiceConnectionGrants:           serviceConnectionGrants,
		DelegatedServicePublicationGrants: delegatedServicePublicationGrants,
		DelegatedServiceConnectionGrants:  delegatedServiceConnectionGrants,
		State:                             ContractState(contract.State),
	}

	for peerID, p := range contract.Peers {
		peer := &Peer{
			ID:   peerID,
			Name: p.Name,
		}
		convertedContract.Peers[peerID] = peer
	}

	return convertedContract
}

// getPeerFromMap returns the peer with the given ID from the given map of peers. If no peer is found, a Peer struct with only the ID set is returned.
// The Peer struct with only the ID set is needed because there is no guarantee that the Peer is present in the map.
func getPeerFromMap(peers map[string]*manager.Peer, peerID string) *Peer {
	peer := peers[peerID]

	if peer == nil {
		return &Peer{
			ID:   peerID,
			Name: "",
		}
	}

	return &Peer{
		ID:   peer.ID,
		Name: peer.Name,
	}
}

func convertSignatures(signatures map[string]manager.Signature) map[string]Signature {
	convertedSignatures := make(map[string]Signature)
	for peerID, signature := range signatures {
		convertedSignatures[peerID] = Signature{SignedAt: signature.SignedAt}
	}

	return convertedSignatures
}
