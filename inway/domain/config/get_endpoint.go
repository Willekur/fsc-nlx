// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package config

import (
	"context"
	"fmt"
	"net/url"
)

func (c *Config) GetEndpointURL(ctx context.Context, serviceName string) (*url.URL, error) {
	if serviceName == "" {
		return nil, fmt.Errorf("serviceName cannot be empty")
	}

	c.services.mut.RLock()
	svc, ok := c.services.services[serviceName]
	c.services.mut.RUnlock()

	if ok && svc.expiresAt.After(c.clock.Now()) {
		return svc.endpointURL, nil
	}

	// get endpoint url from repository via singleflight
	// this will prevent multiple calls for the same service
	endpointURL, err, _ := c.services.getEndpointURL.Do(serviceName, func() (interface{}, error) {
		u, err := c.repo.GetServiceEndpointURL(ctx, serviceName)
		if err != nil {
			return nil, err
		}

		c.services.mut.Lock()
		c.services.services[serviceName] = &service{
			endpointURL: u,
			expiresAt:   c.clock.Now().Add(c.services.cacheDuration),
		}
		c.services.mut.Unlock()

		return u, nil
	})
	if err != nil {
		return nil, err
	}

	return endpointURL.(*url.URL), nil
}
