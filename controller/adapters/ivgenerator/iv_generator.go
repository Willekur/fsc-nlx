// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package ivgenerator

type IVGenerator interface {
	New() (string, error)
}
