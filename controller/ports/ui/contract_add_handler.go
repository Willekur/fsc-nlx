// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
	api_query "gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

const Day = 24 * time.Hour
const htmxReswapHeader = "HX-Reswap"

func (s *Server) addContractGetHandler(w http.ResponseWriter, _ *http.Request) {
	page := getDefaultPageSetup(s.basePage, &AddContractsPageForm{
		ValidFrom:  time.Now().Format("2006-01-02"),
		ValidUntil: time.Now().Add(Day).Format("2006-01-02"),
	})

	page.render(w)
}

func (s *Server) addContractPostHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	formValues, err := decodeAddContractPageForm(r)
	if err != nil {
		s.logger.Error("failed to read form values", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	page := getDefaultPageSetup(s.basePage, formValues)

	peerInfo := s.getPeerInfo(ctx, getAuthzData(r), authData)

	outways, err := s.apiApp.Queries.ListOutways.Handle(ctx, &api_query.ListOutwaysArgs{
		AuthData:              authData,
		AuthorizationMetadata: getAuthzData(r),
		SelfPeerID:            peerInfo.ID,
	})
	if err != nil {
		s.logger.Error("failed to retrieve outways", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	owThumbprints := map[string][]string{}

	for _, outway := range outways {
		outwayMap, ok := owThumbprints[outway.CertificateThumbprint]
		if !ok {
			owThumbprints[outway.CertificateThumbprint] = []string{outway.Name}
		}

		_ = append(outwayMap, outway.Name)
	}

	page.OutwayCertificateThumbprints = owThumbprints

	page.PeerIDs = s.getPeerIDs(ctx, getAuthzData(r), authData)
	page.ServiceNames = s.getServiceNames(ctx, getAuthzData(r), authData)

	if formValues.Action == FormActionAddServicePublicationGrant {
		page.Form.ServicePublicationGrants = append(page.Form.ServicePublicationGrants, &AddContractsPageFormServicePublicationGrant{
			ServicePeerID: s.peerInfo.ID,
		})

		w.Header().Add(htmxReswapHeader, "outerHTML show:none")
		page.render(w)

		return
	}

	if formValues.Action == FormActionAddServiceConnectionGrant {
		page.Form.ServiceConnectionGrants = append(page.Form.ServiceConnectionGrants, &AddContractsPageFormServiceConnectionGrant{
			OutwayPeerID: s.peerInfo.ID,
		})

		w.Header().Add(htmxReswapHeader, "outerHTML show:none")
		page.render(w)

		return
	}

	if formValues.Action == FormActionAddDelegatedServiceConnectionGrant {
		page.Form.DelegatedServiceConnectionGrants = append(page.Form.DelegatedServiceConnectionGrants, &AddContractsPageFormDelegatedServiceConnectionGrant{})

		w.Header().Add(htmxReswapHeader, "outerHTML show:none")
		page.render(w)

		return
	}

	if formValues.Action == FormActionAddDelegatedServicePublicationGrant {
		page.Form.DelegatedServicePublicationGrants = append(page.Form.DelegatedServicePublicationGrants, &AddContractsPageFormDelegatedServicePublicationGrant{
			ServicePeerID: s.peerInfo.ID,
		})

		w.Header().Add(htmxReswapHeader, "outerHTML show:none")
		page.render(w)

		return
	}

	err = s.apiApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractArgs{
		AuthData:                          authData,
		AuthorizationMetadata:             getAuthzData(r),
		HashAlgorithm:                     formValues.HashAlgorithm,
		GroupID:                           formValues.GroupID,
		ContractNotBefore:                 formValues.ValidFrom,
		ContractNotAfter:                  formValues.ValidUntil,
		ServicePublicationGrants:          mapServicePublicationGrants(formValues.ServicePublicationGrants),
		ServiceConnectionGrants:           mapServiceConnectionGrants(formValues.ServiceConnectionGrants),
		DelegatedServiceConnectionGrants:  mapDelegatedServiceConnectionGrants(formValues.DelegatedServiceConnectionGrants),
		DelegatedServicePublicationGrants: mapDelegatedServicePublicationGrants(formValues.DelegatedServicePublicationGrants),
	})
	if err != nil {
		s.logger.Error("create contract", err)

		page.renderWithError(w, err)

		return
	}

	page.renderWithSuccess(w, s.i18n.Translate("The contract was added successfully."))
}

func (s *Server) getPeerIDs(ctx context.Context, authzData *authorization.RestMetaData, authData authentication.Data) map[string]string {
	peers, err := s.apiApp.Queries.ListPeers.Handle(ctx, &api_query.ListPeersArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
	})
	peerIDList := make(map[string]string)

	if err != nil {
		s.logger.Warn("could not retrieve PeerID list from manager, defaulting to empty list", err)

		return peerIDList
	}

	for _, peer := range peers {
		peerIDList[peer.ID] = peer.Name
	}

	return peerIDList
}

func (s *Server) getServiceNames(ctx context.Context, authzData *authorization.RestMetaData, authData authentication.Data) map[string]string {
	serviceList := make(map[string]string)
	services, err := s.apiApp.Queries.ListServices.Handle(ctx, &api_query.ListServicesArgs{
		AuthData:   authData,
		SelfPeerID: s.getPeerInfo(ctx, authzData, authData).ID,
	})

	if err != nil {
		s.logger.Warn("could not retrieve Service list from manager, defaulting to empty list", err)

		return serviceList
	}

	for _, service := range services {
		serviceList[service.Name] = service.EndpointURL.String()
	}

	return serviceList
}

func getDefaultPageSetup(basePage *BasePage, form *AddContractsPageForm) addContractsPage {
	return addContractsPage{
		BasePage: basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathContractsPage,
		},
		Groups: []string{basePage.GroupID},
		HashAlgorithms: HashAlgorithms{
			"SHA3-512",
		},
		OutwayCertificateThumbprints: map[string][]string{},
		Form:                         form,
	}
}

func mapServicePublicationGrants(input []*AddContractsPageFormServicePublicationGrant) []*command.ServicePublicationGrant {
	result := make([]*command.ServicePublicationGrant, len(input))

	for i, grant := range input {
		result[i] = &command.ServicePublicationGrant{
			DirectoryPeerID: grant.DirectoryPeerID,
			ServicePeerID:   grant.ServicePeerID,
			ServiceName:     grant.ServiceName,
		}
	}

	return result
}

func mapServiceConnectionGrants(input []*AddContractsPageFormServiceConnectionGrant) []*command.ServiceConnectionGrant {
	result := make([]*command.ServiceConnectionGrant, len(input))

	for i, grant := range input {
		result[i] = &command.ServiceConnectionGrant{
			ServicePeerID:               grant.ServicePeerID,
			ServiceName:                 grant.ServiceName,
			OutwayPeerID:                grant.OutwayPeerID,
			OutwayCertificateThumbprint: grant.OutwayCertificateThumbprint,
		}
	}

	return result
}

func mapDelegatedServiceConnectionGrants(input []*AddContractsPageFormDelegatedServiceConnectionGrant) []*command.DelegatedServiceConnectionGrant {
	result := make([]*command.DelegatedServiceConnectionGrant, len(input))

	for i, grant := range input {
		result[i] = &command.DelegatedServiceConnectionGrant{
			DelegatorPeerID:             grant.DelegatorPeerID,
			ServicePeerID:               grant.ServicePeerID,
			ServiceName:                 grant.ServiceName,
			OutwayPeerID:                grant.OutwayPeerID,
			OutwayCertificateThumbprint: grant.OutwayCertificateThumbprint,
		}
	}

	return result
}

func mapDelegatedServicePublicationGrants(input []*AddContractsPageFormDelegatedServicePublicationGrant) []*command.DelegatedServicePublicationGrant {
	result := make([]*command.DelegatedServicePublicationGrant, len(input))

	for i, grant := range input {
		result[i] = &command.DelegatedServicePublicationGrant{
			DirectoryPeerID: grant.DirectoryPeerID,
			DelegatorPeerID: grant.DelegatorPeerID,
			ServicePeerID:   grant.ServicePeerID,
			ServiceName:     grant.ServiceName,
		}
	}

	return result
}
