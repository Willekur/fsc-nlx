// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package resttxlog

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/txlog"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/record"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/ports/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/txlog-api/ports/rest/api/server"
)

type RestTXLog struct {
	client api.ClientWithResponsesInterface
}

func New(client api.ClientWithResponsesInterface) (*RestTXLog, error) {
	if client == nil {
		return nil, fmt.Errorf("tx log rest client is required")
	}

	return &RestTXLog{
		client: client,
	}, nil
}

func (g *RestTXLog) ListRecords(ctx context.Context, req *txlog.ListRecordsRequest) ([]*txlog.Record, error) {
	res, err := g.client.GetLogRecordsWithResponse(ctx, reqToRest(req))
	if err != nil {
		return nil, mapError(err, "could not get tx log records from rest txlog")
	}

	err = mapResponse(res, http.StatusOK, "could not get tx log records", res.Body)
	if err != nil {
		return nil, err
	}

	return restToRecords(res.JSON200.Records)
}

func restToRecords(res []models.LogRecord) ([]*txlog.Record, error) {
	records := make([]*txlog.Record, len(res))

	for i := range res {
		source, destination, err := mapSourceAndDestinationToModel(&res[i])
		if err != nil {
			return nil, fmt.Errorf("could not map source and destination: %w", err)
		}

		records[i] = &txlog.Record{
			TransactionID: res[i].TransactionId,
			GroupID:       res[i].GroupId,
			GrantHash:     res[i].GrantHash,
			ServiceName:   res[i].ServiceName,
			Direction:     mapDirection(res[i].Direction),
			Source:        source,
			Destination:   destination,
			CreatedAt:     time.Unix(res[i].CreatedAt, 0),
		}
	}

	return records, nil
}

func mapSourceAndDestinationToModel(r *models.LogRecord) (source, destination interface{}, err error) {
	sourceDiscriminator, err := r.Source.ValueByDiscriminator()
	if err != nil {
		return nil, nil, err
	}
	// nolint:dupl // not the same
	switch convertedSourceModel := sourceDiscriminator.(type) {
	case models.Source:
		source = &txlog.RecordSource{
			OutwayPeerID: convertedSourceModel.OutwayPeerId,
		}
	case models.SourceDelegated:
		source = &txlog.RecordDelegatedSource{
			OutwayPeerID:    convertedSourceModel.OutwayPeerId,
			DelegatorPeerID: convertedSourceModel.DelegatorPeerId,
		}
	}

	destinationDiscriminator, err := r.Destination.ValueByDiscriminator()
	if err != nil {
		return nil, nil, err
	}
	// nolint:dupl // not the same
	switch convertedDestinationModel := destinationDiscriminator.(type) {
	case models.Destination:
		destination = &txlog.RecordDestination{
			ServicePeerID: convertedDestinationModel.ServicePeerId,
		}
	case models.DestinationDelegated:
		destination = &txlog.RecordDelegatedDestination{
			ServicePeerID:   convertedDestinationModel.ServicePeerId,
			DelegatorPeerID: convertedDestinationModel.DelegatorPeerId,
		}
	}

	return source, destination, nil
}

func mapDirection(d models.LogRecordDirection) record.Direction {
	switch d {
	case models.DIRECTIONINCOMING:
		return record.DirectionIn
	case models.DIRECTIONOUTGOING:
		return record.DirectionOut
	default:
		return record.DirectionUnspecified
	}
}

func reqToRest(req *txlog.ListRecordsRequest) *models.GetLogRecordsParams {
	sortOrder := sortOrderToRest(req.Pagination.SortOrder)

	startID := req.Pagination.StartID.String()

	return &models.GetLogRecordsParams{
		GroupId:   req.GroupID,
		Cursor:    &startID,
		Limit:     &req.Pagination.Limit,
		SortOrder: &sortOrder,
	}
}

func sortOrderToRest(o txlog.SortOrder) models.SortOrder {
	switch o {
	case txlog.SortOrderAscending:
		return models.SORTORDERASCENDING
	case txlog.SortOrderDescending:
		return models.SORTORDERDESCENDING
	default:
		return models.SORTORDERASCENDING
	}
}
