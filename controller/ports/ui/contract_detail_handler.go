// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
)

func (s *Server) contractDetailHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	contentHash := chi.URLParam(r, "hash")

	contract, err := s.apiApp.Queries.GetContract.Handle(ctx, &query.GetContractArgs{
		AuthData:              authData,
		AuthorizationMetadata: getAuthzData(r),
		ContentHash:           contentHash,
	})
	if err != nil {
		s.logger.Error("could not get contract", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	contractDistributions, err := s.apiApp.Queries.ListContractDistributions.Handle(ctx, &query.ListContractDistributionsArgs{
		AuthData:              authData,
		AuthorizationMetadata: getAuthzData(r),
		ContentHash:           contentHash,
	})
	if err != nil {
		s.logger.Error("could not get contractDistributions", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	peers := getPeers(contract, contractDistributions)
	servicePublicationGrants := getServicePublicationGrants(contract.ServicePublicationGrants)
	serviceConnectionGrants := getServiceConnectionGrants(contract.ServiceConnectionGrants)
	delegatedServiceConnectionGrants := getDelegatedServiceConnectionGrants(contract.DelegatedServiceConnectionGrants)
	delegatedServicePublicationGrants := getDelegatedServicePublicationGrants(contract.DelegatedServicePublicationGrants)

	page := contractDetailPage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			Peer:                        s.getPeerInfo(ctx, getAuthzData(r), authData),
			PrimaryNavigationActivePath: PathContractsPage,
		},
		IV:                                contract.IV,
		State:                             convertStateToHumanReadableText(contract.State),
		Hash:                              contentHash,
		HashAlgorithm:                     hashAlgorithmString(contract.HashAlgorithm),
		GroupID:                           contract.GroupID,
		Organizations:                     peers,
		CreatedAt:                         contract.CreatedAt.UTC(),
		ValidFrom:                         contract.ValidFrom.UTC(),
		ValidUntil:                        contract.ValidUntil.UTC(),
		ServicePublicationGrants:          servicePublicationGrants,
		ServiceConnectionGrants:           serviceConnectionGrants,
		DelegatedServicePublicationGrants: delegatedServicePublicationGrants,
		DelegatedServiceConnectionGrants:  delegatedServiceConnectionGrants,
		DisableRejectButton:               contract.HasRejected || contract.HasAccepted,
		DisableAcceptButton:               contract.HasRejected || contract.HasAccepted || contract.HasRevoked,
		DisableRevokeButton:               contract.HasRevoked || !contract.HasAccepted,
		IsActive:                          contract.State == query.ContractStateValid,
	}

	err = page.render(w)
	if err != nil {
		s.logger.Error("could not render contract detail page", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}
}

func convertStateToHumanReadableText(input query.ContractState) string {
	switch input {
	case query.ContractStateRejected:
		return "Rejected"
	case query.ContractStateRevoked:
		return "Revoked"
	case query.ContractStateProposed:
		return "Proposed"
	case query.ContractStateValid:
		return "Valid"
	case query.ContractStateExpired:
		return "Expired"
	default:
		return fmt.Sprintf("unknown state %q", input)
	}
}

func hashAlgorithmString(hashAlg query.HashAlg) string {
	switch hashAlg {
	case query.HashAlgSHA3_512:
		return "SHA3-512"
	default:
		return "Unknown"
	}
}

func getPeers(contract *query.Contract, contractDistributions query.ContractDistributions) []*Peer {
	peerMaps := map[string]*Peer{}

	for peerID, peer := range contract.Peers {
		contractPeer := &Peer{
			Name:   peer.Name,
			PeerID: peer.ID,
		}

		signature, ok := contract.RejectSignatures[peerID]
		if ok {
			contractPeer.RejectSignatureSignedAt = signature.SignedAt.UTC().String()
		}

		signature, ok = contract.AcceptSignatures[peerID]
		if ok {
			contractPeer.AcceptSignatureSignedAt = signature.SignedAt.UTC().String()
		}

		signature, ok = contract.RevokeSignatures[peerID]
		if ok {
			contractPeer.RevokeSignatureSignedAt = signature.SignedAt.UTC().String()
		}

		contractDistributionPerAction, ok := contractDistributions[peerID]
		if ok {
			contractPeer.ContractDistribution = make(map[Action]*ContractDistribution)
			for action, contractDistribution := range contractDistributionPerAction {
				contractPeer.ContractDistribution[convertActionToHumanReadableText(action)] = &ContractDistribution{
					ContentHash: contractDistribution.ContentHash,
					Signature:   contractDistribution.Signature,
					Action:      convertActionToHumanReadableText(contractDistribution.Action),
				}
			}
		}

		peerMaps[peerID] = contractPeer
	}

	peers := make([]*Peer, 0)

	for _, peer := range peerMaps {
		peers = append(peers, peer)
	}

	return peers
}

func convertActionToHumanReadableText(input query.Action) Action {
	switch input {
	case query.SubmitContract:
		return SubmitContract
	case query.SubmitAcceptSignature:
		return SubmitAcceptSignature
	case query.SubmitRejectSignature:
		return SubmitRejectSignature
	case query.SubmitRevokeSignature:
		return SubmitRevokeSignatureAction
	}

	return ""
}

func getServicePublicationGrants(grants []*query.ServicePublicationGrant) []*ServicePublicationGrant {
	result := make([]*ServicePublicationGrant, len(grants))

	for i, grant := range grants {
		result[i] = &ServicePublicationGrant{
			Hash:              grant.Hash,
			DirectoryPeerID:   grant.DirectoryPeer.ID,
			DirectoryPeerName: grant.DirectoryPeer.Name,
			ServicePeerID:     grant.ServicePeer.ID,
			ServicePeerName:   grant.ServicePeer.Name,
			ServiceName:       grant.ServiceName,
		}
	}

	return result
}

func getServiceConnectionGrants(grants []*query.ServiceConnectionGrant) []*ServiceConnectionGrant {
	result := make([]*ServiceConnectionGrant, len(grants))

	for i, grant := range grants {
		result[i] = &ServiceConnectionGrant{
			Hash:                        grant.Hash,
			ServicePeerID:               grant.ServicePeer.ID,
			ServicePeerName:             grant.ServicePeer.Name,
			ServiceName:                 grant.ServiceName,
			OutwayPeerID:                grant.OutwayPeer.ID,
			OutwayPeerName:              grant.OutwayPeer.Name,
			OutwayCertificateThumbprint: grant.OutwayCertificateThumbprint,
		}
	}

	return result
}

func getDelegatedServiceConnectionGrants(grants []*query.DelegatedServiceConnectionGrant) []*DelegatedServiceConnectionGrant {
	result := make([]*DelegatedServiceConnectionGrant, len(grants))

	for i, grant := range grants {
		result[i] = &DelegatedServiceConnectionGrant{
			Hash:                        grant.Hash,
			DelegatorPeerID:             grant.DelegatorPeer.ID,
			DelegatorPeerName:           grant.DelegatorPeer.Name,
			OutwayPeerID:                grant.OutwayPeer.ID,
			OutwayPeerName:              grant.OutwayPeer.Name,
			OutwayCertificateThumbprint: grant.OutwayCertificateThumbprint,
			ServicePeerID:               grant.ServicePeer.ID,
			ServicePeerName:             grant.ServicePeer.Name,
			ServiceName:                 grant.ServiceName,
		}
	}

	return result
}

func getDelegatedServicePublicationGrants(grants []*query.DelegatedServicePublicationGrant) []*DelegatedServicePublicationGrant {
	result := make([]*DelegatedServicePublicationGrant, len(grants))

	for i, grant := range grants {
		result[i] = &DelegatedServicePublicationGrant{
			Hash:              grant.Hash,
			DirectoryPeerID:   grant.DirectoryPeer.ID,
			DirectoryPeerName: grant.DirectoryPeer.Name,
			DelegatorPeerID:   grant.DelegatorPeer.ID,
			DelegatorPeerName: grant.DelegatorPeer.Name,
			ServicePeerID:     grant.ServicePeer.ID,
			ServicePeerName:   grant.ServicePeer.Name,
			ServiceName:       grant.ServiceName,
		}
	}

	return result
}
