/**
 * Copyright © VNG Realisatie 2023
 * Licensed under the EUPL
 */

import "htmx.org";
import _hyperscript from "hyperscript.org/src/_hyperscript.js";

import * as dayjs from "dayjs";
import * as localizedFormat from "dayjs/plugin/localizedFormat";
import "dayjs/locale/en";
import "dayjs/locale/nl";

window._hyperscript = _hyperscript.browserInit();

dayjs.extend(localizedFormat);
dayjs.locale(window.config.locale);

document.querySelectorAll("time").forEach(($e) => {
  if ($e.dateTime == null || $e.dateTime == "") {
    return;
  }

  // See dayjs docs for possible formats
  let format = "L"; // date
  let titleFormat = "L LTS"; // date time

  elFormat = $e.dataset.format;
  elTitleFormat = $e.dataset.titleFormat;

  if (elFormat != null && elFormat != "") {
    format = elFormat;
  }

  if (elTitleFormat != null && elTitleFormat != "") {
    titleFormat = elTitleFormat;
  }

  datetime = dayjs($e.dateTime);

  $e.innerHTML = datetime.format(format);
  $e.title = datetime.format(titleFormat);
});
