// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//nolint:dupl // this is a test
package testsignature_test

import (
	"crypto"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	testsignature "gitlab.com/commonground/nlx/fsc-nlx/testing/signature"
)

func TestCreateAccept(t *testing.T) {
	type args struct {
		contentHash           string
		certificateThumbprint string
		privateKey            crypto.PrivateKey
		signedAt              time.Time
	}

	tests := map[string]struct {
		args    args
		want    string
		wantErr bool
	}{
		"happy_flow": {
			args: args{
				contentHash:           "my-content-hash",
				certificateThumbprint: getCertBundle(t).CertificateThumbprint(),
				privateKey:            getCertBundle(t).Cert().PrivateKey,
				signedAt:              time.Date(2023, 1, 2, 3, 4, 5, 6, time.UTC),
			},
			want:    "eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2Ijoia3R4MVlPdHh4ZlU4Ykl6NExoM3pIaXBrSDFueFpYLWI3WWJuUFNBQzNIRSJ9.eyJjb250ZW50SGFzaCI6Im15LWNvbnRlbnQtaGFzaCIsInR5cGUiOiJhY2NlcHQiLCJzaWduZWRBdCI6MTY3MjYyODY0NX0.IImU7ht_GsnZWb_fror_cF5RpRSp_uMiLizZ0AGaA4N34H_03SVQHbtN45ZfnKsul4giBZ-8k2uuAJ6QNufmNuMgB7KBlBhq-L2zsusU7nEQ9jZkVb-uh3ED4dqtiO7KtxhBAbwm5ePumKyGZo7EDpdKH58Y0vwn6WRyq8XRoAsKd5fLxT1Sp1DZRp300rbUe7HfcXIXVbTmIu2ncyYTFTf8H98gGPvnIzUTKZTlpwAZGAH5-5CHdyBS3nPbUfGPsyrL8lb-LlvwTj2sbRmSUCsIe8DrBnEvpP7Rp33SZNnLWA2Suyxu6HFkem7UFGYXxs4Zg83vbGRZqsOcUMeZqCh-7lyyLHK4HVdofZ4JaegpbiEIr0bDSPLtT8DS6Vg146w5Q5ElZY1nyfnCyo3V_Ar20fSomM34ccD6xaX2vl3XZ37P4nMbqw5UhI_YW7dPaZNujHU8CJua3xVkvxaAViaP8t0-14iJtzej54CI4AIwK5vKS9ZRgjDUJaqfxqtD-j4KmkiFVXUsd6wW92ve6gAP0b16d2Xmhq8CBJ1mU2AmQqRuhVBicadYgrkGITchbn9QPwYD7dOKf8rSgPWbLDI4dqKBlzeDxRG8wTUk6vAS1Dsx1RF7Eiy6amuB6DulH1T5sgvTZuDC3GFBTz7nh1dBNbBSBKKQWdTIYJsZwp4",
			wantErr: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := testsignature.CreateAccept(tt.args.contentHash, tt.args.certificateThumbprint, tt.args.privateKey, tt.args.signedAt)
			if (err != nil) != tt.wantErr {
				t.Errorf("CreateAccept() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			assert.Equal(t, tt.want, got)
		})
	}
}
