// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package services

import (
	"context"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func (p *PeersCommunicationService) Announce(ctx context.Context, peers contract.PeersIDs) error {
	return p.sendToPeers(peers, func(peerID contract.PeerID, managerClient manager.Manager) error {
		err := managerClient.Announce(ctx)
		if err != nil {
			return fmt.Errorf("could not announce to peer %q: %w", peerID.Value(), err)
		}

		return nil
	})
}
