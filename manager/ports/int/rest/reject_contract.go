// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"

	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
)

func (s *Server) RejectContract(ctx context.Context, req api.RejectContractRequestObject) (api.RejectContractResponseObject, error) {
	s.logger.Info("rest request RejectContract")

	err := s.app.Commands.RejectContract.Handle(ctx,
		req.Hash,
	)
	if err != nil {
		s.logger.Error("error executing reject contract command", err)
		return nil, err
	}

	return api.RejectContract204Response{}, nil
}
