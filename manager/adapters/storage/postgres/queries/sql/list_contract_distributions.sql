-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL

-- name: ListContractDistributions :many
SELECT
    peer_id,
    content_hash,
    signature,
    action,
    next_retry_at,
    error_message,
    retries,
    last_retry_at
FROM
    contracts.distributions
WHERE
    content_hash = $1;
