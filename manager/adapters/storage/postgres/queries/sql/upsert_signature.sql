-- Copyright © VNG Realisatie 2022
-- Licensed under the EUPL

-- name: UpsertSignature :exec
WITH upsert_peer AS (
    INSERT INTO peers.peers (id)
        VALUES ($1)
        ON CONFLICT DO NOTHING
),
upsert_cert AS (
    INSERT INTO peers.certificates (
        certificate_thumbprint,
        certificate,
        peer_id
    )
    VALUES ($2, $3, $1)
    ON CONFLICT DO NOTHING
)
INSERT INTO contracts.contract_signatures (
    content_hash,
    signature_type,
    signature,
    peer_id,
    certificate_thumbprint,
    signed_at
)
VALUES ($4, $5, $6, $1, $2, $7)
ON CONFLICT DO NOTHING;
