// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

// nolint:dupl // this is a test
package contract_test

import (
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func TestDelegatedServicePublicationHash(t *testing.T) {
	contractIV, err := uuid.Parse("26aad1d8-3653-4e6f-b3ba-70df67a9a0ab")
	assert.NoError(t, err)

	contractIVBytes, err := contractIV.MarshalBinary()
	assert.NoError(t, err)

	now := time.Date(2023, 1, 1, 1, 42, 0, 0, time.UTC)

	c, err := contract.NewContent(&contract.NewContentArgs{
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIVBytes,
		CreatedAt:     now,
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: now,
			NotAfter:  now,
		},
		Grants: []interface{}{
			&contract.NewGrantDelegatedServicePublicationArgs{
				Directory: &contract.NewGrantDelegatedServicePublicationDirectoryArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000001",
						Name:           "",
						ManagerAddress: "",
					},
				},
				Service: &contract.NewGrantDelegatedServicePublicationServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000002",
						Name:           "",
						ManagerAddress: "",
					},
					Name:     "parkeerrechten",
					Protocol: contract.ServiceProtocolTCPHTTP2,
				},
				Delegator: &contract.NewGrantDelegatedServicePublicationDelegatorArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000003",
						Name:           "",
						ManagerAddress: "",
					},
				},
			},
		},
	})
	assert.NoError(t, err)

	assert.Equal(t, "$1$5$Hn3ool7yAV83GkfPqXczFJ7khY4lzxcXN6dDgP9W-Vh7q7u_qbo4qCDm0pAxS3ArZ5PKvESfSHUe-uYcVgdsWg", c.Grants().DelegatedServicePublicationGrants()[0].Hash().String())
}

func TestDelegatedServicePublicationHashWithoutProtocol(t *testing.T) {
	contractIV, err := uuid.Parse("26aad1d8-3653-4e6f-b3ba-70df67a9a0ab")
	assert.NoError(t, err)

	contractIVBytes, err := contractIV.MarshalBinary()
	assert.NoError(t, err)

	now := time.Date(2023, 1, 1, 1, 42, 0, 0, time.UTC)

	c, err := contract.NewContent(&contract.NewContentArgs{
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIVBytes,
		CreatedAt:     now,
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: now,
			NotAfter:  now,
		},
		Grants: []interface{}{
			&contract.NewGrantDelegatedServicePublicationArgs{
				Directory: &contract.NewGrantDelegatedServicePublicationDirectoryArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000001",
						Name:           "",
						ManagerAddress: "",
					},
				},
				Service: &contract.NewGrantDelegatedServicePublicationServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000002",
						Name:           "",
						ManagerAddress: "",
					},
					Name:     "parkeerrechten",
					Protocol: contract.ServiceProtocolTCPHTTP1_1,
				},
				Delegator: &contract.NewGrantDelegatedServicePublicationDelegatorArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000003",
						Name:           "",
						ManagerAddress: "",
					},
				},
			},
		},
	})
	assert.NoError(t, err)

	assert.Equal(t, "$1$5$3Pea4Y2FhJG1BTk3E7R9cPqFeq3iIDWs9-zxkNATtLVzo_IbbsXKbZqMdYFmzckT5CyoOikXTvzBIcFd1zuC_w", c.Grants().DelegatedServicePublicationGrants()[0].Hash().String())
}
