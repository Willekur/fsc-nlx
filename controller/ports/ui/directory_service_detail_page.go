// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import (
	"fmt"
	"net/http"
	"time"
)

type directoryServiceDetailPage struct {
	*BasePage
	BaseAuthenticatedPage
	Service            *directoryServiceDetailPageService
	OutwayCertificates []*directoryServiceDetailPageOutwayCertificate
	SuccessMessage     string
	ErrorMessage       string
}

type directoryServiceDetailPageService struct {
	Name                   string
	PeerID                 string
	PeerName               string
	ProviderID             string
	ProviderName           string
	IsDelegatedPublication bool
}

type directoryServiceDetailPageOutwayCertificate struct {
	CertificateThumbprint string
	OutwayNames           []string
	AccessContracts       []*directoryServiceDetailPageOutwayCertificateAccessContract
}

type directoryServiceDetailPageOutwayCertificateAccessContract struct {
	IsActive   bool
	Status     string
	Title      string
	Hash       string
	ValidFrom  time.Time
	ValidUntil time.Time
	CreatedAt  time.Time
}

func (p *directoryServiceDetailPage) render(w http.ResponseWriter) {
	err := p.renderTemplate(w, "templates/directory-service-detail.html")
	if err != nil {
		p.logger.Error("failed to parse directory service detail page", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}
}

func (p *directoryServiceDetailPage) renderNotFound(w http.ResponseWriter) {
	err := p.renderTemplate(w, "templates/directory-service-detail-not-found.html")
	if err != nil {
		p.logger.Error("failed to parse directory service detail not found page", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}
}

func (p *directoryServiceDetailPage) renderTemplate(w http.ResponseWriter, templatePath string) error {
	baseTemplate := p.TemplateWithHelpers()

	t, err := baseTemplate.
		ParseFS(
			tplFolder,
			"templates/base-authenticated.html",
			templatePath,
		)
	if err != nil {
		return err
	}

	err = t.ExecuteTemplate(w, "base-authenticated.html", p)
	if err != nil {
		return err
	}

	return nil
}
