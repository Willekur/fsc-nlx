// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package command

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	internalapp_errors "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/errors"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/services"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type RevokeContractHandler struct {
	trustedExternalRootCAs    *x509.CertPool
	clock                     Clock
	repository                contract.Repository
	logger                    *logger.Logger
	peersCommunicationService *services.PeersCommunicationService
	signatureCert             *tls.Certificate
}

type NewRevokeContractHandlerArgs struct {
	TrustedExternalRootCAs    *x509.CertPool
	Repository                contract.Repository
	PeersCommunicationService *services.PeersCommunicationService
	Clock                     Clock
	Logger                    *logger.Logger
	SignatureCert             *tls.Certificate
}

func NewRevokeContractHandler(args *NewRevokeContractHandlerArgs) (*RevokeContractHandler, error) {
	if args.TrustedExternalRootCAs == nil {
		return nil, errors.New("trustedRootCA's is required")
	}

	if args.Repository == nil {
		return nil, errors.New("repository is required")
	}

	if args.PeersCommunicationService == nil {
		return nil, errors.New("peers communication service is required")
	}

	if args.Clock == nil {
		return nil, errors.New("clock is required")
	}

	if args.Logger == nil {
		return nil, errors.New("logger is required")
	}

	if args.SignatureCert == nil {
		return nil, errors.New("signature cert is required")
	}

	return &RevokeContractHandler{
		trustedExternalRootCAs:    args.TrustedExternalRootCAs,
		repository:                args.Repository,
		peersCommunicationService: args.PeersCommunicationService,
		clock:                     args.Clock,
		logger:                    args.Logger,
		signatureCert:             args.SignatureCert,
	}, nil
}

func (h *RevokeContractHandler) Handle(ctx context.Context, contentHash string) error {
	hash, err := contract.NewContentHashFromString(contentHash)
	if err != nil {
		return internalapp_errors.NewIncorrectInputError(err)
	}

	contractContent, err := h.repository.GetContentByHash(ctx, hash)
	if err != nil {
		return fmt.Errorf("could not retrieve content by hash: %w", err)
	}

	sig, err := contractContent.Revoke(h.trustedExternalRootCAs, h.signatureCert, h.clock.Now())
	if err != nil {
		return fmt.Errorf("unable to place a revoke signature on content %w", err)
	}

	err = h.repository.UpsertSignature(ctx, sig)
	if err != nil {
		h.logger.Error("could not upsert signature", err)
		return err
	}

	err = h.peersCommunicationService.RevokeContract(ctx, contractContent.PeersIDs(), contractContent, sig)
	if err != nil {
		return fmt.Errorf("could not send signed revoked contract to peers: %w", err)
	}

	return nil
}
