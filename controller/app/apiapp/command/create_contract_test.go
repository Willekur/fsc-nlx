// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
package command_test

import (
	"context"
	"errors"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"golang.org/x/exp/maps"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

// nolint:funlen // these tests should not fit 100 lines
func TestCreateContract(t *testing.T) {
	t.Parallel()

	type tests map[string]struct {
		setup   func(context.Context, *mocks)
		args    *command.CreateContractArgs
		wantErr error
	}

	validArgs := &command.CreateContractArgs{
		AuthData:              &authentication.OIDCData{},
		AuthorizationMetadata: &authorization.RestMetaData{},
		HashAlgorithm:         command.HashAlgSHA3_512,
		GroupID:               "group-id",
		ContractNotBefore:     "2023-05-23",
		ContractNotAfter:      "2023-05-24",
		ServicePublicationGrants: []*command.ServicePublicationGrant{
			{
				DirectoryPeerID: "12345678901234567899",
				ServicePeerID:   "12345678901234567890",
				ServiceName:     "petstore",
			},
		},
		ServiceConnectionGrants: []*command.ServiceConnectionGrant{
			{
				ServicePeerID:               "12345678901234567891",
				ServiceName:                 "petstore",
				OutwayPeerID:                "12345678901234567890",
				OutwayCertificateThumbprint: "1JfTc4daaf_6RRIrhY0hva7o2Drz21tmIMWN4eGgGoE=",
			},
		},
		DelegatedServiceConnectionGrants: []*command.DelegatedServiceConnectionGrant{
			{
				DelegatorPeerID:             "12345678901234567892",
				ServicePeerID:               "12345678901234567891",
				ServiceName:                 "petstore",
				OutwayPeerID:                "12345678901234567890",
				OutwayCertificateThumbprint: "1JfTc4daaf_6RRIrhY0hva7o2Drz21tmIMWN4eGgGoE=",
			},
		},
		DelegatedServicePublicationGrants: []*command.DelegatedServicePublicationGrant{
			{
				DirectoryPeerID: "12345678901234567899",
				DelegatorPeerID: "12345678901234567891",
				ServicePeerID:   "12345678901234567890",
				ServiceName:     "petstore",
			},
		},
	}

	flowTests := tests{
		"when_unauthenticated": {
			args: func(a command.CreateContractArgs) *command.CreateContractArgs {
				a.AuthData = nil
				return &a
			}(*validArgs),
			wantErr: &command.AuthenticationError{},
		},
		"when_unauthorized": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscCreateContract,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeContract, "*"),
					},
				).Return(authz.NewAuthorizationError("arbitrary"))
			},
			wantErr: &command.AuthorizationError{},
		},
		"when_id_generator_errors": {
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscCreateContract,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeContract, "*"),
					},
				).Return(nil)

				m.iv.EXPECT().
					New().
					Return("", fmt.Errorf("arbitrary"))
			},
			args:    validArgs,
			wantErr: &command.InternalError{},
		},
		"when_manager_errors": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscCreateContract,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeContract, "*"),
					},
				).Return(nil)

				m.iv.EXPECT().
					New().
					Return("iv", nil)

				now := time.Now()

				m.clock.EXPECT().
					Now().
					Return(now)

				m.manager.EXPECT().
					CreateContract(ctx, &manager.CreateContractArgs{
						IV:                "iv",
						HashAlgorithm:     manager.HashAlgSHA3_512,
						GroupID:           "group-id",
						ContractNotBefore: time.Date(2023, 5, 23, 0, 0, 0, 0, time.UTC),
						ContractNotAfter:  time.Date(2023, 5, 24, 0, 0, 0, 0, time.UTC),
						CreatedAt:         now,
						ServicePublicationGrants: []*manager.ServicePublicationGrant{
							{
								DirectoryPeerID: "12345678901234567899",
								ServicePeerID:   "12345678901234567890",
								ServiceName:     "petstore",
								ServiceProtocol: "PROTOCOL_TCP_HTTP_1.1",
							},
						},
						ServiceConnectionGrants: []*manager.ServiceConnectionGrant{
							{
								ServicePeerID:               "12345678901234567891",
								ServiceName:                 "petstore",
								OutwayPeerID:                "12345678901234567890",
								OutwayCertificateThumbprint: "1JfTc4daaf_6RRIrhY0hva7o2Drz21tmIMWN4eGgGoE=",
							},
						},
						DelegatedServiceConnectionGrants: []*manager.DelegatedServiceConnectionGrant{
							{
								DelegatorPeerID:             "12345678901234567892",
								ServicePeerID:               "12345678901234567891",
								ServiceName:                 "petstore",
								OutwayPeerID:                "12345678901234567890",
								OutwayCertificateThumbprint: "1JfTc4daaf_6RRIrhY0hva7o2Drz21tmIMWN4eGgGoE=",
							},
						},
						DelegatedServicePublicationGrants: []*manager.DelegatedServicePublicationGrant{
							{
								DirectoryPeerID: "12345678901234567899",
								DelegatorPeerID: "12345678901234567891",
								ServicePeerID:   "12345678901234567890",
								ServiceName:     "petstore",
								ServiceProtocol: "PROTOCOL_TCP_HTTP_1.1",
							},
						},
					}).
					Return(errors.New("arbitrary"))
			},
			wantErr: &command.InternalError{},
		},
		"happy_flow": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscCreateContract,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeContract, "*"),
					},
				).Return(nil)

				m.iv.EXPECT().
					New().
					Return("iv", nil)

				now := time.Now()

				m.clock.EXPECT().
					Now().
					Return(now)

				m.manager.EXPECT().
					CreateContract(ctx, &manager.CreateContractArgs{
						IV:                "iv",
						HashAlgorithm:     manager.HashAlgSHA3_512,
						GroupID:           "group-id",
						ContractNotBefore: time.Date(2023, 5, 23, 0, 0, 0, 0, time.UTC),
						ContractNotAfter:  time.Date(2023, 5, 24, 0, 0, 0, 0, time.UTC),
						CreatedAt:         now,
						ServicePublicationGrants: []*manager.ServicePublicationGrant{
							{
								DirectoryPeerID: "12345678901234567899",
								ServicePeerID:   "12345678901234567890",
								ServiceName:     "petstore",
								ServiceProtocol: "PROTOCOL_TCP_HTTP_1.1",
							},
						},
						ServiceConnectionGrants: []*manager.ServiceConnectionGrant{
							{
								ServicePeerID:               "12345678901234567891",
								ServiceName:                 "petstore",
								OutwayPeerID:                "12345678901234567890",
								OutwayCertificateThumbprint: "1JfTc4daaf_6RRIrhY0hva7o2Drz21tmIMWN4eGgGoE=",
							},
						},
						DelegatedServiceConnectionGrants: []*manager.DelegatedServiceConnectionGrant{
							{
								DelegatorPeerID:             "12345678901234567892",
								ServicePeerID:               "12345678901234567891",
								ServiceName:                 "petstore",
								OutwayPeerID:                "12345678901234567890",
								OutwayCertificateThumbprint: "1JfTc4daaf_6RRIrhY0hva7o2Drz21tmIMWN4eGgGoE=",
							},
						},
						DelegatedServicePublicationGrants: []*manager.DelegatedServicePublicationGrant{
							{
								DirectoryPeerID: "12345678901234567899",
								DelegatorPeerID: "12345678901234567891",
								ServicePeerID:   "12345678901234567890",
								ServiceName:     "petstore",
								ServiceProtocol: "PROTOCOL_TCP_HTTP_1.1",
							},
						},
					}).
					Return(nil)
			},
		},
	}

	validationTests := tests{
		"invalid_not_before_format": {
			args: &command.CreateContractArgs{
				AuthData:                 &authentication.OIDCData{},
				AuthorizationMetadata:    &authorization.RestMetaData{},
				HashAlgorithm:            command.HashAlgSHA3_512,
				GroupID:                  "group-id",
				ContractNotBefore:        "2023-05-23-invalid-format",
				ContractNotAfter:         "2023-05-24",
				ServicePublicationGrants: nil,
				ServiceConnectionGrants:  nil,
			},
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscCreateContract,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeContract, "*"),
					},
				).Return(nil)
			},
			wantErr: &command.ValidationError{},
		},
		"invalid_not_after_format": {
			args: &command.CreateContractArgs{
				AuthData:                 &authentication.OIDCData{},
				AuthorizationMetadata:    &authorization.RestMetaData{},
				HashAlgorithm:            command.HashAlgSHA3_512,
				GroupID:                  "group-id",
				ContractNotBefore:        "2023-05-23",
				ContractNotAfter:         "2023-05-24-invalid-format",
				ServicePublicationGrants: nil,
				ServiceConnectionGrants:  nil,
			},
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscCreateContract,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeContract, "*"),
					},
				).Return(nil)
			},
			wantErr: &command.ValidationError{},
		},
	}

	allTests := tests{}
	maps.Copy(allTests, flowTests)
	maps.Copy(allTests, validationTests)

	for name, tt := range allTests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			mocks := newMocks(t)

			h, err := command.NewCreateContractHandler(mocks.manager, mocks.auth, mocks.clock, mocks.iv, mocks.logger)
			require.NoError(t, err)

			ctx := context.Background()

			if tt.setup != nil {
				tt.setup(ctx, mocks)
			}

			err = h.Handle(ctx, tt.args)

			if tt.wantErr == nil {
				assert.NoError(t, err)
			} else {
				assert.ErrorAs(t, err, &tt.wantErr)
			}
		})
	}
}

func TestCreateContractGrantValidation(t *testing.T) {
	t.Parallel()

	type tests map[string]struct {
		args       *command.ServiceConnectionGrant
		wantErr    error
		wantErrMsg string
	}

	testCases := tests{
		"invalid_name_spaces": {
			args: &command.ServiceConnectionGrant{
				ServiceName: "test service",
			},
			wantErr:    &command.ValidationError{},
			wantErrMsg: "validation error: service name must contain only letters, numbers, -, _ and .",
		},
		"invalid_name_blank": {
			args: &command.ServiceConnectionGrant{
				ServiceName: "",
			},
			wantErr:    &command.ValidationError{},
			wantErrMsg: "validation error: service name cannot be blank\nservice name must contain only letters, numbers, -, _ and .",
		},
	}

	for testCase, test := range testCases {
		tt := test

		t.Run(testCase, func(t *testing.T) {
			t.Parallel()

			err := tt.args.Valid()
			assert.ErrorAs(t, err, &tt.wantErr)
			assert.Equal(t, tt.wantErrMsg, err.Error())
		})
	}
}
