// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package config

import (
	"context"
)

type Repository interface {
	GetServices(ctx context.Context) (Services, error)
	GetToken(ctx context.Context, grantHash string) (*TokenInfo, error)

	Close() error
}
