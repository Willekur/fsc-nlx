-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL


BEGIN transaction;

CREATE TYPE contracts.distribution_action AS
    ENUM ('submit_contract','submit_accept_signature','submit_reject_signature','submit_revoke_signature');

CREATE TABLE contracts.distributions(
        peer_id VARCHAR(20) NOT NULL,
        content_hash VARCHAR(150) NOT NULL,
        signature VARCHAR(2048) NOT NULL,
        action contracts.distribution_action NOT NULL,
        retries INTEGER DEFAULT 0 NOT NULL,
        last_retry_at  timestamptz NOT NULL,
        next_retry_at  timestamptz,
        http_status_code INTEGER NOT NULL,
        error_message VARCHAR(1024) NOT NULL,
        CONSTRAINT distribution_pk PRIMARY KEY (peer_id, content_hash, action)
);

ALTER TABLE contracts.distributions ADD CONSTRAINT distribution_content_hash FOREIGN KEY (content_hash)
    REFERENCES contracts.content (hash) MATCH FULL
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE contracts.distributions ADD CONSTRAINT distribution_peer_id FOREIGN KEY (peer_id)
    REFERENCES peers.peers (id)  MATCH FULL
    ON DELETE CASCADE ON UPDATE CASCADE;

COMMIT;
