// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package postgresadapter

import (
	"context"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/storage/postgres/queries"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func (r *PostgreSQLRepository) DeleteContractDistributions(ctx context.Context, peerID contract.PeerID, contentHash *contract.ContentHash, action contract.DistributionAction) error {
	return r.queries.DeleteContractDistributions(ctx, &queries.DeleteContractDistributionsParams{ContentHash: contentHash.String(), PeerID: peerID.Value(), Action: queries.ContractsDistributionAction(action)})
}
