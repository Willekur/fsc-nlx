// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query_test

import (
	"net/url"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	discardlogger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/discard"
	mock_authorization "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz/mock"
	mock_manager "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager/mock"
	mock_storage "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage/mock"
	auth_helper "gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
)

type mocks struct {
	manager *mock_manager.MockManager
	storage *mock_storage.MockStorage
	auth    *auth_helper.Auth
	authz   *mock_authorization.MockAuthorization
	clock   *clock.MockClock
	logger  *logger.Logger
}

func newMocks(t *testing.T) *mocks {
	nowInUTC := time.Now().UTC()

	authz := mock_authorization.NewMockAuthorization(t)

	auth, err := auth_helper.New(authz)
	assert.NoError(t, err)

	return &mocks{
		manager: mock_manager.NewMockManager(t),
		storage: mock_storage.NewMockStorage(t),
		authz:   authz,
		auth:    auth,
		clock:   clock.NewMock(time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location())),
		logger:  discardlogger.New(),
	}
}

func newURL(t *testing.T, u string) *url.URL {
	ur, err := url.Parse(u)
	require.NoError(t, err)

	return ur
}
