###########
## Chart ##
###########
global:
  groupID: "fsc-default"

postgresql:
  storageSize: 256Mi

opa:
  enabled: true

################
## Sub-charts ##
################
fsc-nlx-manager:
  config:
    selfAddress: "https://rvrd-fsc-nlx-manager-external:8443"
    directoryManagerAddress: "https://shared-fsc-nlx-manager-external:8443"

fsc-nlx-controller:
  ingress:
    hosts:
      - "nlx-controller-rvrd-{{DOMAIN_SUFFIX}}"
