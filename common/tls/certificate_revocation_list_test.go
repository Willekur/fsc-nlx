// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package tls_test

import (
	"crypto/x509"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

func TestCRL_CheckCertificateOnCRL(t *testing.T) {
	crl, err := setupCRL()
	assert.NoError(t, err)

	revokedCertificate, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "testing", "pki"), testingutils.OrgOnCRL)
	assert.NoError(t, err)
	err = crl.CheckCertificate(revokedCertificate.Certificate())
	assert.Error(t, err)
	assert.Equal(t, err.Error(), fmt.Sprintf("certificate with serialnumber: %s is present on a Certificate Revocation List", revokedCertificate.Certificate().SerialNumber))
}

func TestCRL_CheckCertificateWithDistributionPointOnCRL(t *testing.T) {
	crl, err := setupCRL()
	assert.NoError(t, err)

	revokedCertificate, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "testing", "pki"), testingutils.OrgOnCRL)
	revokedCertificate.Certificate().CRLDistributionPoints = []string{"http://localhost"}
	assert.NoError(t, err)
	err = crl.CheckCertificate(revokedCertificate.Certificate())
	assert.Error(t, err)
	assert.Equal(t, err.Error(), fmt.Sprintf("certificate with serialnumber: %s is present on a Certificate Revocation List", revokedCertificate.Certificate().SerialNumber))
}

func TestCRL_FetchAndCheckCRLWithDistributionPoint(t *testing.T) {
	crl, err := tls.NewCRL(nil)
	assert.NoError(t, err)

	crlServer := setupCRLEndpoint()
	defer crlServer.Close()

	revokedCertificate, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "testing", "pki"), testingutils.OrgOnCRL)
	revokedCertificate.Certificate().CRLDistributionPoints = []string{crlServer.URL}
	assert.NoError(t, err)
	err = crl.CheckCertificate(revokedCertificate.Certificate())
	assert.Error(t, err)
	assert.Equal(t, err.Error(), fmt.Sprintf("certificate with serialnumber: %s is present on a Certificate Revocation List", revokedCertificate.Certificate().SerialNumber))
}

func TestCRL_FetchAndCheckCRLFetchFails(t *testing.T) {
	crl, err := setupCRL()
	assert.NoError(t, err)

	crlServer := setupCRLEndpoint()
	defer crlServer.Close()

	testCertificate, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "testing", "pki"), testingutils.NLXTestPeerA)
	testCertificate.Certificate().CRLDistributionPoints = []string{"http://idonotexist"}
	assert.NoError(t, err)
	err = crl.CheckCertificate(testCertificate.Certificate())
	assert.Error(t, err)
	assert.Equal(t, "cannot retrieve CRLsCache from revocationListURL: http://idonotexist", err.Error())
}

func TestCRL_CheckCertificateNotOnCRL(t *testing.T) {
	crl, err := setupCRL()
	assert.NoError(t, err)

	revokedCertificate, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "testing", "pki"), testingutils.NLXTestPeerA)
	assert.NoError(t, err)
	err = crl.CheckCertificate(revokedCertificate.Certificate())
	assert.NoError(t, err)
}

func setupCRL() (*tls.CRLsCache, error) {
	crlFile, err := os.ReadFile(filepath.Join("..", "..", "testing", "pki", "ca.crl"))
	if err != nil {
		return nil, err
	}

	revocationList, err := x509.ParseRevocationList(crlFile)
	if err != nil {
		return nil, err
	}

	crl, err := tls.NewCRL(nil)
	if err != nil {
		return nil, err
	}

	err = crl.AddRevocationList("http://localhost", revocationList)
	if err != nil {
		return nil, err
	}

	return crl, err
}

func setupCRLEndpoint() *httptest.Server {
	crlTestServer := httptest.NewUnstartedServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, filepath.Join("..", "..", "testing", "pki", "ca.crl"))
	}))

	crlTestServer.Start()

	return crlTestServer
}
