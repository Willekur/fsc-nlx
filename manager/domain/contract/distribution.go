// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package contract

import "time"

type DistributionAction string

const (
	DistributionActionSubmitContract        DistributionAction = "submit_contract"
	DistributionActionSubmitAcceptSignature DistributionAction = "submit_accept_signature"
	DistributionActionSubmitRevokeSignature DistributionAction = "submit_revoke_signature"
	DistributionActionSubmitRejectSignature DistributionAction = "submit_reject_signature"
)

type Distribution struct {
	ContentHash  *ContentHash
	PeerID       PeerID
	Action       DistributionAction
	Signature    string
	Retries      int32
	NextRetryAt  *time.Time
	LastRetryAt  time.Time
	ErrorMessage string
}
