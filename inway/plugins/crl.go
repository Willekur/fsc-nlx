/*
 * Copyright © VNG Realisatie 2024
 * Licensed under the EUPL
 */

package plugins

import (
	"errors"

	"gitlab.com/commonground/nlx/fsc-nlx/common/httperrors"
	"gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	inway_http "gitlab.com/commonground/nlx/fsc-nlx/inway/http"
)

type CrlPlugin struct {
	crl *tls.CRLsCache
}

func NewCrlPlugin(crl *tls.CRLsCache) (*CrlPlugin, error) {
	return &CrlPlugin{
		crl: crl,
	}, nil
}

func (c *CrlPlugin) Serve(next ServeFunc) ServeFunc {
	return func(context *Context) error {
		peerCerts := context.Request.TLS.PeerCertificates

		for _, peerCert := range peerCerts {
			err := c.crl.CheckCertificate(peerCert)

			if err != nil {
				var revokeError *tls.CertificateRevokedError
				if errors.As(err, &revokeError) {
					inway_http.WriteError(
						context.Response,
						httperrors.O1,
						httperrors.CertificateRevoked(err),
					)

					return nil
				}

				inway_http.WriteError(context.Response, httperrors.A1, httperrors.ServerError(err))

				return nil
			}
		}

		return next(context)
	}
}
