/*
 * Copyright © VNG Realisatie 2024
 * Licensed under the EUPL
 */

package restmanager

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func (m restManager) ListContractDistribution(ctx context.Context, contentHash string) (contractDistributions manager.ContractDistributions, err error) {
	res, err := m.client.GetContractDistributionsWithResponse(ctx, contentHash)
	if err != nil {
		return nil, fmt.Errorf("could not retrieve contract distribution information from Manager: %s: %w", err, manager.ErrContractDistributionsNotFoundError)
	}

	contractDistributions = make(manager.ContractDistributions, 0, len(res.JSON200.Distributions))

	for _, contractDistributionResponse := range res.JSON200.Distributions {
		contractDistributions = append(contractDistributions, &manager.ContractDistribution{
			PeerID:       contractDistributionResponse.PeerId,
			ContentHash:  contractDistributionResponse.ContractHash,
			Signature:    contractDistributionResponse.Signature,
			Action:       convertAction(contractDistributionResponse.Action),
			Retries:      contractDistributionResponse.Retries,
			LastRetry:    time.Unix(contractDistributionResponse.LastRetryAt, 0),
			ErrorMessage: contractDistributionResponse.ErrorMessage,
		})
	}

	return contractDistributions, nil
}

func convertAction(managerAction models.DistributionAction) manager.Action {
	switch managerAction {
	case models.DISTRIBUTIONACTIONSUBMITCONTRACT:
		return manager.SubmitContract
	case models.DISTRIBUTIONACTIONSUBMITACCEPTSIGNATURE:
		return manager.SubmitAcceptSignature
	case models.DISTRIBUTIONACTIONSUBMITREJECTSIGNATURE:
		return manager.SubmitRejectSignature
	case models.DISTRIBUTIONACTIONSUBMITREVOKESIGNATURE:
		return manager.SubmitRevokeSignature
	default:
		return -1
	}
}
