// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"net/http"
	"time"
)

type contractDetailPage struct {
	*BasePage
	BaseAuthenticatedPage

	IV                                string
	State                             string
	Hash                              string
	HashAlgorithm                     string
	GroupID                           string
	CreatedAt                         time.Time
	ValidFrom                         time.Time
	ValidUntil                        time.Time
	ServicePublicationGrants          []*ServicePublicationGrant
	ServiceConnectionGrants           []*ServiceConnectionGrant
	DelegatedServicePublicationGrants []*DelegatedServicePublicationGrant
	DelegatedServiceConnectionGrants  []*DelegatedServiceConnectionGrant
	Organizations                     []*Peer
	DisableRejectButton               bool
	DisableAcceptButton               bool
	DisableRevokeButton               bool
	IsActive                          bool
}

type Peer struct {
	Name                    string
	PeerID                  string
	RejectSignatureSignedAt string
	AcceptSignatureSignedAt string
	RevokeSignatureSignedAt string
	ContractDistribution    map[Action]*ContractDistribution
}

type ServicePublicationGrant struct {
	Hash              string
	DirectoryPeerID   string
	DirectoryPeerName string
	ServicePeerID     string
	ServicePeerName   string
	ServiceName       string
}

type ServiceConnectionGrant struct {
	Hash                        string
	ServicePeerID               string
	ServicePeerName             string
	ServiceName                 string
	OutwayPeerID                string
	OutwayPeerName              string
	OutwayCertificateThumbprint string
}

type DelegatedServiceConnectionGrant struct {
	Hash                        string
	DelegatorPeerID             string
	DelegatorPeerName           string
	OutwayPeerID                string
	OutwayPeerName              string
	OutwayCertificateThumbprint string
	ServicePeerID               string
	ServicePeerName             string
	ServiceName                 string
}

type DelegatedServicePublicationGrant struct {
	Hash              string
	DirectoryPeerID   string
	DirectoryPeerName string
	DelegatorPeerID   string
	DelegatorPeerName string
	ServicePeerID     string
	ServicePeerName   string
	ServiceName       string
}

type ContractDistribution struct {
	ContentHash    string
	Signature      string
	Action         Action
	HTTPStatusCode int
}

type Action string

const (
	SubmitContract              Action = "Submit Contract"
	SubmitAcceptSignature       Action = "Submit Accept Signature"
	SubmitRejectSignature       Action = "Submit Reject Signature"
	SubmitRevokeSignatureAction Action = "Submit Revoke Signature"
)

func (p *contractDetailPage) render(w http.ResponseWriter) error {
	baseTemplate := p.TemplateWithHelpers()

	t, err := baseTemplate.
		ParseFS(
			tplFolder,
			"templates/base-authenticated.html",
			"templates/contract-detail.html",
		)
	if err != nil {
		return err
	}

	err = t.ExecuteTemplate(w, "base-authenticated.html", p)
	if err != nil {
		return err
	}

	return nil
}
