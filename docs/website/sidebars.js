/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

module.exports = {
  docs: [
    {
      type: 'category',
      label: 'Introduction',
      collapsible: true,
      collapsed: false,
      items: [
        'introduction/introduction',
        'introduction/implementation-strategies',
      ],
    },
    {
      type: 'category',
      label: 'Try FSC NLX',
      collapsible: true,
      collapsed: true,
      items: [
        {
          type: 'category',
          label: 'Docker Compose',
          collapsible: true,
          collapsed: true,
          items: [
            'try-fsc-nlx/docker/introduction',
            'try-fsc-nlx/docker/setup-your-environment',
            'try-fsc-nlx/docker/retrieve-a-demo-certificate',
            'try-fsc-nlx/docker/getting-up-and-running',
            'try-fsc-nlx/docker/provide-an-api',
          ],
        },
        {
          type: 'category',
          label: 'Helm',
          collapsible: true,
          collapsed: true,
          items: [
            'try-fsc-nlx/helm/introduction',
            'try-fsc-nlx/helm/preparation',
            'try-fsc-nlx/helm/create-namespace',
            'try-fsc-nlx/helm/create-certificate',
            'try-fsc-nlx/helm/postgresql',
            'try-fsc-nlx/helm/transaction-log',
            'try-fsc-nlx/helm/nlx-controller',
            'try-fsc-nlx/helm/nlx-manager',
            'try-fsc-nlx/helm/nlx-inway',
            'try-fsc-nlx/helm/nlx-outway',
            'try-fsc-nlx/helm/sample-api',
            'try-fsc-nlx/helm/access-api',
            'try-fsc-nlx/helm/finish',
          ],
        },
      ],
    },
    {
      type: 'category',
      label: 'FSC NLX in production',
      collapsible: true,
      collapsed: true,
      items: [
        'fsc-nlx-in-production/deployment-strategies',
        'fsc-nlx-in-production/setup-authorization',
      ],
    },
    {
      type: 'category',
      label: 'Support',
      collapsible: true,
      collapsed: true,
      items: [
        'support/contact',
        'support/release-strategy',
      ],
    },
  ],
};
