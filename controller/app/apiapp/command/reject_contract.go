// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//nolint:dupl // looks like accept contract but is different
package command

import (
	"context"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

type RejectContractHandler struct {
	manager manager.Manager
	auth    *auth.Auth
	lgr     *logger.Logger
}

func NewRejectContractHandler(m manager.Manager, a *auth.Auth, l *logger.Logger) (*RejectContractHandler, error) {
	if m == nil {
		return nil, fmt.Errorf("manager is required")
	}

	if a == nil {
		return nil, fmt.Errorf("auth is required")
	}

	if l == nil {
		return nil, fmt.Errorf("logger is required")
	}

	return &RejectContractHandler{
		manager: m,
		auth:    a,
		lgr:     l,
	}, nil
}

type RejectContractArgs struct {
	AuthData              authentication.Data
	AuthorizationMetadata authorization.Metadata
	ContentHash           string
}

func (h *RejectContractHandler) Handle(ctx context.Context, args *RejectContractArgs) error {
	err := h.auth.Auth(ctx, args.AuthData, args.AuthorizationMetadata, authz.FscRejectContract, []*authz.ResourceURN{
		authz.NewResourceURN(authz.ResourceTypeContract, args.ContentHash),
	})
	if err != nil {
		return mapError(err, "could not authenticate or authorize")
	}

	if args.ContentHash == "" {
		return newValidationError("contentHash cannot be empty")
	}

	err = h.manager.RejectContract(ctx, args.ContentHash)
	if err != nil {
		h.lgr.Error("failed to reject contract", err)

		return mapError(err, "could not reject contract in manager")
	}

	return nil
}
