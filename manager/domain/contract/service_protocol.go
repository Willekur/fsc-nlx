// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package contract

import "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api/models"

type ServiceProtocol int32

const (
	ServiceProtocolUnspecified ServiceProtocol = iota
	ServiceProtocolTCPHTTP1_1
	ServiceProtocolTCPHTTP2
)

var ServiceProtocols = []ServiceProtocol{ServiceProtocolTCPHTTP1_1, ServiceProtocolTCPHTTP2}

// String returns the string representation of a Service Protocol
// Needs to conform to the service protocol enum described in the
// Core OpenAPI specification
func (p ServiceProtocol) String() string {
	switch p {
	case ServiceProtocolTCPHTTP1_1:
		return string(models.PROTOCOLTCPHTTP11)
	case ServiceProtocolTCPHTTP2:
		return string(models.PROTOCOLTCPHTTP2)
	default:
		return ""
	}
}
