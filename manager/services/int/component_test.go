// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package internalservice_test

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"path"
	"path/filepath"
	"strings"
	"testing"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	discardlogger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/discard"
	"gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	postgresadapter "gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/storage/postgres"
	internalapp "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/services"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	restport "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
	internalservice "gitlab.com/commonground/nlx/fsc-nlx/manager/services/int"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

var nowInUTC = time.Date(2021, 1, 2, 1, 2, 3, 0, time.UTC).UTC()

var (
	testClock     = clock.NewMock(time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location()))
	peerA         *peerInfo
	peerB         *peerInfo
	peerC         *peerInfo
	peerDirectory *peerInfo
)

func TestMain(m *testing.M) {
	var err error

	peerA, err = newPeerInfo(testingutils.NLXTestPeerA, "https://manager.org-a.nlx.local:443")
	if err != nil {
		log.Fatal(err)
	}

	peerB, err = newPeerInfo(testingutils.NLXTestPeerB, "https://manager.org-b.nlx.local:443")
	if err != nil {
		log.Fatal(err)
	}

	peerC, err = newPeerInfo(testingutils.NLXTestPeerC, "https://manager.org-c.nlx.local:443")
	if err != nil {
		log.Fatal(err)
	}

	peerDirectory, err = newPeerInfo(testingutils.NLXTestInternal, "https://directory.nlx.local:443")
	if err != nil {
		log.Fatal(err)
	}

	m.Run()
}

type peerInfo struct {
	CertBundle     *tls.CertificateBundle
	ManagerAddress string
}

func newPeerInfo(organisationName testingutils.CertificateBundlePeerName, managerAddress string) (*peerInfo, error) {
	orgCertBundle, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "../", "testing", "pki"), organisationName)
	if err != nil {
		return nil, err
	}

	return &peerInfo{
		CertBundle:     orgCertBundle,
		ManagerAddress: managerAddress,
	}, nil
}

func (o *peerInfo) GetName() string {
	return o.CertBundle.GetPeerInfo().Name
}

func (o *peerInfo) GetPeerID() string {
	return o.CertBundle.GetPeerInfo().SerialNumber
}

//nolint:funlen,gocyclo // this is a test
func newService(testName string) (*httptest.Server, *internalapp.Application) {
	logger := discardlogger.New()

	peerACertBundle, err := testingutils.GetCertificateBundle(path.Join("..", "..", "../", "testing", "pki"), testingutils.NLXTestPeerA)
	if err != nil {
		log.Fatalf("failed to load organization a cert bundle: %v", err)
	}

	postgresDSN := os.Getenv("POSTGRES_DSN")

	if postgresDSN == "" {
		postgresDSN = "postgres://postgres@localhost:5432?sslmode=disable"
	}

	dbName := strings.ToLower(testName)
	dbName = fmt.Sprintf("int_%s", dbName)
	dbName = strings.ReplaceAll(dbName, "/", "_")

	testDB, err := testingutils.CreateTestDatabase(postgresDSN, dbName)
	if err != nil {
		log.Fatalf("failed to setup test database: %v", err)
	}

	db, err := postgresadapter.NewConnection(testDB)
	if err != nil {
		log.Fatalf("failed to create db connection: %v", err)
	}

	err = postgresadapter.PerformMigrations(testDB)
	if err != nil {
		log.Fatalf("failed to perform dbmigrations: %v", err)
	}

	postgresqlRepository, err := postgresadapter.New(peerACertBundle.RootCAs(), db)
	if err != nil {
		log.Fatalf("failed to setup postgresql database: %s", err)
	}

	selfPeer, err := contract.NewPeer(&contract.NewPeerArgs{
		ID:             peerACertBundle.GetPeerInfo().SerialNumber,
		Name:           peerACertBundle.GetPeerInfo().Name,
		ManagerAddress: "https://manager.org-a.nlx.local:443",
	})
	if err != nil {
		log.Fatalf("failed to create self peer: %v", err)
	}

	controllerRepository := newFakeController()

	managerFactory := testManagerFactory{}

	peersCommunicationService, err := services.NewPeersCommunicationService(&services.NewPeersCommunicationServiceArgs{
		Ctx:                            context.Background(),
		Logger:                         logger,
		LocalRepo:                      postgresqlRepository,
		DirectoryManagerAddress:        "https://directory.nlx.local:443",
		ManagerFactory:                 &managerFactory,
		SelfPeerID:                     selfPeer.ID(),
		ContractDistributionRepository: postgresqlRepository,
	})
	if err != nil {
		log.Fatalf("failed to setup peers adapter: %s", err)
	}

	txlogRepository := newFakeTxLog()

	app, err := internalservice.NewApplication(&internalservice.NewApplicationArgs{
		Context:                     context.Background(),
		Clock:                       testClock,
		Logger:                      logger,
		Repository:                  postgresqlRepository,
		PeersCommunicationService:   peersCommunicationService,
		TXLog:                       txlogRepository,
		SelfPeer:                    selfPeer,
		TrustedExternalRootCAs:      peerACertBundle.RootCAs(),
		SignatureCertificate:        peerACertBundle.Cert(),
		ControllerRepository:        controllerRepository,
		ManagerFactory:              &managerFactory,
		GroupID:                     "fsc-local",
		DirectoryPeerManagerAddress: "https://directory.nlx.local:443",
		DirectoryPeerID:             selfPeer.ID(),
	})
	if err != nil {
		log.Fatalf("failed to setup application: %s", err)
	}

	server, err := restport.New(&restport.NewArgs{
		Logger: logger,
		App:    app,
		Cert:   peerACertBundle,
		Clock:  testClock,
	})
	if err != nil {
		log.Fatalf("failed to setup rest port: %s", err)
	}

	srv := httptest.NewUnstartedServer(server.Handler())
	srv.TLS = peerACertBundle.TLSConfig(peerACertBundle.WithTLSClientAuth())
	srv.StartTLS()

	return srv, app
}

func createInternalManagerAPIClient(managerURL string, certBundle *tls.CertificateBundle) (*api.ClientWithResponses, error) {
	return api.NewClientWithResponses(managerURL, func(c *api.Client) error {
		t := &http.Transport{
			TLSClientConfig: certBundle.TLSConfig(certBundle.WithTLSClientAuth()),
		}
		c.Client = &http.Client{
			Transport: t,
		}
		return nil
	})
}
