// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package main

import (
	"context"
	"errors"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/jessevdk/go-flags"
	_ "github.com/lib/pq"
	"go.uber.org/zap"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/cmd"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logoptions"
	"gitlab.com/commonground/nlx/fsc-nlx/common/monitoring"
	"gitlab.com/commonground/nlx/fsc-nlx/common/process"
	common_tls "gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/common/transactionlog"
	"gitlab.com/commonground/nlx/fsc-nlx/common/version"
	controllerapi "gitlab.com/commonground/nlx/fsc-nlx/controller/ports/internalrest/api/server"
	"gitlab.com/commonground/nlx/fsc-nlx/inway"
	restconfig "gitlab.com/commonground/nlx/fsc-nlx/inway/adapters/config/rest"
	restcontroller "gitlab.com/commonground/nlx/fsc-nlx/inway/adapters/controller/rest"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/intunauthenticated/rest/api/server"
	txlogapi "gitlab.com/commonground/nlx/fsc-nlx/txlog-api/ports/rest/api/server"
)

const readHeaderTimeout = time.Second * 60

var options struct {
	ListenAddress               string   `long:"listen-address" env:"LISTEN_ADDRESS" default:"127.0.0.1:8443" description:"Address for the inway to listen on."`
	Address                     string   `long:"self-address" env:"SELF_ADDRESS" description:"The address that outways can use to reach me" required:"true"`
	ControllerAPIAddress        string   `long:"controller-api-address" env:"CONTROLLER_API_ADDRESS" description:"The Controller API address." required:"true"`
	MonitoringAddress           string   `long:"monitoring-address" env:"MONITORING_ADDRESS" default:"127.0.0.1:8081" description:"Address for the inway monitoring endpoints to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs."`
	TxLogAPIAddress             string   `long:"tx-log-api-address" env:"TX_LOG_API_ADDRESS" description:"The address of the transaction log API" required:"true"`
	Name                        string   `long:"name" env:"INWAY_NAME" description:"Name of the inway. Every inway should have a unique name within the organization." required:"true"`
	GroupID                     string   `long:"group-id" env:"GROUP_ID" description:"Group ID of the FSC Group" required:"true"`
	AuthorizationServiceAddress string   `long:"authorization-service-address" env:"AUTHORIZATION_SERVICE_ADDRESS" description:"Address of the authorization service. If set calls will go through the authorization service before being send to the service"`
	AuthorizationCA             string   `long:"authorization-root-ca" env:"AUTHORIZATION_ROOT_CA" description:"absolute path to root CA used to verify auth service certificate"`
	ManagerInternalAddress      string   `long:"manager-internal-unauthenticated-address" env:"MANAGER_INTERNAL_UNAUTHENTICATED_ADDRESS" description:"Internal Unauthenticated Manager address. This service provides certificates." required:"true"`
	ConfigExpiresAfter          string   `long:"config-expires-after" env:"CONFIG_EXPIRES_AFTER" default:"5s" description:"Time after which the config is invalided. See https://pkg.go.dev/time#ParseDuration" required:"false"`
	ServiceProxyCacheSize       int      `long:"service-proxy-cache-size" env:"SERVICE_PROXY_CACHE_SIZE" default:"1024" description:"The maximum size of the Service proxy cache" required:"false"`
	CRLURLs                     []string `long:"crl-urls" env:"CRL_URLS" description:"List of URL's of Certificate Revocation Lists used to retrieve Certificate Revocation Lists" required:"false"`

	logoptions.LogOptions
	cmd.TLSGroupOptions
	cmd.TLSOptions
}

//nolint:funlen // long setup
func main() {
	parseOptions()

	p := process.NewProcess()

	logger := setupLogger()
	ctx := context.Background()

	if errValidate := common_tls.VerifyPrivateKeyPermissions(options.GroupKeyFile); errValidate != nil {
		logger.Warn("invalid organization key permissions", zap.Error(errValidate), zap.String("file-path", options.GroupKeyFile))
	}

	groupCert, err := common_tls.NewBundleFromFiles(options.GroupCertFile, options.GroupKeyFile, options.GroupRootCert)
	if err != nil {
		logger.Fatal("loading TLS files", zap.Error(err))
	}

	if errValidate := common_tls.VerifyPrivateKeyPermissions(options.KeyFile); errValidate != nil {
		logger.Warn("invalid internal PKI key permissions", zap.Error(errValidate), zap.String("file-path", options.KeyFile))
	}

	internalCert, err := common_tls.NewBundleFromFiles(options.CertFile, options.KeyFile, options.RootCertFile)
	if err != nil {
		logger.Fatal("loading TLS files", zap.Error(err))
	}

	txlogClient, err := txlogapi.NewClientWithResponses(options.TxLogAPIAddress, func(c *txlogapi.Client) error {
		t := http.DefaultTransport.(*http.Transport).Clone()
		t.TLSClientConfig = internalCert.TLSConfig()

		c.Client = &http.Client{Transport: t}
		return nil
	})
	if err != nil {
		logger.Fatal("could not create txlog client", zap.Error(err))
	}

	txLogger, err := transactionlog.NewRestAPITransactionLogger(&transactionlog.NewRestAPITransactionLoggerArgs{
		Logger: logger,
		Client: txlogClient,
	})
	if err != nil {
		logger.Fatal("unable to setup the transaction logger", zap.Error(err))
	}

	configExpiresAfter, err := time.ParseDuration(options.ConfigExpiresAfter)
	if err != nil {
		logger.Fatal("invalid Config expiration duration", zap.Error(err))
	}

	managerInternalClient, err := api.NewClientWithResponses(options.ManagerInternalAddress, func(c *api.Client) error {
		t := http.DefaultTransport.(*http.Transport).Clone()
		t.TLSClientConfig = internalCert.TLSConfig()

		c.Client = &http.Client{Transport: t}
		return nil
	})
	if err != nil {
		logger.Fatal("could not create manager internal client", zap.Error(err))
	}

	controllerClient, err := controllerapi.NewClientWithResponses(options.ControllerAPIAddress, func(c *controllerapi.Client) error {
		t := http.DefaultTransport.(*http.Transport).Clone()
		t.TLSClientConfig = internalCert.TLSConfig()

		c.Client = &http.Client{Transport: t}
		return nil
	})
	if err != nil {
		logger.Fatal("could not create rest controller client", zap.Error(err))
	}

	restConfigRepository, err := restconfig.New(&restconfig.NewRestConfigArgs{
		SelfAddress:      options.Address,
		ManagerClient:    managerInternalClient,
		ControllerClient: controllerClient,
		TrustedRootCert:  groupCert.RootCAs(),
		GroupID:          options.GroupID,
	})
	if err != nil {
		logger.Fatal("failed to setup rest Config repository", zap.Error(err))
	}

	controller, err := restcontroller.New(controllerClient)
	if err != nil {
		logger.Fatal("failed to setup rest controller", zap.Error(err))
	}

	if !strings.HasPrefix(options.Address, "https://") {
		logger.Fatal("invalid self-address: scheme is missing", zap.String("self-address", options.Address))
	}

	selfAddress, err := url.Parse(options.Address)
	if err != nil {
		logger.Fatal("failed to parse self address as a URL", zap.Error(err))
	}

	crl, err := common_tls.NewCRL(options.CRLURLs)
	if err != nil {
		logger.Fatal("cannot create CRLsCache Cache")
	}

	iw, err := inway.NewInway(&inway.Params{
		Clock:                        clock.New(),
		Context:                      ctx,
		Logger:                       logger,
		Txlogger:                     txLogger,
		Controller:                   controller,
		GroupID:                      options.GroupID,
		Name:                         options.Name,
		Address:                      selfAddress,
		OrgCertBundle:                groupCert,
		AuthServiceURL:               options.AuthorizationServiceAddress,
		AuthCAPath:                   options.AuthorizationCA,
		ConfigRepository:             restConfigRepository,
		ServiceEndpointCacheDuration: configExpiresAfter,
		ServiceProxyCacheSize:        options.ServiceProxyCacheSize,
		CRL:                          crl,
	})
	if err != nil {
		logger.Fatal("cannot setup inway", zap.Error(err))
	}

	err = iw.RegisterToController(ctx)
	if err != nil {
		logger.Fatal("failed to register with controller")
	}

	tlsConfig := groupCert.TLSConfig(groupCert.WithTLSClientAuth())
	serverTLS := &http.Server{
		Addr:              options.ListenAddress,
		Handler:           iw.Handler(),
		TLSConfig:         tlsConfig,
		ReadHeaderTimeout: readHeaderTimeout,
	}

	monitoringService, err := monitoring.NewMonitoringService(options.MonitoringAddress, logger)
	if err != nil {
		logger.Fatal("unable to create monitoring service", zap.Error(err))
	}

	go func() {
		if err := serverTLS.ListenAndServeTLS("", ""); err != nil {
			if !errors.Is(err, http.ErrServerClosed) {
				log.Fatal(err, errors.New("error listening on TLS server"))
			}
		}
	}()

	go func() {
		if err := monitoringService.Start(); err != nil {
			if !errors.Is(err, http.ErrServerClosed) {
				log.Fatal(err, errors.New("error listening on monitoring service"))
			}

			log.Fatal("cannot start monitoringService")
		}
	}()

	monitoringService.SetReady()

	p.Wait()

	shutdown(logger, serverTLS, monitoringService)
}

func shutdown(logger *zap.Logger, serverTLS *http.Server, monitoringService *monitoring.Service) {
	logger.Info("starting graceful shutdown")

	gracefulCtx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()

	monitoringService.SetNotReady()

	err := serverTLS.Shutdown(gracefulCtx)
	if err != nil {
		logger.Error("failed to shutdown http server", zap.Error(err))
	}

	if err := monitoringService.Stop(); err != nil {
		logger.Error("failed to stop monitoringService")
	}
}

func setupLogger() *zap.Logger {
	zapConfig := options.LogOptions.ZapConfig()

	logger, err := zapConfig.Build()
	if err != nil {
		log.Fatalf("failed to create new zap logger: %v", err)
	}

	logger.Info("version info", zap.String("version", version.BuildVersion), zap.String("source-hash", version.BuildSourceHash))
	logger = logger.With(zap.String("version", version.BuildVersion))

	logger.Info("starting inway")

	return logger
}

func parseOptions() {
	args, err := flags.Parse(&options)
	if err != nil {
		if et, ok := err.(*flags.Error); ok {
			if et.Type == flags.ErrHelp {
				return
			}
		}

		log.Fatalf("error parsing flags: %v", err)
	}

	if len(args) > 0 {
		log.Fatalf("unexpected arguments: %v", args)
	}
}
