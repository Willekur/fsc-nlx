/*
 * Copyright © VNG Realisatie 2023
 * Licensed under the EUPL
 *
 */

package outway_test

import (
	"context"
	"crypto/x509"
	"log"
	"net/http/httptest"
	"os"
	"path/filepath"
	"testing"
	"time"

	"go.uber.org/zap"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/common/transactionlog"
	"gitlab.com/commonground/nlx/fsc-nlx/outway"
	"gitlab.com/commonground/nlx/fsc-nlx/outway/domain/config"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

var (
	orgACertBundle *tls.CertificateBundle
	orgBCertBundle *tls.CertificateBundle
	orgCCertBundle *tls.CertificateBundle
	orgOnCRL       *tls.CertificateBundle
	testClock      = clock.NewMock(time.Date(2023, 11, 6, 14, 10, 5, 0, time.UTC))
)

func TestMain(m *testing.M) {
	var err error

	orgACertBundle, err = testingutils.GetCertificateBundle(filepath.Join("..", "testing", "pki"), testingutils.NLXTestPeerA)
	if err != nil {
		log.Fatal(err)
	}

	orgBCertBundle, err = testingutils.GetCertificateBundle(filepath.Join("..", "testing", "pki"), testingutils.NLXTestPeerB)
	if err != nil {
		log.Fatal(err)
	}

	orgCCertBundle, err = testingutils.GetCertificateBundle(filepath.Join("..", "testing", "pki"), testingutils.NLXTestPeerC)
	if err != nil {
		log.Fatal(err)
	}

	orgOnCRL, err = testingutils.GetCertificateBundle(filepath.Join("..", "testing", "pki"), testingutils.OrgOnCRL)
	if err != nil {
		log.Fatal(err)
	}

	m.Run()
}

func newOutwayWithCRLCache(repository config.Repository, transactionLogger transactionlog.TransactionLogger) *httptest.Server {
	crlFileDer, err := os.ReadFile(filepath.Join("..", "testing", "pki", "ca.crl"))
	if err != nil {
		log.Panic(err)
	}

	crlFile, err := x509.ParseRevocationList(crlFileDer)
	if err != nil {
		log.Panic(err)
	}

	crlCache, err := tls.NewCRL(nil)
	if err != nil {
		log.Fatal("cannot create CRLsCache Cache")
	}

	err = crlCache.AddRevocationList("localhost", crlFile)
	if err != nil {
		log.Fatal("cannot add CRL to cache")
	}

	return newOutway(repository, transactionLogger, crlCache)
}

func newOutway(repository config.Repository, transactionLogger transactionlog.TransactionLogger, crlCache *tls.CRLsCache) *httptest.Server {
	if transactionLogger == nil {
		transactionLogger = newFakeTransactionLogger()
	}

	if repository == nil {
		repository = newFakeEmptyRepository()
	}

	controller := newFakeController()

	outwayArgs := &outway.NewOutwayArgs{
		Clock:             testClock,
		Name:              "testOutway",
		GroupID:           "fsc-local",
		Ctx:               context.Background(),
		Logger:            zap.NewNop(),
		Txlogger:          transactionLogger,
		Controller:        controller,
		ConfigRepository:  repository,
		MonitoringAddress: "localhost:8080",
		ExternalCert:      orgACertBundle,
		InternalCert:      orgACertBundle,
		CrlCache:          crlCache,
	}

	testOutway, err := outway.New(outwayArgs)
	if err != nil {
		log.Fatal("cannot create outway from args")
	}

	srv := httptest.NewUnstartedServer(testOutway)
	srv.Start()

	return srv
}
