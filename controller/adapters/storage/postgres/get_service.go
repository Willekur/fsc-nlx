// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package postgresstorage

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"net/url"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage/postgres/queries"
)

func (p *PostgresStorage) GetService(ctx context.Context, groupID, serviceName string) (*storage.Service, error) {
	iwAddress := sql.NullString{}

	service, err := p.queries.GetService(ctx, &queries.GetServiceParams{
		GroupID:      groupID,
		Name:         serviceName,
		InwayAddress: iwAddress,
	})
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, storage.ErrServiceNotFound
		}

		return nil, fmt.Errorf("could not get service from database: %w", err)
	}

	endpointURL, err := url.Parse(service.EndpointUrl)
	if err != nil {
		return nil, fmt.Errorf("invalid endpoint URL in database: %w", err)
	}

	return &storage.Service{
		GroupID:      service.GroupID,
		Name:         service.Name,
		InwayAddress: service.InwayAddress,
		EndpointURL:  endpointURL,
	}, nil
}
