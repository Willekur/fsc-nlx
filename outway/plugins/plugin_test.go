// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package plugins

import (
	"net/http"
	"net/http/httptest"

	"go.uber.org/zap"

	"gitlab.com/commonground/nlx/fsc-nlx/outway/domain/config"
)

func fakeContext(token *config.TokenInfo) *Context {
	recorder := httptest.NewRecorder()
	request, _ := http.NewRequest("GET", "/test?foo=bar&bar=foo", http.NoBody)

	return &Context{
		TokenInfo: token,
		Request:   request,
		Response:  recorder,
		Logger:    zap.NewNop(),
		LogData:   map[string]string{},
	}
}

func nopServeFunc(context *Context) error {
	return nil
}
