// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
package query_test

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

// nolint:funlen // these tests should not fit 100 lines
func TestListServices(t *testing.T) {
	t.Parallel()

	validArgs := &query.ListServicesArgs{
		AuthData:              &authentication.OIDCData{},
		AuthorizationMetadata: &authorization.RestMetaData{},
		SelfPeerID:            "1234567891",
	}

	groupID := contract.GroupID("mock-group")

	tests := map[string]struct {
		setup   func(context.Context, *mocks)
		args    *query.ListServicesArgs
		want    query.Services
		wantErr error
	}{
		"when_storage_errors": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscListServices,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeService, "*"),
					},
				).Return(nil)

				m.storage.EXPECT().
					ListServices(ctx, groupID.String()).
					Return(nil, errors.New("unexpected error"))
			},
			wantErr: &query.InternalError{},
		},
		"when_unauthenticated": {
			args: func(a query.ListServicesArgs) *query.ListServicesArgs {
				a.AuthData = nil
				return &a
			}(*validArgs),
			wantErr: &query.AuthenticationError{},
		},
		"when_unauthorized": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscListServices,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeService, "*"),
					},
				).Return(authz.NewAuthorizationError("arbitrary"))
			},
			wantErr: &query.AuthorizationError{},
		},
		"happy_flow_one": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscListServices,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeService, "*"),
					},
				).Return(nil)

				m.manager.EXPECT().
					ListContracts(ctx, []string{}, []manager.GrantType{manager.GrantTypeServicePublication, manager.GrantTypeDelegatedServicePublication}).
					Return([]*manager.Contract{}, nil)

				m.storage.EXPECT().
					ListServices(ctx, groupID.String()).
					Return(storage.Services{
						{
							Name:         "service-1",
							EndpointURL:  newURL(t, "api1.com"),
							InwayAddress: "inway1.com",
							GroupID:      groupID.String(),
						},
					}, nil)
			},
			want: query.Services{
				{
					Name:         "service-1",
					EndpointURL:  newURL(t, "api1.com"),
					InwayAddress: "inway1.com",
					IsPublished:  false,
				},
			},
		},
		"happy_flow_multiple": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscListServices,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeService, "*"),
					},
				).Return(nil)

				m.manager.EXPECT().
					ListContracts(ctx, []string{}, []manager.GrantType{manager.GrantTypeServicePublication, manager.GrantTypeDelegatedServicePublication}).
					Return([]*manager.Contract{
						// expired contract
						{
							IV:            "ce754ff9-3d1d-4dac-9f76-2267ff36cb25",
							Hash:          "hash-1",
							HashAlgorithm: manager.HashAlgSHA3_512,
							GroupID:       groupID.String(),
							CreatedAt:     m.clock.Now(),
							ValidFrom:     m.clock.Now().Add(-time.Hour),
							ValidUntil:    m.clock.Now().Add(-time.Second),
							Peers: map[string]*manager.Peer{"1234567891": {
								ID: "1234567891",
							}},
							AcceptSignatures: map[string]manager.Signature{"1234567891": {SignedAt: m.clock.Now()}},
							ServicePublicationGrants: []*manager.ServicePublicationGrant{
								{
									Hash:            "hash-1",
									DirectoryPeerID: "1234567890",
									ServicePeerID:   "1234567891",
									ServiceName:     "service-2",
								},
							},
							State: manager.ContractStateExpired,
						},
						// revoked contract
						{
							IV:            "df754ff9-3d1d-4dac-9f76-2267ff36cb25",
							Hash:          "hash-2",
							HashAlgorithm: manager.HashAlgSHA3_512,
							GroupID:       groupID.String(),
							CreatedAt:     m.clock.Now(),
							ValidFrom:     m.clock.Now(),
							ValidUntil:    m.clock.Now().Add(time.Hour),
							Peers: map[string]*manager.Peer{"1234567891": {
								ID: "1234567891",
							}},
							AcceptSignatures: map[string]manager.Signature{"1234567891": {SignedAt: m.clock.Now()}},
							RevokeSignatures: map[string]manager.Signature{"1234567891": {SignedAt: m.clock.Now()}},
							ServicePublicationGrants: []*manager.ServicePublicationGrant{
								{
									Hash:            "hash-1",
									DirectoryPeerID: "1234567890",
									ServicePeerID:   "1234567891",
									ServiceName:     "service-3",
								},
							},
							State: manager.ContractStateRevoked,
						},
						// valid contract
						{
							IV:            "df754ff9-3d1d-4dac-9f76-2267ff36cb25",
							Hash:          "hash-3",
							HashAlgorithm: manager.HashAlgSHA3_512,
							GroupID:       groupID.String(),
							CreatedAt:     m.clock.Now(),
							ValidFrom:     m.clock.Now(),
							ValidUntil:    m.clock.Now().Add(time.Hour),
							Peers: map[string]*manager.Peer{"1234567891": {
								ID: "1234567891",
							}},
							AcceptSignatures: map[string]manager.Signature{"1234567891": {SignedAt: m.clock.Now()}},
							ServicePublicationGrants: []*manager.ServicePublicationGrant{
								{
									Hash:            "hash-1",
									DirectoryPeerID: "1234567890",
									ServicePeerID:   "1234567891",
									ServiceName:     "service-4",
								},
							},
							State: manager.ContractStateValid,
						},
					}, nil)

				m.storage.EXPECT().
					ListServices(ctx, groupID.String()).
					Return(storage.Services{
						{
							GroupID:      groupID.String(),
							Name:         "service-1",
							EndpointURL:  newURL(t, "api1.com"),
							InwayAddress: "inway1.com",
						},
						{
							GroupID:      groupID.String(),
							Name:         "service-2",
							EndpointURL:  newURL(t, "api2.com"),
							InwayAddress: "inway2.com",
						},
						{
							GroupID:      groupID.String(),
							Name:         "service-3",
							EndpointURL:  newURL(t, "api3.com"),
							InwayAddress: "inway3.com",
						},
						{
							GroupID:      groupID.String(),
							Name:         "service-4",
							EndpointURL:  newURL(t, "api4.com"),
							InwayAddress: "inway4.com",
						},
					}, nil)
			},
			want: query.Services{
				{
					Name:         "service-1",
					EndpointURL:  newURL(t, "api1.com"),
					InwayAddress: "inway1.com",
					IsPublished:  false,
				},
				{
					Name:         "service-2",
					EndpointURL:  newURL(t, "api2.com"),
					InwayAddress: "inway2.com",
					IsPublished:  false,
				},
				{
					Name:         "service-3",
					EndpointURL:  newURL(t, "api3.com"),
					InwayAddress: "inway3.com",
					IsPublished:  false,
				},
				{
					Name:         "service-4",
					EndpointURL:  newURL(t, "api4.com"),
					InwayAddress: "inway4.com",
					IsPublished:  true,
				},
			},
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			m := newMocks(t)

			h, err := query.NewListServicesHandler(&query.NewListServicesHandlerArgs{
				Storage: m.storage,
				Manager: m.manager,
				Auth:    m.auth,
				GroupID: &groupID,
			})
			require.NoError(t, err)

			ctx := context.Background()

			if tt.setup != nil {
				tt.setup(ctx, m)
			}

			actual, err := h.Handle(ctx, tt.args)

			if tt.wantErr == nil {
				assert.NoError(t, err)
				assert.Equal(t, tt.want, actual)
			} else {
				assert.ErrorAs(t, err, &tt.wantErr)
			}
		})
	}
}
