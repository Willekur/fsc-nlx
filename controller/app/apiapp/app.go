// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package apiapp

import (
	"context"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"

	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/ivgenerator"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
)

type Application struct {
	Commands Commands
	Queries  Queries
}

type Commands struct {
	AcceptContract *command.AcceptContractHandler
	RevokeContract *command.RevokeContractHandler
	RejectContract *command.RejectContractHandler
	CreateContract *command.CreateContractHandler
	CreateService  *command.CreateServiceHandler
	DeleteService  *command.DeleteServiceHandler
	UpdateService  *command.UpdateServiceHandler
	RetryContract  *command.RetryContractHandler
}

type Queries struct {
	GetContract               *query.GetContractHandler
	GetService                *query.GetServiceHandler
	ListServices              *query.ListServicesHandler
	ListContracts             *query.ListContractsHandler
	ListContractDistributions *query.ListContractDistributionsHandler
	ListPeerServices          *query.ListPeerServicesHandler
	ListTXLogRecords          *query.ListTXLogRecordsHandler
	ListInways                *query.ListInwaysHandler
	ListOutways               *query.ListOutwaysHandler
	GetPeerInfo               *query.GetPeerInfoHandler
	ListPeers                 *query.ListPeersHandler
}

type NewApplicationArgs struct {
	Manager     manager.Manager
	Storage     storage.Storage
	Clock       clock.Clock
	IVGenerator ivgenerator.IVGenerator
	Logger      *logger.Logger
	GroupID     contract.GroupID
	Auth        *auth.Auth
}

//nolint:gocyclo // new function
func NewApplication(ctx context.Context, args *NewApplicationArgs) (*Application, error) {
	getService, err := query.NewGetServiceHandler(&query.NewGetServiceHandlerArgs{
		Storage: args.Storage,
		Auth:    args.Auth,
		GroupID: &args.GroupID,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not create new GetService handler")
	}

	listServices, err := query.NewListServicesHandler(&query.NewListServicesHandlerArgs{
		Storage: args.Storage,
		Manager: args.Manager,
		Auth:    args.Auth,
		GroupID: &args.GroupID,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not create new ListServices handler")
	}

	listContracts, err := query.NewListContractsHandler(args.Manager, args.Auth)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new ListContracts handler")
	}

	listContractDistributions, err := query.NewListContractDistributionHandler(args.Manager, args.Auth)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new ListContractDistributins handler")
	}

	getContract, err := query.NewGetContractHandler(args.Manager, args.Auth)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new GetContract handler")
	}

	createContract, err := command.NewCreateContractHandler(args.Manager, args.Auth, args.Clock, args.IVGenerator, args.Logger)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new CreateContract handler")
	}

	acceptContract, err := command.NewAcceptContractHandler(args.Manager, args.Auth, args.Logger)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new AcceptContract handler")
	}

	revokeContract, err := command.NewRevokeContractHandler(args.Manager, args.Auth, args.Logger)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new RevokeContract handler")
	}

	rejectContract, err := command.NewRejectContractHandler(args.Manager, args.Auth, args.Logger)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new RejectContract handler")
	}

	retryContract, err := command.NewRetryContractHandler(args.Manager, args.Auth, args.Logger)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new RetryContract handler")
	}

	createService, err := command.NewCreateServiceHandler(args.GroupID, args.Storage, args.Auth, args.Logger)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new CreateService handler")
	}

	deleteService, err := command.NewDeleteServiceHandler(args.GroupID, args.Storage, args.Auth, args.Logger)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new DeleteService handler")
	}

	updateService, err := command.NewUpdateServiceHandler(args.GroupID, args.Storage, args.Auth, args.Logger)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new UpdateService handler")
	}

	listPeerServices, err := query.NewListPeerServicesHandler(args.Manager, args.Auth, args.Logger)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new ListPeerServices handler")
	}

	listTXLogRecords, err := query.NewListTransactionLogRecordsHandler(args.Manager, args.Auth)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new ListTXLogRecords handler")
	}

	getPeerInfo, err := query.NewGetPeerInfoHandler(ctx, args.Manager, args.Auth)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new GetPeerInfo handler")
	}

	listPeers, err := query.NewListPeersHandler(args.Manager, args.Auth, args.Logger)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new ListPeers handler")
	}

	listInways, err := query.NewListInwaysHandler(&query.NewListInwaysHandlerArgs{
		Storage: args.Storage,
		Auth:    args.Auth,
		GroupID: &args.GroupID,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not create new ListInways handler")
	}

	listOutways, err := query.NewListOutwaysHandler(&query.NewListOutwaysHandlerArgs{
		Storage: args.Storage,
		Auth:    args.Auth,
		GroupID: &args.GroupID,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not create new ListOutways handler")
	}

	application := &Application{
		Commands: Commands{
			AcceptContract: acceptContract,
			RevokeContract: revokeContract,
			RejectContract: rejectContract,
			RetryContract:  retryContract,
			CreateContract: createContract,
			CreateService:  createService,
			DeleteService:  deleteService,
			UpdateService:  updateService,
		},
		Queries: Queries{
			GetContract:               getContract,
			GetService:                getService,
			ListServices:              listServices,
			ListContracts:             listContracts,
			ListContractDistributions: listContractDistributions,
			ListPeerServices:          listPeerServices,
			ListTXLogRecords:          listTXLogRecords,
			ListInways:                listInways,
			GetPeerInfo:               getPeerInfo,
			ListPeers:                 listPeers,
			ListOutways:               listOutways,
		},
	}

	return application, nil
}
