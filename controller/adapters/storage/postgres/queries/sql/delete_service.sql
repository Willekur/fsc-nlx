-- Copyright © VNG Realisatie 2023
-- Licensed under the EUPL

-- name: DeleteService :exec
DELETE
FROM controller.services
WHERE group_id=$1
AND name=$2;
