// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package plugins

import (
	"context"

	"github.com/pkg/errors"
	"go.uber.org/zap"

	"gitlab.com/commonground/nlx/fsc-nlx/common/httperrors"
	"gitlab.com/commonground/nlx/fsc-nlx/common/transactionlog"
	outway_http "gitlab.com/commonground/nlx/fsc-nlx/outway/http"
	"gitlab.com/commonground/nlx/fsc-nlx/txlog-api/domain/record"
)

type LogRecordPlugin struct {
	organizationPeerID string
	txLogger           transactionlog.TransactionLogger
}

func NewLogRecordPlugin(organizationPeerID string, txLogger transactionlog.TransactionLogger) *LogRecordPlugin {
	return &LogRecordPlugin{
		organizationPeerID: organizationPeerID,
		txLogger:           txLogger,
	}
}

func (plugin *LogRecordPlugin) Serve(next ServeFunc) ServeFunc {
	return func(context *Context) error {
		logRecordID, err := plugin.createLogRecord(context)
		if err != nil {
			context.Logger.Error("failed to store transactionlog record", zap.Error(err))

			outway_http.WriteError(context.Response, httperrors.C1, httperrors.LogRecordWriteError())

			return nil
		}

		context.Request.Header.Set("Fsc-Transaction-Id", logRecordID.String())

		return next(context)
	}
}

func (plugin *LogRecordPlugin) createLogRecord(requestContext *Context) (*transactionlog.TransactionID, error) {
	id, err := transactionlog.NewTransactionID()
	if err != nil {
		return nil, errors.Wrap(err, "could not get new request ID")
	}

	var source interface{}
	if requestContext.TokenInfo.OutwayDelegatorPeerID == "" {
		source = &transactionlog.RecordSource{
			OutwayPeerID: requestContext.TokenInfo.OutwayPeerID,
		}
	} else {
		source = &transactionlog.RecordDelegatedSource{
			OutwayPeerID:    requestContext.TokenInfo.OutwayPeerID,
			DelegatorPeerID: requestContext.TokenInfo.OutwayDelegatorPeerID,
		}
	}

	var destination interface{}
	if requestContext.TokenInfo.ServiceDelegatorPeerID == "" {
		destination = &transactionlog.RecordDestination{
			ServicePeerID: requestContext.TokenInfo.ServicePeerID,
		}
	} else {
		destination = &transactionlog.RecordDelegatedDestination{
			ServicePeerID:   requestContext.TokenInfo.ServicePeerID,
			DelegatorPeerID: requestContext.TokenInfo.ServiceDelegatorPeerID,
		}
	}

	rec := &transactionlog.Record{
		TransactionID: id,
		GroupID:       requestContext.TokenInfo.GroupID,
		GrantHash:     requestContext.TokenInfo.GrantHash,
		ServiceName:   requestContext.TokenInfo.ServiceName,
		Direction:     record.DirectionOut,
		Source:        source,
		Destination:   destination,
		CreatedAt:     requestContext.RequestCreatedAt,
		Data: map[string]interface{}{
			"request-path": requestContext.Request.URL.Path,
		},
	}

	if err := plugin.txLogger.AddRecords(context.TODO(), []*transactionlog.Record{rec}); err != nil {
		return nil, errors.Wrap(err, "unable to add records to txlog")
	}

	return id, nil
}
