// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

package internalservice_test

import (
	"context"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	internalapp "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/command"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

type testContractServiceConfig struct {
	t           *testing.T
	clock       clock.Clock
	internalApp *internalapp.Application
}

type testContractService struct {
	config *testContractServiceConfig
}

func newTestContractService(cf *testContractServiceConfig) *testContractService {
	return &testContractService{
		config: cf,
	}
}

func (f *testContractService) New() {
	iv := uuid.New()

	args := &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
		IV:                iv.String(),
		GroupID:           "test-group",
		ContractNotBefore: f.config.clock.Now(),
		ContractNotAfter:  f.config.clock.Now(),
		Grants: []interface{}{
			&command.GrantServicePublicationArgs{
				DirectoryPeerID: peerC.GetPeerID(),
				ServicePeerID:   peerA.GetPeerID(),
				ServiceName:     "parkeerrechten",
				ServiceProtocol: contract.ServiceProtocolTCPHTTP1_1,
			},
		},
		CreatedAt: f.config.clock.Now(),
	}

	_, err := f.config.internalApp.Commands.CreateContract.Handle(context.Background(), args)
	assert.NoError(f.config.t, err)
}

func (f *testContractService) NewWithCreatedAt(createdAt time.Time) uuid.UUID {
	iv := uuid.New()

	args := &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
		IV:                iv.String(),
		GroupID:           "test-group",
		ContractNotBefore: f.config.clock.Now(),
		ContractNotAfter:  f.config.clock.Now(),
		Grants: []interface{}{
			&command.GrantServicePublicationArgs{
				DirectoryPeerID: peerC.GetPeerID(),
				ServicePeerID:   peerA.GetPeerID(),
				ServiceName:     "parkeerrechten",
				ServiceProtocol: contract.ServiceProtocolTCPHTTP1_1,
			},
		},
		CreatedAt: createdAt,
	}

	_, err := f.config.internalApp.Commands.CreateContract.Handle(context.Background(), args)
	assert.NoError(f.config.t, err)

	return iv
}

func (f *testContractService) NewServiceConnectionWithCreatedAt(createdAt time.Time) uuid.UUID {
	iv := uuid.New()

	args := &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
		IV:                iv.String(),
		GroupID:           "test-group",
		ContractNotBefore: f.config.clock.Now(),
		ContractNotAfter:  f.config.clock.Now(),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				CertificateThumbprint: peerB.CertBundle.CertificateThumbprint(),
				OutwayPeerID:          peerB.GetPeerID(),
				ServicePeerID:         peerA.GetPeerID(),
				ServiceName:           "parkeerrechten",
			},
		},
		CreatedAt: createdAt,
	}

	_, err := f.config.internalApp.Commands.CreateContract.Handle(context.Background(), args)
	assert.NoError(f.config.t, err)

	return iv
}

func (f *testContractService) NewServiceConnectionWithCreatedAtAndID(createdAt time.Time, iv uuid.UUID) uuid.UUID {
	args := &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
		IV:                iv.String(),
		GroupID:           "test-group",
		ContractNotBefore: f.config.clock.Now(),
		ContractNotAfter:  f.config.clock.Now(),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				CertificateThumbprint: peerB.CertBundle.CertificateThumbprint(),
				OutwayPeerID:          peerB.GetPeerID(),
				ServicePeerID:         peerA.GetPeerID(),
				ServiceName:           "parkeerrechten",
			},
		},
		CreatedAt: createdAt,
	}

	_, err := f.config.internalApp.Commands.CreateContract.Handle(context.Background(), args)
	assert.NoError(f.config.t, err)

	return iv
}

func (f *testContractService) NewDelegatedServiceConnectionWithCreatedAt(createdAt time.Time) uuid.UUID {
	iv := uuid.New()

	args := &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
		IV:                iv.String(),
		GroupID:           "test-group",
		ContractNotBefore: f.config.clock.Now(),
		ContractNotAfter:  f.config.clock.Now(),
		Grants: []interface{}{
			&command.GrantDelegatedServiceConnectionArgs{
				OutwayCertificateThumbprint: peerB.CertBundle.CertificateThumbprint(),
				OutwayPeerID:                peerB.GetPeerID(),
				ServicePeerID:               peerA.GetPeerID(),
				ServiceName:                 "parkeerrechten",
				DelegatorPeerID:             peerC.GetPeerID(),
			},
		},
		CreatedAt: createdAt,
	}

	_, err := f.config.internalApp.Commands.CreateContract.Handle(context.Background(), args)
	assert.NoError(f.config.t, err)

	return iv
}
