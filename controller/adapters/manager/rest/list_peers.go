/*
 * Copyright © VNG Realisatie 2023
 * Licensed under the EUPL
 *
 */

package restmanager

import (
	"context"
	"net/http"

	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
)

func (m *restManager) ListPeers(ctx context.Context) (manager.Peers, error) {
	res, err := m.client.GetPeersWithResponse(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "could not retrieve peers from rest manager")
	}

	err = mapResponse(res, http.StatusOK, "could not get peers", res.Body)
	if err != nil {
		return nil, err
	}

	peers := make(manager.Peers, 0, len(res.JSON200.Peers))

	for _, peer := range res.JSON200.Peers {
		peers = append(peers, &manager.Peer{
			ID:   peer.Id,
			Name: peer.Name,
		})
	}

	return peers, nil
}
