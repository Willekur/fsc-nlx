// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"

	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/rest/api"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/rest/api/models"
)

func (s *Server) GetServices(ctx context.Context, _ api.GetServicesRequestObject) (api.GetServicesResponseObject, error) {
	s.logger.Info("rest request GetServices")

	authData := authentication.NoneData{}
	peerInfo := s.getPeerInfo(ctx, &authData)

	services, err := s.app.Queries.ListServices.Handle(ctx, &query.ListServicesArgs{
		AuthData:   &authData,
		SelfPeerID: peerInfo.ID,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not obtain available services")
	}

	resp := api.GetServices200JSONResponse{}

	for _, s := range services {
		endpointURL := s.EndpointURL.String()

		resp.Services = append(resp.Services, models.Service{
			Name:         s.Name,
			EndpointUrl:  endpointURL,
			InwayAddress: s.InwayAddress,
		})
	}

	return resp, nil
}
