// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"fmt"
	"net/http"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
)

// nolint:dupl // this is not a duplicate of the directory
func (s *Server) contractsHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	grantTypeFilter := r.URL.Query().Get("grant_type")

	resp, err := s.apiApp.Queries.ListContracts.Handle(ctx, &query.ListContractsArgs{
		AuthData:              authData,
		AuthorizationMetadata: getAuthzData(r),
		Filters: []*query.ListContractsFilter{{
			GrantType: query.GrantType(grantTypeFilter),
		}},
	})

	if err != nil {
		s.logger.Error("could not render contracts page", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	contracts := mapQueryToUIContracts(resp)

	page := contractsPage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathContractsPage,
			Title:                       s.i18n.Translate("Contracts"),
			Description:                 s.i18n.Translate("Manage the contracts of your organization."),
			Username:                    authData.Username(),
			LogoutURL:                   s.authn.LogoutURL(),
			Peer:                        s.getPeerInfo(ctx, getAuthzData(r), authData),
		},
		GrantTypeFilters: getGrantTypeFilters(),
		GrantType:        grantTypeFilter,
		Contracts:        contracts,
	}

	err = page.render(w)
	if err != nil {
		s.logger.Error("could not render contracts page", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}
}

func mapQueryToUIContracts(qContracts query.Contracts) []*contractsPageContract {
	contracts := make([]*contractsPageContract, len(qContracts))

	for i, c := range qContracts {
		contract := &contractsPageContract{
			IV:                                c.IV,
			IsActive:                          c.State == query.ContractStateValid,
			Hash:                              c.Hash,
			CreatedAt:                         c.CreatedAt.UTC(),
			ServicePublicationGrants:          make([]*ServicePublicationGrant, len(c.ServicePublicationGrants)),
			ServiceConnectionGrants:           make([]*ServiceConnectionGrant, len(c.ServiceConnectionGrants)),
			DelegatedServicePublicationGrants: make([]*DelegatedServicePublicationGrant, len(c.DelegatedServicePublicationGrants)),
			DelegatedServiceConnectionGrants:  make([]*DelegatedServiceConnectionGrant, len(c.DelegatedServiceConnectionGrants)),
		}

		for i, grant := range c.ServicePublicationGrants {
			contract.ServicePublicationGrants[i] = &ServicePublicationGrant{
				DirectoryPeerID:   grant.DirectoryPeer.ID,
				DirectoryPeerName: grant.DirectoryPeer.Name,
				ServicePeerID:     grant.ServicePeer.ID,
				ServicePeerName:   grant.ServicePeer.Name,
				ServiceName:       grant.ServiceName,
			}
		}

		for i, grant := range c.ServiceConnectionGrants {
			contract.ServiceConnectionGrants[i] = &ServiceConnectionGrant{
				ServicePeerID:               grant.ServicePeer.ID,
				ServicePeerName:             grant.ServicePeer.Name,
				ServiceName:                 grant.ServiceName,
				OutwayPeerID:                grant.OutwayPeer.ID,
				OutwayPeerName:              grant.OutwayPeer.Name,
				OutwayCertificateThumbprint: grant.OutwayCertificateThumbprint,
			}
		}

		for i, grant := range c.DelegatedServicePublicationGrants {
			contract.DelegatedServicePublicationGrants[i] = &DelegatedServicePublicationGrant{
				DirectoryPeerID:   grant.DirectoryPeer.ID,
				DirectoryPeerName: grant.DirectoryPeer.Name,
				DelegatorPeerID:   grant.DelegatorPeer.ID,
				DelegatorPeerName: grant.DelegatorPeer.Name,
				ServicePeerID:     grant.ServicePeer.ID,
				ServicePeerName:   grant.ServicePeer.Name,
				ServiceName:       grant.ServiceName,
			}
		}

		for i, grant := range c.DelegatedServiceConnectionGrants {
			contract.DelegatedServiceConnectionGrants[i] = &DelegatedServiceConnectionGrant{
				DelegatorPeerID:             grant.DelegatorPeer.ID,
				DelegatorPeerName:           grant.DelegatorPeer.Name,
				OutwayPeerID:                grant.OutwayPeer.ID,
				OutwayPeerName:              grant.OutwayPeer.Name,
				OutwayCertificateThumbprint: grant.OutwayCertificateThumbprint,
				ServicePeerID:               grant.ServicePeer.ID,
				ServicePeerName:             grant.ServicePeer.Name,
				ServiceName:                 grant.ServiceName,
			}
		}

		contracts[i] = contract
	}

	return contracts
}

func getGrantTypeFilters() map[query.GrantType]string {
	return map[query.GrantType]string{
		query.GrantTypeServicePublication:          "Service Publication",
		query.GrantTypeServiceConnection:           "Service Connection",
		query.GrantTypeDelegatedServicePublication: "Delegated Service Publication",
		query.GrantTypeDelegatedServiceConnection:  "Delegated Service Connection",
	}
}
