// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	internalapp "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/intunauthenticated/rest/api/server"
)

type Server struct {
	app     *internalapp.Application
	logger  *logger.Logger
	handler http.Handler
}

type NewArgs struct {
	Logger *logger.Logger
	App    *internalapp.Application
}

func New(args *NewArgs) (*Server, error) {
	if args.Logger == nil {
		return nil, errors.New("logger is required")
	}

	if args.App == nil {
		return nil, errors.New("app is required")
	}

	s := &Server{
		app:    args.App,
		logger: args.Logger,
	}

	strict := api.NewStrictHandlerWithOptions(s, []api.StrictMiddlewareFunc{}, api.StrictHTTPServerOptions{
		RequestErrorHandlerFunc:  RequestErrorHandler,
		ResponseErrorHandlerFunc: ResponseErrorHandler,
	})

	r := chi.NewRouter()
	api.HandlerFromMux(strict, r)

	s.handler = r

	return s, nil
}

func (s *Server) Handler() http.Handler {
	return s.handler
}
