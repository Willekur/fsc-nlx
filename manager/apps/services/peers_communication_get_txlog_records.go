// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package services

import (
	"context"
	"errors"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func (p *PeersCommunicationService) GetTXLogRecords(ctx context.Context, peerID contract.PeerID) (contract.TXLogRecords, error) {
	peer, err := p.getManagerClient(peerID)
	if err != nil {
		return nil, fmt.Errorf("could not get client for peer %q to get tx log records: %w", peerID.Value(), err)
	}

	records, err := peer.GetTXLogRecords(ctx)
	if err != nil {
		return nil, errors.Join(err, fmt.Errorf("could not get tx log records from peer: %s", peerID.Value()))
	}

	return records, nil
}
