// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"errors"
	"fmt"
	"time"

	internalapp_errors "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/errors"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type ListContractDistributionsHandler struct {
	repo contract.Repository
}

type ListContractDistributions []*ListContractDistribution

type ListContractDistribution struct {
	ContractHash string
	PeerID       string
	Action       DistributionAction
	Signature    string
	ErrorMessage string
	Retries      int32
	LastRetryAt  time.Time
	NextRetryAt  *time.Time
}

type DistributionAction string

const (
	DistributionActionSubmitContract        DistributionAction = "submit_contract"
	DistributionActionSubmitRevokeSignature DistributionAction = "submit_revoke_signature"
	DistributionActionSubmitRejectSignature DistributionAction = "submit_reject_signature"
	DistributionActionSubmitAcceptSignature DistributionAction = "submit_accept_signature"
)

func NewListContractDistributionsHandler(repository contract.Repository) (*ListContractDistributionsHandler, error) {
	if repository == nil {
		return nil, errors.New("repository is required")
	}

	return &ListContractDistributionsHandler{
		repo: repository,
	}, nil
}

func (h *ListContractDistributionsHandler) Handle(ctx context.Context, contentHash string) (ListContractDistributions, error) {
	hash, err := contract.NewContentHashFromString(contentHash)
	if err != nil {
		return nil, internalapp_errors.NewIncorrectInputError(err)
	}

	cds, err := h.repo.ListContractDistributions(ctx, hash)
	if err != nil {
		return nil, errors.Join(err, fmt.Errorf("could not retrieve contract distributions from the database"))
	}

	distributions := make(ListContractDistributions, len(cds))

	for i, cd := range cds {
		distributions[i] = &ListContractDistribution{
			ContractHash: cd.ContentHash.String(),
			PeerID:       cd.PeerID.Value(),
			Action:       DistributionAction(cd.Action),
			Signature:    cd.Signature,
			Retries:      cd.Retries,
			ErrorMessage: cd.ErrorMessage,
			LastRetryAt:  cd.LastRetryAt,
			NextRetryAt:  cd.NextRetryAt,
		}
	}

	return distributions, nil
}
