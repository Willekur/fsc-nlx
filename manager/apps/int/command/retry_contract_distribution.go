// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package command

import (
	"context"
	"errors"
	"fmt"

	"github.com/go-jose/go-jose/v3"

	internalapp_errors "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/errors"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/services"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type RetryContractDistributionHandler struct {
	repository               contract.Repository
	peerCommunicationService *services.PeersCommunicationService
}

type NewRetryContractDistributionHandlerArgs struct {
	Repository               contract.Repository
	PeerCommunicationService *services.PeersCommunicationService
}

type RetryContractDistributionHandlerArgs struct {
	ContentHash string
	PeerID      string
	Action      Action
}

type Action int

const (
	DistributionActionSubmitContract Action = iota
	DistributionActionSubmitAcceptSignature
	DistributionActionSubmitRejectSignature
	DistributionActionSubmitRevokeSignature
)

func NewRetryContractDistributionHandler(args *NewRetryContractDistributionHandlerArgs) (*RetryContractDistributionHandler, error) {
	if args.Repository == nil {
		return nil, errors.New("repository is required")
	}

	if args.PeerCommunicationService == nil {
		return nil, errors.New("peerCommunicationService is required")
	}

	return &RetryContractDistributionHandler{
		repository:               args.Repository,
		peerCommunicationService: args.PeerCommunicationService,
	}, nil
}

func (h *RetryContractDistributionHandler) Handle(ctx context.Context, args *RetryContractDistributionHandlerArgs) error {
	hash, err := contract.NewContentHashFromString(args.ContentHash)
	if err != nil {
		return internalapp_errors.NewIncorrectInputError(err)
	}

	pID, err := contract.NewPeerID(args.PeerID)
	if err != nil {
		return internalapp_errors.NewIncorrectInputError(err)
	}

	distributionAction, err := convertAction(args.Action)
	if err != nil {
		return internalapp_errors.NewIncorrectInputError(err)
	}

	cd, err := h.repository.GetContractDistribution(ctx, hash, pID, distributionAction)
	if err != nil {
		return errors.Join(err, fmt.Errorf("could not retrieve contract distribution"))
	}

	contractContent, err := h.repository.GetContentByHash(ctx, cd.ContentHash)
	if err != nil {
		return errors.Join(err, fmt.Errorf("could not get contract content of hash %q", cd.ContentHash.String()))
	}

	certificateThumbprint, err := getCertificateThumbprintFromSignature(cd.Signature)
	if err != nil {
		return errors.Join(err, fmt.Errorf("could not get certificate thumbprint from signature"))
	}

	peerCerts, err := h.repository.GetPeersCertsByCertThumbprints(ctx, contract.CertificateThumbprints{contract.CertificateThumbprint(certificateThumbprint)})
	if err != nil {
		return errors.Join(err, fmt.Errorf("could not get peer certs by certificate thumbprint"))
	}

	err = h.retry(ctx, distributionAction, cd, contractContent, peerCerts)
	if err != nil {
		return errors.Join(err, fmt.Errorf("could not retry contract distribution"))
	}

	err = h.repository.DeleteContractDistributions(ctx, pID, hash, distributionAction)
	if err != nil {
		return errors.Join(err, fmt.Errorf("could not delete contract distribution"))
	}

	return nil
}

func convertAction(action Action) (contract.DistributionAction, error) {
	switch action {
	case DistributionActionSubmitContract:
		return contract.DistributionActionSubmitContract, nil
	case DistributionActionSubmitAcceptSignature:
		return contract.DistributionActionSubmitAcceptSignature, nil
	case DistributionActionSubmitRevokeSignature:
		return contract.DistributionActionSubmitRevokeSignature, nil
	case DistributionActionSubmitRejectSignature:
		return contract.DistributionActionSubmitRejectSignature, nil
	default:
		return "", errors.New("provided ContractDistributionRetry Action is not valid")
	}
}

//nolint:gocyclo // retry is a complex function because the many distribution actions
func (h *RetryContractDistributionHandler) retry(ctx context.Context, a contract.DistributionAction, cd *contract.Distribution, contractContent *contract.Content, peerCerts *contract.PeersCertificates) error {
	peerIDs, err := contract.NewPeerIDs([]string{cd.PeerID.Value()})
	if err != nil {
		return errors.Join(err, fmt.Errorf("could not parse peer ID %q", cd.PeerID))
	}

	switch a {
	case contract.DistributionActionSubmitRejectSignature:
		rejectSignature, errCreateSig := contract.NewSignature(&contract.NewSignatureArgs{
			SigType:    contract.SignatureTypeReject,
			Signature:  cd.Signature,
			Content:    contractContent,
			PeersCerts: *peerCerts,
		})
		if errCreateSig != nil {
			return errors.Join(errCreateSig, fmt.Errorf("could not create reject signature"))
		}

		err = h.peerCommunicationService.RejectContract(ctx, peerIDs, contractContent, rejectSignature)
		if err != nil {
			return errors.Join(err, fmt.Errorf("could not reject contract"))
		}

	case contract.DistributionActionSubmitAcceptSignature:
		acceptSignature, errCreateSig := contract.NewSignature(&contract.NewSignatureArgs{
			SigType:    contract.SignatureTypeAccept,
			Signature:  cd.Signature,
			Content:    contractContent,
			PeersCerts: *peerCerts,
		})
		if errCreateSig != nil {
			return errors.Join(errCreateSig, fmt.Errorf("could not create accept signature"))
		}

		err = h.peerCommunicationService.AcceptContract(ctx, peerIDs, contractContent, acceptSignature)
		if err != nil {
			return errors.Join(err, fmt.Errorf("could not accept contract"))
		}

	case contract.DistributionActionSubmitRevokeSignature:
		revokeSignature, errCreateSig := contract.NewSignature(&contract.NewSignatureArgs{
			SigType:    contract.SignatureTypeRevoke,
			Signature:  cd.Signature,
			Content:    contractContent,
			PeersCerts: *peerCerts,
		})
		if errCreateSig != nil {
			return errors.Join(errCreateSig, fmt.Errorf("could not create accept signature"))
		}

		err = h.peerCommunicationService.RevokeContract(ctx, peerIDs, contractContent, revokeSignature)
		if err != nil {
			return errors.Join(err, fmt.Errorf("could not revoke contract"))
		}
	case contract.DistributionActionSubmitContract:
		acceptSignature, errCreateSig := contract.NewSignature(&contract.NewSignatureArgs{
			SigType:    contract.SignatureTypeAccept,
			Signature:  cd.Signature,
			Content:    contractContent,
			PeersCerts: *peerCerts,
		})
		if errCreateSig != nil {
			return errors.Join(errCreateSig, fmt.Errorf("could not create accept signature"))
		}

		err = h.peerCommunicationService.SubmitContract(ctx, peerIDs, contractContent, acceptSignature)
		if err != nil {
			return errors.Join(err, fmt.Errorf("could not submit contract"))
		}
	default:
		return fmt.Errorf("unexpected action %s", a)
	}

	return nil
}

func getCertificateThumbprintFromSignature(signature string) (string, error) {
	jws, err := jose.ParseSigned(signature)
	if err != nil {
		return "", fmt.Errorf("could not parse signature: %v", err)
	}

	if len(jws.Signatures) != 1 {
		return "", fmt.Errorf("exactly one signature is required, found either zero or multiple signatures in jws token")
	}

	header, ok := jws.Signatures[0].Header.ExtraHeaders["x5t#S256"]
	if !ok {
		return "", fmt.Errorf("could not find required header in jws signature: x5t#S256")
	}

	pubKeyFingerPrintString, ok := header.(string)
	if !ok {
		return "", fmt.Errorf("certificate thumbprint must be of type string")
	}

	return pubKeyFingerPrintString, nil
}
