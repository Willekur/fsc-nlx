/*
 * Copyright © VNG Realisatie 2024
 * Licensed under the EUPL
 */

package uiport

import (
	"errors"
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
)

func (s *Server) contractDistributionHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	contentHash := chi.URLParam(r, "hash")
	peerID := chi.URLParam(r, "peerID")

	page := contractDistributionPage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathContractsPage,
		},
		PeerID: peerID,
	}

	peers := s.getPeerIDs(ctx, getAuthzData(r), authData)

	contractDistributions, err := s.apiApp.Queries.ListContractDistributions.Handle(ctx, &query.ListContractDistributionsArgs{
		AuthData:              authData,
		AuthorizationMetadata: getAuthzData(r),
		ContentHash:           contentHash,
	})
	if err != nil {
		if errors.Is(err, manager.ErrContractDistributionsNotFoundError) {
			page.renderNotFound(w)
			return
		}

		s.logger.Error("could not get contractDistributions", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	contractDistributionForPeer := contractDistributions[peerID]

	contractDistributionPerAction := make(map[Action]*contractDistributionDetails)
	for action, contractDistribution := range contractDistributionForPeer {
		contractDistributionPerAction[convertActionToHumanReadableText(action)] = &contractDistributionDetails{
			ContentHash:  contractDistribution.ContentHash,
			Signature:    contractDistribution.Signature,
			Action:       convertActionToHumanReadableText(contractDistribution.Action),
			Retries:      contractDistribution.Retries,
			LastRetry:    contractDistribution.LastRetry,
			ErrorMessage: contractDistribution.ErrorMessage,
		}
	}

	page.PeerName = peers[peerID]
	page.ContentHash = contentHash
	page.ContractDistribution = contractDistributionPerAction

	page.render(w)
}
