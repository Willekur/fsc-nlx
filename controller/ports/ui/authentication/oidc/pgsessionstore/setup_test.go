// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package pgsessionstore_test

import (
	"os"
	"sync"
	"testing"

	"github.com/DATA-DOG/go-txdb"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/require"

	discardlogger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/discard"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/ui/authentication/oidc/pgsessionstore"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/ui/authentication/oidc/pgsessionstore/migrations"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

var setupOnce sync.Once

const dbDriver = "txdb"

// nolint:gocritic // this is a helper function
func setup(t *testing.T) {
	setupOnce.Do(func() {
		setupPostgreSQL(t)
	})
}

// nolint:gocritic // this is a helper function
func setupPostgreSQL(t *testing.T) {
	dbName := "test_httpsessions"

	dsnBase := os.Getenv("POSTGRES_DSN")

	dsn, err := testingutils.CreateTestDatabase(dsnBase, dbName)
	if err != nil {
		t.Fatal(err)
	}

	dsnForMigrations := testingutils.AddQueryParamToAddress(dsn, "x-migrations-table", dbName)

	err = migrations.PerformMigrations(dsnForMigrations)
	if err != nil {
		t.Fatal(err)
	}

	txdb.Register(dbDriver, "postgres", dsn)
}

func newDB(t *testing.T, id string) *sqlx.DB {
	db, err := sqlx.Open(dbDriver, id)
	require.NoError(t, err)

	return db
}

func New(t *testing.T, secret string) *pgsessionstore.PGStore {
	setup(t)

	s, err := pgsessionstore.New(discardlogger.New(), newDB(t, t.Name()), []byte(secret))
	require.NoError(t, err)

	return s
}
