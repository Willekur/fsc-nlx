// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//nolint:dupl // looks like revoke contract but is different
package command

import (
	"context"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

type AcceptContractHandler struct {
	manager manager.Manager
	auth    *auth.Auth
	lgr     *logger.Logger
}

func NewAcceptContractHandler(m manager.Manager, a *auth.Auth, l *logger.Logger) (*AcceptContractHandler, error) {
	if m == nil {
		return nil, fmt.Errorf("manager is required")
	}

	if a == nil {
		return nil, fmt.Errorf("auth is required")
	}

	if l == nil {
		return nil, fmt.Errorf("logger is required")
	}

	return &AcceptContractHandler{
		manager: m,
		auth:    a,
		lgr:     l,
	}, nil
}

type AcceptContractArgs struct {
	AuthData              authentication.Data
	AuthorizationMetadata authorization.Metadata
	ContentHash           string
}

func (h *AcceptContractHandler) Handle(ctx context.Context, args *AcceptContractArgs) error {
	err := h.auth.Auth(ctx, args.AuthData, args.AuthorizationMetadata, authz.FscAcceptContract, []*authz.ResourceURN{
		authz.NewResourceURN(authz.ResourceTypeContract, args.ContentHash),
	})
	if err != nil {
		return mapError(err, "could not authenticate or authorize")
	}

	if args.ContentHash == "" {
		return newValidationError("contentHash cannot be empty")
	}

	err = h.manager.AcceptContract(ctx, args.ContentHash)
	if err != nil {
		h.lgr.Error("failed to accept contract", err)

		return mapError(err, "could not accept contract in manager")
	}

	return nil
}
