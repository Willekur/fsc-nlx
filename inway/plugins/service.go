// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package plugins

type Service struct {
	Grants      []*Grant `json:"grants"`
	Name        string   `json:"name"`
	EndpointURL string   `json:"endpoint_url"`
}

type Grant struct {
	OrganizationPeerID    string `json:"organization_serial_number"`
	PublicKeyPEM          string `json:"public_key_pem"`
	CertificateThumbprint string `json:"certificate_thumbprint"`
}
