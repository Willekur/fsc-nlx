// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"context"
	"fmt"
	"net/http"

	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
	auth "gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/ui/authentication"
)

func mapError(err error) (statusCode int, message string) {
	var e *command.ValidationError
	if errors.As(err, &e) {
		return http.StatusBadRequest, e.Error()
	}

	return http.StatusInternalServerError, fmt.Sprintf("unexpected error: %s", err)
}

func (s *Server) getAuthDataFromCtx(ctx context.Context, w http.ResponseWriter) (auth.Data, bool) {
	v := ctx.Value(authentication.ContextAuthenticationDataKey)
	if v == nil {
		s.logger.Error("failed to authenticate", fmt.Errorf("this route needs to be added to the OnlyAuthenticated middleware"))
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return nil, false
	}

	switch a := v.(type) {
	case *auth.OIDCData:
		return a, true
	case *auth.NoneData:
		return a, true
	default:
		s.logger.Error("failed to authenticate", fmt.Errorf("invalid authenticationData type: %T", a))
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return nil, false
	}
}

func getAuthzData(r *http.Request) *authorization.RestMetaData {
	return &authorization.RestMetaData{
		Headers: r.Header,
		URL:     r.URL.String(),
	}
}

func (s *Server) getPeerInfo(ctx context.Context, authzData *authorization.RestMetaData, authData auth.Data) *PeerInfo {
	if s.peerInfo != nil {
		return s.peerInfo
	}

	p, err := s.apiApp.Queries.GetPeerInfo.Handle(ctx, &query.GetPeerInfoArgs{
		AuthData:              authData,
		AuthorizationMetadata: authzData,
	})
	if err != nil {
		// this info is not important enough to error on
		return &PeerInfo{
			ID:   "",
			Name: "",
		}
	}

	peerInfo := &PeerInfo{
		ID:   p.ID,
		Name: p.Name,
	}

	s.peerInfo = peerInfo

	return peerInfo
}
