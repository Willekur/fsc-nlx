// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"

	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
)

func (s *Server) RevokeContract(ctx context.Context, req api.RevokeContractRequestObject) (api.RevokeContractResponseObject, error) {
	s.logger.Info("rest request RevokeContract")

	err := s.app.Commands.RevokeContract.Handle(ctx,
		req.Hash,
	)
	if err != nil {
		s.logger.Error("error executing revoke contract command", err)
		return nil, err
	}

	return api.RevokeContract204Response{}, nil
}
