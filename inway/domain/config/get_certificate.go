// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package config

import (
	"context"
	"fmt"

	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func (c *Config) GetCertificate(ctx context.Context, thumbPrint contract.CertificateThumbprint) (*contract.PeerCertificate, error) {
	c.certificates.mut.RLock()
	cert, ok := c.certificates.certificates[thumbPrint]
	c.certificates.mut.RUnlock()

	if ok {
		return cert, nil
	}

	certificate, err, _ := c.certificates.getCertificate.Do(thumbPrint.Value(), func() (interface{}, error) {
		peerCerts, errCerts := c.repo.GetCertificates(ctx)
		if errCerts != nil {
			return nil, errors.Wrap(errCerts, "unable to get certificates from repository")
		}

		c.certificates.mut.Lock()

		c.certificates.certificates = *peerCerts

		c.certificates.mut.Unlock()

		cert, err := peerCerts.GetCertificate(thumbPrint)
		if err != nil {
			return nil, fmt.Errorf("peer cert with thumbprint %q not found in jwks set: %w", thumbPrint.Value(), err)
		}

		return cert, nil
	})
	if err != nil {
		return nil, err
	}

	return certificate.(*contract.PeerCertificate), nil
}
