Tests
---

The System Under Test (SUT) is an External Manager API from Organization A.
We spin up a new instance for every test. A new database is created per test.

Each test will test an API endpoint of the External Manager using the same pattern.
We will send the requests using the certificates from Organization B.

1. Arrange: prepare input data for the HTTP request
2. Act: perform the HTTP request
3. Assert: validate the expected outcomes. Most of the time this will require fetching data from the External Manager.
