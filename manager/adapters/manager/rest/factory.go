// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	common_tls "gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/manager"
)

type Factory struct {
	certs       *common_tls.CertificateBundle
	selfAddress string
	crlCache    *common_tls.CRLsCache
}

func NewFactory(certs *common_tls.CertificateBundle, selfAddress string, crlCache *common_tls.CRLsCache) *Factory {
	return &Factory{certs: certs, selfAddress: selfAddress, crlCache: crlCache}
}

type NewManagerArgs struct {
}

func (f *Factory) New(address string) (manager.Manager, error) {
	return NewClient(f.selfAddress, address, f.certs, f.crlCache)
}
