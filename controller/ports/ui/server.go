// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"context"
	"embed"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/pkg/errors"
	"github.com/zitadel/schema"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/ui/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/ui/i18n"
	jsoni18n "gitlab.com/commonground/nlx/fsc-nlx/controller/ports/ui/i18n/json"
)

//go:embed templates/**
var tplFolder embed.FS

var decoder = schema.NewDecoder()

type Server struct {
	locale     string
	staticPath string
	apiApp     *apiapp.Application
	i18n       i18n.I18n
	logger     *logger.Logger
	httpServer *http.Server
	basePage   *BasePage
	peerInfo   *PeerInfo
	authn      authentication.Authenticator
}

type NewServerArgs struct {
	Locale        string
	StaticPath    string
	GroupID       string
	Logger        *logger.Logger
	APIApp        *apiapp.Application
	Authenticator authentication.Authenticator
}

func New(_ context.Context, args *NewServerArgs) (*Server, error) {
	if args.Locale == "" {
		return nil, fmt.Errorf("locale must be set (nl/en)")
	}

	if args.Logger == nil {
		return nil, fmt.Errorf("logger cannot be nil")
	}

	if args.APIApp == nil {
		return nil, fmt.Errorf("API app cannot be nil")
	}

	if args.Authenticator == nil {
		return nil, fmt.Errorf("authenticator cannot be nil")
	}

	translations, err := jsoni18n.New(args.Locale)
	if err != nil {
		return nil, errors.Wrap(err, "unable to create new json i18n instance")
	}

	basePage, err := NewBasePage(args.StaticPath, translations, args.GroupID, args.Logger)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create base page")
	}

	server := &Server{
		locale:     args.Locale,
		staticPath: args.StaticPath,
		logger:     args.Logger,
		i18n:       translations,
		apiApp:     args.APIApp,
		basePage:   basePage,
		authn:      args.Authenticator,
	}

	return server, nil
}

const compressionLevel = 5

func (s *Server) ListenAndServe(address string) error {
	r := chi.NewRouter()
	r.Use(middleware.Compress(compressionLevel))
	r.Use(middleware.Logger)
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/directory", http.StatusTemporaryRedirect)
	})
	r.Get("/services", s.authn.OnlyAuthenticated(s.servicesHandler))
	r.Get("/services/{name}", s.authn.OnlyAuthenticated(s.serviceDetailHandler))
	r.Get("/services/add-service", s.authn.OnlyAuthenticated(s.addServiceGetHandler))
	r.Post("/services/add-service", s.authn.OnlyAuthenticated(s.addServicePostHandler))
	r.Get("/services/{name}/edit", s.authn.OnlyAuthenticated(s.updateServiceGetHandler))
	r.Post("/services/{name}/edit", s.authn.OnlyAuthenticated(s.updateServicePostHandler))
	r.Get("/services/{name}/publish", s.authn.OnlyAuthenticated(s.publishServiceGetHandler))
	r.Post("/services/{name}/publish", s.authn.OnlyAuthenticated(s.publishServicePostHandler))
	r.Get("/directory", s.authn.OnlyAuthenticated(s.directoryHandler))
	r.Get("/directory/{peerID}/{serviceName}/delegator/{delegatorID}", s.authn.OnlyAuthenticated(s.directoryServiceDetailHandler))
	r.Get("/directory/{peerID}/{serviceName}", s.authn.OnlyAuthenticated(s.directoryServiceDetailHandler))
	r.Get("/contracts", s.authn.OnlyAuthenticated(s.contractsHandler))
	r.Get("/contract/{hash}", s.authn.OnlyAuthenticated(s.contractDetailHandler))
	r.Get("/contract/{hash}/distributions/{peerID}", s.authn.OnlyAuthenticated(s.contractDistributionHandler))
	r.Post("/contract/{hash}/retries/{peerID}/{action}", s.authn.OnlyAuthenticated(s.contractRetryHandler))
	r.Get("/contracts/add", s.authn.OnlyAuthenticated(s.addContractGetHandler))
	r.Post("/contracts/add", s.authn.OnlyAuthenticated(s.addContractPostHandler))
	r.Post("/contracts/{hash}/sign", s.authn.OnlyAuthenticated(s.signContractPostHandler))
	r.Get("/transaction-logs", s.authn.OnlyAuthenticated(s.transactionLogsHandler))

	s.authn.MountRoutes(r)

	filesDir := http.Dir(s.staticPath)
	r.Handle("/*", http.FileServer(filesDir))

	const readHeaderTimeout = 5 * time.Second

	s.httpServer = &http.Server{
		Addr:              address,
		Handler:           r,
		ReadHeaderTimeout: readHeaderTimeout,
	}

	err := s.httpServer.ListenAndServe()
	if err != http.ErrServerClosed {
		return err
	}

	return nil
}

func (s *Server) Shutdown(ctx context.Context) error {
	err := s.httpServer.Shutdown(ctx)
	if err != http.ErrServerClosed {
		return err
	}

	return nil
}
