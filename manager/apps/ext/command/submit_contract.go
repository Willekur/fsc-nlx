// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package command

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"fmt"

	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	common_tls "gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	app_errors "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/ext/errors"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/services"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type SubmitContractHandler struct {
	groupID                   contract.GroupID
	trustedRootCAs            *x509.CertPool
	clock                     clock.Clock
	repository                contract.Repository
	logger                    *logger.Logger
	peersCommunicationService *services.PeersCommunicationService
	autoSignGrants            map[contract.GrantType]bool
	selfPeerID                contract.PeerID
	autoSignCertificate       *tls.Certificate
}

type NewSubmitContractHandlerArgs struct {
	SelfPeerID                contract.PeerID
	GroupID                   contract.GroupID
	AutoSignGrants            []string
	TrustedRootCAs            *x509.CertPool
	Repository                contract.Repository
	PeersCommunicationService *services.PeersCommunicationService
	Logger                    *logger.Logger
	Clock                     clock.Clock
	AutoSignCertificate       *tls.Certificate
}

func NewSubmitContractHandler(args *NewSubmitContractHandlerArgs) (*SubmitContractHandler, error) {
	if args.GroupID == "" {
		return nil, fmt.Errorf("groupID is required")
	}

	if args.TrustedRootCAs == nil {
		return nil, errors.New("trustedRootCAs is required")
	}

	if args.Repository == nil {
		return nil, errors.New("repository is required")
	}

	if args.Clock == nil {
		return nil, errors.New("clock is required")
	}

	if args.Logger == nil {
		return nil, errors.New("logger is required")
	}

	if args.PeersCommunicationService == nil {
		return nil, errors.New("peers communication service is required")
	}

	a := make(map[contract.GrantType]bool)

	for _, g := range args.AutoSignGrants {
		t, err := contract.NewGrantTypeFromString(g)
		if err != nil {
			return nil, fmt.Errorf("could not convert grant type from string: %w", err)
		}

		a[t] = true
	}

	return &SubmitContractHandler{
		groupID:                   args.GroupID,
		trustedRootCAs:            args.TrustedRootCAs,
		repository:                args.Repository,
		clock:                     args.Clock,
		logger:                    args.Logger,
		peersCommunicationService: args.PeersCommunicationService,
		selfPeerID:                args.SelfPeerID,
		autoSignCertificate:       args.AutoSignCertificate,
		autoSignGrants:            a,
	}, nil
}

type HandleSubmitContractArgs struct {
	ContractContent *ContractContentArgs
	Signature       string
	SubmittedByPeer *PeerArgs
	PeerCert        *common_tls.CertificateBundle
}

func (h *SubmitContractHandler) Handle(ctx context.Context, args *HandleSubmitContractArgs) error {
	submittedByPeer, err := peerToModel(args.SubmittedByPeer)
	if err != nil {
		return app_errors.NewIncorrectInputError(err)
	}

	err = h.repository.UpsertPeer(ctx, submittedByPeer)
	if err != nil {
		return errors.Wrapf(err, "could not upsert peer: %s into repository", submittedByPeer.ID())
	}

	content, err := contentToModel(args.ContractContent)
	if err != nil {
		return app_errors.FromDomainValidationError(err)
	}

	if content.GroupID() != h.groupID.String() {
		return app_errors.NewIncorrectGroupIDError(fmt.Errorf("wrong group ID in contract, want: %q, got: %q", h.groupID, content.GroupID()))
	}

	if !content.ContainsPeer(submittedByPeer.ID()) {
		return app_errors.NewPeerNotPartOfContractError(contract.ErrorSubmittingPeerNotPartOfContract(submittedByPeer.ID().Value()))
	}

	if !content.ContainsPeer(h.selfPeerID) {
		return app_errors.NewPeerNotPartOfContractError(contract.ErrorReceivingPeerNotPartOfContract(h.selfPeerID.Value()))
	}

	if err != nil {
		return app_errors.NewIncorrectInputError(err)
	}

	CertificateThumbprint, err := getCertificateThumbprintFromSignature(args.Signature)
	if err != nil {
		return app_errors.NewCertificateVerificationFailedError(fmt.Errorf("could not get certificate thumbprint from signature: %s.%s", args.Signature, err))
	}

	var certs contract.PeersCertificates

	if h.selfPeerID.Value() != args.SubmittedByPeer.ID {
		certsToRequest := make(map[contract.PeerID][]string)
		certsToRequest[submittedByPeer.ID()] = []string{CertificateThumbprint}

		certs, err = h.peersCommunicationService.GetCertificates(ctx, certsToRequest)
		if err != nil {
			h.logger.Info(fmt.Sprintf("unable to retrieve certificate from peer %q: %s", submittedByPeer.ID().Value(), err))
			return app_errors.NewCertificateVerificationFailedError(errors.Errorf("unable to retrieve certificate from Peer %q", submittedByPeer.ID().Value()))
		}
	} else {
		certs, err = contract.NewPeersCertificates(h.trustedRootCAs, contract.NewPeersCertificatesArgs{
			h.selfPeerID.Value(): contract.RawDERPeerCertificates{
				args.PeerCert.Certificate().Raw,
			},
		})
		if err != nil {
			h.logger.Error("unable to create PeersCertificates", err)
			return err
		}
	}

	sig, err := contract.NewSignature(
		&contract.NewSignatureArgs{
			SigType:    contract.SignatureTypeAccept,
			Signature:  args.Signature,
			Content:    content,
			PeersCerts: certs,
		})
	if err != nil {
		return app_errors.FromDomainValidationError(err)
	}

	if !sig.Peer().IsEqual(submittedByPeer) {
		return app_errors.NewSignatureMismatchError(fmt.Errorf("signature submitted by peer: %s, but signature is signed by peer: %s", submittedByPeer.ID(), sig.Peer().ID().Value()))
	}

	err = h.repository.UpsertContent(ctx, content)
	if err != nil {
		h.logger.Error("could not upsert contract", err)
		return err
	}

	err = h.repository.UpsertSignature(ctx, sig)
	if err != nil {
		h.logger.Error("could not upsert signature", err)
		return err
	}

	if !h.shouldAutoSignContract(content.Grants().Types()) {
		return nil
	}

	err = h.autoSign(ctx, content)
	if err != nil {
		return errors.Wrap(err, "could not autosign contract")
	}

	return nil
}

func (h *SubmitContractHandler) shouldAutoSignContract(grants map[contract.GrantType]bool) bool {
	for key, value := range grants {
		if val, ok := h.autoSignGrants[key]; !ok || val != value {
			return false
		}
	}

	return true
}

func (h *SubmitContractHandler) autoSign(ctx context.Context, content *contract.Content) error {
	// Automatically sign this contract
	sig, err := content.Accept(h.trustedRootCAs, h.autoSignCertificate, h.clock.Now())
	if err != nil {
		return fmt.Errorf("unable to place a accept signature on content %w", err)
	}

	err = h.repository.UpsertSignature(ctx, sig)
	if err != nil {
		h.logger.Error("could not upsert signature", err)
		return err
	}

	// send signed content to all peers
	err = h.peersCommunicationService.AcceptContract(ctx, content.PeersIDs(), content, sig)
	if err != nil {
		return fmt.Errorf("could not send signed contract to peers: %w", err)
	}

	return nil
}
