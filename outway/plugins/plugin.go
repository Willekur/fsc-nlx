// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package plugins

import (
	"net/http"
	"time"

	"go.uber.org/zap"

	"gitlab.com/commonground/nlx/fsc-nlx/outway/domain/config"
)

type Context struct {
	Logger           *zap.Logger
	TokenInfo        *config.TokenInfo
	Response         http.ResponseWriter
	Request          *http.Request
	LogData          map[string]string
	RequestCreatedAt time.Time
}

type ServeFunc func(context *Context) error

type Plugin interface {
	Serve(next ServeFunc) ServeFunc
}
