// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package postgresstorage

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage/postgres/queries"
)

func (p *PostgresStorage) UpdateService(ctx context.Context, args *storage.UpdateServiceArgs) error {
	_, err := p.queries.UpdateService(ctx, &queries.UpdateServiceParams{
		GroupID:      args.GroupID,
		Name:         args.Name,
		EndpointUrl:  args.EndpointURL,
		InwayAddress: args.InwayAddress,
	})
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return storage.ErrServiceNotFound
		}

		return fmt.Errorf("could not update service in database: %w", err)
	}

	return nil
}
