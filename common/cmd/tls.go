// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package cmd

type TLSOptions struct {
	RootCertFile string `long:"tls-root-cert" env:"TLS_ROOT_CERT" description:"Absolute or relative path to the CA root cert .pem" required:"false"`
	CertFile     string `long:"tls-cert" env:"TLS_CERT" description:"Absolute or relative path to the cert .pem" required:"false"`
	KeyFile      string `long:"tls-key" env:"TLS_KEY" description:"Absolute or relative path to the key .pem" required:"false"`
}

type TLSGroupOptions struct {
	GroupRootCert string `long:"tls-group-root-cert" env:"TLS_GROUP_ROOT_CERT" description:"Absolute or relative path to the CA root cert .pem of the FSC Group" required:"true"`
	GroupCertFile string `long:"tls-group-cert" env:"TLS_GROUP_CERT" description:"Absolute or relative path to the FSC Group cert .pem" required:"true"`
	GroupKeyFile  string `long:"tls-group-key" env:"TLS_GROUP_KEY" description:"Absolute or relative path to the FSC Group key .pem" required:"true"`
}
