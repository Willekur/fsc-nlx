// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package services

import (
	"context"
	"errors"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func (p *PeersCommunicationService) RejectContract(ctx context.Context, peerIDs contract.PeersIDs, contractContent *contract.Content, sig *contract.Signature) error {
	return p.sendToPeers(peerIDs, func(peerID contract.PeerID, managerClient manager.Manager) error {
		err := managerClient.RejectContract(ctx, contractContent, sig)
		if err != nil {
			errUpsertDistribution := p.contractDistributionRepository.UpsertContractDistribution(ctx, peerID, contractContent.Hash(), sig, contract.DistributionActionSubmitRejectSignature, err.Error(), nil)
			if errUpsertDistribution != nil {
				return errors.Join(err, errUpsertDistribution, fmt.Errorf("could not update contract distribution"))
			}

			return fmt.Errorf("could not reject contract %q: %w", peerID.Value(), err)
		}

		return nil
	})
}
