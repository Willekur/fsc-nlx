-- Copyright © VNG Realisatie 2023
-- Licensed under the EUPL

-- name: ListDelegatedServicesDetails :many
SELECT
    g.delegator_peer_id as delegator_peer_id,
    g.service_name as service_name
FROM contracts.grants_delegated_service_publication as g
WHERE
    g.service_peer_id = @self_peer_id::text
    AND g.service_name = ANY(@service_names::text[]);

-- name: ListServicesDetails :many
SELECT
    p.service_name as service_name
FROM contracts.grants_service_publication as p
WHERE
    p.service_peer_id = @self_peer_id::text
    AND p.service_name = ANY(@service_names::text[]);
