// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"
	"encoding/base64"
	"fmt"
	"net/http"
	"time"

	"github.com/pkg/errors"

	common_tls "gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api/models"
)

var contractGrantTypes = map[contract.GrantType]models.FSCCoreGrantType{
	contract.GrantTypeServicePublication:          models.GRANTTYPESERVICEPUBLICATION,
	contract.GrantTypeServiceConnection:           models.GRANTTYPESERVICECONNECTION,
	contract.GrantTypeDelegatedServicePublication: models.GRANTTYPEDELEGATEDSERVICEPUBLICATION,
	contract.GrantTypeDelegatedServiceConnection:  models.GRANTTYPEDELEGATEDSERVICECONNECTION,
}

type Client struct {
	manager.Manager
	c           *api.ClientWithResponses
	cert        *common_tls.CertificateBundle
	selfAddress string
}

const fetchLimitGetContracts uint32 = 20
const fetchLimitGetServices uint32 = 20
const fetchLimitGetTransactionLogs uint32 = 20
const fetchLimitGetPeers uint32 = 20

func NewClient(selfAddress, address string, cert *common_tls.CertificateBundle, crlCache *common_tls.CRLsCache) (*Client, error) {
	if selfAddress == "" {
		return nil, fmt.Errorf("self address required")
	}

	if address == "" {
		return nil, fmt.Errorf("address required")
	}

	if cert == nil {
		return nil, fmt.Errorf("cert required")
	}

	tlsConfig := cert.TLSConfig(common_tls.WithCRLPeerCertificateVerification(crlCache))

	c, err := api.NewClientWithResponses(address, func(c *api.Client) error {
		t := http.DefaultTransport.(*http.Transport).Clone()
		t.TLSClientConfig = tlsConfig

		c.Client = &http.Client{Transport: t}

		return nil
	})

	if err != nil {
		return nil, err
	}

	return &Client{c: c, selfAddress: selfAddress, cert: cert}, nil
}

func (c *Client) GetCertificates(ctx context.Context) (*contract.PeerCertificates, error) {
	response, err := c.c.GetJSONWebKeySetWithResponse(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "could not get certificates")
	}

	if response.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("could not get certificates, received invalid status code %d: %s", response.StatusCode(), string(response.Body))
	}

	rawCerts := make([][]byte, 0)

	for _, cert := range response.JSON200.Keys {
		certBytes := make([]byte, 0)

		for _, c := range *cert.X5c {
			var b []byte

			b, err = base64.StdEncoding.DecodeString(c)
			if err != nil {
				return nil, errors.Wrap(err, "unable to decode certificate base64 string")
			}

			certBytes = append(certBytes, b...)
		}

		rawCerts = append(rawCerts, certBytes)
	}

	peerCertificates, err := contract.NewPeerCertificates(c.cert.RootCAs(), rawCerts)
	if err != nil {
		return nil, err
	}

	return peerCertificates, nil
}

func (c *Client) GetToken(ctx context.Context, grantHash string) (string, error) {
	response, err := c.c.GetTokenWithFormdataBodyWithResponse(ctx, models.GetTokenFormdataRequestBody{
		GrantType: models.ClientCredentials,
		Scope:     grantHash,
	})
	if err != nil {
		return "", errors.Wrap(err, "could not get token")
	}

	if response.StatusCode() != http.StatusOK {
		return "", fmt.Errorf("could not get token: received invalid status code %d: %s", response.StatusCode(), string(response.Body))
	}

	if response.JSON200.TokenType != models.Bearer {
		return "", fmt.Errorf("invalid token type: %s", response.JSON200.TokenType)
	}

	return response.JSON200.AccessToken, nil
}

func (c *Client) GetContracts(ctx context.Context, grantType *contract.GrantType, grantHashes *[]string) ([]*contract.Contract, error) {
	sortOrder := models.FSCCoreSortOrderSORTORDERDESCENDING

	var grantTypeFilter *models.FSCCoreGrantType

	if grantType != nil {
		apiGrantType, ok := contractGrantTypes[*grantType]
		if !ok {
			return nil, fmt.Errorf("unknown grant type: %d", *grantType)
		}

		grantTypeFilter = &apiGrantType
	}

	limit := models.FSCCoreQueryPaginationLimit(fetchLimitGetContracts)

	response, err := c.c.GetContractsWithResponse(ctx, &models.GetContractsParams{
		Limit:     &limit,
		SortOrder: &sortOrder,
		GrantType: grantTypeFilter,
		GrantHash: grantHashes,
	})
	if err != nil {
		return nil, err
	}

	if response.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("received invalid status code %d: %s", response.StatusCode(), string(response.Body))
	}

	return nil, err
}

func (c *Client) Announce(ctx context.Context) error {
	resp, err := c.c.AnnounceWithResponse(ctx, &models.AnnounceParams{
		FscManagerAddress: c.selfAddress,
	})
	if err != nil {
		return errors.Wrap(err, "could not announce to peer")
	}

	if resp.StatusCode() != http.StatusOK {
		return fmt.Errorf("received invalid status code %d: %s", resp.StatusCode(), string(resp.Body))
	}

	return nil
}

func (c *Client) SubmitContract(ctx context.Context, contractContent *contract.Content, signature *contract.Signature) error {
	cc, err := ContractContentToAPIModel(contractContent)
	if err != nil {
		return errors.Wrap(err, "could not submit contract")
	}

	resp, err := c.c.SubmitContractWithResponse(ctx, &models.SubmitContractParams{
		FscManagerAddress: c.selfAddress,
	},
		models.SubmitContractJSONRequestBody(models.SubmitContractJSONBody{
			ContractContent: *cc,
			Signature:       signature.JWS(),
		}),
	)
	if err != nil {
		return errors.Wrap(err, "could not submit contract")
	}

	if resp.StatusCode() != http.StatusCreated {
		return fmt.Errorf("received invalid status code %d: %s", resp.StatusCode(), string(resp.Body))
	}

	return nil
}

func (c *Client) AcceptContract(ctx context.Context, contractContent *contract.Content, signature *contract.Signature) error {
	cc, err := ContractContentToAPIModel(contractContent)
	if err != nil {
		return errors.Wrap(err, "could not accept contract")
	}

	resp, err := c.c.AcceptContractWithResponse(ctx, contractContent.Hash().String(), &models.AcceptContractParams{
		FscManagerAddress: c.selfAddress,
	},
		models.AcceptContractJSONRequestBody(models.SubmitContractJSONBody{
			ContractContent: *cc,
			Signature:       signature.JWS(),
		}),
	)
	if err != nil {
		return errors.Wrap(err, "could not accept contract")
	}

	if resp.StatusCode() != http.StatusCreated {
		return fmt.Errorf("received invalid status code %d: %s", resp.StatusCode(), string(resp.Body))
	}

	return nil
}

func (c *Client) RevokeContract(ctx context.Context, contractContent *contract.Content, signature *contract.Signature) error {
	cc, err := ContractContentToAPIModel(contractContent)
	if err != nil {
		return errors.Wrap(err, "could not revoke contract")
	}

	resp, err := c.c.RevokeContractWithResponse(ctx, contractContent.Hash().String(), &models.RevokeContractParams{
		FscManagerAddress: c.selfAddress,
	},
		models.RevokeContractJSONRequestBody(models.RevokeContractJSONBody{
			ContractContent: *cc,
			Signature:       signature.JWS(),
		}),
	)
	if err != nil {
		return errors.Wrap(err, "could not accept contract")
	}

	if resp.StatusCode() != http.StatusCreated {
		return fmt.Errorf("received invalid status code %d: %s", resp.StatusCode(), string(resp.Body))
	}

	return nil
}

func (c *Client) RejectContract(ctx context.Context, contractContent *contract.Content, signature *contract.Signature) error {
	cc, err := ContractContentToAPIModel(contractContent)
	if err != nil {
		return errors.Wrap(err, "could not reject contract")
	}

	resp, err := c.c.RejectContractWithResponse(ctx, contractContent.Hash().String(), &models.RejectContractParams{
		FscManagerAddress: c.selfAddress,
	},
		models.RejectContractJSONRequestBody(models.RejectContractJSONBody{
			ContractContent: *cc,
			Signature:       signature.JWS(),
		}),
	)
	if err != nil {
		return errors.Wrap(err, "could not reject contract")
	}

	if resp.StatusCode() != http.StatusCreated {
		return fmt.Errorf("received invalid status code %d: %s", resp.StatusCode(), string(resp.Body))
	}

	return nil
}

func (c *Client) GetPeerInfo(ctx context.Context) (*contract.Peer, error) {
	resp, err := c.c.GetPeerInfoWithResponse(ctx)

	if err != nil {
		return nil, err
	}

	if resp.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("received invalid status code %d: %s", resp.StatusCode(), string(resp.Body))
	}

	p, err := contract.NewPeer(&contract.NewPeerArgs{
		ID:   resp.JSON200.PeerId,
		Name: resp.JSON200.PeerName,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not create new peer")
	}

	return p, nil
}

func (c *Client) GetPeers(ctx context.Context, peerIDs contract.PeersIDs) (contract.Peers, error) {
	sortOrder := models.FSCCoreSortOrderSORTORDERDESCENDING

	peerIDFilter := make([]models.FSCCorePeerID, 0, len(peerIDs))

	for p := range peerIDs {
		peerIDFilter = append(peerIDFilter, p.Value())
	}

	limit := models.FSCCoreQueryPaginationLimit(fetchLimitGetPeers)

	getPeersParams := &models.GetPeersParams{
		SortOrder: &sortOrder,
		Limit:     &limit,
	}

	// Generated client transforms empty slice into slice with empty string, which breaks the server handler.
	if len(peerIDFilter) > 0 {
		getPeersParams.PeerId = &peerIDFilter
	}

	resp, err := c.c.GetPeersWithResponse(ctx, getPeersParams)

	if err != nil {
		return nil, err
	}

	if resp.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("received invalid status code %d: %s", resp.StatusCode(), string(resp.Body))
	}

	peers := make(contract.Peers)

	for _, p := range resp.JSON200.Peers {
		peer, errNewPeer := contract.NewPeer(&contract.NewPeerArgs{
			ID:             p.Id,
			Name:           p.Name,
			ManagerAddress: p.ManagerAddress,
		})
		if errNewPeer != nil {
			return nil, errors.Wrap(errNewPeer, "could not create new peer")
		}

		peers[peer.ID()] = peer
	}

	return peers, nil
}

func (c *Client) GetServices(ctx context.Context, peerID *contract.PeerID, serviceName string) ([]*contract.Service, error) {
	sortOrder := models.FSCCoreSortOrderSORTORDERDESCENDING

	var peerIDFilter *models.FSCCorePeerID

	if peerID != nil {
		pf := models.FSCCorePeerID(*peerID)
		peerIDFilter = &pf
	}

	var serviceNameFilter *models.FSCCoreServiceName

	if serviceName != "" {
		serviceNameFilter = &serviceName
	}

	limit := models.FSCCoreQueryPaginationLimit(fetchLimitGetServices)

	resp, err := c.c.GetServicesWithResponse(ctx, &models.GetServicesParams{
		SortOrder:   &sortOrder,
		PeerId:      peerIDFilter,
		Limit:       &limit,
		ServiceName: serviceNameFilter,
	})

	if err != nil {
		return nil, err
	}

	if resp.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("received invalid status code %d: %s", resp.StatusCode(), string(resp.Body))
	}

	servicesList := make([]*contract.Service, len(resp.JSON200.Services))

	for i, s := range resp.JSON200.Services {
		serviceDiscriminator, err := s.Data.ValueByDiscriminator()
		if err != nil {
			return nil, err
		}

		switch convertedModel := serviceDiscriminator.(type) {
		case models.FSCCoreServiceListingDelegatedService:
			p, err := mapServiceProtocolToContract(convertedModel.Protocol)
			if err != nil {
				return nil, fmt.Errorf("invalid protocol in get services request: %w", err)
			}

			servicesList[i] = &contract.Service{
				Name:              convertedModel.Name,
				PeerID:            convertedModel.Peer.Id,
				PeerName:          convertedModel.Peer.Name,
				DelegatorPeerID:   convertedModel.Delegator.PeerId,
				DelegatorPeerName: convertedModel.Delegator.PeerName,
				Protocol:          p,
			}
		case models.FSCCoreServiceListingService:
			p, err := mapServiceProtocolToContract(convertedModel.Protocol)
			if err != nil {
				return nil, fmt.Errorf("invalid protocol in get services request: %w", err)
			}

			servicesList[i] = &contract.Service{
				Name:     convertedModel.Name,
				PeerID:   convertedModel.Peer.Id,
				PeerName: convertedModel.Peer.Name,
				Protocol: p,
			}
		}
	}

	return servicesList, nil
}

func mapServiceProtocolToContract(p models.FSCCoreProtocol) (contract.ServiceProtocol, error) {
	switch p {
	case models.PROTOCOLTCPHTTP11:
		return contract.ServiceProtocolTCPHTTP1_1, nil
	case models.PROTOCOLTCPHTTP2:
		return contract.ServiceProtocolTCPHTTP2, nil
	default:
		return contract.ServiceProtocolUnspecified, fmt.Errorf("unknown protocol: %s", p)
	}
}

func (c *Client) GetTXLogRecords(ctx context.Context) (contract.TXLogRecords, error) {
	sortOrder := models.FSCLoggingSortOrderSORTORDERDESCENDING
	limit := fetchLimitGetTransactionLogs

	resp, err := c.c.GetLogsWithResponse(ctx, &models.GetLogsParams{
		SortOrder: &sortOrder,
		Limit:     &limit,
	})
	if err != nil {
		return nil, err
	}

	if resp.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("received invalid status code %d: %s", resp.StatusCode(), string(resp.Body))
	}

	records := make(contract.TXLogRecords, len(resp.JSON200.Records))

	for i := range resp.JSON200.Records {
		r := resp.JSON200.Records[i]

		source, destination, err := mapSourceAndDestinationToModel(&r)
		if err != nil {
			return nil, fmt.Errorf("could not map source and destination: %w", err)
		}

		records[i] = &contract.TXLogRecord{
			TransactionID: r.TransactionId,
			GrantHash:     r.GrantHash,
			ServiceName:   r.ServiceName,
			Direction:     mapDirection(r.Direction),
			Source:        source,
			Destination:   destination,
			CreatedAt:     time.Unix(r.CreatedAt, 0),
		}
	}

	return records, nil
}

func mapSourceAndDestinationToModel(r *models.FSCLoggingLogRecord) (source, destination interface{}, err error) {
	sourceDiscriminator, err := r.Source.ValueByDiscriminator()
	if err != nil {
		return nil, nil, err
	}
	// nolint:dupl // not the same
	switch convertedSourceModel := sourceDiscriminator.(type) {
	case models.FSCLoggingSource:
		source = &contract.TXLogRecordSource{
			OutwayPeerID: convertedSourceModel.OutwayPeerId,
		}
	case models.FSCLoggingSourceDelegated:
		source = &contract.TXLogRecordDelegatedSource{
			OutwayPeerID:    convertedSourceModel.OutwayPeerId,
			DelegatorPeerID: convertedSourceModel.DelegatorPeerId,
		}
	}

	destinationDiscriminator, err := r.Destination.ValueByDiscriminator()
	if err != nil {
		return nil, nil, err
	}
	// nolint:dupl // not the same
	switch convertedDestinationModel := destinationDiscriminator.(type) {
	case models.FSCLoggingDestination:
		destination = &contract.TXLogRecordDestination{
			ServicePeerID: convertedDestinationModel.ServicePeerId,
		}
	case models.FSCLoggingDestinationDelegated:
		destination = &contract.TXLogRecordDelegatedDestination{
			ServicePeerID:   convertedDestinationModel.ServicePeerId,
			DelegatorPeerID: convertedDestinationModel.DelegatorPeerId,
		}
	}

	return source, destination, nil
}

func mapDirection(d models.FSCLoggingLogRecordDirection) contract.TXLogDirection {
	switch d {
	case models.DIRECTIONINCOMING:
		return contract.TXLogDirectionIn
	case models.DIRECTIONOUTGOING:
		return contract.TXLogDirectionOut
	default:
		return contract.TXLogDirectionUnspecified
	}
}

func ContractContentToAPIModel(p *contract.Content) (*models.FSCCoreContractContent, error) {
	grants := p.Grants()

	grantAPIModels := make([]models.FSCCoreGrant, len(grants))

	for i, grant := range grants {
		switch g := grant.(type) {
		case *contract.GrantServicePublication:
			var err error

			grantAPIModels[i], err = servicePublicationGrantToAPIModel(g)
			if err != nil {
				return nil, err
			}
		case *contract.GrantServiceConnection:
			grantAPIModels[i] = serviceConnectionGrantToAPIModel(g)
		case *contract.GrantDelegatedServiceConnection:
			grantAPIModels[i] = delegatedServiceConnectionGrantToAPIModel(g)
		case *contract.GrantDelegatedServicePublication:
			var err error

			grantAPIModels[i], err = delegatedServicePublicationGrantToAPIModel(g)
			if err != nil {
				return nil, err
			}
		default:
			return nil, fmt.Errorf("unsupported grant type: %T", g)
		}
	}

	return &models.FSCCoreContractContent{
		HashAlgorithm: models.HASHALGORITHMSHA3512,
		Iv:            p.IV().String(),
		GroupId:       p.GroupID(),
		Validity: models.FSCCoreValidity{
			NotBefore: p.NotBefore().Unix(),
			NotAfter:  p.NotAfter().Unix(),
		},
		Grants:    grantAPIModels,
		CreatedAt: p.CreatedAt().Unix(),
	}, nil
}

func servicePublicationGrantToAPIModel(g *contract.GrantServicePublication) (models.FSCCoreGrant, error) {
	data := models.FSCCoreGrant_Data{}

	var protocol models.FSCCoreProtocol

	switch g.Service().Protocol() {
	case contract.ServiceProtocolTCPHTTP1_1:
		protocol = models.PROTOCOLTCPHTTP11
	case contract.ServiceProtocolTCPHTTP2:
		protocol = models.PROTOCOLTCPHTTP2
	default:
		return models.FSCCoreGrant{}, fmt.Errorf("unsupported protocol: %s", g.Service().Protocol())
	}

	_ = data.FromFSCCoreGrantServicePublication(models.FSCCoreGrantServicePublication{
		Type: models.GRANTTYPESERVICEPUBLICATION,
		Directory: models.FSCCoreDirectory{
			PeerId: g.Directory().Peer().ID().Value(),
		},
		Service: models.FSCCoreServicePublication{
			Name:     g.Service().Name(),
			PeerId:   g.Service().Peer().ID().Value(),
			Protocol: protocol,
		},
	})

	return models.FSCCoreGrant{
		Data: data,
	}, nil
}

func serviceConnectionGrantToAPIModel(g *contract.GrantServiceConnection) models.FSCCoreGrant {
	data := models.FSCCoreGrant_Data{}
	_ = data.FromFSCCoreGrantServiceConnection(models.FSCCoreGrantServiceConnection{
		Type: models.GRANTTYPESERVICECONNECTION,
		Outway: models.FSCCoreOutway{
			PeerId:                g.Outway().Peer().ID().Value(),
			CertificateThumbprint: g.Outway().CertificateThumbprint().Value(),
		},
		Service: models.FSCCoreService{
			Name:   g.Service().Name(),
			PeerId: g.Service().Peer().ID().Value(),
		},
	})

	return models.FSCCoreGrant{
		Data: data,
	}
}

func delegatedServiceConnectionGrantToAPIModel(g *contract.GrantDelegatedServiceConnection) models.FSCCoreGrant {
	data := models.FSCCoreGrant_Data{}
	_ = data.FromFSCCoreGrantDelegatedServiceConnection(models.FSCCoreGrantDelegatedServiceConnection{
		Type: models.GRANTTYPEDELEGATEDSERVICECONNECTION,
		Delegator: models.FSCCoreDelegator{
			PeerId: g.Delegator().Peer().ID().Value(),
		},
		Outway: models.FSCCoreOutway{
			PeerId:                g.Outway().Peer().ID().Value(),
			CertificateThumbprint: g.Outway().CertificateThumbprint().Value(),
		},
		Service: models.FSCCoreService{
			Name:   g.Service().Name(),
			PeerId: g.Service().Peer().ID().Value(),
		},
	})

	return models.FSCCoreGrant{
		Data: data,
	}
}

func delegatedServicePublicationGrantToAPIModel(g *contract.GrantDelegatedServicePublication) (models.FSCCoreGrant, error) {
	data := models.FSCCoreGrant_Data{}

	var protocol models.FSCCoreProtocol

	switch g.Service().Protocol() {
	case contract.ServiceProtocolTCPHTTP1_1:
		protocol = models.PROTOCOLTCPHTTP11
	case contract.ServiceProtocolTCPHTTP2:
		protocol = models.PROTOCOLTCPHTTP2
	default:
		return models.FSCCoreGrant{}, fmt.Errorf("unsupported protocol: %s", g.Service().Protocol())
	}

	_ = data.FromFSCCoreGrantDelegatedServicePublication(models.FSCCoreGrantDelegatedServicePublication{
		Type: models.GRANTTYPEDELEGATEDSERVICEPUBLICATION,
		Delegator: models.FSCCoreDelegator{
			PeerId: g.Delegator().Peer().ID().Value(),
		},
		Directory: models.FSCCoreDirectory{
			PeerId: g.Directory().Peer().ID().Value(),
		},
		Service: models.FSCCoreServicePublication{
			Name:     g.Service().Name(),
			PeerId:   g.Service().Peer().ID().Value(),
			Protocol: protocol,
		},
	})

	return models.FSCCoreGrant{
		Data: data,
	}, nil
}
