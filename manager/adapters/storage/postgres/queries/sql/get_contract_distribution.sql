-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL

-- name: GetContractDistribution :one
SELECT
    peer_id,
    content_hash,
    signature,
    action,
    next_retry_at,
    error_message,
    retries,
    last_retry_at
FROM
    contracts.distributions
WHERE
    content_hash = $1
AND
    peer_id = $2
AND
    action = $3 ;
