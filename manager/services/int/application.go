// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package internalservice

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"github.com/cenkalti/backoff/v4"
	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/controller"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/txlog"
	app "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/command"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/query"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/services"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type NewApplicationArgs struct {
	Context                     context.Context
	Clock                       command.Clock
	Logger                      *logger.Logger
	Repository                  contract.Repository
	PeersCommunicationService   *services.PeersCommunicationService
	TXLog                       txlog.TXLog
	SelfPeer                    *contract.Peer
	TrustedExternalRootCAs      *x509.CertPool
	SignatureCertificate        *tls.Certificate
	ControllerRepository        controller.Controller
	ManagerFactory              manager.Factory
	GroupID                     contract.GroupID
	DirectoryPeerID             contract.PeerID
	DirectoryPeerManagerAddress string
	IsTxlogDisabled             bool
}

func NewApplication(args *NewApplicationArgs) (*app.Application, error) {
	registerPeer, err := command.NewAnnouncePeerHandler(args.TrustedExternalRootCAs, args.PeersCommunicationService, args.Logger)
	if err != nil {
		return nil, err
	}

	createContract, err := command.NewCreateContractHandler(&command.NewCreateContractHandlerArgs{
		TrustedExternalRootCAs:    args.TrustedExternalRootCAs,
		Repository:                args.Repository,
		PeersCommunicationService: args.PeersCommunicationService,
		Clock:                     args.Clock,
		Logger:                    args.Logger,
		SignatureCert:             args.SignatureCertificate,
		SelfPeerID:                args.SelfPeer.ID(),
	})
	if err != nil {
		return nil, err
	}

	approveContract, err := command.NewAcceptContractHandler(&command.NewAcceptContractHandlerArgs{
		TrustedExternalRootCAs:    args.TrustedExternalRootCAs,
		Repository:                args.Repository,
		PeersCommunicationService: args.PeersCommunicationService,
		Clock:                     args.Clock,
		Logger:                    args.Logger,
		SignatureCert:             args.SignatureCertificate,
	})
	if err != nil {
		return nil, err
	}

	rejectContract, err := command.NewRejectContractHandler(&command.NewRejectContractHandlerArgs{
		TrustedExternalRootCAs:    args.TrustedExternalRootCAs,
		Repository:                args.Repository,
		PeersCommunicationService: args.PeersCommunicationService,
		Clock:                     args.Clock,
		Logger:                    args.Logger,
		SignatureCert:             args.SignatureCertificate,
	})
	if err != nil {
		return nil, err
	}

	revokeContract, err := command.NewRevokeContractHandler(&command.NewRevokeContractHandlerArgs{
		TrustedExternalRootCAs:    args.TrustedExternalRootCAs,
		Repository:                args.Repository,
		PeersCommunicationService: args.PeersCommunicationService,
		Clock:                     args.Clock,
		Logger:                    args.Logger,
		SignatureCert:             args.SignatureCertificate,
	})
	if err != nil {
		return nil, err
	}

	retryContractDistribution, err := command.NewRetryContractDistributionHandler(&command.NewRetryContractDistributionHandlerArgs{
		Repository:               args.Repository,
		PeerCommunicationService: args.PeersCommunicationService,
	})
	if err != nil {
		return nil, err
	}

	getToken, err := query.NewGetTokenHandler(&query.NewGetTokenHandlerArgs{
		Repository:                args.Repository,
		PeersCommunicationService: args.PeersCommunicationService,
		Clock:                     args.Clock,
	})
	if err != nil {
		return nil, err
	}

	getServiceEndpointURL, err := query.NewGetServiceEndpointURLHandler(args.ControllerRepository)
	if err != nil {
		return nil, err
	}

	getServicesForOutway, err := query.NewGetServicesForOutwayHandler(args.Repository)
	if err != nil {
		return nil, err
	}

	getCertificates, err := query.NewGetCertificatesHandler(&query.NewGetCertificatesHandlerArg{
		Repository: args.Repository,
		SelfPeerID: args.SelfPeer.ID(),
	})
	if err != nil {
		return nil, err
	}

	getContracts, err := query.NewListContractsHandler(args.Repository, args.SelfPeer.ID())
	if err != nil {
		return nil, errors.Wrap(err, "could not create get contracts handler")
	}

	getContractDistributions, err := query.NewListContractDistributionsHandler(args.Repository)
	if err != nil {
		return nil, errors.Wrap(err, "could not create get contract distribution handler")
	}

	getServices, err := query.NewGetServicesHandler(args.ManagerFactory, args.DirectoryPeerManagerAddress)
	if err != nil {
		return nil, errors.Wrap(err, "could not create get services handler")
	}

	createCertificate, err := command.NewCreateCertificateHandler(args.TrustedExternalRootCAs, args.Repository)
	if err != nil {
		return nil, err
	}

	getTXLogRecords, err := query.NewListTransactionLogRecordsHandler(args.GroupID, args.TXLog, args.PeersCommunicationService, args.IsTxlogDisabled)
	if err != nil {
		return nil, errors.Wrap(err, "could not create list txlog records handler")
	}

	getPeerInfo, err := query.NewGetPeerInfoHandler(args.SelfPeer.ID().Value(), args.SelfPeer.Name().Value())
	if err != nil {
		return nil, errors.Wrap(err, "could not create get peer info handler")
	}

	getPeers, err := query.NewListPeersHandler(args.ManagerFactory, args.DirectoryPeerManagerAddress)
	if err != nil {
		return nil, errors.Wrap(err, "could not create get peers handler")
	}

	application := &app.Application{
		Queries: app.Queries{
			GetToken:                 getToken,
			GetServiceEndpointURL:    getServiceEndpointURL,
			GetServicesForOutway:     getServicesForOutway,
			GetCertificates:          getCertificates,
			GetContracts:             getContracts,
			GetContractDistributions: getContractDistributions,
			GetServices:              getServices,
			GetTXLogRecords:          getTXLogRecords,
			GetPeerInfo:              getPeerInfo,
			ListPeers:                getPeers,
		},
		Commands: app.Commands{
			AnnouncePeer:              registerPeer,
			CreateContract:            createContract,
			AcceptContract:            approveContract,
			RejectContract:            rejectContract,
			RevokeContract:            revokeContract,
			CreateCertificate:         createCertificate,
			RetryContractDistribution: retryContractDistribution,
		},
	}

	err = args.Repository.UpsertPeer(context.Background(), args.SelfPeer)
	if err != nil {
		return nil, errors.Wrap(err, "could not create self peer")
	}

	announce := func() error {
		err = application.Commands.AnnouncePeer.Handle(context.Background(), []string{
			args.DirectoryPeerID.Value(),
		})
		if err != nil {
			return errors.Wrap(err, "could not announce to directory peer")
		}

		return nil
	}

	err = backoff.Retry(announce, backoff.WithContext(backoff.NewExponentialBackOff(), context.Background()))
	if err != nil {
		return nil, errors.Wrap(err, "announcement to directory peer permanently failed")
	}

	return application, nil
}
