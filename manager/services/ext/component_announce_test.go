// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"
	"net/http"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api/models"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

func TestAnnounce(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, _ := newService(t.Name())
	defer externalHTTPServer.Close()

	orgBCertBundle, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "../", "testing", "pki"), testingutils.NLXTestPeerB)
	assert.NoError(t, err)

	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, orgBCertBundle)
	assert.NoError(t, err)

	// Act
	res, err := client.Announce(context.Background(), &models.AnnounceParams{
		FscManagerAddress: "https://my-manager-address:8443",
	})
	assert.NoError(t, err)

	// Assert
	assert.Equal(t, http.StatusOK, res.StatusCode)

	resPeers, err := client.GetPeersWithResponse(context.Background(), &models.GetPeersParams{
		PeerId: &[]models.FSCCorePeerID{peerB.GetPeerID()},
	})
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resPeers.StatusCode())

	expectedPeers := []models.FSCCorePeer{
		{
			Id:             peerB.GetPeerID(),
			ManagerAddress: "https://my-manager-address:8443",
			Name:           peerB.GetName(),
		},
	}
	assert.Equal(t, expectedPeers, resPeers.JSON200.Peers)

}
