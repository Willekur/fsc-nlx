-- Copyright © VNG Realisatie 2023
-- Licensed under the EUPL

BEGIN transaction;

CREATE SCHEMA controller;

CREATE TABLE controller.services (
    group_id VARCHAR(150) NOT NULL,
    name VARCHAR(150) NOT NULL,
    endpoint_url VARCHAR(2000) NOT NULL,
    inway_address VARCHAR(2000) NOT NULL,
    CONSTRAINT services_pk PRIMARY KEY (group_id, name)
);

CREATE TABLE controller.inways (
    group_id VARCHAR(150) NOT NULL,
    name VARCHAR(255) NOT NULL,
    address VARCHAR(2000) NOT NULL,
    CONSTRAINT inways_pk PRIMARY KEY (group_id, name)
);

CREATE TABLE controller.outways (
  group_id VARCHAR(150) NOT NULL,
  name VARCHAR(100) NOT NULL UNIQUE,
  certificate_thumbprint VARCHAR(4096) NOT NULL,
  CONSTRAINT outways_pk PRIMARY KEY (group_id, name)
);

-- TODO add indexes and references

COMMIT;
