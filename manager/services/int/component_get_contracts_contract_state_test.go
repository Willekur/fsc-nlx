// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

//nolint:funlen,dupl // funlen: letting up the contracts requires more lines of code than allowed. dupl: the tests differ in subtle ways but do require a similar setup
package internalservice_test

import (
	"context"
	"net/http"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/command"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

// the following tests verify that the returned state of the contract is correct

func TestListContracts_ContractState_Proposed(t *testing.T) {
	t.Parallel()

	now := time.Date(2023, 5, 23, 0, 0, 0, 0, time.UTC)

	tests := map[string]struct {
		ContractsToCreate []*command.CreateContractHandlerArgs
		Params            *models.GetContractsParams
		Want              []models.Contract
	}{
		"happy_flow": {
			ContractsToCreate: func() []*command.CreateContractHandlerArgs {
				servicePublicationGrant := models.Grant{}

				err := servicePublicationGrant.FromGrantServicePublication(models.GrantServicePublication{
					Type: models.GRANTTYPESERVICEPUBLICATION,
					Directory: models.DirectoryPeer{
						PeerId: peerC.GetPeerID(),
					},
					Service: models.ServicePublicationPeer{
						Name:     "parkeerrechten",
						PeerId:   peerA.GetPeerID(),
						Protocol: models.PROTOCOLTCPHTTP11,
					},
				})
				assert.NoError(t, err)

				iv := uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee79")
				assert.NoError(t, err)

				result := []*command.CreateContractHandlerArgs{
					{
						HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
						IV:                iv.String(),
						GroupID:           "test-group",
						ContractNotBefore: now.Add(-1 * time.Hour),
						ContractNotAfter:  now.Add(time.Hour),
						Grants: []interface{}{
							&command.GrantServicePublicationArgs{
								DirectoryPeerID: peerC.GetPeerID(),
								ServicePeerID:   peerA.GetPeerID(),
								ServiceName:     "parkeerrechten",
								ServiceProtocol: contract.ServiceProtocolTCPHTTP1_1,
							},
						},
						CreatedAt: now,
					},
				}

				return result
			}(),
			Params: &models.GetContractsParams{
				GrantType:   nil,
				ContentHash: &[]string{},
			},
			Want: func() []models.Contract {
				servicePublicationGrant := models.Grant{}

				err := servicePublicationGrant.FromGrantServicePublication(models.GrantServicePublication{
					Directory: models.DirectoryPeer{
						PeerId: peerC.GetPeerID(),
					},
					Hash: "$1$2$MdEF9br_Qkx1psi3bfyEWnef9ZqzJMXoE0tOL1r1lzA77bP3Ps0tzo8SM816s0ykia_51iV8FgYmG1OBd8AgqA",
					Service: models.ServicePublicationPeer{
						PeerId:   peerA.GetPeerID(),
						Name:     "parkeerrechten",
						Protocol: "PROTOCOL_TCP_HTTP_1.1",
					},
					Type: models.GRANTTYPESERVICEPUBLICATION,
				})
				assert.NoError(t, err)

				result := []models.Contract{
					{
						Content: models.ContractContent{
							CreatedAt: now.Unix(),
							Grants: []models.Grant{
								servicePublicationGrant,
							},
							GroupId:       "test-group",
							HashAlgorithm: "HASH_ALGORITHM_SHA3_512",
							Iv:            "102635ef-7053-4151-8fd1-2537f99bee79",
							Validity: models.Validity{
								NotAfter:  1684803600,
								NotBefore: 1684796400,
							},
						},
						HasAccepted: true,
						HasRejected: false,
						HasRevoked:  false,
						Hash:        "$1$1$ZtNWN0ysfWT_9Qpb0MOLothOs2K1P1azbuaFP1bgbtHYM_iPlJqq1DkU2sBwQLg2IvX83BU7gkdl7EtiEWvpZw",
						Peers: map[string]models.Peer{
							peerA.GetPeerID(): {
								Id:   peerA.GetPeerID(),
								Name: peerA.GetName(),
							},
							peerC.GetPeerID(): {
								Id:   peerC.GetPeerID(),
								Name: peerC.GetName(),
							},
						},
						Signatures: models.Signatures{
							Accept: models.SignatureMap{
								peerA.GetPeerID(): models.Signature{
									Peer: models.Peer{
										Id:   peerA.GetPeerID(),
										Name: peerA.GetName(),
									},
									SignedAt: time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location()).Unix(),
								},
							},
							Reject: models.SignatureMap{},
							Revoke: models.SignatureMap{},
						},
						State: models.CONTRACTSTATEPROPOSED,
					},
				}

				return result
			}(),
		},
	}

	for testName, test := range tests {
		tc := test

		t.Run(testName, func(t *testing.T) {
			externalHTTPServer, app := newService(t.Name())
			defer externalHTTPServer.Close()

			apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
			assert.NoError(t, err)

			for _, createContractArgs := range tc.ContractsToCreate {
				_, createErr := app.Commands.CreateContract.Handle(context.Background(), createContractArgs)
				assert.NoError(t, createErr)
			}

			res, err := apiClient.GetContractsWithResponse(context.Background(), tc.Params)
			assert.NoError(t, err)

			assert.Equal(t, http.StatusOK, res.StatusCode())
			assert.Len(t, res.JSON200.Contracts, len(test.Want))
			assert.Equal(t, test.Want, res.JSON200.Contracts)
		})
	}
}

func TestListContracts_ContractState_Rejected(t *testing.T) {
	t.Parallel()

	now := time.Date(2023, 5, 23, 0, 0, 0, 0, time.UTC)

	tests := map[string]struct {
		ContractsToCreate []*command.CreateContractHandlerArgs
		Params            *models.GetContractsParams
		Want              []models.Contract
	}{
		"happy_flow": {
			ContractsToCreate: func() []*command.CreateContractHandlerArgs {
				servicePublicationGrant := models.Grant{}

				err := servicePublicationGrant.FromGrantServicePublication(models.GrantServicePublication{
					Type: models.GRANTTYPESERVICEPUBLICATION,
					Directory: models.DirectoryPeer{
						PeerId: peerC.GetPeerID(),
					},
					Service: models.ServicePublicationPeer{
						Name:     "parkeerrechten",
						PeerId:   peerA.GetPeerID(),
						Protocol: models.PROTOCOLTCPHTTP11,
					},
				})
				assert.NoError(t, err)

				iv := uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee79")
				assert.NoError(t, err)

				result := []*command.CreateContractHandlerArgs{
					{
						HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
						IV:                iv.String(),
						GroupID:           "test-group",
						ContractNotBefore: now.Add(-1 * time.Hour),
						ContractNotAfter:  now.Add(time.Hour),
						Grants: []interface{}{
							&command.GrantServicePublicationArgs{
								DirectoryPeerID: peerC.GetPeerID(),
								ServicePeerID:   peerA.GetPeerID(),
								ServiceName:     "parkeerrechten",
								ServiceProtocol: contract.ServiceProtocolTCPHTTP1_1,
							},
						},
						CreatedAt: now,
					},
				}

				return result
			}(),
			Params: &models.GetContractsParams{
				GrantType:   nil,
				ContentHash: &[]string{},
			},
			Want: func() []models.Contract {
				servicePublicationGrant := models.Grant{}

				err := servicePublicationGrant.FromGrantServicePublication(models.GrantServicePublication{
					Directory: models.DirectoryPeer{
						PeerId: peerC.GetPeerID(),
					},
					Hash: "$1$2$MdEF9br_Qkx1psi3bfyEWnef9ZqzJMXoE0tOL1r1lzA77bP3Ps0tzo8SM816s0ykia_51iV8FgYmG1OBd8AgqA",
					Service: models.ServicePublicationPeer{
						PeerId:   peerA.GetPeerID(),
						Name:     "parkeerrechten",
						Protocol: "PROTOCOL_TCP_HTTP_1.1",
					},
					Type: models.GRANTTYPESERVICEPUBLICATION,
				})
				assert.NoError(t, err)

				result := []models.Contract{
					{
						Content: models.ContractContent{
							CreatedAt: now.Unix(),
							Grants: []models.Grant{
								servicePublicationGrant,
							},
							GroupId:       "test-group",
							HashAlgorithm: "HASH_ALGORITHM_SHA3_512",
							Iv:            "102635ef-7053-4151-8fd1-2537f99bee79",
							Validity: models.Validity{
								NotAfter:  1684803600,
								NotBefore: 1684796400,
							},
						},
						HasAccepted: true,
						HasRejected: true,
						HasRevoked:  false,
						Hash:        "$1$1$ZtNWN0ysfWT_9Qpb0MOLothOs2K1P1azbuaFP1bgbtHYM_iPlJqq1DkU2sBwQLg2IvX83BU7gkdl7EtiEWvpZw",
						Peers: map[string]models.Peer{
							peerA.GetPeerID(): {
								Id:   peerA.GetPeerID(),
								Name: peerA.GetName(),
							},
							peerC.GetPeerID(): {
								Id:   peerC.GetPeerID(),
								Name: peerC.GetName(),
							},
						},
						Signatures: models.Signatures{
							Accept: models.SignatureMap{
								peerA.GetPeerID(): models.Signature{
									Peer: models.Peer{
										Id:   peerA.GetPeerID(),
										Name: peerA.GetName(),
									},
									SignedAt: time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location()).Unix(),
								},
							},
							Reject: models.SignatureMap{
								peerA.GetPeerID(): models.Signature{
									Peer: models.Peer{
										Id:   peerA.GetPeerID(),
										Name: peerA.GetName(),
									},
									SignedAt: time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location()).Unix(),
								},
							},
							Revoke: models.SignatureMap{},
						},
						State: models.CONTRACTSTATEREJECTED,
					},
				}

				return result
			}(),
		},
	}

	for testName, test := range tests {
		tc := test

		t.Run(testName, func(t *testing.T) {
			externalHTTPServer, app := newService(t.Name())
			defer externalHTTPServer.Close()

			apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
			assert.NoError(t, err)

			for _, createContractArgs := range tc.ContractsToCreate {
				contractHash, createErr := app.Commands.CreateContract.Handle(context.Background(), createContractArgs)
				assert.NoError(t, createErr)

				rejectErr := app.Commands.RejectContract.Handle(context.Background(), contractHash)
				assert.NoError(t, rejectErr)
			}

			res, err := apiClient.GetContractsWithResponse(context.Background(), tc.Params)
			assert.NoError(t, err)

			assert.Equal(t, http.StatusOK, res.StatusCode())
			assert.Len(t, res.JSON200.Contracts, len(test.Want))
			assert.Equal(t, test.Want, res.JSON200.Contracts)
		})
	}
}

func TestListContracts_ContractState_Revoked(t *testing.T) {
	t.Parallel()

	now := time.Date(2023, 5, 23, 0, 0, 0, 0, time.UTC)

	tests := map[string]struct {
		ContractsToCreate []*command.CreateContractHandlerArgs
		Params            *models.GetContractsParams
		Want              []models.Contract
	}{
		"happy_flow": {
			ContractsToCreate: func() []*command.CreateContractHandlerArgs {
				servicePublicationGrant := models.Grant{}

				err := servicePublicationGrant.FromGrantServicePublication(models.GrantServicePublication{
					Type: models.GRANTTYPESERVICEPUBLICATION,
					Directory: models.DirectoryPeer{
						PeerId: peerC.GetPeerID(),
					},
					Service: models.ServicePublicationPeer{
						Name:     "parkeerrechten",
						PeerId:   peerA.GetPeerID(),
						Protocol: models.PROTOCOLTCPHTTP11,
					},
				})
				assert.NoError(t, err)

				iv := uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee79")
				assert.NoError(t, err)

				result := []*command.CreateContractHandlerArgs{
					{
						HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
						IV:                iv.String(),
						GroupID:           "test-group",
						ContractNotBefore: now.Add(-1 * time.Hour),
						ContractNotAfter:  now.Add(time.Hour),
						Grants: []interface{}{
							&command.GrantServicePublicationArgs{
								DirectoryPeerID: peerC.GetPeerID(),
								ServicePeerID:   peerA.GetPeerID(),
								ServiceName:     "parkeerrechten",
								ServiceProtocol: contract.ServiceProtocolTCPHTTP1_1,
							},
						},
						CreatedAt: now,
					},
				}

				return result
			}(),
			Params: &models.GetContractsParams{
				GrantType:   nil,
				ContentHash: &[]string{},
			},
			Want: func() []models.Contract {
				servicePublicationGrant := models.Grant{}

				err := servicePublicationGrant.FromGrantServicePublication(models.GrantServicePublication{
					Directory: models.DirectoryPeer{
						PeerId: peerC.GetPeerID(),
					},
					Hash: "$1$2$MdEF9br_Qkx1psi3bfyEWnef9ZqzJMXoE0tOL1r1lzA77bP3Ps0tzo8SM816s0ykia_51iV8FgYmG1OBd8AgqA",
					Service: models.ServicePublicationPeer{
						PeerId:   peerA.GetPeerID(),
						Name:     "parkeerrechten",
						Protocol: "PROTOCOL_TCP_HTTP_1.1",
					},
					Type: models.GRANTTYPESERVICEPUBLICATION,
				})
				assert.NoError(t, err)

				result := []models.Contract{
					{
						Content: models.ContractContent{
							CreatedAt: now.Unix(),
							Grants: []models.Grant{
								servicePublicationGrant,
							},
							GroupId:       "test-group",
							HashAlgorithm: "HASH_ALGORITHM_SHA3_512",
							Iv:            "102635ef-7053-4151-8fd1-2537f99bee79",
							Validity: models.Validity{
								NotAfter:  1684803600,
								NotBefore: 1684796400,
							},
						},
						HasAccepted: true,
						HasRejected: false,
						HasRevoked:  true,
						Hash:        "$1$1$ZtNWN0ysfWT_9Qpb0MOLothOs2K1P1azbuaFP1bgbtHYM_iPlJqq1DkU2sBwQLg2IvX83BU7gkdl7EtiEWvpZw",
						Peers: map[string]models.Peer{
							peerA.GetPeerID(): {
								Id:   peerA.GetPeerID(),
								Name: peerA.GetName(),
							},
							peerC.GetPeerID(): {
								Id:   peerC.GetPeerID(),
								Name: peerC.GetName(),
							},
						},
						Signatures: models.Signatures{
							Accept: models.SignatureMap{
								peerA.GetPeerID(): models.Signature{
									Peer: models.Peer{
										Id:   peerA.GetPeerID(),
										Name: peerA.GetName(),
									},
									SignedAt: time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location()).Unix(),
								},
							},
							Reject: models.SignatureMap{},
							Revoke: models.SignatureMap{
								peerA.GetPeerID(): models.Signature{
									Peer: models.Peer{
										Id:   peerA.GetPeerID(),
										Name: peerA.GetName(),
									},
									SignedAt: time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location()).Unix(),
								},
							},
						},
						State: models.CONTRACTSTATEREVOKED,
					},
				}

				return result
			}(),
		},
	}

	for testName, test := range tests {
		tc := test

		t.Run(testName, func(t *testing.T) {
			externalHTTPServer, app := newService(t.Name())
			defer externalHTTPServer.Close()

			apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
			assert.NoError(t, err)

			for _, createContractArgs := range tc.ContractsToCreate {
				contractHash, createErr := app.Commands.CreateContract.Handle(context.Background(), createContractArgs)
				assert.NoError(t, createErr)

				rejectErr := app.Commands.RevokeContract.Handle(context.Background(), contractHash)
				assert.NoError(t, rejectErr)
			}

			res, err := apiClient.GetContractsWithResponse(context.Background(), tc.Params)
			assert.NoError(t, err)

			assert.Equal(t, http.StatusOK, res.StatusCode())
			assert.Len(t, res.JSON200.Contracts, len(test.Want))
			assert.Equal(t, test.Want, res.JSON200.Contracts)
		})
	}
}

func TestListContracts_ContractState_Expired(t *testing.T) {
	t.Parallel()

	tests := map[string]struct {
		ContractsToCreate []*command.CreateContractHandlerArgs
		Params            *models.GetContractsParams
		Want              []models.Contract
	}{
		"happy_flow": {
			ContractsToCreate: func() []*command.CreateContractHandlerArgs {
				servicePublicationGrant := models.Grant{}

				err := servicePublicationGrant.FromGrantServicePublication(models.GrantServicePublication{
					Type: models.GRANTTYPESERVICEPUBLICATION,
					Directory: models.DirectoryPeer{
						PeerId: peerA.GetPeerID(),
					},
					Service: models.ServicePublicationPeer{
						Name:     "parkeerrechten",
						PeerId:   peerA.GetPeerID(),
						Protocol: models.PROTOCOLTCPHTTP11,
					},
				})
				assert.NoError(t, err)

				iv := uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee79")
				assert.NoError(t, err)

				result := []*command.CreateContractHandlerArgs{
					{
						HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
						IV:                iv.String(),
						GroupID:           "test-group",
						ContractNotBefore: testClock.Now().Add(-2 * time.Hour),
						ContractNotAfter:  testClock.Now().Add(-1 * time.Hour),
						Grants: []interface{}{
							&command.GrantServicePublicationArgs{
								DirectoryPeerID: peerA.GetPeerID(),
								ServicePeerID:   peerA.GetPeerID(),
								ServiceName:     "parkeerrechten",
								ServiceProtocol: contract.ServiceProtocolTCPHTTP1_1,
							},
						},
						CreatedAt: testClock.Now(),
					},
				}

				return result
			}(),
			Params: &models.GetContractsParams{
				GrantType:   nil,
				ContentHash: &[]string{},
			},
			Want: func() []models.Contract {
				servicePublicationGrant := models.Grant{}

				err := servicePublicationGrant.FromGrantServicePublication(models.GrantServicePublication{
					Directory: models.DirectoryPeer{
						PeerId: peerA.GetPeerID(),
					},
					Hash: "$1$2$SbCpjO1HxdYGJCG_ge0XJ2wkevVvtc1jTVlA5aH0EydPOUEjNnxQKg0TgH6KjvzlAljDOFQ6ox59XyIdFLSIIw",
					Service: models.ServicePublicationPeer{
						PeerId:   peerA.GetPeerID(),
						Name:     "parkeerrechten",
						Protocol: "PROTOCOL_TCP_HTTP_1.1",
					},
					Type: models.GRANTTYPESERVICEPUBLICATION,
				})
				assert.NoError(t, err)

				result := []models.Contract{
					{
						Content: models.ContractContent{
							CreatedAt: testClock.Now().Unix(),
							Grants: []models.Grant{
								servicePublicationGrant,
							},
							GroupId:       "test-group",
							HashAlgorithm: "HASH_ALGORITHM_SHA3_512",
							Iv:            "102635ef-7053-4151-8fd1-2537f99bee79",
							Validity: models.Validity{
								NotAfter:  1609545723,
								NotBefore: 1609542123,
							},
						},
						HasAccepted: true,
						HasRejected: false,
						HasRevoked:  false,
						Hash:        "$1$1$FgwzoZPOZ0onDQqs2K-FAKikDCjvMY_k5qjC9kp4X5NdQu_FtJun-Q7TUBVvaixmHB5FqxM-huO-9pExqfLIGg",
						Peers: map[string]models.Peer{
							peerA.GetPeerID(): {
								Id:   peerA.GetPeerID(),
								Name: peerA.GetName(),
							},
						},
						Signatures: models.Signatures{
							Accept: models.SignatureMap{
								peerA.GetPeerID(): models.Signature{
									Peer: models.Peer{
										Id:   peerA.GetPeerID(),
										Name: peerA.GetName(),
									},
									SignedAt: time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location()).Unix(),
								},
							},
							Reject: models.SignatureMap{},
							Revoke: models.SignatureMap{},
						},
						State: models.CONTRACTSTATEEXPIRED,
					},
				}

				return result
			}(),
		},
	}

	for testName, test := range tests {
		tc := test

		t.Run(testName, func(t *testing.T) {
			externalHTTPServer, app := newService(t.Name())
			defer externalHTTPServer.Close()

			apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
			assert.NoError(t, err)

			for _, createContractArgs := range tc.ContractsToCreate {
				_, createErr := app.Commands.CreateContract.Handle(context.Background(), createContractArgs)
				assert.NoError(t, createErr)
			}

			res, err := apiClient.GetContractsWithResponse(context.Background(), tc.Params)
			assert.NoError(t, err)

			assert.Equal(t, http.StatusOK, res.StatusCode())
			assert.Len(t, res.JSON200.Contracts, len(test.Want))

			for i, c := range res.JSON200.Contracts {
				for j, g := range c.Content.Grants {
					wantGrant, grantErr := test.Want[i].Content.Grants[j].AsGrantServicePublication()
					assert.NoError(t, grantErr)

					gotGrant, grantErr := g.AsGrantServicePublication()
					assert.NoError(t, grantErr)

					assert.Equal(t, wantGrant, gotGrant)
				}
			}
			assert.Equal(t, test.Want, res.JSON200.Contracts)
		})
	}
}

func TestListContracts_ContractState_Valid(t *testing.T) {
	t.Parallel()

	tests := map[string]struct {
		ContractsToCreate []*command.CreateContractHandlerArgs
		Params            *models.GetContractsParams
		Want              []models.Contract
	}{
		"happy_flow": {
			ContractsToCreate: func() []*command.CreateContractHandlerArgs {
				servicePublicationGrant := models.Grant{}

				err := servicePublicationGrant.FromGrantServicePublication(models.GrantServicePublication{
					Type: models.GRANTTYPESERVICEPUBLICATION,
					Directory: models.DirectoryPeer{
						PeerId: peerA.GetPeerID(),
					},
					Service: models.ServicePublicationPeer{
						Name:     "parkeerrechten",
						PeerId:   peerA.GetPeerID(),
						Protocol: models.PROTOCOLTCPHTTP11,
					},
				})
				assert.NoError(t, err)

				iv := uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee79")
				assert.NoError(t, err)

				result := []*command.CreateContractHandlerArgs{
					{
						HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
						IV:                iv.String(),
						GroupID:           "test-group",
						ContractNotBefore: testClock.Now().Add(-1 * time.Hour),
						ContractNotAfter:  testClock.Now().Add(time.Hour),
						Grants: []interface{}{
							&command.GrantServicePublicationArgs{
								DirectoryPeerID: peerA.GetPeerID(),
								ServicePeerID:   peerA.GetPeerID(),
								ServiceName:     "parkeerrechten",
								ServiceProtocol: contract.ServiceProtocolTCPHTTP1_1,
							},
						},
						CreatedAt: testClock.Now(),
					},
				}

				return result
			}(),
			Params: &models.GetContractsParams{
				GrantType:   nil,
				ContentHash: &[]string{},
			},
			Want: func() []models.Contract {
				servicePublicationGrant := models.Grant{}

				err := servicePublicationGrant.FromGrantServicePublication(models.GrantServicePublication{
					Directory: models.DirectoryPeer{
						PeerId: peerA.GetPeerID(),
					},
					Hash: "$1$2$SbCpjO1HxdYGJCG_ge0XJ2wkevVvtc1jTVlA5aH0EydPOUEjNnxQKg0TgH6KjvzlAljDOFQ6ox59XyIdFLSIIw",
					Service: models.ServicePublicationPeer{
						PeerId:   peerA.GetPeerID(),
						Name:     "parkeerrechten",
						Protocol: "PROTOCOL_TCP_HTTP_1.1",
					},
					Type: models.GRANTTYPESERVICEPUBLICATION,
				})
				assert.NoError(t, err)

				result := []models.Contract{
					{
						Content: models.ContractContent{
							CreatedAt: testClock.Now().Unix(),
							Grants: []models.Grant{
								servicePublicationGrant,
							},
							GroupId:       "test-group",
							HashAlgorithm: "HASH_ALGORITHM_SHA3_512",
							Iv:            "102635ef-7053-4151-8fd1-2537f99bee79",
							Validity: models.Validity{
								NotAfter:  testClock.Now().Add(time.Hour).Unix(),
								NotBefore: testClock.Now().Add(-1 * time.Hour).Unix(),
							},
						},
						HasAccepted: true,
						HasRejected: false,
						HasRevoked:  false,
						Hash:        "$1$1$CxqtPvpuhm3_8fe8aBpwVQjyj0AgyEijFFlCLSIOORan_h5vs_lx41rftqMnGGJgRIN7eU9YXUIlI_OGdu-TvA",
						Peers: map[string]models.Peer{
							peerA.GetPeerID(): {
								Id:   peerA.GetPeerID(),
								Name: peerA.GetName(),
							},
						},
						Signatures: models.Signatures{
							Accept: models.SignatureMap{
								peerA.GetPeerID(): models.Signature{
									Peer: models.Peer{
										Id:   peerA.GetPeerID(),
										Name: peerA.GetName(),
									},
									SignedAt: time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location()).Unix(),
								},
							},
							Reject: models.SignatureMap{},
							Revoke: models.SignatureMap{},
						},
						State: models.CONTRACTSTATEVALID,
					},
				}

				return result
			}(),
		},
	}

	for testName, test := range tests {
		tc := test

		t.Run(testName, func(t *testing.T) {
			externalHTTPServer, app := newService(t.Name())
			defer externalHTTPServer.Close()

			apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
			assert.NoError(t, err)

			for _, createContractArgs := range tc.ContractsToCreate {
				_, createErr := app.Commands.CreateContract.Handle(context.Background(), createContractArgs)
				assert.NoError(t, createErr)
			}

			res, err := apiClient.GetContractsWithResponse(context.Background(), tc.Params)
			assert.NoError(t, err)

			assert.Equal(t, http.StatusOK, res.StatusCode())
			assert.Len(t, res.JSON200.Contracts, len(test.Want))

			for i, c := range res.JSON200.Contracts {
				for j, g := range c.Content.Grants {
					wantGrant, grantErr := test.Want[i].Content.Grants[j].AsGrantServicePublication()
					assert.NoError(t, grantErr)

					gotGrant, grantErr := g.AsGrantServicePublication()
					assert.NoError(t, grantErr)

					assert.Equal(t, wantGrant, gotGrant)
				}
			}
			assert.Equal(t, test.Want, res.JSON200.Contracts)
		})
	}
}
