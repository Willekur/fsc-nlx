// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"context"
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
)

func (s *Server) addServiceGetHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	inways, err := s.apiApp.Queries.ListInways.Handle(ctx, &query.ListInwaysArgs{
		AuthData:              authData,
		AuthorizationMetadata: getAuthzData(r),
		SelfPeerID:            s.peerInfo.ID,
	})
	if err != nil {
		s.logger.Error("get Inway addresses for form", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	inwayAddresses := []string{}

	for _, inway := range inways {
		inwayAddresses = append(inwayAddresses, inway.Address)
	}

	page := addServicePage{
		BasePage:       s.basePage,
		InwayAddresses: inwayAddresses,
		Form: &AddServicePageForm{
			Name:         "",
			EndpointURL:  "",
			InwayAddress: "",
		},
	}

	page.render(w)
}

func (s *Server) addServicePostHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	inways, err := s.apiApp.Queries.ListInways.Handle(ctx, &query.ListInwaysArgs{
		AuthData:              authData,
		AuthorizationMetadata: getAuthzData(r),
		SelfPeerID:            s.peerInfo.ID,
	})
	if err != nil {
		s.logger.Error("get Inway addresses for form", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	inwayAddresses := []string{}

	for _, inway := range inways {
		inwayAddresses = append(inwayAddresses, inway.Address)
	}

	formValues, err := decodeAddServicePageForm(r)
	if err != nil {
		s.logger.Error("failed to read form values", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	page := addServicePage{
		BasePage:       s.basePage,
		InwayAddresses: inwayAddresses,
		Form:           formValues,
	}

	err = s.apiApp.Commands.CreateService.Handle(context.Background(), &command.CreateServiceArgs{
		AuthData:              authData,
		AuthorizationMetadata: getAuthzData(r),
		Name:                  formValues.Name,
		EndpointURL:           formValues.EndpointURL,
		InwayAddress:          formValues.InwayAddress,
	})
	if err != nil {
		s.logger.Error("create service", err)

		if errors.Is(err, command.ErrServiceNameInUse) {
			page.renderWithErrorMessage(w, http.StatusInternalServerError, s.i18n.Translate("The name of the Service is already in use. Please use a different name."))

			return
		}

		page.renderWithError(w, err)

		return
	}

	page.renderWithSuccess(s.i18n.Translate("The Service has been added"), w)
}
