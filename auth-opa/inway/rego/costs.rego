package httpapi.inway

import rego.v1

default allow := {
	"allowed": false,
	"status": {"reason": "permission denied"},
}

allow := {"allowed": true, "status": {"reason": reason}} if {
	allowed

	reason := "request within budget"
}

# Allow organizations with enough budget left
allowed if {
	peer_id := input.headers["Fsc-Request-Peer-Id"][0]

	data.organizations[peer_id].budget_left >= 100
}
