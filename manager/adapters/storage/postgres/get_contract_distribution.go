// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/storage/postgres/queries"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func (r *PostgreSQLRepository) GetContractDistribution(ctx context.Context, contentHash *contract.ContentHash, peerID contract.PeerID, action contract.DistributionAction) (*contract.Distribution, error) {
	row, err := r.queries.GetContractDistribution(ctx, &queries.GetContractDistributionParams{ContentHash: contentHash.String(), PeerID: peerID.Value(), Action: queries.ContractsDistributionAction(action)})
	if err != nil {
		return nil, err
	}

	var nextRetryAt *time.Time
	if row.NextRetryAt.Valid {
		nextRetryAt = &row.NextRetryAt.Time
	}

	return &contract.Distribution{
		ContentHash:  contentHash,
		PeerID:       peerID,
		Action:       action,
		Signature:    row.Signature,
		Retries:      row.Retries,
		NextRetryAt:  nextRetryAt,
		LastRetryAt:  row.LastRetryAt,
		ErrorMessage: row.ErrorMessage,
	}, nil
}
