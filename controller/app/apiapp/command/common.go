// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package command

import (
	"errors"
	"fmt"
	"regexp"
	"strings"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
)

var (
	ErrServiceNotFound = errors.New("service could not be found")
)

var serviceNameValidator = regexp.MustCompile("^[a-zA-Z0-9-._]{1,100}$")
var serviceEndpointURLValidator = regexp.MustCompile(`^(https?)://\S+[^/]$`)
var inwayAddressValidator = regexp.MustCompile(`^https://\S+:443$`)

type ValidationError struct {
	m string
}

func newValidationError(message string) *ValidationError {
	return &ValidationError{
		m: message,
	}
}

func (v *ValidationError) Error() string {
	return "validation error: " + v.m
}

func mapValidationError(validationErrors []string) *ValidationError {
	if len(validationErrors) > 0 {
		return newValidationError(strings.Join(validationErrors, "\n"))
	}

	return nil
}

func squashValidationErrors(validationErrors []*ValidationError) *ValidationError {
	errorMessages := make([]string, 0, len(validationErrors))
	for _, validationError := range validationErrors {
		errorMessages = append(errorMessages, validationError.m)
	}

	return mapValidationError(errorMessages)
}

type InternalError struct {
	m string
}

func newInternalError(message string) *InternalError {
	return &InternalError{
		m: message,
	}
}

func (v *InternalError) Error() string {
	return "internal error: " + v.m
}

type AuthenticationError struct {
	m string
}

func newAuthenticationError(message string) *AuthenticationError {
	return &AuthenticationError{
		m: message,
	}
}

func (v *AuthenticationError) Error() string {
	return "authentication error: " + v.m
}

type AuthorizationError struct {
	m string
}

func newAuthorizationError(message string) *AuthorizationError {
	return &AuthorizationError{
		m: message,
	}
}

func (v *AuthorizationError) Error() string {
	return "authorization error: " + v.m
}

func mapError(err error, message string) error {
	var e *manager.ValidationError
	if errors.As(err, &e) {
		return fmt.Errorf("%s: %s: %w", message, e.Error(), err)
	}

	var a *authentication.AuthenticationError
	if errors.As(err, &a) {
		return fmt.Errorf("%s: %s: %w", message, newAuthenticationError(a.Error()), err)
	}

	var az *authz.AuthorizationError
	if errors.As(err, &az) {
		return fmt.Errorf("%s: %s: %w", message, newAuthorizationError(az.Error()), err)
	}

	return fmt.Errorf("%s: %w", newInternalError(message), err)
}

type Validator interface {
	Valid() *ValidationError
}
