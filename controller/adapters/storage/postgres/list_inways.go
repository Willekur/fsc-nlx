// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package postgresstorage

import (
	"context"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage"
)

func (p *PostgresStorage) ListInways(ctx context.Context, groupID string) ([]*storage.Inway, error) {
	rows, err := p.queries.ListInways(ctx, groupID)
	if err != nil {
		return nil, fmt.Errorf("could not list inways from database: %w", err)
	}

	inways := make([]*storage.Inway, len(rows))

	for i, inway := range rows {
		inways[i] = &storage.Inway{
			Name:    inway.Name,
			Address: inway.Address,
		}
	}

	return inways, nil
}
