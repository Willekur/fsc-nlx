// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package services

import (
	"context"
	"errors"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func (p *PeersCommunicationService) GetToken(ctx context.Context, peerID contract.PeerID, grantHash string) (string, error) {
	peer, err := p.getManagerClient(peerID)
	if err != nil {
		return "", fmt.Errorf("could not get client for peer %q to get token: %w", peerID.Value(), err)
	}

	token, err := peer.GetToken(ctx, grantHash)

	if err != nil {
		return "", errors.Join(err, fmt.Errorf("could not get token from peer: %s", peerID.Value()))
	}

	return token, nil
}
