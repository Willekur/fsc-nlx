// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"
	"crypto/x509"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"path"
	"path/filepath"
	"strings"
	"testing"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/services"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	discardlogger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/discard"
	"gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	postgresadapter "gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/storage/postgres"
	internalapp "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	restport "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api"
	externalservice "gitlab.com/commonground/nlx/fsc-nlx/manager/services/ext"
	internalservice "gitlab.com/commonground/nlx/fsc-nlx/manager/services/int"
	"gitlab.com/commonground/nlx/fsc-nlx/testing/testingutils"
)

var nowInUTC = time.Now().UTC()

var (
	testClock      = clock.NewMock(time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location()))
	accessTokenTTL = time.Minute
	peerA          *peerInfo
	peerB          *peerInfo
	peerC          *peerInfo
	peerDirectory  *peerInfo
	peerOnCRL      *peerInfo
)

func TestMain(m *testing.M) {
	var err error

	peerA, err = newPeerInfo(testingutils.NLXTestPeerA, "https://manager.org-a.nlx.local:443")
	if err != nil {
		log.Fatal(err)
	}

	peerB, err = newPeerInfo(testingutils.NLXTestPeerB, "https://manager.org-b.nlx.local:443")
	if err != nil {
		log.Fatal(err)
	}

	peerC, err = newPeerInfo(testingutils.NLXTestPeerC, "https://manager.org-c.nlx.local:443")
	if err != nil {
		log.Fatal(err)
	}

	peerDirectory, err = newPeerInfo(testingutils.NLXTestOrgDirectory, "https://directory.nlx.local:443")
	if err != nil {
		log.Fatal(err)
	}

	peerOnCRL, err = newPeerInfo(testingutils.OrgOnCRL, "https://manager.org-on-crl.nlx.local:443")
	if err != nil {
		log.Fatal(err)
	}

	m.Run()
}

type peerInfo struct {
	CertBundle     *tls.CertificateBundle
	ManagerAddress string
}

func newPeerInfo(organisationName testingutils.CertificateBundlePeerName, managerAddress string) (*peerInfo, error) {
	peerCertBundle, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "../", "testing", "pki"), organisationName)
	if err != nil {
		return nil, err
	}

	return &peerInfo{
		CertBundle:     peerCertBundle,
		ManagerAddress: managerAddress,
	}, nil
}

func (o *peerInfo) GetName() string {
	return o.CertBundle.GetPeerInfo().Name
}

func (o *peerInfo) GetPeerID() string {
	return o.CertBundle.GetPeerInfo().SerialNumber
}

//nolint:funlen,gocyclo // this is a test
func newService(testName string) (*httptest.Server, *internalapp.Application) {
	logger := discardlogger.New()

	orgACertBundle, err := testingutils.GetCertificateBundle(path.Join("..", "..", "../", "testing", "pki"), testingutils.NLXTestPeerA)
	if err != nil {
		log.Fatalf("failed to load organization a cert bundle: %v", err)
	}

	postgresDSN := os.Getenv("POSTGRES_DSN")

	if postgresDSN == "" {
		postgresDSN = "postgres://postgres@localhost:5432?sslmode=disable"
	}

	dbName := strings.ToLower(testName)
	dbName = fmt.Sprintf("ext_%s", dbName)

	testDB, err := testingutils.CreateTestDatabase(postgresDSN, dbName)
	if err != nil {
		log.Fatalf("failed to setup test database: %v", err)
	}

	db, err := postgresadapter.NewConnection(testDB)
	if err != nil {
		log.Fatalf("failed to create db connection: %v", err)
	}

	err = postgresadapter.PerformMigrations(testDB)
	if err != nil {
		log.Fatalf("failed to perform dbmigrations: %v", err)
	}

	postgresqlRepository, err := postgresadapter.New(orgACertBundle.RootCAs(), db)
	if err != nil {
		log.Fatalf("failed to setup postgresql database: %s", err)
	}

	controllerRepository := newFakeController()

	selfPeer, err := contract.NewPeer(&contract.NewPeerArgs{
		ID:             orgACertBundle.GetPeerInfo().SerialNumber,
		Name:           orgACertBundle.GetPeerInfo().Name,
		ManagerAddress: "https://manager.org-a.nlx.local:443",
	})
	if err != nil {
		log.Fatalf("failed to create self peer: %v", err)
	}

	managerFactory := testManagerFactory{}

	crlFileDer, err := os.ReadFile(filepath.Join("..", "..", "..", "testing", "pki", "ca.crl"))
	if err != nil {
		log.Fatal(err)
	}

	crlFile, err := x509.ParseRevocationList(crlFileDer)
	if err != nil {
		log.Fatal(err)
	}

	crlCache, err := tls.NewCRL(nil)
	if err != nil {
		log.Fatal("cannot create CRLsCache Cache")
	}

	err = crlCache.AddRevocationList("localhost", crlFile)
	if err != nil {
		log.Fatal("cannot add CRL to cache")
	}

	peersCommunicationService, err := services.NewPeersCommunicationService(&services.NewPeersCommunicationServiceArgs{
		Ctx:                            context.Background(),
		Logger:                         logger,
		LocalRepo:                      postgresqlRepository,
		DirectoryManagerAddress:        peerDirectory.ManagerAddress,
		ManagerFactory:                 &managerFactory,
		SelfPeerID:                     contract.PeerID(peerA.GetPeerID()),
		ContractDistributionRepository: postgresqlRepository,
	})
	if err != nil {
		log.Fatalf("failed to setup peers communication service: %s", err)
	}

	txlogRepository := newFakeTxLog()

	app, err := externalservice.NewApplication(&externalservice.NewApplicationArgs{
		Context:                   context.Background(),
		Clock:                     testClock,
		Logger:                    logger,
		Repository:                postgresqlRepository,
		ControllerRepository:      controllerRepository,
		TXLogRepository:           txlogRepository,
		PeersCommunicationService: peersCommunicationService,
		AutoSignGrants:            []string{},
		GroupID:                   "fsc-local",
		SelfPeer:                  selfPeer,
		TrustedRootCAs:            orgACertBundle.RootCAs(),
		AutoSignCertificate:       orgACertBundle.Cert(),
		TokenSignCertificate:      orgACertBundle.Cert(),
		TokenTTL:                  accessTokenTTL,
		IsTxlogDisabled:           false,
	})
	if err != nil {
		log.Fatalf("failed to setup application: %s", err)
	}

	server, err := restport.New(&restport.NewArgs{
		Logger:      logger,
		App:         app,
		Cert:        orgACertBundle,
		SelfAddress: peerA.ManagerAddress,
		CRLCache:    crlCache,
	})
	if err != nil {
		log.Fatalf("failed to setup rest port: %s", err)
	}

	intApp, err := internalservice.NewApplication(&internalservice.NewApplicationArgs{
		Context:                     context.Background(),
		Clock:                       testClock,
		Logger:                      logger,
		Repository:                  postgresqlRepository,
		PeersCommunicationService:   peersCommunicationService,
		TXLog:                       txlogRepository,
		SelfPeer:                    selfPeer,
		TrustedExternalRootCAs:      orgACertBundle.RootCAs(),
		SignatureCertificate:        orgACertBundle.Cert(),
		ControllerRepository:        controllerRepository,
		ManagerFactory:              &managerFactory,
		DirectoryPeerID:             contract.PeerID(peerDirectory.GetPeerID()),
		DirectoryPeerManagerAddress: peerDirectory.ManagerAddress,
	})
	if err != nil {
		log.Fatalf("failed to setup internal app: %s", err)
	}

	srv := httptest.NewUnstartedServer(server.Handler())
	srv.TLS = orgACertBundle.TLSConfig(orgACertBundle.WithTLSClientAuth())
	srv.StartTLS()

	return srv, intApp
}

func createExternalManagerAPIClient(managerURL string, certBundle *tls.CertificateBundle) (*api.ClientWithResponses, error) {
	return api.NewClientWithResponses(managerURL, func(c *api.Client) error {
		t := &http.Transport{
			TLSClientConfig: certBundle.TLSConfig(certBundle.WithTLSClientAuth()),
		}
		c.Client = &http.Client{
			Transport: t,
		}
		return nil
	})
}
