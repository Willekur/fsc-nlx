// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package internalservice_test

import (
	"context"
	"net/http"
	"testing"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/command"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	testhash "gitlab.com/commonground/nlx/fsc-nlx/testing/hash"
)

func TestListContracts(t *testing.T) {
	t.Parallel()

	now := time.Date(2023, 5, 23, 0, 0, 0, 0, time.UTC)

	tests := map[string]struct {
		ContractsToCreate []*command.CreateContractHandlerArgs
		Params            *models.GetContractsParams
		Want              []models.Contract
	}{
		"filter_by_content_hash": {
			ContractsToCreate: func() []*command.CreateContractHandlerArgs {
				servicePublicationGrant := models.Grant{}

				err := servicePublicationGrant.FromGrantServicePublication(models.GrantServicePublication{
					Type: models.GRANTTYPESERVICEPUBLICATION,
					Directory: models.DirectoryPeer{
						PeerId: peerC.GetPeerID(),
					},
					Service: models.ServicePublicationPeer{
						Name:     "basisregister-fictieve-kentekens",
						PeerId:   peerA.GetPeerID(),
						Protocol: models.PROTOCOLTCPHTTP11,
					},
				})
				assert.NoError(t, err)

				result := []*command.CreateContractHandlerArgs{
					{
						HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
						IV:                "102635ef-7053-4151-8fd1-2537f99bee79",
						GroupID:           "test-group",
						ContractNotBefore: now.Add(-1 * time.Hour),
						ContractNotAfter:  now.Add(time.Hour),
						Grants: []interface{}{
							&command.GrantServicePublicationArgs{
								DirectoryPeerID: peerC.GetPeerID(),
								ServicePeerID:   peerA.GetPeerID(),
								ServiceName:     "basisregister-fictieve-kentekens",
								ServiceProtocol: contract.ServiceProtocolTCPHTTP1_1,
							},
						},
						CreatedAt: now,
					},
				}

				return result
			}(),
			Params: &models.GetContractsParams{
				GrantType: nil,
				ContentHash: func() *[]string {
					hash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
						IV:             "102635ef-7053-4151-8fd1-2537f99bee79",
						GroupID:        "test-group",
						HashAlgorithm:  1,
						ValidNotBefore: now.Add(-1 * time.Hour).Unix(),
						ValidNotAfter:  now.Add(time.Hour).Unix(),
						ServicePublicationGrants: []testhash.ContractContentServicePublicationGrantArgs{
							{
								DirectoryPeerID: peerC.GetPeerID(),
								ServicePeerID:   peerA.GetPeerID(),
								ServiceName:     "basisregister-fictieve-kentekens",
								ServiceProtocol: string(models.PROTOCOLTCPHTTP11),
							},
						},
						ServiceConnectionGrants:           nil,
						DelegatedServiceConnectionGrants:  nil,
						DelegatedServicePublicationGrants: nil,
						CreatedAt:                         now.Unix(),
					})
					assert.NoError(t, err)

					return &[]string{hash}
				}(),
			},
			Want: func() []models.Contract {
				servicePublicationGrant := models.Grant{}

				err := servicePublicationGrant.FromGrantServicePublication(models.GrantServicePublication{
					Directory: models.DirectoryPeer{
						PeerId: peerC.GetPeerID(),
					},
					Hash: "$1$2$Uth-oG_uLYQSpKE_-3l_7Qh6fKYN3X68yoy7IY2V8JcNpA_7GQF-z3lvuM73Zmxlf6cIPnVQJflC1JHfWzy-DQ",
					Service: models.ServicePublicationPeer{
						PeerId:   peerA.GetPeerID(),
						Name:     "basisregister-fictieve-kentekens",
						Protocol: "PROTOCOL_TCP_HTTP_1.1",
					},
					Type: models.GRANTTYPESERVICEPUBLICATION,
				})
				assert.NoError(t, err)

				result := []models.Contract{
					{
						Content: models.ContractContent{
							CreatedAt: now.Unix(),
							Grants: []models.Grant{
								servicePublicationGrant,
							},
							GroupId:       "test-group",
							HashAlgorithm: "HASH_ALGORITHM_SHA3_512",
							Iv:            "102635ef-7053-4151-8fd1-2537f99bee79",
							Validity: models.Validity{
								NotAfter:  1684803600,
								NotBefore: 1684796400,
							},
						},
						HasAccepted: true,
						HasRejected: false,
						HasRevoked:  false,
						Hash:        "$1$1$Ly49CiNH6wOjOGbjuTwfaoFbyMZ9yfEDN0B5RQfdmy7eUnaS5kHm1aOgGe--gdJ0aEYkObJSf3D_oz0bWceMYw",
						Peers: map[string]models.Peer{
							peerA.GetPeerID(): {
								Id:   peerA.GetPeerID(),
								Name: peerA.GetName(),
							},
							peerC.GetPeerID(): {
								Id:   peerC.GetPeerID(),
								Name: peerC.GetName(),
							},
						},
						Signatures: models.Signatures{
							Accept: models.SignatureMap{
								peerA.GetPeerID(): models.Signature{
									Peer: models.Peer{
										Id:   peerA.GetPeerID(),
										Name: peerA.GetName(),
									},
									SignedAt: time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location()).Unix(),
								},
							},
							Reject: models.SignatureMap{},
							Revoke: models.SignatureMap{},
						},
						State: models.CONTRACTSTATEPROPOSED,
					},
				}

				return result
			}(),
		},
		"filter_by_grant_type": {
			ContractsToCreate: func() []*command.CreateContractHandlerArgs {
				servicePublicationGrant := models.Grant{}

				err := servicePublicationGrant.FromGrantServicePublication(models.GrantServicePublication{
					Type: models.GRANTTYPESERVICEPUBLICATION,
					Directory: models.DirectoryPeer{
						PeerId: peerC.GetPeerID(),
					},
					Service: models.ServicePublicationPeer{
						Name:     "basisregister-fictieve-personen",
						PeerId:   peerA.GetPeerID(),
						Protocol: models.PROTOCOLTCPHTTP11,
					},
				})
				assert.NoError(t, err)

				iv := uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee79")
				assert.NoError(t, err)

				result := []*command.CreateContractHandlerArgs{
					{
						HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
						IV:                iv.String(),
						GroupID:           "test-group",
						ContractNotBefore: now.Add(-1 * time.Hour),
						ContractNotAfter:  now.Add(time.Hour),
						Grants: []interface{}{
							&command.GrantServicePublicationArgs{
								DirectoryPeerID: peerC.GetPeerID(),
								ServicePeerID:   peerA.GetPeerID(),
								ServiceName:     "basisregister-fictieve-personen",
								ServiceProtocol: contract.ServiceProtocolTCPHTTP1_1,
							},
						},
						CreatedAt: now,
					},
				}

				return result
			}(),
			Params: &models.GetContractsParams{
				GrantType:   &[]models.GrantType{models.GRANTTYPESERVICEPUBLICATION},
				ContentHash: &[]string{},
			},
			Want: func() []models.Contract {
				servicePublicationGrant := models.Grant{}

				err := servicePublicationGrant.FromGrantServicePublication(models.GrantServicePublication{
					Directory: models.DirectoryPeer{
						PeerId: peerC.GetPeerID(),
					},
					Hash: "$1$2$HBB__t_EMgqEqSJYC5Aa58qZ1Q0Tgith3seqT1VYwB4SJHC57PzCNdW3YwnAiwAGRLVskc1SO3jqzapa65cbdQ",
					Service: models.ServicePublicationPeer{
						PeerId:   peerA.GetPeerID(),
						Name:     "basisregister-fictieve-personen",
						Protocol: "PROTOCOL_TCP_HTTP_1.1",
					},
					Type: models.GRANTTYPESERVICEPUBLICATION,
				})
				assert.NoError(t, err)

				result := []models.Contract{
					{
						Content: models.ContractContent{
							CreatedAt: now.Unix(),
							Grants: []models.Grant{
								servicePublicationGrant,
							},
							GroupId:       "test-group",
							HashAlgorithm: "HASH_ALGORITHM_SHA3_512",
							Iv:            "102635ef-7053-4151-8fd1-2537f99bee79",
							Validity: models.Validity{
								NotAfter:  1684803600,
								NotBefore: 1684796400,
							},
						},
						HasAccepted: true,
						HasRejected: false,
						HasRevoked:  false,
						Hash:        "$1$1$k7431-D7KUbmAXyrpJo5CWZx22sHkUH6MEf-moqfPZNiPGBkvWHkSDX3SiukV_eWAa6bvgZzfX_TBy7nnBhNAg",
						Peers: map[string]models.Peer{
							peerA.GetPeerID(): {
								Id:   peerA.GetPeerID(),
								Name: peerA.GetName(),
							},
							peerC.GetPeerID(): {
								Id:   peerC.GetPeerID(),
								Name: peerC.GetName(),
							},
						},
						Signatures: models.Signatures{
							Accept: models.SignatureMap{
								peerA.GetPeerID(): models.Signature{
									Peer: models.Peer{
										Id:   peerA.GetPeerID(),
										Name: peerA.GetName(),
									},
									SignedAt: time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location()).Unix(),
								},
							},
							Reject: models.SignatureMap{},
							Revoke: models.SignatureMap{},
						},
						State: models.CONTRACTSTATEPROPOSED,
					},
				}

				return result
			}(),
		},
		"happy_flow": {
			ContractsToCreate: func() []*command.CreateContractHandlerArgs {
				servicePublicationGrant := models.Grant{}

				err := servicePublicationGrant.FromGrantServicePublication(models.GrantServicePublication{
					Type: models.GRANTTYPESERVICEPUBLICATION,
					Directory: models.DirectoryPeer{
						PeerId: peerC.GetPeerID(),
					},
					Service: models.ServicePublicationPeer{
						Name:     "parkeerrechten",
						PeerId:   peerA.GetPeerID(),
						Protocol: models.PROTOCOLTCPHTTP11,
					},
				})
				assert.NoError(t, err)

				iv := uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee79")
				assert.NoError(t, err)

				result := []*command.CreateContractHandlerArgs{
					{
						HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
						IV:                iv.String(),
						GroupID:           "test-group",
						ContractNotBefore: now.Add(-1 * time.Hour),
						ContractNotAfter:  now.Add(time.Hour),
						Grants: []interface{}{
							&command.GrantServicePublicationArgs{
								DirectoryPeerID: peerC.GetPeerID(),
								ServicePeerID:   peerA.GetPeerID(),
								ServiceName:     "parkeerrechten",
								ServiceProtocol: contract.ServiceProtocolTCPHTTP1_1,
							},
						},
						CreatedAt: now,
					},
				}

				return result
			}(),
			Params: &models.GetContractsParams{
				GrantType:   nil,
				ContentHash: &[]string{},
			},
			Want: func() []models.Contract {
				servicePublicationGrant := models.Grant{}

				err := servicePublicationGrant.FromGrantServicePublication(models.GrantServicePublication{
					Directory: models.DirectoryPeer{
						PeerId: peerC.GetPeerID(),
					},
					Hash: "$1$2$MdEF9br_Qkx1psi3bfyEWnef9ZqzJMXoE0tOL1r1lzA77bP3Ps0tzo8SM816s0ykia_51iV8FgYmG1OBd8AgqA",
					Service: models.ServicePublicationPeer{
						PeerId:   peerA.GetPeerID(),
						Name:     "parkeerrechten",
						Protocol: "PROTOCOL_TCP_HTTP_1.1",
					},
					Type: models.GRANTTYPESERVICEPUBLICATION,
				})
				assert.NoError(t, err)

				result := []models.Contract{
					{
						Content: models.ContractContent{
							CreatedAt: now.Unix(),
							Grants: []models.Grant{
								servicePublicationGrant,
							},
							GroupId:       "test-group",
							HashAlgorithm: "HASH_ALGORITHM_SHA3_512",
							Iv:            "102635ef-7053-4151-8fd1-2537f99bee79",
							Validity: models.Validity{
								NotAfter:  1684803600,
								NotBefore: 1684796400,
							},
						},
						HasAccepted: true,
						HasRejected: false,
						HasRevoked:  false,
						Hash:        "$1$1$ZtNWN0ysfWT_9Qpb0MOLothOs2K1P1azbuaFP1bgbtHYM_iPlJqq1DkU2sBwQLg2IvX83BU7gkdl7EtiEWvpZw",
						Peers: map[string]models.Peer{
							peerA.GetPeerID(): {
								Id:   peerA.GetPeerID(),
								Name: peerA.GetName(),
							},
							peerC.GetPeerID(): {
								Id:   peerC.GetPeerID(),
								Name: peerC.GetName(),
							},
						},
						Signatures: models.Signatures{
							Accept: models.SignatureMap{
								peerA.GetPeerID(): models.Signature{
									Peer: models.Peer{
										Id:   peerA.GetPeerID(),
										Name: peerA.GetName(),
									},
									SignedAt: time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location()).Unix(),
								},
							},
							Reject: models.SignatureMap{},
							Revoke: models.SignatureMap{},
						},
						State: models.CONTRACTSTATEPROPOSED,
					},
				}

				return result
			}(),
		},
	}

	for testName, test := range tests {
		tc := test

		t.Run(testName, func(t *testing.T) {
			externalHTTPServer, app := newService(t.Name())
			defer externalHTTPServer.Close()

			apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
			assert.NoError(t, err)

			for _, createContractArgs := range tc.ContractsToCreate {
				_, createErr := app.Commands.CreateContract.Handle(context.Background(), createContractArgs)
				assert.NoError(t, createErr)
			}

			res, err := apiClient.GetContractsWithResponse(context.Background(), tc.Params)
			assert.NoError(t, err)

			assert.Equal(t, http.StatusOK, res.StatusCode())
			assert.Len(t, res.JSON200.Contracts, len(test.Want))
			assert.Equal(t, test.Want, res.JSON200.Contracts)
		})
	}
}

// contracts should be ordered by their created_at date by default
func TestListContractsOrder(t *testing.T) {
	t.Parallel()

	now := testClock.Now()

	externalHTTPServer, app := newService(t.Name())
	defer externalHTTPServer.Close()

	contractService := newTestContractService(&testContractServiceConfig{
		clock:       testClock,
		internalApp: app,
	})

	idA := contractService.NewWithCreatedAt(now.Add(-5 * time.Minute))
	idB := contractService.NewWithCreatedAt(now)
	idC := contractService.NewWithCreatedAt(now.Add(-48 * time.Hour))

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	res, err := apiClient.GetContractsWithResponse(context.Background(), &models.GetContractsParams{})
	assert.NoError(t, err)

	assert.Equal(t, http.StatusOK, res.StatusCode())
	assert.Len(t, res.JSON200.Contracts, 3)
	assert.Equal(t, idB.String(), res.JSON200.Contracts[0].Content.Iv)
	assert.Equal(t, idA.String(), res.JSON200.Contracts[1].Content.Iv)
	assert.Equal(t, idC.String(), res.JSON200.Contracts[2].Content.Iv)
}

// contracts should also be ordered by their created_at date when using a filter
func TestListContractsOrderWithFilter(t *testing.T) {
	t.Parallel()

	now := testClock.Now()

	externalHTTPServer, app := newService(t.Name())
	defer externalHTTPServer.Close()

	contractService := newTestContractService(&testContractServiceConfig{
		clock:       testClock,
		internalApp: app,
	})

	idA := contractService.NewServiceConnectionWithCreatedAt(now.Add(-48 * time.Hour))
	idB := contractService.NewServiceConnectionWithCreatedAt(now.Add(-10 * time.Minute))
	contractService.NewDelegatedServiceConnectionWithCreatedAt(now)
	idC := contractService.NewServiceConnectionWithCreatedAt(now.Add(-5 * time.Minute))
	idD := contractService.NewServiceConnectionWithCreatedAt(now)

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	res, err := apiClient.GetContractsWithResponse(context.Background(), &models.GetContractsParams{
		GrantType: &[]models.GrantType{
			models.GRANTTYPESERVICECONNECTION,
		},
	})
	assert.NoError(t, err)

	assert.Equal(t, http.StatusOK, res.StatusCode())
	assert.Len(t, res.JSON200.Contracts, 4)
	assert.Equal(t, idD.String(), res.JSON200.Contracts[0].Content.Iv)
	assert.Equal(t, idC.String(), res.JSON200.Contracts[1].Content.Iv)
	assert.Equal(t, idB.String(), res.JSON200.Contracts[2].Content.Iv)
	assert.Equal(t, idA.String(), res.JSON200.Contracts[3].Content.Iv)
}

func TestListContractsFilterAndPagination(t *testing.T) {
	t.Parallel()

	now := testClock.Now()

	externalHTTPServer, app := newService(t.Name())
	defer externalHTTPServer.Close()

	contractService := newTestContractService(&testContractServiceConfig{
		clock:       testClock,
		internalApp: app,
	})

	// multiple contracts so we have multiple pages.
	// including contracts with the same value for created_at, so
	// we can test if sorting based on the contract content hash is stable
	idA := contractService.NewServiceConnectionWithCreatedAtAndID(now.Add(-48*time.Hour), uuid.MustParse("94ed0235-c25c-44aa-a131-45a2fc743adf"))
	idB := contractService.NewServiceConnectionWithCreatedAtAndID(now.Add(-10*time.Minute), uuid.MustParse("6b14f7ac-13d1-4ccd-8a57-fc127beef9bf"))
	idC := contractService.NewServiceConnectionWithCreatedAtAndID(now.Add(-5*time.Minute), uuid.MustParse("7e8d9ad5-09ed-4fba-8c9d-960bf2817009"))
	idD := contractService.NewServiceConnectionWithCreatedAtAndID(now.Add(-5*time.Minute), uuid.MustParse("c82cfff0-6f71-438b-83c4-d7dac1b6d340"))
	idE := contractService.NewServiceConnectionWithCreatedAtAndID(now.Add(-5*time.Minute), uuid.MustParse("4ddef465-764c-413d-b64c-0b5b995f8c19"))
	idF := contractService.NewServiceConnectionWithCreatedAtAndID(now, uuid.MustParse("737b23c4-5807-4b72-8a9e-8a0a5c140ad5"))

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	limit := models.QueryPaginationLimit(3)
	sortOrder := models.SORTORDERASCENDING

	resFirstPage, err := apiClient.GetContractsWithResponse(context.Background(), &models.GetContractsParams{
		Limit:     &limit,
		SortOrder: &sortOrder,
		GrantType: &[]models.GrantType{
			models.GRANTTYPESERVICECONNECTION,
		},
	})
	assert.NoError(t, err)

	assert.Equal(t, http.StatusOK, resFirstPage.StatusCode())
	assert.Len(t, resFirstPage.JSON200.Contracts, 3)
	assert.Equal(t, idA.String(), resFirstPage.JSON200.Contracts[0].Content.Iv)
	assert.Equal(t, idB.String(), resFirstPage.JSON200.Contracts[1].Content.Iv)
	assert.Equal(t, idD.String(), resFirstPage.JSON200.Contracts[2].Content.Iv)

	resSecondPage, err := apiClient.GetContractsWithResponse(context.Background(), &models.GetContractsParams{
		Cursor:    &resFirstPage.JSON200.Pagination.NextCursor,
		Limit:     &limit,
		SortOrder: &sortOrder,
		GrantType: &[]models.GrantType{
			models.GRANTTYPESERVICECONNECTION,
		},
	})
	assert.NoError(t, err)

	if !assert.Equal(t, http.StatusOK, resSecondPage.StatusCode()) {
		t.Errorf("response body: %s", resSecondPage.Body)
	}

	assert.Len(t, resSecondPage.JSON200.Contracts, 3)
	assert.Equal(t, idC.String(), resSecondPage.JSON200.Contracts[0].Content.Iv)
	assert.Equal(t, idE.String(), resSecondPage.JSON200.Contracts[1].Content.Iv)
	assert.Equal(t, idF.String(), resSecondPage.JSON200.Contracts[2].Content.Iv)
}

// the contracts endpoint should take the pagination limit into account
func TestListContractsLimit(t *testing.T) {
	t.Parallel()

	externalHTTPServer, app := newService(t.Name())
	defer externalHTTPServer.Close()

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	contractService := newTestContractService(&testContractServiceConfig{
		clock:       testClock,
		internalApp: app,
	})

	contractService.New()
	contractService.New()
	contractService.New()

	limit := models.QueryPaginationLimit(2)

	res, err := apiClient.GetContractsWithResponse(context.Background(), &models.GetContractsParams{
		Limit: &limit,
	})
	assert.NoError(t, err)

	assert.Equal(t, http.StatusOK, res.StatusCode())
	assert.Len(t, res.JSON200.Contracts, 2)
}
