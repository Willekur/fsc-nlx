// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package testsignature_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/common/tls"
)

func getCertBundle(t *testing.T) *tls.CertificateBundle {
	bundle, err := tls.NewBundleFromFiles(
		"../../pki/internal/organization-b/certs/manager/cert.pem",
		"../../pki/internal/organization-b/certs/manager/key.pem",
		"../../pki/internal/organization-b/ca/root.pem",
	)
	assert.NoError(t, err)

	return bundle
}
