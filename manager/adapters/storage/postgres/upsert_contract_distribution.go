// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"database/sql"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/storage/postgres/queries"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func (r *PostgreSQLRepository) UpsertContractDistribution(ctx context.Context, peerID contract.PeerID, contentHash *contract.ContentHash, signature *contract.Signature, action contract.DistributionAction, errorMessage string, nextRetryAt *time.Time) error {
	var nextRetryAtSQL sql.NullTime

	if nextRetryAt != nil {
		nextRetryAtSQL = sql.NullTime{Time: *nextRetryAt, Valid: true}
	}

	return r.queries.UpsertContractDistribution(ctx, &queries.UpsertContractDistributionParams{
		PeerID:       peerID.Value(),
		ContentHash:  contentHash.String(),
		Signature:    signature.JWS(),
		Action:       queries.ContractsDistributionAction(action),
		ErrorMessage: errorMessage,
		NextRetryAt:  nextRetryAtSQL,
	})
}
