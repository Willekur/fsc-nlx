/*
 * Copyright © VNG Realisatie 2023
 * Licensed under the EUPL
 *
 */

package restmanager_test

import (
	"testing"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	discardlogger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/discard"
	mock_client "gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/manager/mock"
)

type mocks struct {
	client *mock_client.MockClient
	lgr    *logger.Logger
}

func newMocks(t *testing.T) *mocks {
	return &mocks{
		client: mock_client.NewMockClient(t),
		lgr:    discardlogger.New(),
	}
}
