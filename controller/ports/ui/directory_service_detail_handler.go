// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package uiport

import (
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
)

func (s *Server) directoryServiceDetailHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	authData, ok := s.getAuthDataFromCtx(ctx, w)
	if !ok {
		return
	}

	page := directoryServiceDetailPage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathDirectoryPage,
			Username:                    authData.Username(),
			LogoutURL:                   s.authn.LogoutURL(),
			Peer:                        s.getPeerInfo(ctx, getAuthzData(r), authData),
		},
	}

	peerServices, err := s.apiApp.Queries.ListPeerServices.Handle(ctx, &query.ListPeerServicesArgs{
		AuthData:              authData,
		AuthorizationMetadata: getAuthzData(r),
	})
	if err != nil {
		s.logger.Error("failed to retrieve peer services", err)
		http.Error(w, fmt.Sprintf("%s: %s", http.StatusText(http.StatusInternalServerError), err), http.StatusInternalServerError)

		return
	}

	if len(peerServices) == 0 {
		page.renderNotFound(w)
		return
	}

	serviceName := chi.URLParam(r, "serviceName")
	peerID := chi.URLParam(r, "peerID")
	delegatorID := chi.URLParam(r, "delegatorID")

	peerService := findPeerService(s.logger, peerServices, serviceName, peerID, delegatorID)

	if peerService == nil {
		page.renderNotFound(w)
		return
	}

	page.Service = convertPeerServiceToUI(peerService)

	outways, err := s.apiApp.Queries.ListOutways.Handle(ctx, &query.ListOutwaysArgs{
		AuthData:   authData,
		SelfPeerID: s.getPeerInfo(ctx, getAuthzData(r), authData).ID,
	})
	if err != nil {
		s.logger.Error("failed to retrieve outways", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	accessContracts, err := s.apiApp.Queries.ListContracts.Handle(ctx, &query.ListContractsArgs{
		AuthData: authData,
		Filters: []*query.ListContractsFilter{
			{
				GrantType: query.GrantTypeServiceConnection,
			},
		},
	})
	if err != nil {
		s.logger.Error("failed to retrieve contracts with service connection grant", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	peerInfo := s.getPeerInfo(ctx, getAuthzData(r), authData)

	page.OutwayCertificates = mapDataToView(outways, accessContracts, peerID, peerInfo.ID, serviceName)

	page.render(w)
}

func findPeerService(lgr *logger.Logger, services query.PeerServices, serviceName, peerID, delegatorID string) interface{} {
	for _, srv := range services {
		switch service := srv.(type) {
		case *query.DelegatedPeerService:
			if service.Name == serviceName && service.PeerID == delegatorID && service.DelegatorID == peerID {
				return service
			}
		case *query.PeerService:
			if service.Name == serviceName && service.PeerID == peerID {
				return service
			}
		default:
			lgr.Info(fmt.Sprintf("unknown peer service type %v", service))
		}
	}

	return nil
}

func convertPeerServiceToUI(peerService interface{}) *directoryServiceDetailPageService {
	result := &directoryServiceDetailPageService{}

	switch service := peerService.(type) {
	case *query.DelegatedPeerService:
		result = &directoryServiceDetailPageService{
			Name:                   service.Name,
			PeerID:                 service.DelegatorID,
			PeerName:               service.DelegatorName,
			ProviderID:             service.PeerID,
			ProviderName:           service.PeerName,
			IsDelegatedPublication: true,
		}
	case *query.PeerService:
		result = &directoryServiceDetailPageService{
			Name:                   service.Name,
			PeerID:                 service.PeerID,
			PeerName:               service.PeerName,
			IsDelegatedPublication: false,
		}
	}

	return result
}

func mapDataToView(outways []*query.Outway, accessContracts query.Contracts, servicePeerID, selfPeerID, serviceName string) []*directoryServiceDetailPageOutwayCertificate {
	result := []*directoryServiceDetailPageOutwayCertificate{}

	outwaysByCertificate := mapOutwaysByCertificate(outways)

	certificateAccessMap := map[string]*directoryServiceDetailPageOutwayCertificate{}

	for _, outway := range outways {
		entry, ok := certificateAccessMap[outway.CertificateThumbprint]
		if !ok {
			entry = &directoryServiceDetailPageOutwayCertificate{
				AccessContracts: []*directoryServiceDetailPageOutwayCertificateAccessContract{},
			}

			certificateAccessMap[outway.CertificateThumbprint] = entry
		}

		entry.CertificateThumbprint = outway.CertificateThumbprint
		entry.OutwayNames = append(entry.OutwayNames, outway.Name)
	}

	for _, contract := range accessContracts {
		for _, grant := range contract.ServiceConnectionGrants {
			if grant.ServicePeer.ID != servicePeerID {
				continue
			}

			if grant.ServiceName != serviceName {
				continue
			}

			if grant.OutwayPeer.ID != selfPeerID {
				continue
			}

			access, ok := certificateAccessMap[grant.OutwayCertificateThumbprint]
			if !ok {
				certificateAccessMap[grant.OutwayCertificateThumbprint] = &directoryServiceDetailPageOutwayCertificate{
					AccessContracts: []*directoryServiceDetailPageOutwayCertificateAccessContract{},
				}
				access = certificateAccessMap[grant.OutwayCertificateThumbprint]
			}

			access.CertificateThumbprint = grant.OutwayCertificateThumbprint
			access.OutwayNames = outwaysByCertificate[grant.OutwayCertificateThumbprint]
			access.AccessContracts = append(access.AccessContracts, &directoryServiceDetailPageOutwayCertificateAccessContract{
				IsActive:   contract.State == query.ContractStateValid,
				Status:     convertStateToHumanReadableText(contract.State),
				Title:      contract.IV,
				Hash:       contract.Hash,
				ValidFrom:  contract.ValidFrom.UTC(),
				ValidUntil: contract.ValidUntil.UTC(),
				CreatedAt:  contract.CreatedAt.UTC(),
			})
		}
	}

	for _, access := range certificateAccessMap {
		result = append(result, access)
	}

	return result
}

func mapOutwaysByCertificate(outways []*query.Outway) map[string][]string {
	result := map[string][]string{}

	for _, outway := range outways {
		outwayNames, owOk := result[outway.CertificateThumbprint]
		if !owOk {
			outwayNames = []string{}
		}

		result[outway.CertificateThumbprint] = append(outwayNames, outway.Name)
	}

	return result
}
