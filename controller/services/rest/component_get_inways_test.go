// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

// nolint:dupl // structure is similar but the details differ
package rest_test

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/internalapp/commands"
)

func TestGetInways(t *testing.T) {
	t.Parallel()

	// Arrange
	externalRESTAPIServer, internalApp := newService(t.Name())
	defer externalRESTAPIServer.Close()

	client, err := createExternalAPIClient(externalRESTAPIServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	err = internalApp.Commands.RegisterInway.Handle(context.Background(), &commands.RegisterInwayArgs{
		GroupID: "test-group",
		Name:    "my-inway",
		Address: "https://inway.local",
	})
	assert.NoError(t, err)

	// Act
	res, err := client.GetInwaysWithResponse(context.Background())
	assert.NoError(t, err)

	// Assert
	assert.Equal(t, http.StatusOK, res.StatusCode())
	assert.Len(t, res.JSON200.Inways, 1)
	assert.Equal(t, "my-inway", res.JSON200.Inways[0].Name)
	assert.Equal(t, "https://inway.local", res.JSON200.Inways[0].Address)
}
