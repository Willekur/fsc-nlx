-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL


BEGIN transaction;

ALTER TABLE contracts.distributions DROP COLUMN http_status_code;

COMMIT;
