// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"

	"github.com/pkg/errors"
	"golang.org/x/sync/singleflight"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type GetServicesHandler struct {
	managerFactory              manager.Factory
	directoryPeerManagerAddress string
	directoryPeer               manager.Manager
	directoryConnection         *singleflight.Group
}

type Services []interface{}

type Service struct {
	Name     string
	PeerID   string
	PeerName string
	Protocol contract.ServiceProtocol
}

type DelegatedService struct {
	Name          string
	PeerID        string
	PeerName      string
	DelegatorID   string
	DelegatorName string
	Protocol      contract.ServiceProtocol
}

func NewGetServicesHandler(managerFactory manager.Factory, directoryPeerManagerAddress string) (*GetServicesHandler, error) {
	if managerFactory == nil {
		return nil, errors.New("managerFactory is required")
	}

	if directoryPeerManagerAddress == "" {
		return nil, errors.New("directoryPeerManagerAddress is required")
	}

	return &GetServicesHandler{
		managerFactory:              managerFactory,
		directoryPeerManagerAddress: directoryPeerManagerAddress,
		directoryConnection:         &singleflight.Group{},
	}, nil
}

func (h *GetServicesHandler) Handle(ctx context.Context) (Services, error) {
	res, err, _ := h.directoryConnection.Do(h.directoryPeerManagerAddress, func() (interface{}, error) {
		if h.directoryPeer == nil {
			directory, err := h.managerFactory.New(h.directoryPeerManagerAddress)
			if err != nil {
				return nil, fmt.Errorf("could not create directory peer client: %w", err)
			}

			h.directoryPeer = directory
		}

		return h.directoryPeer.GetServices(ctx, nil, "")
	})
	if err != nil {
		return nil, fmt.Errorf("could not get services from directory: %w", err)
	}

	svcs := res.([]*contract.Service)

	services := make(Services, len(svcs))

	for i, s := range svcs {
		if s.DelegatorPeerID != "" {
			services[i] = &DelegatedService{
				Name:          s.Name,
				PeerID:        s.PeerID,
				PeerName:      s.PeerName,
				DelegatorID:   s.DelegatorPeerID,
				DelegatorName: s.DelegatorPeerName,
				Protocol:      s.Protocol,
			}
		} else {
			services[i] = &Service{
				Name:     s.Name,
				PeerID:   s.PeerID,
				PeerName: s.PeerName,
				Protocol: s.Protocol,
			}
		}
	}

	return services, nil
}
