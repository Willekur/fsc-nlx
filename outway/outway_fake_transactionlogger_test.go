// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package outway_test

import (
	"context"
	"errors"
	"time"

	"gitlab.com/commonground/nlx/fsc-nlx/common/transactionlog"
)

type fakeTransactionLogger struct {
	records []*transactionlog.Record
	time    time.Time
}

func newFakeTransactionLogger() *fakeTransactionLogger {
	return &fakeTransactionLogger{}
}

func (m *fakeTransactionLogger) AddRecords(_ context.Context, records []*transactionlog.Record) error {
	m.records = append(m.records, records...)
	m.time = time.Now()

	return nil
}

func (m *fakeTransactionLogger) Close() error {
	return nil
}

func (m *fakeTransactionLogger) GetRecords(grantHash string) []*transactionlog.Record {
	matchingRecords := make([]*transactionlog.Record, 0)

	for _, record := range m.records {
		if record.GrantHash == grantHash {
			matchingRecords = append(matchingRecords, record)
		}
	}

	return matchingRecords
}

type errorTransactionLogger struct{}

func (m *errorTransactionLogger) AddRecords(_ context.Context, records []*transactionlog.Record) error {
	return errors.New("cannot write record to transaction log")
}

func (m *errorTransactionLogger) Close() error {
	return nil
}
