// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package internalservice_test

import (
	"context"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/adapters/txlog"
)

type fakeTxLogAPI struct{}

func newFakeTxLog() txlog.TXLog {
	return &fakeTxLogAPI{}
}

func (t *fakeTxLogAPI) ListRecords(_ context.Context, _ *txlog.ListRecordsRequest) ([]*txlog.Record, error) {
	return []*txlog.Record{}, nil
}
