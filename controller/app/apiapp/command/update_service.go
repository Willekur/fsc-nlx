// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package command

import (
	"context"
	"errors"
	"fmt"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type UpdateServiceHandler struct {
	groupID contract.GroupID
	lgr     *logger.Logger
	s       storage.Storage
	auth    *auth.Auth
}

func NewUpdateServiceHandler(groupID contract.GroupID, s storage.Storage, a *auth.Auth, l *logger.Logger) (*UpdateServiceHandler, error) {
	if groupID == "" {
		return nil, fmt.Errorf("group id is required")
	}

	if l == nil {
		return nil, fmt.Errorf("logger is required")
	}

	if s == nil {
		return nil, fmt.Errorf("storage is required")
	}

	if a == nil {
		return nil, fmt.Errorf("auth is required")
	}

	return &UpdateServiceHandler{
		groupID: groupID,
		lgr:     l,
		s:       s,
		auth:    a,
	}, nil
}

type UpdateServiceArgs struct {
	AuthData              authentication.Data
	AuthorizationMetadata authorization.Metadata
	Name                  string
	EndpointURL           string
	InwayAddress          string
}

func (h *UpdateServiceHandler) Handle(ctx context.Context, args *UpdateServiceArgs) error {
	err := h.auth.Auth(ctx, args.AuthData, args.AuthorizationMetadata, authz.FscUpdateService, []*authz.ResourceURN{
		authz.NewResourceURN(authz.ResourceTypeService, args.Name),
	})
	if err != nil {
		return mapError(err, "could not authenticate or authorize")
	}

	if err := args.Valid(); err != nil {
		return err
	}

	err = h.s.UpdateService(ctx, &storage.UpdateServiceArgs{
		GroupID:      h.groupID.String(),
		Name:         args.Name,
		EndpointURL:  args.EndpointURL,
		InwayAddress: args.InwayAddress,
	})
	if err != nil {
		if errors.Is(err, storage.ErrServiceNotFound) {
			return ErrServiceNotFound
		}

		h.lgr.Error("failed to update service", err)

		return mapError(err, "could not update service in storage")
	}

	return nil
}

func (c *UpdateServiceArgs) Valid() *ValidationError {
	validationErrors := make([]string, 0)

	if c.Name == "" {
		validationErrors = append(validationErrors, "service name cannot be blank")
	}

	if !serviceNameValidator.MatchString(c.Name) {
		validationErrors = append(validationErrors, "service name must contain only letters, numbers, -, _ and .")
	}

	if !serviceEndpointURLValidator.MatchString(c.EndpointURL) {
		validationErrors = append(validationErrors, "endpoint URL cannot be blank, must start with https:// or http://, cannot contain spaces and cannot contain a trailing slash")
	}

	return mapValidationError(validationErrors)
}
