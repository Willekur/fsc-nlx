// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package rest

import (
	"context"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/query"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
)

func (s *Server) GetContractDistributions(ctx context.Context, req api.GetContractDistributionsRequestObject) (api.GetContractDistributionsResponseObject, error) {
	s.logger.Info("rest request GetContractDistributions")

	res, err := s.app.Queries.GetContractDistributions.Handle(ctx, req.Hash)
	if err != nil {
		s.logger.Warn("could not get contract distributions from querier", err)
		return nil, err
	}

	apiResponse := &api.GetContractDistributions200JSONResponse{
		Distributions: make([]models.ContractDistribution, len(res)),
	}
	for i, distribution := range res {
		apiResponse.Distributions[i] = convertContractDistribution(distribution)
	}

	return apiResponse, nil
}

func convertContractDistribution(distribution *query.ListContractDistribution) models.ContractDistribution {
	convertedDistribution := models.ContractDistribution{
		ContractHash: distribution.ContractHash,
		ErrorMessage: distribution.ErrorMessage,
		LastRetryAt:  distribution.LastRetryAt.Unix(),
		PeerId:       distribution.PeerID,
		Retries:      int(distribution.Retries),
		Signature:    distribution.Signature,
	}

	if distribution.NextRetryAt != nil {
		unixTimestamp := distribution.NextRetryAt.Unix()
		convertedDistribution.NextRetryAt = &unixTimestamp
	}

	switch distribution.Action {
	case query.DistributionActionSubmitContract:
		convertedDistribution.Action = models.DISTRIBUTIONACTIONSUBMITCONTRACT
	case query.DistributionActionSubmitRevokeSignature:
		convertedDistribution.Action = models.DISTRIBUTIONACTIONSUBMITREVOKESIGNATURE
	case query.DistributionActionSubmitRejectSignature:
		convertedDistribution.Action = models.DISTRIBUTIONACTIONSUBMITREJECTSIGNATURE
	case query.DistributionActionSubmitAcceptSignature:
		convertedDistribution.Action = models.DISTRIBUTIONACTIONSUBMITACCEPTSIGNATURE
	default:
		convertedDistribution.Action = models.DISTRIBUTIONACTIONSUBMITCONTRACT
	}

	return convertedDistribution
}
