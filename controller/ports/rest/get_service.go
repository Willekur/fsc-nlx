// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

package rest

import (
	"context"

	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/query"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/rest/api"
)

func (s *Server) GetService(ctx context.Context, req api.GetServiceRequestObject) (api.GetServiceResponseObject, error) {
	s.logger.Info("rest request GetService")

	service, err := s.app.Queries.GetService.Handle(ctx, &query.GetServiceArgs{
		AuthData: authData,
		Name:     req.Name,
	})
	if err != nil {
		if errors.Is(err, storage.ErrServiceNotFound) {
			return api.GetService404Response{}, nil
		}

		return nil, errors.Wrapf(err, "could not get service %q", req.Name)
	}

	return api.GetService200JSONResponse{
		EndpointUrl:  service.EndpointURL.String(),
		InwayAddress: service.InwayAddress,
		Name:         service.Name,
	}, nil
}
