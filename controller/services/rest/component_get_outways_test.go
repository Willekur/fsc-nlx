// Copyright © VNG Realisatie 2024
// Licensed under the EUPL

//go:build integration

// nolint:dupl // structure is similar but the details differ
package rest_test

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/internalapp/commands"
)

func TestGetOutways(t *testing.T) {
	t.Parallel()

	// Arrange
	externalRESTAPIServer, internalApp := newService(t.Name())
	defer externalRESTAPIServer.Close()

	client, err := createExternalAPIClient(externalRESTAPIServer.URL, peerA.CertBundle)
	assert.NoError(t, err)

	err = internalApp.Commands.RegisterOutway.Handle(context.Background(), &commands.RegisterOutwayArgs{
		GroupID:               "test-group",
		Name:                  "my-outway",
		CertificateThumbprint: "Vy92xRlrO0umpgf10jAjG0SB_JL8WMEbp5azkgcZku8",
	})
	assert.NoError(t, err)

	// Act
	res, err := client.GetOutwaysWithResponse(context.Background())
	assert.NoError(t, err)

	// Assert
	assert.Equal(t, http.StatusOK, res.StatusCode())
	assert.Len(t, res.JSON200.Outways, 1)
	assert.Equal(t, "my-outway", res.JSON200.Outways[0].Name)
	assert.Equal(t, "Vy92xRlrO0umpgf10jAjG0SB_JL8WMEbp5azkgcZku8", res.JSON200.Outways[0].CertificateThumbprint)
}
