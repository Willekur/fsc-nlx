// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"
	"errors"
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	common_tls "gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	externalapp "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/ext"
	external_errors "gitlab.com/commonground/nlx/fsc-nlx/manager/apps/ext/errors"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api"
)

type Server struct {
	app         *externalapp.Application
	logger      *logger.Logger
	cert        *common_tls.CertificateBundle
	selfAddress string
	handler     http.Handler
}

type connectingPeer struct {
	id             string
	name           string
	managerAddress string
}

type ContextField string

const (
	HeaderManagerAddress string = "fsc-manager-address"
)

const (
	ContextManagerAddress ContextField = "manager-address"
	ContextPeerID         ContextField = "peer-id"
	ContextPeerName       ContextField = "peer-name"
)

type NewArgs struct {
	Logger      *logger.Logger
	App         *externalapp.Application
	Cert        *common_tls.CertificateBundle
	SelfAddress string
	CRLCache    *common_tls.CRLsCache
}

func New(args *NewArgs) (*Server, error) {
	if args.SelfAddress == "" {
		return nil, errors.New("self address is required")
	}

	crl := args.CRLCache

	s := &Server{
		app:         args.App,
		logger:      args.Logger,
		cert:        args.Cert,
		selfAddress: args.SelfAddress,
	}

	strict := api.NewStrictHandlerWithOptions(s, []api.StrictMiddlewareFunc{
		func(f api.StrictHandlerFunc, operationID string) api.StrictHandlerFunc {
			return func(ctx context.Context, w http.ResponseWriter, r *http.Request, args interface{}) (interface{}, error) {
				if len(r.TLS.PeerCertificates) == 0 {
					return nil, fmt.Errorf("client certificate missing")
				}

				peerCert := r.TLS.PeerCertificates[0]

				if len(peerCert.Subject.Organization) == 0 || peerCert.Subject.Organization[0] == "" {
					return nil, fmt.Errorf("missing organization in subject of client certifcate")
				}

				if peerCert.Subject.SerialNumber == "" {
					return nil, fmt.Errorf("missing subject serial number in client certificate")
				}

				err := crl.CheckCertificate(peerCert)
				if err != nil {
					return nil, external_errors.NewErrorTypeCertificateRevoked(err, peerCert.SerialNumber.String())
				}

				if r.Header.Get(HeaderManagerAddress) == "" && r.Method != http.MethodGet && r.URL.Path != "/token" {
					return nil, fmt.Errorf("missing fsc-manager-address header")
				}

				ctx = context.WithValue(ctx, ContextPeerID, peerCert.Subject.SerialNumber)
				ctx = context.WithValue(ctx, ContextPeerName, peerCert.Subject.Organization[0])
				ctx = context.WithValue(ctx, ContextManagerAddress, r.Header.Get(HeaderManagerAddress))

				return f(ctx, w, r, args)
			}
		},
	}, api.StrictHTTPServerOptions{
		RequestErrorHandlerFunc:  RequestErrorHandler,
		ResponseErrorHandlerFunc: ResponseErrorHandler,
	})

	r := chi.NewRouter()
	api.HandlerFromMux(strict, r)

	s.handler = r

	return s, nil
}

func (s *Server) Handler() http.Handler {
	return s.handler
}

func getConnectingPeerFromCtx(ctx context.Context) (*connectingPeer, error) {
	managerAddress, ok := ctx.Value(ContextManagerAddress).(string)
	if !ok {
		return nil, fmt.Errorf("invalid peer manager address type in context, must be type string")
	}

	peerName, ok := ctx.Value(ContextPeerName).(string)
	if !ok {
		return nil, fmt.Errorf("invalid peer name type in context, must be type string")
	}

	peerID, ok := ctx.Value(ContextPeerID).(string)
	if !ok {
		return nil, fmt.Errorf("invalid peer ID type in context, must be type string")
	}

	return &connectingPeer{
		id:             peerID,
		name:           peerName,
		managerAddress: managerAddress,
	}, nil
}
