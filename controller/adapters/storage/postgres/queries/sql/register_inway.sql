-- Copyright © VNG Realisatie 2023
-- Licensed under the EUPL

-- name: RegisterInway :exec
INSERT INTO controller.inways (
    group_id,
    name,
    address
)
VALUES ($1, $2, $3)
ON CONFLICT ON CONSTRAINT inways_pk
DO
   UPDATE SET address = EXCLUDED.address;
