// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package restconfig

import (
	"context"
	"fmt"

	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
	"gitlab.com/commonground/nlx/fsc-nlx/outway/domain/config"
)

type Repository struct {
	managerClient          *api.ClientWithResponses
	externalCertThumbprint string
}

func New(ctx context.Context, managerClient *api.ClientWithResponses, externalCertThumbprint string) (config.Repository, error) {
	if externalCertThumbprint == "" {
		return nil, fmt.Errorf("missing external certificate thumbprint")
	}

	if managerClient == nil {
		return nil, fmt.Errorf("manager client is required")
	}

	return &Repository{
		managerClient:          managerClient,
		externalCertThumbprint: externalCertThumbprint,
	}, nil
}

func (r *Repository) Close() error {
	return nil
}
