// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"
	"net/http"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/command"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/ext/rest/api/models"
	internal_models "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
)

func TestGetServices(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, intApp := newService(t.Name())
	defer externalHTTPServer.Close()

	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, peerB.CertBundle)
	assert.NoError(t, err)

	contractIV, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIV,
		CreatedAt:     time.Now(),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: time.Now(),
			NotAfter:  time.Now().Add(time.Hour),
		},
		Grants: []interface{}{
			&contract.NewGrantServicePublicationArgs{
				Directory: &contract.NewGrantServicePublicationDirectoryArgs{
					Peer: &contract.NewPeerArgs{
						ID:             peerB.GetPeerID(),
						Name:           peerB.GetName(),
						ManagerAddress: peerB.ManagerAddress,
					},
				},
				Service: &contract.NewGrantServicePublicationServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:             peerA.GetPeerID(),
						Name:           peerA.GetName(),
						ManagerAddress: peerA.ManagerAddress,
					},
					Name:     "parkeerrechten",
					Protocol: contract.ServiceProtocolTCPHTTP1_1,
				},
			},
		},
	})
	assert.NoError(t, err)

	_, err = intApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
		IV:                contractContent.IV().String(),
		GroupID:           contractContent.GroupID(),
		ContractNotBefore: contractContent.NotBefore(),
		ContractNotAfter:  contractContent.NotAfter(),
		Grants: []interface{}{
			&command.GrantServicePublicationArgs{
				DirectoryPeerID: contractContent.Grants().ServicePublicationGrants()[0].Directory().Peer().ID().Value(),
				ServicePeerID:   contractContent.Grants().ServicePublicationGrants()[0].Service().Peer().ID().Value(),
				ServiceName:     contractContent.Grants().ServicePublicationGrants()[0].Service().Name(),
				ServiceProtocol: contractContent.Grants().ServicePublicationGrants()[0].Service().Protocol(),
			},
		},
		CreatedAt: time.Now(),
	})
	assert.NoError(t, err)

	acceptSignatureOrgB, err := contractContent.Accept(peerB.CertBundle.RootCAs(), peerB.CertBundle.Cert(), time.Now())
	assert.NoError(t, err)

	grantData := models.FSCCoreGrant_Data{}
	_ = grantData.FromFSCCoreGrantServicePublication(models.FSCCoreGrantServicePublication{
		Type: models.GRANTTYPESERVICEPUBLICATION,
		Directory: models.FSCCoreDirectory{
			PeerId: peerB.GetPeerID(),
		},
		Service: models.FSCCoreServicePublication{
			Name:     "parkeerrechten",
			PeerId:   peerA.GetPeerID(),
			Protocol: models.PROTOCOLTCPHTTP11,
		},
	})

	acceptContractResp, err := client.AcceptContractWithResponse(context.Background(), contractContent.Hash().String(), &models.AcceptContractParams{
		FscManagerAddress: peerB.ManagerAddress,
	}, models.AcceptContractJSONRequestBody{
		ContractContent: models.FSCCoreContractContent{
			CreatedAt: contractContent.CreatedAt().Unix(),
			Grants: []models.FSCCoreGrant{
				{
					Data: grantData,
				},
			},
			GroupId:       contractContent.GroupID(),
			HashAlgorithm: models.HASHALGORITHMSHA3512,
			Iv:            contractContent.IV().String(),
			Validity: models.FSCCoreValidity{
				NotAfter:  contractContent.NotAfter().Unix(),
				NotBefore: contractContent.NotBefore().Unix(),
			},
		},
		Signature: acceptSignatureOrgB.JWS(),
	})
	assert.NoError(t, err)
	assert.Equal(t, http.StatusCreated, acceptContractResp.StatusCode())
	assert.Equal(t, "", string(acceptContractResp.Body))

	// Act
	peerID := peerA.GetPeerID()
	serviceName := "parkeerrechten"

	res, err := client.GetServicesWithResponse(context.Background(), &models.GetServicesParams{
		Cursor:      nil,
		Limit:       nil,
		SortOrder:   nil,
		PeerId:      &peerID,
		ServiceName: &serviceName,
	})
	assert.NoError(t, err)

	// Assert
	assert.Equal(t, http.StatusOK, res.StatusCode())

	data := models.FSCCoreServiceListing_Data{}

	_ = data.FromFSCCoreServiceListingService(models.FSCCoreServiceListingService{
		Type: models.TYPESERVICE,
		Name: "parkeerrechten",
		Peer: models.FSCCorePeer{
			Id:             peerA.GetPeerID(),
			ManagerAddress: peerA.ManagerAddress,
			Name:           peerA.GetName(),
		},
		Protocol: models.PROTOCOLTCPHTTP11,
	})

	expectedServices := []models.FSCCoreServiceListing{
		{
			Data: data,
		},
	}

	assert.Equal(t, expectedServices, res.JSON200.Services)
}
