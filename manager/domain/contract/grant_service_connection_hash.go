// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package contract

import (
	"github.com/pkg/errors"
)

type grantServiceConnectionHash Hash

func newGrantServiceConnectionHash(gID GroupID, cID contentIV, alg HashAlg, gc *GrantServiceConnection) (*grantServiceConnectionHash, error) {
	h, err := newHash(alg, HashTypeGrantServiceConnection, getSortedGrantConnectionBytes(gID, cID, gc))
	if err != nil {
		return nil, errors.Wrap(err, "could not create grant service connection hash")
	}

	return (*grantServiceConnectionHash)(h), nil
}

func getSortedGrantConnectionBytes(gID GroupID, cID contentIV, gc *GrantServiceConnection) []byte {
	grantBytes := make([]byte, 0)

	grantBytes = append(grantBytes, []byte(gID)...)
	grantBytes = append(grantBytes, cID.Bytes()...)
	grantBytes = append(grantBytes, []byte(gc.Outway().Peer().ID().Value())...)
	grantBytes = append(grantBytes, []byte(gc.Outway().CertificateThumbprint().Value())...)
	grantBytes = append(grantBytes, []byte(gc.Service().Peer().ID())...)
	grantBytes = append(grantBytes, []byte(gc.Service().Name())...)

	return grantBytes
}

func (h grantServiceConnectionHash) String() string {
	return Hash(h).String()
}
