// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package command_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/common/logger"
	discard_logger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/discard"

	mock_clock "gitlab.com/commonground/nlx/fsc-nlx/common/clock/mock"
	mock_authorization "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz/mock"
	mock_idgenerator "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/ivgenerator/mock_iv_generator"
	mock_manager "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager/mock"
	mock_storage "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage/mock"
	auth_helper "gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
)

type mocks struct {
	manager *mock_manager.MockManager
	storage *mock_storage.MockStorage
	auth    *auth_helper.Auth
	authz   *mock_authorization.MockAuthorization
	clock   *mock_clock.MockClock
	iv      *mock_idgenerator.MockIVGenerator
	logger  *logger.Logger
}

func newMocks(t *testing.T) *mocks {
	authz := mock_authorization.NewMockAuthorization(t)

	auth, err := auth_helper.New(authz)
	assert.NoError(t, err)

	return &mocks{
		manager: mock_manager.NewMockManager(t),
		storage: mock_storage.NewMockStorage(t),
		authz:   authz,
		auth:    auth,
		clock:   mock_clock.NewMockClock(t),
		iv:      mock_idgenerator.NewMockIVGenerator(t),
		logger:  discard_logger.New(),
	}
}
