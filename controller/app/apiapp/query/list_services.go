// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"
	"net/url"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

type ListServicesHandler struct {
	storage storage.Storage
	manager manager.Manager
	auth    *auth.Auth
	groupID *contract.GroupID
}

type NewListServicesHandlerArgs struct {
	Storage storage.Storage
	Manager manager.Manager
	Auth    *auth.Auth
	GroupID *contract.GroupID
}

type Services []*Service

type Service struct {
	Name         string
	EndpointURL  *url.URL
	InwayAddress string
	IsPublished  bool
}

func NewListServicesHandler(args *NewListServicesHandlerArgs) (*ListServicesHandler, error) {
	if args.Storage == nil {
		return nil, fmt.Errorf("storage is required")
	}

	if args.Manager == nil {
		return nil, fmt.Errorf("manager is required")
	}

	if args.Auth == nil {
		return nil, fmt.Errorf("auth is required")
	}

	if args.GroupID == nil {
		return nil, fmt.Errorf("group ID is required")
	}

	return &ListServicesHandler{
		storage: args.Storage,
		manager: args.Manager,
		auth:    args.Auth,
		groupID: args.GroupID,
	}, nil
}

type ListServicesArgs struct {
	AuthData              authentication.Data
	AuthorizationMetadata authorization.Metadata
	SelfPeerID            string
}

func (h *ListServicesHandler) Handle(ctx context.Context, args *ListServicesArgs) (Services, error) {
	err := h.auth.Auth(ctx, args.AuthData, args.AuthorizationMetadata, authz.FscListServices, []*authz.ResourceURN{
		authz.NewResourceURN(authz.ResourceTypeService, "*"),
	})
	if err != nil {
		return nil, mapError(err, "could not authenticate or authorize")
	}

	selfPeerID, err := contract.NewPeerID(args.SelfPeerID)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", newInternalError("could not parse self peer ID"), err)
	}

	services, err := h.storage.ListServices(ctx, h.groupID.String())
	if err != nil {
		return nil, fmt.Errorf("%s: %w", newInternalError("could not get services from storage"), err)
	}

	contracts, err := h.manager.ListContracts(ctx, []string{}, []manager.GrantType{
		manager.GrantTypeServicePublication,
		manager.GrantTypeDelegatedServicePublication,
	})
	if err != nil {
		return nil, fmt.Errorf("%s: %w", newInternalError("could not get contracts from manager"), err)
	}

	validContracts := retrieveValidContracts(contracts)

	resp := make(Services, len(services))

	for i, s := range services {
		resp[i] = &Service{
			Name:         s.Name,
			EndpointURL:  s.EndpointURL,
			InwayAddress: s.InwayAddress,
			IsPublished:  isServicePublished(selfPeerID, *s, validContracts),
		}
	}

	return resp, nil
}

func retrieveValidContracts(contracts []*manager.Contract) []*manager.Contract {
	var validContracts []*manager.Contract

	for _, c := range contracts {
		if c.State == manager.ContractStateValid {
			validContracts = append(validContracts, c)
		}
	}

	return validContracts
}

func isServicePublished(selfPeerID contract.PeerID, s storage.Service, validContracts []*manager.Contract) bool {
	for _, c := range validContracts {
		for _, grant := range c.ServicePublicationGrants {
			if grant.ServiceName == s.Name && c.GroupID == s.GroupID && grant.ServicePeerID == selfPeerID.Value() {
				return true
			}
		}

		for _, grant := range c.DelegatedServicePublicationGrants {
			if grant.ServiceName == s.Name && c.GroupID == s.GroupID && grant.ServicePeerID == selfPeerID.Value() {
				return true
			}
		}
	}

	return false
}
