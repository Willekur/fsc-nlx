// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package cmd

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"time"

	oidc_server "github.com/coreos/go-oidc/v3/oidc"
	"github.com/spf13/cobra"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/common/cmd"
	zaplogger "gitlab.com/commonground/nlx/fsc-nlx/common/logger/zap"
	"gitlab.com/commonground/nlx/fsc-nlx/common/logoptions"
	"gitlab.com/commonground/nlx/fsc-nlx/common/monitoring"
	"gitlab.com/commonground/nlx/fsc-nlx/common/process"
	common_tls "gitlab.com/commonground/nlx/fsc-nlx/common/tls"
	"gitlab.com/commonground/nlx/fsc-nlx/common/version"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz/rbac"
	authz_rest "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz/rest"
	authz_rest_client "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz/rest/api/client"
	uuidgenerator "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/ivgenerator/uuid"
	restmanager "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/manager/rest"
	postgresstorage "gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/storage/postgres"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/internalapp"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/auth"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/internalrest"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/rest"
	uiport "gitlab.com/commonground/nlx/fsc-nlx/controller/ports/ui"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/ui/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/ui/authentication/none"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/ui/authentication/oidc"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
)

type oidcOptions oidc.Options

var serveOpts struct {
	ListenAddressUI       string
	ListenAddressInternal string
	ListenAddressAPI      string

	MonitoringAddress      string
	GroupID                string
	ManagerAddressInternal string
	Environment            string
	StaticPath             string

	StoragePostgresDSN string

	AuthnType string
	oidcOptions

	AuthzType        string
	AuthzRestAddress string

	logoptions.LogOptions
	cmd.TLSOptions
}

//nolint:gochecknoinits,funlen,gocyclo // this is the recommended way to use cobra, also a lot of flags..
func init() {
	serveCommand.Flags().StringVarP(&serveOpts.ListenAddressUI, "listen-address-ui", "", "127.0.0.1:3001", "Address for the UI to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs.")
	serveCommand.Flags().StringVarP(&serveOpts.ListenAddressInternal, "listen-address-internal", "", "127.0.0.1:443", "Address for the internal api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs.")
	serveCommand.Flags().StringVarP(&serveOpts.ListenAddressAPI, "listen-address-api", "", "127.0.0.1:444", "Address for the api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs.")
	serveCommand.Flags().StringVarP(&serveOpts.MonitoringAddress, "monitoring-address", "", "127.0.0.1:8081", "Address for the monitoring endpoints to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs.")

	serveCommand.Flags().StringVarP(&serveOpts.GroupID, "group-id", "", "", "Group ID of the FSC Group")
	serveCommand.Flags().StringVarP(&serveOpts.ManagerAddressInternal, "manager-address-internal", "", "", "URL to the Manager internal API")

	serveCommand.Flags().StringVarP(&serveOpts.StoragePostgresDSN, "storage-postgres-dsn", "", "", "Postgres Connection URL")

	serveCommand.Flags().StringVarP(&serveOpts.StaticPath, "static-path", "", "public", "Path to the static web files")

	serveCommand.Flags().StringVarP(&serveOpts.LogOptions.LogType, "log-type", "", "live", "Set the logging config. See NewProduction and NewDevelopment at https://godoc.org/go.uber.org/zap#Logger.")
	serveCommand.Flags().StringVarP(&serveOpts.LogOptions.LogLevel, "log-level", "", "debug", "Set loglevel")

	serveCommand.Flags().StringVarP(&serveOpts.TLSOptions.RootCertFile, "tls-root-cert", "", "", "Absolute or relative path to the root cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSOptions.CertFile, "tls-cert", "", "", "Absolute or relative path to the cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSOptions.KeyFile, "tls-key", "", "", "Absolute or relative path to the key .pem")

	serveCommand.Flags().StringVarP(&serveOpts.AuthnType, "authn-type", "", "none", fmt.Sprintf("Type of the authentication adapter, valid values: %s", strings.Join(authentication.ValidStringAuthTypes(), ", ")))
	serveCommand.Flags().StringVarP(&serveOpts.oidcOptions.SecretKey, "authn-oidc-secret-key", "", "", "Secret key that is used for signing sessions")
	serveCommand.Flags().StringVarP(&serveOpts.oidcOptions.ClientID, "authn-oidc-client-id", "", "", "The OIDC client ID")
	serveCommand.Flags().StringVarP(&serveOpts.oidcOptions.ClientSecret, "authn-oidc-client-secret", "", "", "The OIDC client secret")
	serveCommand.Flags().StringVarP(&serveOpts.oidcOptions.DiscoveryURL, "authn-oidc-discovery-url", "", "", "The OIDC discovery URL")
	serveCommand.Flags().StringVarP(&serveOpts.oidcOptions.RedirectURL, "authn-oidc-redirect-url", "", "", "The OIDC redirect URL")
	serveCommand.Flags().BoolVarP(&serveOpts.oidcOptions.SessionCookieSecure, "authn-oidc-session-cookie-secure", "", false, "Use 'secure' cookies")

	serveCommand.Flags().StringVarP(&serveOpts.AuthzType, "authz-type", "", "rbac", fmt.Sprintf("Type of the authorization adapter, valid values: %s", strings.Join(authorization.ValidStringTypes(), ", ")))
	serveCommand.Flags().StringVarP(&serveOpts.AuthzRestAddress, "authz-rest-address", "", "", "Address for the authorization endpoint")

	if err := serveCommand.MarkFlagRequired("group-id"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("manager-address-internal"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("storage-postgres-dsn"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tls-root-cert"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tls-cert"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tls-key"); err != nil {
		log.Fatal(err)
	}
}

var serveCommand = &cobra.Command{
	Use:   "serve",
	Short: "Start the UI",
	Run: func(cmd *cobra.Command, args []string) {
		p := process.NewProcess()

		logger, err := zaplogger.New(serveOpts.LogOptions.LogLevel, serveOpts.LogOptions.LogType)
		if err != nil {
			log.Fatalf("failed to create logger: %v", err)
		}

		logger.Info(fmt.Sprintf("version info: version: %s source-hash: %s", version.BuildVersion, version.BuildSourceHash))

		zapLogger, err := serveOpts.LogOptions.ZapConfig().Build()
		if err != nil {
			log.Fatalf("failed to create new zap logger: %v", err)
		}

		monitoringService, err := monitoring.NewMonitoringService(serveOpts.MonitoringAddress, zapLogger)
		if err != nil {
			logger.Fatal("unable to create monitoring service", err)
		}

		go func() {
			if err = monitoringService.Start(); err != nil {
				if !errors.Is(err, http.ErrServerClosed) {
					logger.Fatal("error listening on monitoring service", err)
				}

				logger.Fatal("cannot start monitoringService", err)
			}
		}()

		if errValidate := common_tls.VerifyPrivateKeyPermissions(serveOpts.KeyFile); errValidate != nil {
			logger.Warn("invalid key permissions", err)
		}

		cert, err := common_tls.NewBundleFromFiles(serveOpts.CertFile, serveOpts.KeyFile, serveOpts.RootCertFile)
		if err != nil {
			logger.Fatal("loading internal TLS files", err)
		}

		ctx := context.Background()

		managerInternalClient, err := api.NewClientWithResponses(serveOpts.ManagerAddressInternal, func(c *api.Client) error {
			t := http.DefaultTransport.(*http.Transport).Clone()
			t.TLSClientConfig = cert.TLSConfig()

			c.Client = &http.Client{Transport: t}
			return nil
		})
		if err != nil {
			logger.Fatal("could not create manager internal client", err)
		}

		mgr, err := restmanager.New(managerInternalClient, logger)
		if err != nil {
			logger.Fatal("could not create rest manager", err)
		}

		db, err := postgresstorage.NewConnection(serveOpts.StoragePostgresDSN)
		if err != nil {
			logger.Fatal("could not create db connection", err)
		}

		storage, err := postgresstorage.New(db)
		if err != nil {
			logger.Fatal("could not create postgres storage", err)
		}

		idGenerator := uuidgenerator.New()

		groupID, err := contract.NewGroupID(serveOpts.GroupID)
		if err != nil {
			logger.Fatal(fmt.Sprintf("invalid group ID provided '%s'", serveOpts.GroupID), err)
		}

		authzType, err := authorization.TypeFromString(serveOpts.AuthzType)
		if err != nil {
			logger.Fatal("unknown authorization type", err)
		}

		var authz authz.Authorization

		switch authzType {
		case authorization.TypeRBAC:
			authz = rbac.New()
		case authorization.TypeRest:
			var address *url.URL
			address, err = url.Parse(serveOpts.AuthzRestAddress)
			if err != nil {
				logger.Fatal("invalid authz rest address", err)
			}

			if address.Scheme != "https" {
				logger.Fatal("invalid authz rest address", fmt.Errorf("must be an https address"))
			}

			var authzClient *authz_rest_client.ClientWithResponses
			authzClient, err = authz_rest_client.NewClientWithResponses(serveOpts.AuthzRestAddress)
			if err != nil {
				logger.Fatal("could not create rest authorization client", err)
			}

			authz, err = authz_rest.New(authzClient)
			if err != nil {
				logger.Fatal("failed to create rest authorization adapter", err)
			}
		}

		auth, err := auth.New(authz)
		if err != nil {
			logger.Fatal("could not create auth", err)
		}

		app, err := apiapp.NewApplication(ctx, &apiapp.NewApplicationArgs{
			Manager:     mgr,
			Storage:     storage,
			Auth:        auth,
			Clock:       clock.New(),
			IVGenerator: idGenerator,
			Logger:      logger,
			GroupID:     groupID,
		})
		if err != nil {
			logger.Fatal("could not create application", err)
		}

		workDir, err := os.Getwd()
		if err != nil {
			logger.Fatal("failed to get work dir", err)
		}

		staticFilesPath := filepath.Join(workDir, serveOpts.StaticPath)

		authType, err := authentication.AuthTypeFromString(serveOpts.AuthnType)
		if err != nil {
			logger.Fatal("unknown authentication type", err)
		}

		var authn authentication.Authenticator

		switch authType {
		case authentication.AuthTypeNone:
			authn = none.New()
		case authentication.AuthTypeOIDC:
			var oidcProvider *oidc_server.Provider

			oidcProvider, err = oidc_server.NewProvider(context.Background(), serveOpts.oidcOptions.DiscoveryURL)
			if err != nil {
				logger.Fatal("could not initialize OIDC provider", err)
			}

			oidcConfig := &oidc_server.Config{
				ClientID: serveOpts.oidcOptions.ClientID,
			}

			verifier := oidcProvider.Verifier(oidcConfig)

			authn, err = oidc.New(db, logger, oidcProvider, verifier, func(idToken *oidc_server.IDToken) (*oidc.IDTokenClaims, error) {
				claims := &oidc.IDTokenClaims{}
				err = idToken.Claims(claims)
				if err != nil {
					return nil, err
				}

				return claims, nil
			}, (*oidc.Options)(&serveOpts.oidcOptions))
			if err != nil {
				logger.Fatal("failed to create oidc authenticator", err)
			}
		}

		uiServer, err := uiport.New(ctx, &uiport.NewServerArgs{
			Locale:        "nl",
			StaticPath:    staticFilesPath,
			GroupID:       serveOpts.GroupID,
			Logger:        logger,
			APIApp:        app,
			Authenticator: authn,
		})
		if err != nil {
			logger.Fatal("failed to create ui server", err)
		}

		go func() {
			err = uiServer.ListenAndServe(serveOpts.ListenAddressUI)
			if err != nil {
				logger.Fatal("could not listen and serve", err)
			}
		}()

		internalApp, err := internalapp.NewApplication(&internalapp.NewApplicationArgs{
			Context: ctx,
			Storage: storage,
		})
		if err != nil {
			logger.Fatal("could not create application", err)
		}

		internalRestServer, err := internalrest.New(&internalrest.NewArgs{
			Logger: logger.Logger,
			App:    internalApp,
		})
		if err != nil {
			logger.Fatal("could not create internal rest server", err)
		}

		var readHeaderTimeout = 5 * time.Second

		internalSrv := &http.Server{
			ReadHeaderTimeout: readHeaderTimeout,
			Handler:           internalRestServer.Handler(),
			Addr:              serveOpts.ListenAddressInternal,
			TLSConfig:         cert.TLSConfig(cert.WithTLSClientAuth()),
		}

		go func() {
			logger.Info(fmt.Sprintf("starting internal rest server. listen address: %q", serveOpts.ListenAddressInternal))

			err = internalSrv.ListenAndServeTLS(serveOpts.CertFile, serveOpts.KeyFile)
			if err != nil {
				logger.Fatal("could not listen and serve internal rest server", err)
			}
		}()

		restAPI, err := rest.New(&rest.NewArgs{
			Logger: logger,
			App:    app,
			Cert:   cert,
		})
		if err != nil {
			logger.Fatal("could not create rest api", err)
		}

		srv := &http.Server{
			ReadHeaderTimeout: readHeaderTimeout,
			Handler:           restAPI.Handler(),
			Addr:              serveOpts.ListenAddressAPI,
			TLSConfig:         cert.TLSConfig(cert.WithTLSClientAuth()),
		}

		go func() {
			err = srv.ListenAndServeTLS(serveOpts.TLSOptions.CertFile, serveOpts.TLSOptions.KeyFile)
			if err != nil {
				logger.Fatal("could not listen and serve rest api server", err)
			}
		}()

		monitoringService.SetReady()

		p.Wait()

		logger.Info("starting graceful shutdown")

		gracefulCtx, cancel := context.WithTimeout(context.Background(), time.Minute)
		defer cancel()

		monitoringService.SetNotReady()

		err = internalSrv.Shutdown(gracefulCtx)
		if err != nil {
			logger.Error("could not shutdown internal rest server", err)
		}

		err = srv.Shutdown(gracefulCtx)
		if err != nil {
			logger.Error("could not shutdown rest api server", err)
		}

		err = uiServer.Shutdown(gracefulCtx)
		if err != nil {
			logger.Error("could not shutdown server", err)
		}

		if err := monitoringService.Stop(); err != nil {
			logger.Error("could not shutdown monitoringService", err)
		}
	},
}
