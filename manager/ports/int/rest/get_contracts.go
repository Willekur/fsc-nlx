// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"
	"encoding/base64"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/common/clock"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/apps/int/query"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
	"gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/models"
	api "gitlab.com/commonground/nlx/fsc-nlx/manager/ports/int/rest/api/server"
)

func (s *Server) GetContracts(ctx context.Context, req api.GetContractsRequestObject) (api.GetContractsResponseObject, error) {
	s.logger.Info("rest request GetContracts")

	grantTypes := []query.GrantType{}

	if req.Params.GrantType != nil {
		for _, h := range *req.Params.GrantType {
			if h == "" {
				continue
			}

			gt := convertGrantType(h)
			grantTypes = append(grantTypes, gt)
		}
	}

	contentHashes := make([]string, 0)

	if req.Params.ContentHash != nil {
		for _, h := range *req.Params.ContentHash {
			if h == "" {
				continue
			}

			contentHashes = append(contentHashes, h)
		}
	}

	sortOrder := query.SortOrderDescending

	if req.Params.SortOrder != nil && *req.Params.SortOrder == models.SORTORDERASCENDING {
		sortOrder = query.SortOrderAscending
	}

	limit := uint32(0)

	if req.Params.Limit != nil {
		limit = *req.Params.Limit
	}

	paginationCursorStartDate := time.Time{}
	paginationCursorStartHash := ""

	if req.Params.Cursor != nil {
		startDate, hash, err := decodePaginationCursor(*req.Params.Cursor)
		if err != nil {
			return nil, err
		}

		paginationCursorStartDate = startDate
		paginationCursorStartHash = hash
	}

	res, err := s.app.Queries.GetContracts.Handle(ctx, &query.ListContractsHandlerArgs{
		GrantTypes:          grantTypes,
		ContentHashes:       contentHashes,
		PaginationStartDate: paginationCursorStartDate,
		PaginationStartHash: paginationCursorStartHash,
		PaginationLimit:     limit,
		PaginationSortOrder: sortOrder,
	})
	if err != nil {
		s.logger.Error("get contracts query", err)
		return nil, err
	}

	response := api.GetContracts200JSONResponse{
		Contracts:  make([]models.Contract, len(res)),
		Pagination: models.PaginationResult{},
	}

	for i, c := range res {
		var convertedContract *models.Contract

		convertedContract, err = convertContract(c, s.clock)
		if err != nil {
			s.logger.Error("get contracts converting contract", err)
			return nil, err
		}

		response.Contracts[i] = *convertedContract
	}

	if len(res) == 0 {
		return response, nil
	}

	lastContract := res[len(res)-1]
	nextCursor := createNextCursorForContract(lastContract.CreatedAt, lastContract.Hash)

	response.Pagination.NextCursor = nextCursor

	return response, nil
}

func decodePaginationCursor(input string) (time.Time, string, error) {
	decodedCursor, err := base64.RawURLEncoding.DecodeString(input)
	if err != nil {
		return time.Now(), "", errors.Wrapf(err, "could not decode cursor from base64 string: %s", input)
	}

	parts := strings.Split(string(decodedCursor), ",")

	const partsInCursor = 2

	if len(parts) != partsInCursor {
		return time.Now(), "", fmt.Errorf("cursor should consist of two parts. found %d", len(parts))
	}

	integerDatetime, err := strconv.Atoi(parts[0])
	if err != nil {
		return time.Now(), "", fmt.Errorf("datetime is not a valid integer: %q", parts[0])
	}

	return time.Unix(int64(integerDatetime), 0), parts[1], nil
}

func createNextCursorForContract(createdAt time.Time, contentHash string) string {
	cursor := fmt.Sprintf("%d,%s", createdAt.Unix(), contentHash)

	return base64.RawURLEncoding.EncodeToString([]byte(cursor))
}

func convertContract(c *query.ListContractsContract, clck clock.Clock) (*models.Contract, error) {
	grants, err := convertGrants(c)
	if err != nil {
		return nil, err
	}

	iv, err := uuid.FromBytes(c.IV)
	if err != nil {
		return nil, err
	}

	content := models.ContractContent{
		Iv:            iv.String(),
		CreatedAt:     c.CreatedAt.Unix(),
		Grants:        grants,
		GroupId:       c.GroupID,
		HashAlgorithm: models.HASHALGORITHMSHA3512,
		Validity: models.Validity{
			NotAfter:  c.NotAfter.Unix(),
			NotBefore: c.NotBefore.Unix(),
		},
	}

	peers := make(map[string]models.Peer)

	for _, p := range c.Peers {
		peers[p.ID] = models.Peer{
			Id:   p.ID,
			Name: p.Name,
		}
	}

	state := getContractState(c, clck)

	return &models.Contract{
		Hash:        c.Hash,
		Content:     content,
		Peers:       peers,
		HasAccepted: c.HasAccepted,
		HasRejected: c.HasRejected,
		HasRevoked:  c.HasRevoked,
		Signatures: models.Signatures{
			Accept: mapSignatureToRest(c.AcceptSignatures),
			Reject: mapSignatureToRest(c.RejectSignatures),
			Revoke: mapSignatureToRest(c.RevokeSignatures),
		},
		State: state,
	}, nil
}

func getContractState(c *query.ListContractsContract, clck clock.Clock) models.ContractState {
	result := models.CONTRACTSTATEPROPOSED

	if len(c.RejectSignatures) > 0 {
		return models.CONTRACTSTATEREJECTED
	}

	if len(c.RevokeSignatures) > 0 {
		return models.CONTRACTSTATEREVOKED
	}

	if len(c.Peers) == len(c.AcceptSignatures) {
		if clck.Now().After(c.NotAfter) {
			return models.CONTRACTSTATEEXPIRED
		} else {
			return models.CONTRACTSTATEVALID
		}
	}

	return result
}

func mapSignatureToRest(sigs map[string]query.Signature) models.SignatureMap {
	s := make(models.SignatureMap)

	for k, v := range sigs {
		s[k] = models.Signature{
			Peer: models.Peer{
				Id:   v.Peer.ID,
				Name: v.Peer.Name,
			},
			SignedAt: v.SignedAt.Unix(),
		}
	}

	return s
}

//nolint:gocyclo // complex function because of the number of grant types
func convertGrants(contract *query.ListContractsContract) ([]models.Grant, error) {
	grants := make([]models.Grant, 0)

	for _, g := range contract.ServiceConnectionGrants {
		grant, err := convertServiceConnectionGrant(g)
		if err != nil {
			return nil, err
		}

		grants = append(grants, *grant)
	}

	for _, g := range contract.ServicePublicationGrants {
		grant, err := convertServicePublicationGrant(g)
		if err != nil {
			return nil, err
		}

		grants = append(grants, *grant)
	}

	for _, g := range contract.DelegatedServiceConnectionGrants {
		grant, err := convertDelegatedServiceConnectionGrant(g)
		if err != nil {
			return nil, err
		}

		grants = append(grants, *grant)
	}

	for _, g := range contract.DelegatedServicePublicationGrants {
		grant, err := convertDelegatedServicePublicationGrant(g)
		if err != nil {
			return nil, err
		}

		grants = append(grants, *grant)
	}

	return grants, nil
}

func convertServicePublicationGrant(grant *query.ServicePublicationGrant) (*models.Grant, error) {
	data := models.Grant{}

	err := data.FromGrantServicePublication(models.GrantServicePublication{
		Type: models.GRANTTYPESERVICEPUBLICATION,
		Hash: grant.Hash,
		Directory: models.DirectoryPeer{
			PeerId: grant.DirectoryPeerID,
		},
		Service: models.ServicePublicationPeer{
			Name:     grant.ServiceName,
			PeerId:   grant.ServicePeerID,
			Protocol: mapServiceProtocolToRest(grant.ServiceProtocol),
		},
	})

	if err != nil {
		return nil, err
	}

	return &data, nil
}

func convertServiceConnectionGrant(grant *query.ServiceConnectionGrant) (*models.Grant, error) {
	data := models.Grant{}
	err := data.FromGrantServiceConnection(models.GrantServiceConnection{
		Type: models.GRANTTYPESERVICECONNECTION,
		Hash: grant.Hash,
		Outway: models.OutwayPeer{
			PeerId:                grant.OutwayPeerID,
			CertificateThumbprint: grant.OutwayCertificateThumbprint,
		},
		Service: models.ServicePeer{
			PeerId: grant.ServicePeerID,
			Name:   grant.ServiceName,
		},
	})

	if err != nil {
		return nil, err
	}

	return &data, nil
}

func convertDelegatedServiceConnectionGrant(grant *query.DelegatedServiceConnectionGrant) (*models.Grant, error) {
	data := models.Grant{}
	err := data.FromGrantDelegatedServiceConnection(models.GrantDelegatedServiceConnection{
		Type: models.GRANTTYPEDELEGATEDSERVICECONNECTION,
		Hash: grant.Hash,
		Delegator: models.DelegatorPeer{
			PeerId: grant.DelegatorPeerID,
		},
		Service: models.ServicePeer{
			PeerId: grant.ServicePeerID,
			Name:   grant.ServiceName,
		},
		Outway: models.OutwayPeer{
			PeerId:                grant.OutwayPeerID,
			CertificateThumbprint: grant.OutwayCertificateThumbprint,
		},
	})

	if err != nil {
		return nil, err
	}

	return &data, nil
}

func convertDelegatedServicePublicationGrant(grant *query.DelegatedServicePublicationGrant) (*models.Grant, error) {
	data := models.Grant{}
	err := data.FromGrantDelegatedServicePublication(models.GrantDelegatedServicePublication{
		Type: models.GRANTTYPEDELEGATEDSERVICEPUBLICATION,
		Hash: grant.Hash,
		Delegator: models.DelegatorPeer{
			PeerId: grant.DelegatorPeerID,
		},
		Service: models.ServicePublicationPeer{
			Name:     grant.ServiceName,
			PeerId:   grant.ServicePeerID,
			Protocol: mapServiceProtocolToRest(grant.ServiceProtocol),
		},
		Directory: models.DirectoryPeer{
			PeerId: grant.DirectoryPeerID,
		},
	})

	if err != nil {
		return nil, err
	}

	return &data, nil
}

func convertGrantType(grantType models.GrantType) query.GrantType {
	switch grantType {
	case models.GRANTTYPESERVICECONNECTION:
		return query.GrantTypeServiceConnection
	case models.GRANTTYPESERVICEPUBLICATION:
		return query.GrantTypeServicePublication
	case models.GRANTTYPEDELEGATEDSERVICECONNECTION:
		return query.GrantTypeDelegatedServiceConnection
	case models.GRANTTYPEDELEGATEDSERVICEPUBLICATION:
		return query.GrantTypeDelegatedServicePublication
	default:
		return query.GrantTypeUnspecified
	}
}

func mapServiceProtocolToRest(p contract.ServiceProtocol) models.Protocol {
	switch p {
	case contract.ServiceProtocolTCPHTTP1_1:
		return models.PROTOCOLTCPHTTP11
	case contract.ServiceProtocolTCPHTTP2:
		return models.PROTOCOLTCPHTTP2
	default:
		return ""
	}
}
