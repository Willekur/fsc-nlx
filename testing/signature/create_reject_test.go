// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//nolint:dupl // this is a test
package testsignature_test

import (
	"crypto"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	testsignature "gitlab.com/commonground/nlx/fsc-nlx/testing/signature"
)

func TestCreateReject(t *testing.T) {
	type args struct {
		contentHash           string
		certificateThumbprint string
		privateKey            crypto.PrivateKey
		signedAt              time.Time
	}

	tests := map[string]struct {
		args    args
		want    string
		wantErr bool
	}{
		"happy_flow": {
			args: args{
				contentHash:           "my-content-hash",
				certificateThumbprint: getCertBundle(t).CertificateThumbprint(),
				privateKey:            getCertBundle(t).Cert().PrivateKey,
				signedAt:              time.Date(2023, 1, 2, 3, 4, 5, 6, time.UTC),
			},
			want:    "eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2Ijoia3R4MVlPdHh4ZlU4Ykl6NExoM3pIaXBrSDFueFpYLWI3WWJuUFNBQzNIRSJ9.eyJjb250ZW50SGFzaCI6Im15LWNvbnRlbnQtaGFzaCIsInR5cGUiOiJyZWplY3QiLCJzaWduZWRBdCI6MTY3MjYyODY0NX0.SeEj5I7MGnYWHMgTtTjua_F0zsnge6T7mqGbq148oXSvO0YKFel1FPLC1iXcerrM_NCvpiKlmvV1KhCfPwg5SytJWLw9DafrYBy4p64uSfJZrrjDXC36ZkrD-8LfRM3Md3PUkBuADR9WkFTSGVfYFNEN2_j8RiSVtya6n3uto7k7Y5SI7rwu0Sl-NsdgOqKJgKvqgPheYhUIQMky25ZQ0ndUny8ESyW5GOZp_35KmXh8v3bjsG9IkfBXlrpyZkDUQNHP4cHeX27OHVHv6vTu2Y59eaJmrZ2wFbatZKrFZp_u8egUyGiPEqRvpJc1jPY0i7zj08aujhFbe_Kme00dNH2lvQfwdlorTF6LibYLI0vKAYmL6Xe9NeAOsgQghZVwUUudVKI-cjloNqwPiNa6LUKmIBypUUwwhjZBlNZSpyLG0m_s3se5HAGzUCeVX2rQVIIDH-KycSp5uVXH-okuBhhkbSg34y7xU6WIi0INzZazeuq9ebm1acrf7fYREvQ2L3QTrwhYnMXE0N87P_vdqLcaaDTHO-zSETDp6XR6jQySTsHSbGynUau2Wf_HZJOQnx6SSFsK-knDvU-RKoXgIHg7RhYfFghCggIcSTIqZ0sodByVHQH4IoPVa_Hsaw-o2lp0rcECuuN7RUsCvRcbPS53fsAZxP-X9ToYl0pdbl4",
			wantErr: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := testsignature.CreateReject(tt.args.contentHash, tt.args.certificateThumbprint, tt.args.privateKey, tt.args.signedAt)
			if (err != nil) != tt.wantErr {
				t.Errorf("CreateReject() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			assert.Equal(t, tt.want, got)
		})
	}
}
