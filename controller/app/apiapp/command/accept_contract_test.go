// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//nolint:dupl // looks like revoke contract but is different
package command_test

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"golang.org/x/exp/maps"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/adapters/authz"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authentication"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/pkg/authorization"
)

func TestAcceptContract(t *testing.T) {
	t.Parallel()

	type tests map[string]struct {
		setup   func(context.Context, *mocks)
		args    *command.AcceptContractArgs
		wantErr error
	}

	validArgs := &command.AcceptContractArgs{
		AuthData:              &authentication.OIDCData{},
		AuthorizationMetadata: &authorization.RestMetaData{},
		ContentHash:           "$1$1$mTfBbWw2A6UQU5oU7RR2JbEtTB9GnT2syfopGwiCT5Mo9qDgFb1L_o0kFF0-TG13LX2czdAL60KaY7NbB-uMAQ==",
	}

	flowTests := tests{
		"when_manager_errors": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscAcceptContract,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeContract, validArgs.ContentHash),
					},
				).Return(nil)

				m.manager.EXPECT().
					AcceptContract(ctx, validArgs.ContentHash).
					Return(errors.New("arbitrary"))
			},
			wantErr: &command.InternalError{},
		},
		"when_unauthenticated": {
			args: func(a command.AcceptContractArgs) *command.AcceptContractArgs {
				a.AuthData = nil
				return &a
			}(*validArgs),
			wantErr: &command.AuthenticationError{},
		},
		"when_unauthorized": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscAcceptContract,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeContract, validArgs.ContentHash),
					},
				).Return(authz.NewAuthorizationError("arbitrary"))
			},
			wantErr: &command.AuthorizationError{},
		},
		"happy_flow": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscAcceptContract,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeContract, validArgs.ContentHash),
					},
				).Return(nil)

				m.manager.EXPECT().
					AcceptContract(ctx, validArgs.ContentHash).
					Return(nil)
			},
			wantErr: nil,
		},
	}

	validationTests := tests{
		"empty_content_hash": {
			args: func(a command.AcceptContractArgs) *command.AcceptContractArgs {
				a.ContentHash = ""
				return &a
			}(*validArgs),
			setup: func(ctx context.Context, m *mocks) {
				m.authz.EXPECT().Authorize(ctx,
					validArgs.AuthData,
					validArgs.AuthorizationMetadata,
					authz.FscAcceptContract,
					[]*authz.ResourceURN{
						authz.NewResourceURN(authz.ResourceTypeContract, ""),
					},
				).Return(nil)
			},
			wantErr: &command.ValidationError{},
		},
	}

	allTests := tests{}
	maps.Copy(allTests, flowTests)
	maps.Copy(allTests, validationTests)

	for name, tt := range allTests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			mocks := newMocks(t)

			h, err := command.NewAcceptContractHandler(mocks.manager, mocks.auth, mocks.logger)
			require.NoError(t, err)

			ctx := context.Background()

			if tt.setup != nil {
				tt.setup(ctx, mocks)
			}

			err = h.Handle(ctx, tt.args)

			if tt.wantErr == nil {
				assert.NoError(t, err)
			} else {
				assert.ErrorAs(t, err, &tt.wantErr)
			}
		})
	}
}
