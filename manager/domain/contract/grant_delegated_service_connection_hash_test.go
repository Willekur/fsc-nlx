// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package contract_test

import (
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/nlx/fsc-nlx/manager/domain/contract"
)

func TestDelegatedServiceConnectionHash(t *testing.T) {
	contractIV, err := uuid.Parse("26aad1d8-3653-4e6f-b3ba-70df67a9a0ab")
	assert.NoError(t, err)

	contractIVBytes, err := contractIV.MarshalBinary()
	assert.NoError(t, err)

	now := time.Date(2023, 1, 1, 1, 42, 0, 0, time.UTC)

	c, err := contract.NewContent(&contract.NewContentArgs{
		HashAlgorithm: contract.HashAlgSHA3_512,
		IV:            contractIVBytes,
		CreatedAt:     now,
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: now,
			NotAfter:  now,
		},
		Grants: []interface{}{
			&contract.NewGrantDelegatedServiceConnectionArgs{
				Outway: &contract.NewGrantDelegatedServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000001",
						Name:           "",
						ManagerAddress: "",
					},
					CertificateThumbprint: "_0yCZurQcAtocDFSdbWcwhFJNzQHoZw1paeFr7D2G-c",
				},
				Service: &contract.NewGrantDelegatedServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000002",
						Name:           "",
						ManagerAddress: "",
					},
					Name: "parkeerrechten",
				},
				Delegator: &contract.NewGrantDelegatedServiceConnectionDelegatorArgs{
					Peer: &contract.NewPeerArgs{
						ID:             "00000000000000000003",
						Name:           "",
						ManagerAddress: "",
					},
				},
			},
		},
	})
	assert.NoError(t, err)

	assert.Equal(t, "$1$4$y3pytGc7clKE44alpZjBt7jJwjI7Mh2ZEUQ2M1ejiMgiR90FzYIeOvUZVJMCvsToq7XEiY1Q57ONjRHTQ5JOSw", c.Grants().DelegatedServiceConnectionGrants()[0].Hash().String())
}
