-- Copyright © VNG Realisatie 2024
-- Licensed under the EUPL

-- name: ListInways :many
SELECT
    name,
    address
FROM
    controller.inways
WHERE
    group_id = $1
GROUP BY
    name,
    address;
