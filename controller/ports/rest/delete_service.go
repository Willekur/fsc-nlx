// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"

	"github.com/pkg/errors"

	"gitlab.com/commonground/nlx/fsc-nlx/controller/app/apiapp/command"
	"gitlab.com/commonground/nlx/fsc-nlx/controller/ports/rest/api"
)

func (s *Server) DeleteService(ctx context.Context, req api.DeleteServiceRequestObject) (api.DeleteServiceResponseObject, error) {
	s.logger.Info("rest request DeleteService")

	err := s.app.Commands.DeleteService.Handle(ctx, &command.DeleteServiceArgs{
		AuthData: authData,
		Name:     req.Name,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not create service")
	}

	return api.DeleteService204Response{}, nil
}
